# PATH
Exec {
  path => ["/usr/bin", "/bin", "/usr/sbin", "/sbin", "/usr/local/bin", "/usr/local/sbin"]
}

# Common packages
include bootstrap
include common
include bashrc
include ntp
include apache

# Default node
node default {
  class { "mysql::server":
    service_enabled => true,
  }

  # Create database
  mysql::db { 'project':
    user      => 'vagrant',
    password  => 'vagrant',
    sql       => '/vagrant/db/db.sql'
  }
}

# PHP 5.3
#node /^php53\d+$/ {
include php::php53
#}

# PHP 5.4
# @todo add later
#node /^php54\d+$/ {
#  include php::php54
#}

