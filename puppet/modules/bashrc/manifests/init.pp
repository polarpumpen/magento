# == Class: bashrc
#
# Class for custom .bashrc
#

class bashrc {
    file { "/home/vagrant/.bashrc":
        ensure => file,
        source => "puppet:///modules/bashrc/bashrc",
        owner => "vagrant",
        group => "vagrant",
        mode => "644",
        require => Package["bash"],
    }
}