# == Class: php
#
# NetConsult local ubuntu mirror
#

class netconsult::apt {
  file { '/etc/apt/sources.list':
    ensure => file,
    source => "puppet:///modules/netconsult/apt-source.list"
  }
}