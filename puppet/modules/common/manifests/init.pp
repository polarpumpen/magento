# == Class: common
#
# Common class
# 

class common {
	package { [
			"nfs-common",
			"bash",
			"vim",
			"tmux",
			"git",
			"curl",
			"libnfsidmap2",
			"screen"
		]:
		require => Exec["apt-get update"],
		ensure => installed
	}
}
