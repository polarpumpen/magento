# == Class: php
#
# PHP Class
#

class php::php53 {
	package { [ 	
			"php5", 
			"php-pear",
			"php-soap",
			"php5-cli",
			"php5-dev",
			"php5-gd",
			"php5-imagick",
			"php5-mcrypt",
			"php5-mysql",
			"php5-xmlrpc",
			"php5-xsl",
			"libzend-framework-php",
			"libapache2-mod-php5",
			"php5-curl"
		]:
		ensure => installed,
		require => Exec["apt-get update"],
	}

	exec { "a2enmod":
		command => "/usr/sbin/a2enmod php5",
		require => Service["apache2"],
	}

	file { "/etc/php5/apache2/php.ini":
		ensure => file,
		owner => "root",
		group => "root",
		mode => 644,
		source => "puppet:///modules/php/php.ini",
		require => Package["libapache2-mod-php5"],
	}
}
