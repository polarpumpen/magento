# == Class: apache
#
# Apache class
#

class apache {
	package { 'apache2':
		ensure => installed,
		require => Exec["apt-get update"]
	}

	file { "/etc/apache2/mods-enabled/rewrite.load":
		ensure => link,
		target => "/etc/apache2/mods-available/rewrite.load",
		require => Package['apache2']
	}
	file { "/etc/apache2/sites-available/vagrant":
		ensure => file,
		source => "puppet:///modules/apache/vagrant-vhost",
		require => Package["apache2"],
	}
	file { "/etc/apache2/sites-enabled/000-default":
		ensure => link,
		target => "/etc/apache2/sites-available/vagrant",
		require => File["/etc/apache2/sites-available/vagrant"],
		notify => Service["apache2"],
	}

	service { "apache2":
		ensure => running,
		require => Package["apache2"],
		subscribe => [
		    File["/etc/php5/apache2/php.ini"],
		    File["/etc/apache2/mods-enabled/rewrite.load"],
		    File["/etc/apache2/sites-enabled/000-default"]
		],
	}
}
