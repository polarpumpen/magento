# == Class: ntp
#
# NTP Class
#

class ntp {
	case $operatingsystem {
		centos, redhat: { $service_name = 'ntpd' }
		debian, ubuntu: { $service_name = 'ntp' }
	}

	package { 'ntp':
		ensure  => installed
	}

	service { 'ntp':
		name      => $service_name,
		ensure    => running,
		enable    => true
	}
}
