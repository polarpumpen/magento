usage="$(basename "$0") [-h] [-v n] -- installation script for NetConsult Vagrant

where:
    -h  show this help text
    -v  set the PHP version (default: 5.3)"

version="5.3"
while getopts ':hv:' option; do
  case "$option" in
    h) echo "$usage"
       exit
       ;;
    v) version=$OPTARG
       ;;
    :) printf "missing argument for -%s\n" "$OPTARG" >&2
       echo "$usage" >&2
       exit 1
       ;;
   \?) printf "illegal option: -%s\n" "$OPTARG" >&2
       echo "$usage" >&2
       exit 1
       ;;
  esac
done
shift $((OPTIND - 1))

printf "\n"
echo "Composer dependencies"
echo "#######################"
printf "\n"
if [ -z $(which composer) ]; then
    echo "(ERR) Composer is missing. Eg. run 'brew update && brew install composer'."
    echo "(WARN) Aborting..,"
    exit
else
    echo "(OK) Composer found. Install dependencies"
    $(which composer) up
fi
printf "\n"

# PHP 5.3
if [ $version == "5.3" ]; then
    VAGRANTFILE="vendor/netconsult/vagrant-puppet/Vagrantfile.php53.template"
fi

# PHP 5.4
if [ $version == "5.4" ]; then
    VAGRANTFILE="vendor/netconsult/vagrant-puppet/Vagrantfile.php54.template"
fi

PUPPETDIR="vendor/netconsult/vagrant-puppet/puppet"

printf "\n"
echo "Creating symbolic links"
echo "#######################"
printf "\n"

# Create symlink for Vagrantfile
if [ ! -f Vagrantfile ]; then
    echo "(OK) Created symlink for Vagrantfile"
    ln -s $VAGRANTFILE Vagrantfile
else
    echo "(ERR) Symlink for Vagrantfile exists, skipping!"
fi

# Create symlink for puppet
if [ ! -d puppet ]; then
    echo "(OK) Created symlink for Puppet"
    ln -s $PUPPETDIR puppet
else
    echo "(ERR) Symlink for Puppet exists, skipping!"
fi

printf "\n"

# Check for config files
echo "Checking for config file"
echo "#######################"
printf "\n"
if [ ! -f public/app/etc/local.xml ]; then
    echo "(OK) No Magento configuration found."
    echo "(INFO) Copying sample configuration"
    cp dist-files/local.xml.dist public/app/etc/local.xml
else
    echo "(ERR) Config found. Moving on..."
fi

printf "\n"

echo "Finished"
echo "#######################"
printf "\n"
echo "(INFO) Please run 'vagrant up' to start project."
echo "(INFO) To import database on boot place a sql-dump in 'db/db.sql'."
echo "(WARN) Database will be overwritten if provision is done."
printf "\n"