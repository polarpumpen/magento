<?php

$installer = $this;

$baseDir = Mage::getBaseDir();
$varDir = $baseDir.DS.'var';
$importDir = $varDir.DS.'import';
$imagesyncDir = $importDir.DS.'imagesync';

$io = new Varien_Io_File();

if($io->isWriteable($varDir)) {
	$io->checkAndCreateFolder($importDir);
}
if($io->isWriteable($importDir)) {
	$io->checkAndCreateFolder($imagesyncDir);
}