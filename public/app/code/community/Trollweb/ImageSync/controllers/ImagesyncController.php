<?php

class Trollweb_ImageSync_ImagesyncController extends Mage_Adminhtml_Controller_Action {

	const UPL = 'UPLOADED';
	
	protected function _initAction() {

		$this->loadLayout()->_setActiveMenu('catalog/imagesync');
		return $this;
	}

	public function indexAction() {

		$this->_initAction();
		$this->renderLayout();
	}

	public function dropboxAuthAction() {

		$this->_initAction();
		$this->renderLayout();
	}

	public function dropboxAuthPostAction() {

		$dropboxCode = $this->getRequest()->getPost('dropboxcode');

		$dropboxAuth = Mage::getModel('imagesync/dropboxauth');
		$dropboxAuthToken = $dropboxAuth->getAuthToken($dropboxCode);

		$userId = Mage::getSingleton('admin/session')->getUser()->getId();
		$dropboxAuth->load($userId, 'user_id');
		$dropboxAuth->setUserId($userId);
		$dropboxAuth->setToken($dropboxAuthToken);
		$dropboxAuth->save();

		$this->_redirect('*/*/index');
	}

	public function massAttachAction() {

		$method = Mage::getStoreConfig('trollweb_imagesync/imagesync/method', Mage::app()->getStore()->getId());
		$mainImage = Mage::getStoreConfig('trollweb_imagesync/imagesync/mainimage', Mage::app()->getStore()->getId());
		$rmImages = Mage::getStoreConfig('trollweb_imagesync/imagesync/deleteimages', Mage::app()->getStore()->getId());
		$rmFile = Mage::getStoreConfig('trollweb_imagesync/imagesync/deletefiles', Mage::app()->getStore()->getId());
		$rnFile = Mage::getStoreConfig('trollweb_imagesync/imagesync/renamefiles', Mage::app()->getStore()->getId());

		if($method == 'dropbox') {
			$accessToken = Mage::getModel('imagesync/dropboxauth')->getAccessToken();
			
			if($accessToken) {
				$dropbox = Mage::getModel('imagesync/dropbox', array('access_token' => $accessToken));
			} else {
				Mage::app()->getResponse()->setRedirect(Mage::helper('adminhtml')->getUrl('*/*/dropboxAuth'));
				Mage::app()->getResponse()->sendResponse();
			}
		}

		$ids = $this->getRequest()->getParam('imagesyncid');
		$collection = Mage::getModel('imagesync/imagesync')->getCollection();

		$invalidCount = 0;
		$successCount = 0;
		$cleanedProducts = array();

		$logStartMessage = Mage::helper('imagesync')->__('**********START OF IMPORT**********');
		$this->_doLog($logStartMessage);
		
		foreach ($ids as $id) {

			$item = $collection->getItemByColumnValue('id',$id);


			if(!$item) {
				if($method == 'path') {
					$baseDir = Mage::getBaseDir();
					$errorFilename = str_replace($baseDir, '', $id);
				}
				if($method == 'dropbox') {
					$errorFilename = $id;
				}
				$errorNoFile = Mage::helper('imagesync')->__('Rejected: The file %s does not exist!', $errorFilename);
				$this->_doLog($errorNoFile);
				Mage::getSingleton('adminhtml/session')->addError($errorNoFile);
				$invalidCount++;
				continue;
			}

			if($item->getStatus() == 0) {

				$logRejectedMessage = Mage::helper('imagesync')->__('Rejected %s: %s', $item->getFilename(), $item->getStatusmsg());
				$this->_doLog($logRejectedMessage);

				$invalidCount++;
				continue;
			}

			$sku = $item->getSku();
			
			$product = Mage::getModel('catalog/product')->setStoreId(Mage::app()->getStore()->getId());
			$productId = $product->getIdBySku($sku);
			$product->load($productId);
			if (!$product->getId()) {
				$errorNoSku = Mage::helper('imagesync')->__('Rejected: No product exists with sku %s', $sku);
				$this->_doLog($errorNoSku);
				Mage::getSingleton('adminhtml/session')->addError($errorNoSku);
				$invalidCount++;
				continue;
			}

			if($rmImages && !in_array($productId, $cleanedProducts)) {

				$cleanedCount = 0;

				$attributes = $product->getTypeInstance(true)->getSetAttributes($product);
				if(isset($attributes['media_gallery'])) {
					$gallery=$attributes['media_gallery'];
					$galleryData = $product->getMediaGallery();
					if(isset($galleryData['images']) && is_array($galleryData['images'])) {
						foreach($galleryData['images'] as $galleryImage) {
							if($gallery->getBackend()->getImage($product, $galleryImage['file'])) {
								$gallery->getBackend()->removeImage($product, $galleryImage['file']);
								$cleanedCount++;
							}
						}
					}
				}				
				$logCleanedMessage = Mage::helper('imagesync')->__('Removed %d old image(s) from product with sku %s', $cleanedCount, $sku);
				$this->_doLog($logCleanedMessage);
				$cleanedProducts[] = $productId;
			}
			
			$path = $item->getPath();
			$filename = $item->getFilename();

			if($method == 'path') {
				$fullPath = $path . DS . $filename;
				$uploadedFullPath = $path . DS . self::UPL . '-' . $filename;

				$mvFile = $rmFile;
			}

			if($method == 'dropbox') {
				$dbPath = $path . '/' . $filename;
				$tmpDir = Mage::getBaseDir('tmp');
				$fullPath = $tmpDir . DS . $filename;
				$uploadedDbPath = $path . '/' . self::UPL . '-' . $filename;

				$tmpFile = fopen($fullPath, 'w');
				$dropbox->getFile($dbPath, $tmpFile);
				fclose($tmpFile);

				$mvFile = true;
			}

			$mediaAttribute = ($mainImage && $item->getDupid() == 0 ? array('thumbnail', 'small_image', 'image') : '');
			$product->addImageToMediaGallery($fullPath, $mediaAttribute, $mvFile, false);
			$product->save();

			$logAttachedMessage = Mage::helper('imagesync')->__('Attached image %s to product with sku %s', $filename, $sku);
			$this->_doLog($logAttachedMessage);
				
			$successCount++;

			if($rmFile) {

				if($method == 'dropbox') {
					$dropbox->delete($dbPath);
				}

			} elseif($rnFile) {

				if($method == 'path' ){
					$io = new Varien_Io_File();
					$io->open(array('path' => $path));
					$io->mv($fullPath, $uploadedFullPath);
					$io->close();
				}

				if($method == 'dropbox' ){
					$dropbox->move($dbPath, $uploadedDbPath);
				}
			}
		}

		if($invalidCount) {
			$errorMessage = Mage::helper('imagesync')->__('%d invalid item(s) were rejected', $invalidCount);
			$this->_doLog($errorMessage);
			Mage::getSingleton('adminhtml/session')->addError($errorMessage);
		}

		if($successCount) {
			$successMessage = Mage::helper('imagesync')->__('%d image(s) were successfully attached to products', $successCount);
			$this->_doLog($successMessage);
			Mage::getSingleton('adminhtml/session')->addSuccess($successMessage);
		}

		$logEndMessage = Mage::helper('imagesync')->__('**********END OF IMPORT**********');
		$this->_doLog($logEndMessage);
		$this->_redirect('*/*/index');
	}

	public function massDeleteAction() {

		$method = Mage::getStoreConfig('trollweb_imagesync/imagesync/method', Mage::app()->getStore()->getId());

		if($method == 'dropbox') {
			$accessToken = Mage::getModel('imagesync/dropboxauth')->getAccessToken();

			if($accessToken) {
				$dropbox = Mage::getModel('imagesync/dropbox', array('access_token' => $accessToken));
			} else {
				Mage::app()->getResponse()->setRedirect(Mage::helper('adminhtml')->getUrl('*/*/dropboxAuth'));
				Mage::app()->getResponse()->sendResponse();
			}
		}

		$ids = $this->getRequest()->getParam('imagesyncid');
		$collection = Mage::getModel('imagesync/imagesync')->getCollection();

		$deleteCount = 0;

		foreach ($ids as $id) {

			$item = $collection->getItemByColumnValue('id',$id);

			if(!$item) {
				if($method == 'path') {
					$baseDir = Mage::getBaseDir();
					$errorFilename = str_replace($baseDir, '', $id);
				}
				if($method == 'dropbox') {
					$errorFilename = $id;
				}
				$errorNoFile = Mage::helper('imagesync')->__('The file %s does not exist!', $errorFilename);
				Mage::getSingleton('adminhtml/session')->addError($errorNoFile);
				continue;
			}

			$path = $item->getPath();
			$filename = $item->getFilename();

			if($method == 'path') {
				$fullPath = $path . DS . $filename;
				$io = new Varien_Io_File();
				$io->open(array('path' => $path));
				$io->rm($fullPath);
				$io->close();
			}

			if($method == 'dropbox') {
				$dbPath = $path . '/' . $filename;
				$dropbox->delete($dbPath);
			}

			$deleteCount++;
		}

		if($deleteCount) {
			$message = mage::helper('imagesync')->__('%d file(s) were successfully deleted', $deleteCount);
			Mage::getSingleton('adminhtml/session')->addSuccess($message);
		}

		$this->_redirect('*/*/index');
	}

	private function _doLog($logline) {
		if(Mage::getStoreConfig('trollweb_imagesync/imagesync/log', Mage::app()->getStore()->getId())) {
			$logDir = Mage::getBaseDir('log');
			$fh = fopen($logDir. DS . "trollweb_imagesync.log","a");
			if ($fh) {
				fwrite($fh,"[".date("d.m.Y H:i:s")."] ".$logline."\n");
				fclose($fh);
			}
		}
	}
}