<?php

class Trollweb_ImageSync_Block_Adminhtml_Imagesync_Grid extends Mage_Adminhtml_Block_Widget_Grid {

	public function __construct() {
		parent::__construct();
		$this->setId('imagesync_grid');
		$this->setDefaultSort('sku');
		$this->setDefaultDir('desc');
		$this->setFilterVisibility(false);
		$this->setPagerVisibility(false);
		$this->setSaveParametersInSession(false);
	}

	protected function _prepareCollection() {
		$collection = Mage::getModel('imagesync/imagesync')->getCollection();
		$this->setCollection($collection);
		return parent::_prepareCollection();
	}

	protected function _prepareColumns() {
		$this->addColumn('sku', array (
			'header' => Mage::helper('imagesync')->__('SKU'),
			'align' => 'left',
			'width' => '150px',
			'index' => 'sku',
			'sortable' => false
		));

		$this->addColumn('path', array (
			'header' => Mage::helper('imagesync')->__('Path'),
			'align' => 'left',
			'width' => '200px',
			'index' => 'relative_path',
			'sortable' => false
		));

		$this->addColumn('filename', array (
			'header' => Mage::helper('imagesync')->__('File name'),
			'align' => 'left',
			'width' => '200px',
			'index' => 'filename',
			'sortable' => false
		));

		$this->addColumn('filesize', array (
			'header' => Mage::helper('imagesync')->__('File size'),
			'align' => 'left',
			'width' => '50px',
			'index' => 'filesize',
			'sortable' => false
		));

		$this->addColumn('status', array (
			'header' => Mage::helper('imagesync')->__('Status'),
			'align' => 'left',
			'width' => '100px',
			'index' => 'statusmsg',
			'sortable' => false
		));
		return parent::_prepareColumns();
	}

	public function getRowUrl($row) {
		return NULL;
	}

	protected function _prepareMassaction() {
		$this->setMassactionIdField('id');
		$this->getMassactionBlock()->setFormFieldName('imagesyncid');

		$this->getMassactionBlock()->addItem('attach', array(
			'label' => Mage::helper('imagesync')->__('Attach images'),
			'url' => $this->getUrl('*/*/massAttach')
		));

		$this->getMassactionBlock()->addItem('delete', array(
			'label' => Mage::helper('imagesync')->__('Delete files'),
			'url' => $this->getUrl('*/*/massDelete'),
			'confirm' => Mage::helper('imagesync')->__('Are you sure?')
		));

		return $this;
	}
}