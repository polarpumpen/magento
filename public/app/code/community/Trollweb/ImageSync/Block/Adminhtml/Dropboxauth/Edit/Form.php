<?php

class Trollweb_ImageSync_Block_Adminhtml_Dropboxauth_Edit_Form extends Mage_Adminhtml_Block_Widget_Form {

	protected function _prepareForm() {

		$form = new Varien_Data_Form(
			array(
				'id' => 'edit_form',
				'action' => $this->getUrl('*/*/dropboxAuthPost'),
				'method' => 'post'
			));
	
		$form->setUseContainer(true);
		$this->setForm($form);
	
		$fieldset = $form->addFieldset('dropboxauth', array(
			'legend' => Mage::helper('imagesync')->__('Dropbox Auth Code'),
			'class' => 'fieldset-wide'
		));

		$dropboxAuth = Mage::getModel('imagesync/dropboxauth');
		$dropboxAuthUrl = $dropboxAuth->getAuthUrl();
		
		$instructions = '<p>';
		$instructions .= Mage::helper('imagesync')->__('Instructions:') . '<br />';
		$instructions .= Mage::helper('imagesync')->__('1. Go here:');
		$instructions .= ' <a href="' . $dropboxAuthUrl .'" target="_blank">' . $dropboxAuthUrl .'</a> ';
		$instructions .= Mage::helper('imagesync')->__('and authorize ImageSync to access your drobox.') . '<br />';
		$instructions .= Mage::helper('imagesync')->__('2. Copy the code from the Dropbox page') . '<br />';
		$instructions .= Mage::helper('imagesync')->__('3. Enter the code and press "Save".') . '</p>';

		$fieldset->addField('dropboxcode', 'text', array(
			'name' => 'dropboxcode',
			'label' => Mage::helper('imagesync')->__('Code'),
			'after_element_html' => $instructions
		));

		return parent::_prepareForm();
	}
}