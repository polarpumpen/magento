<?php
class Trollweb_ImageSync_Block_Adminhtml_Imagesync extends Mage_Adminhtml_Block_Widget_Grid_Container {

	public function __construct() {
		parent::__construct();
		$this->_controller = 'adminhtml_imagesync';
		$this->_blockGroup = 'imagesync';
		$this->_removeButton('add');

		$method = Mage::getStoreConfig('trollweb_imagesync/imagesync/method', Mage::app()->getStore()->getId());
		if($method == 'path') {
			$this->_headerText = Mage::helper('imagesync')->__('Local images');
		}
		if($method == 'dropbox') {
			$this->_headerText = Mage::helper('imagesync')->__('Dropbox images');
		}
	}
}