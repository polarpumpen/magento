<?php

class Trollweb_ImageSync_Block_Adminhtml_Dropboxauth_Edit extends Mage_Adminhtml_Block_Widget_Form_Container {

	public function __construct() {

		parent::__construct();

		$this->_blockGroup = 'imagesync';
		$this->_controller = 'adminhtml_dropboxauth';
		$this->_removeButton('back');
		$this->_headerText = Mage::helper('imagesync')->__('Dropbox Authorization');
	}
}