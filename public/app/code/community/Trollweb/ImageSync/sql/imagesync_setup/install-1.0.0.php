<?php

$installer = $this;
$installer->startSetup();

$tableExists = $installer->getConnection()->showTableStatus($installer->getTable('imagesync/dropboxauth'));

if(!$tableExists) {

	$table = $installer->getConnection()
		->newTable($installer->getTable('imagesync/dropboxauth'))
			->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
					'unsigned'	=> true,
					'nullable'  => false,
					'primary'   => true,
					'identity'	=> true
				), 'Id')
			->addColumn('user_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
					'nullable'  => false,
				), 'UserId')
			->addColumn('token', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(
					'nullable'  => false
				), 'Token');

	$installer->getConnection()->createTable($table);
}

$installer->endSetup();