<?php

class Trollweb_ImageSync_Model_Imagesync extends Mage_Core_Model_Abstract {

	private $collection;
	private $io;
	private $baseDir;
	private $importDir;
	private $product;
	private $fileList;
	private $dirList = array();
	private $dirCount = 0;
	private $method;
	private $dropbox;
	const UPL = 'UPLOADED';

	public function __construct() {

		$this->collection = new Varien_Data_Collection();
		$this->product = Mage::getModel('catalog/product')->setStoreId(Mage::app()->getStore()->getId());
		$this->method = Mage::getStoreConfig('trollweb_imagesync/imagesync/method', Mage::app()->getStore()->getId());

		if($this->method == 'path') {
			$this->io = new Varien_Io_File();
			$this->baseDir = Mage::getBaseDir();
			$this->importDir = $this->baseDir . DS . Mage::getStoreConfig('trollweb_imagesync/imagesync/importdir', Mage::app()->getStore()->getId());
		}

		if($this->method == 'dropbox') {
			$accessToken = Mage::getModel('imagesync/dropboxauth')->getAccessToken();

			if($accessToken) {
				$this->dropbox = Mage::getModel('imagesync/dropbox', array('access_token' => $accessToken));
			} else {
				Mage::app()->getResponse()->setRedirect(Mage::helper('adminhtml')->getUrl('*/*/dropboxAuth'));
				Mage::app()->getResponse()->sendResponse();
			}

			$this->importDir = '/' . Mage::getStoreConfig('trollweb_imagesync/imagesync/dropboxdir', Mage::app()->getStore()->getId());
		}
	}

	public function getCollection() {

		$itemlist = $this->_getList();
		if($itemlist) {
			ksort($itemlist);
			foreach($itemlist as $sku) {
				ksort($sku);
				foreach($sku as $item) {
					$this->collection->addItem($item);
				}
			}
		}
		return $this->collection;
	}

	private function _getList($dir = '') {

		if($this->method == 'path' && !$this->io->fileExists($this->importDir, false)) {
			return NULL;
		}

		if($dir == '') {
			$dir = $this->importDir;
		}

		$dirContent = $this->_getDirContent($dir);

		foreach($dirContent as $file) {

			$isDir = $this->_dirCheck($file);
			if($isDir) {
				$this->dirList[] = $isDir;
				continue;
			}

			$filename = $this->_getFilename($file);

			if(substr($filename, 0, strlen(self::UPL)) != self::UPL) {

				$item = new Varien_Object();
				
				$fullPath = $this->_getFullPath($dir, $file);
				$item->setId($fullPath);

				$item->setPath($dir);
				$item->setRelativePath(substr($dir, strlen($this->importDir)));
				$item->setFilename($filename);

				$filesize = $this->_getFilesize($file);
				$item->setFilesize((int)($filesize/1024).' kB');

				$name = substr($filename, 0, strrpos($filename, '.'));
				if(strpos($name, '_') && !$this->product->getIdBySku($name)) {
					$nameArray = explode ('_', $name);
					$nameEnd = array_pop($nameArray);
					$nameStart = implode('_', $nameArray);
					if(is_numeric($nameEnd)) {
						$sku = $nameStart;
						$dupid = $nameEnd;
					} else {
						$sku = $name;
						$dupid = 0;
					}
				} else {
					$sku = $name;
					$dupid = 0;
				}

				if($this->product->getIdBySku($sku)) {
					$item->setSku($sku);
					$item->setDupid($dupid);
				}

				if ($this->product->getIdBySku($sku) && $this->_imageCheck($file)) {
					$item->setStatus(1);
					$item->setStatusmsg('OK');
				} elseif($this->product->getIdBySku($sku)) {
					$item->setStatus(0);
					$item->setStatusmsg('Invalid Filetype');
				} else {
					$item->setStatus(0);
					$item->setStatusmsg('SKU not found');
				}
				$this->fileList[$sku][$dupid] = $item;
			}
		}
		if(count($this->dirList) > 0) {
			$nextDir = $this->dirList[$this->dirCount];
			unset($this->dirList[$this->dirCount]);
			$this->dirCount++;
			$this->_getList($nextDir);
		}
		return $this->fileList;
	}

	private function _getDirContent($dir) {
		if($this->method == 'path') {
			$this->io->cd($dir);
			$contents = $this->io->ls();
			return $contents;
		}
		if($this->method == 'dropbox') {
			$fileList = $this->dropbox->getMetadataWithChildren($dir);
			$contents = $fileList['contents'];
			return $contents;
		}
	}

	private function _dirCheck($file) {
		if($this->method == 'path') {
			if(array_key_exists('id', $file)) {
				return $file['id'];
			} else {
				return false;
			}
		}
		if($this->method == 'dropbox') {
			if($file['is_dir'] == 1) {
				return $file['path'];
			} else {
				return false;
			}
		}
	}

	private function _getFilename($file) {
		if($this->method == 'path') {
			return $file['text'];
		}
		if($this->method == 'dropbox') {
			return substr($file['path'], strrpos($file['path'], '/')+1);
		}
	}

	private function _getFullPath($dir, $file) {
		if($this->method == 'path') {
			return $dir . DS . $file['text'];
		}
		if($this->method == 'dropbox') {
			return $file['path'];
		}
	}

	private function _getFilesize($file) {
		if($this->method == 'path') {
			return $file['size'];
		}
		if($this->method == 'dropbox') {
			return $file['bytes'];
		}
	}

	private function _imageCheck($file) {
		if($this->method == 'path') {
			if($file['is_image'] == '1') {
				return true;
			} else {
				return false;
			}
		}
		if($this->method == 'dropbox') {
					if($file['mime_type'] == 'image/jpeg' || $file['mime_type'] == 'image/gif' || $file['mime_type'] == 'image/png') {
				return true;
			} else {
				return false;
			}
		}
	}
}