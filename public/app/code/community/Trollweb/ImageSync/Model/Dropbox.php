<?php

function dropbox_autoload($name) {

	if (\substr_compare($name, 'Dropbox\\', 0, 8) !== 0) return;

	$pathified_stem = \str_replace(array('\\', '_'), DS, $name);
	$path = $pathified_stem . '.php';
	require_once $path;
}

\spl_autoload_register('dropbox_autoload',true,true);

class Trollweb_ImageSync_Model_Dropbox extends \Dropbox\Client {

	public function __construct($args) {

		$clientIdentifier = 'Trollweb ImageSync/1.0.1';
		$accessToken = $args['access_token'];
		parent::__construct($accessToken, $clientIdentifier);
	}
}