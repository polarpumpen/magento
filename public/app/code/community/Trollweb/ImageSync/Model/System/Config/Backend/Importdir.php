<?php

class Trollweb_ImageSync_Model_System_Config_Backend_Importdir extends Mage_Core_Model_Config_Data {

	public function save() {

		$baseDir = mage::getBaseDir();
		$importDir = $this->getValue();
		$dir = $baseDir . DS . $importDir;
		$io = new Varien_Io_File();

		if(!$io->fileExists($dir, false)) {
			Mage::getSingleton('adminhtml/session')->addError(mage::helper('imagesync')->__('Directory "%s" does not exist. Please create it or choose another directory.', $importDir));
		}

		return parent::save();
	}
}