<?php

class Trollweb_ImageSync_Model_System_Config_Source_Method {

	public function toOptionArray() {
		return array(
			array('value' => 'path', 'label'=>Mage::helper('imagesync')->__('Local directory')),
			array('value' => 'dropbox', 'label'=>Mage::helper('imagesync')->__('Dropbox'))
		);
	}
}