<?php

class Trollweb_ImageSync_Model_Resource_Dropboxauth extends Mage_Core_Model_Resource_Db_Abstract {

	protected function _construct() {

		$this->_init('imagesync/dropboxauth', 'id');
	}
}