<?php

function dropboxauth_autoload($name) {

	if (\substr_compare($name, 'Dropbox\\', 0, 8) !== 0) return;

	$pathified_stem = \str_replace(array('\\', '_'), DS, $name);
	$path = $pathified_stem . '.php';
	require_once $path;
}

\spl_autoload_register('dropboxauth_autoload',true,true);

class Trollweb_ImageSync_Model_Dropboxauth extends Mage_Core_Model_Abstract {

	protected function _construct() {

		$this->_init('imagesync/dropboxauth');
	}

	public function getAuthUrl() {

		$webAuth = $this->_getWebAuth();
		$authorizeUrl = $webAuth->start();
		return $authorizeUrl;
	}

	public function getAuthToken($dropboxCode) {

		$webAuth = $this->_getWebAuth();
		list($accessToken, $dropboxUserId) = $webAuth->finish($dropboxCode);
		return $accessToken;
	}

	public function getAccessToken() {

		$userId = Mage::getSingleton('admin/session')->getUser()->getId();
		$this->load($userId, 'user_id');
		$accessToken = $this->getToken();

		return $accessToken;
	}

	private function _getWebAuth() {

		$apiKeys = '{"key": "6zq8cekujn4b0iz", "secret": "ds1ijc9fi4xtov0"}';
		$apiKeysJson = json_decode($apiKeys, true);
		$appInfo = \Dropbox\AppInfo::loadFromJson($apiKeysJson);
		$webAuth = new \Dropbox\WebAuthNoRedirect($appInfo, 'Trollweb ImageSync');
		return $webAuth;
	}
}