<?php

class Trollweb_BBSNetAxept_Model_Resource_Panhash_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract {
    public function _construct() {
        $this->_init('bbsnetaxept/panhash');
    }
}
