<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2016 Amasty (https://www.amasty.com)
 * @package Amasty_Groupcat
 */

class Amasty_Groupcat_Model_ObserverProduct
{

    const FORBIDDEN_ACTION_404      = '1';
    const FORBIDDEN_ACTION_REDIRECT = '2';


    /*
     *  hide products on category list
     */
    public function hideProducts(Varien_Event_Observer $observer)
    {
        if (!Mage::getStoreConfig('amgroupcat/general/disable') || Mage::registry('amgroupcat_fetching_category')) {
            return false;
        }

        Mage::register('amgroupcat_fetching_category', true, true);

        $productIds = array();
        $activeRules = Mage::helper('amgroupcat')->getActiveRules(array('remove_product_links = 1'));

        if ($activeRules) {


            foreach ($activeRules as $rule) {

                // get directly restricted products
                $currentRuleProductIds = Mage::getModel('amgroupcat/product')->getCollection()
                                             ->addFieldToSelect('product_id')
                                             ->addFieldToFilter('rule_id', $rule['rule_id'])
                                             ->getData();

                foreach ($currentRuleProductIds as $productId) {
                    $productIds[] = $productId['product_id'];
                }

                // get all restricted products from restricted categories
                $catIds = array_merge(explode(',', trim($rule['categories'], ',')));
                if (!empty($catIds)) {
                    foreach ($catIds as $catId) {
                        if ($catId > 0) {
                            $products = Mage::getModel('catalog/category')->load($catId)
                                            ->getProductCollection()
                                ->addAttributeToSelect('entity_id')// add all attributes - optional
                                ->addAttributeToFilter('status', 1)
                                ->getData();
                            foreach ($products as $product) {
                                $productIds[] = $product['entity_id'];
                            }
                        }
                    }
                }
            }


            // add products to collection filter
            if (!empty($productIds)) {
                $productIds = array_unique($productIds);

                $observer->getCollection()
                         ->addFieldToFilter('entity_id', array(
                                 'nin' => $productIds,
                             )
                         );
            }

            // delete trigger
            Mage::unregister('amgroupcat_fetching_category');
        }

        return $this;
    }


     /*
     * direct product access by link
     */
    public function checkProductRestrictions(Varien_Event_Observer $observer, $productId = false, $action = true)
    {
        if (!Mage::getStoreConfig('amgroupcat/general/disable')) {
            return false;
        }

        if (!$productId) {
            $action    = $observer->getEvent()->getData('controller_action')->getRequest()->getParams();
            $productId = $action['id'];
        }

        $result = false;

        /*
         * check product restrictions on forbidden direct links
         */
        $activeRules = Mage::helper('amgroupcat')->getActiveRulesForProduct($productId);
        if ($activeRules){
            $result = Mage::getModel('amgroupcat/observerCategory')->checkForbidRestrictions($activeRules, $action);
        }

        /*
         * check category restrictions
         */
        $catLoad = Mage::getModel('catalog/product');
        $catLoad->setId($productId);
        $categoryIds = $catLoad->getResource()->getCategoryIds($catLoad);
        if (is_array($categoryIds)) {
            foreach ($categoryIds as $categoryId) {
                $categoryId = is_array($categoryId) ? $categoryId[0] : $categoryId;
                if ($categoryId > 0) {
                    $result = $result || Mage::getModel('amgroupcat/observerCategory')->checkCategoryTreeRestrictions($categoryId, $action);
                }
            }
        }

        return $result;
    }

}