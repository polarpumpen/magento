<?php


class Itoris_ProductPriceFormula_Block_Product_View extends Mage_Catalog_Block_Product_View {

    public function getProduct() {
        return $this->getData('product');
    }

    public function hasOptions() {
        /** getJsonConfig should return config for all products */
        return true;
    }

    public function _prepareLayout() {
        return $this;
    }
}
?>