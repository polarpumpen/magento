<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_PRODUCTPRICEFORMULA
 * @copyright  Copyright (c) 2013 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

 
class Itoris_ProductPriceFormula_Block_Formula extends Mage_Core_Block_Template {

    /**
     * @return Mage_Catalog_Model_Product
     */
    public function getCurrentProduct() {
        return Mage::registry('current_product');
    }

    public function getConditions($productId) {
        $conditions = $this->prepareConditions($productId);
        return $conditions;
    }

    protected function prepareConditions($productId) {
        $conditionCollection = Mage::getModel('itoris_productpriceformula/condition')->getCollection();
        $conditionCollection->getSelect()->join(array('settings' => Mage::getSingleton('core/resource')->getTableName('itoris_productpriceformula_settings_formula')),
            'settings.formula_id = main_table.formula_id and settings.status=1 and settings.product_id = ' . $productId,
            array('active_from' => 'settings.active_from', 'active_to' => 'settings.active_to', 'run_always' => 'settings.run_always', 
			'apply_to_total' => 'settings.apply_to_total', 'frontend_total' => 'settings.frontend_total', 'disallow_criteria' => 'settings.disallow_criteria')
        );
		$conditionCollection->getSelect()->order(array('settings.position', 'main_table.position'));
        $formulas = array();
        $resource = Mage::getSingleton('core/resource');
        $connection = $resource->getConnection('read');
        $tableGroup = $resource->getTableName('itoris_productpriceformula_group');
        foreach ($conditionCollection as $model) {
            $formulaId = $model->getFormulaId();
            $conditionId = $model->getId();
            $groupIds = $connection->fetchAll("select group_id from {$tableGroup} where formula_id={$formulaId}");
            if (!array_key_exists($formulaId, $formulas)) {
                $formulas[$formulaId] = array(
                    'formula_id' => $formulaId,
                    'active_from' => $model->getActiveFrom(),
                    'active_to'   => $model->getActiveTo(),
                    'run_always'  => $model->getRunAlways(),
                    'apply_to_total'  => $model->getApplyToTotal(),
                    'frontend_total'  => $model->getFrontendTotal(),
					'disallow_criteria'  => (array) json_decode($model->getDisallowCriteria()),
                    'groups'      => $groupIds,
                    'conditions'  => array()
                );
            }
            if (!array_key_exists($conditionId, $formulas[$formulaId]['conditions'])) {
                $formulas[$formulaId]['conditions'][$conditionId] = array(
                    'condition_id' => $conditionId,
                    'condition'    => $model->getCondition(),
                    'price'        => $model->getPrice()
                );
            }
        }
        foreach ($formulas as $key => $condition) {
            $formulas[$key]['conditions'] = array_values($condition['conditions']);
        }
        $formulas = array_values($formulas);
        return $formulas;
    }

    public function getDataBySku($product) {
        $dataBySku = array();
        $allConditionPriceArray = array();
        foreach ($this->getConditions($product->getId()) as $conditionData) {
            foreach ($conditionData['conditions'] as $value) {
                preg_match_all('/{[A-Za-z0-9_^}]*}|[A-Za-z0-9_]*\(.*\)/', $value['condition'], $conditionMatch);
                $allConditionPriceArray[] = $conditionMatch[0];
                preg_match_all('/{[A-Za-z0-9_^}]*}|[A-Za-z0-9_]*\(.*\)/', $value['price'], $priceMatch);
                $allConditionPriceArray[] = $priceMatch[0];
            }
        }
        $dataBySku = $this->prepareDataBySku($allConditionPriceArray, $product);
        return $dataBySku;
    }

    protected function prepareDataBySku($allConditionPriceArray, $product) {
        $dataBySku = array();
        foreach ($allConditionPriceArray as $value) {
            foreach ($value as $condition) {
                if (!array_key_exists($condition, $dataBySku)) {
                    $options = $product->getProductOptionsCollection();
                    if (count($options)) {
                        foreach ($options as $option) {
                            $optionSku = '{' . $option->getSku() . '}';
                            if ($optionSku == $condition) {
                                $dataBySku[$condition] = array('type' => $option->getType(), 'id' => $option->getId());
                            } else {
                                $values  = $option->getValues();
                                if (count($values)) {
                                    foreach ($values as $value) {
                                        $optionSku = '{' . $value->getSku() . '}';
                                        if ($optionSku == $condition) {
                                            $dataBySku[$condition] = array('type' => $option->getType(), 'id' => $option->getId());
                                        }
                                    }
                                }
                            }
                        }
                    }
                    $attributes = $product->getAttributes();
                    foreach ($attributes as $attribute) {
                        $attributeCode = $attribute->getAttributeCode();
                        $attributeCodeStr = '{' . $attributeCode . '}';
                        if ($attributeCodeStr == $condition && $attributeCodeStr != '{price}') {
                            $dataBySku[$condition] = array('value' => $product->getData($attributeCode));
                        }
                    }
                }
            }
        }
        return $dataBySku;
    }

    public function getOptions($product) {
        return $this->prepareOptions($product);
    }

    protected function prepareOptions($product) {
        $options = $product->getProductOptionsCollection();
        $productId = $product->getId();
        $optionsData = array();
        foreach ($options as $option) {
            $optionId = $option->getId();
            $values  = $option->getValues();
            if (!array_key_exists($optionId, $optionsData)) {
                $optionsData[$optionId] = array(
                    'sku'    => $option->getSku(),
                    'type'   => $option->getType(),
                    'id'     => $optionId,
                    'values' => array()
                );
            }
            foreach ($values as $value) {
                $valueId = $value->getId();
                if (!array_key_exists($valueId, $optionsData[$optionId]['values'])) {
                    $optionsData[$optionId]['values'][$valueId] = array(
                        'sku' => $value->getSku(),
                        'id'  => $valueId
                    );
                }
            }
        }
        $optionsData = array_values($optionsData);
        return $optionsData;
    }

    public function specialPrice($product) {
        return $product->getSpecialPrice();
    }

    /**
     * @return Itoris_ProductPriceFormula_Helper_Data
     */
    public function getDataHelper() {
        return Mage::helper('itoris_productpriceformula');
    }

    public function getJsonConfig($product) {
        return $this->getLayout()->createBlock('itoris_productpriceformula/product_view')->setProduct($product)->getJsonConfig();
    }
}
?>