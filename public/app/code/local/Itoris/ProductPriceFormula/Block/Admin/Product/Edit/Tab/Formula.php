<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_PRODUCTPRICEFORMULA
 * @copyright  Copyright (c) 2013 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

  

class Itoris_ProductPriceFormula_Block_Admin_Product_Edit_Tab_Formula
        extends Mage_Adminhtml_Block_Catalog_Form
        implements Mage_Adminhtml_Block_Widget_Tab_Interface
{

    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('itoris/productpriceformula/product/formula.phtml');
        $this->_setAfter();
    }

    /**
     * Set after price tab
     *
     * @return Itoris_ProductPriceFormula_Block_Admin_Product_Edit_Tab_Formula
     */
    protected function _setAfter() {
        /** @var $product Mage_Catalog_Model_Product */
        $product = Mage::registry('current_product');
        $attributes = $product->getAttributes();
        $altAttribute = null;
        /** @var $attribute Mage_Catalog_Model_Resource_Eav_Attribute */
        foreach ($attributes as $attribute) {
            if ($attribute->getAttributeCode() == 'price') {
                return $this->_setAfterAttribute($attribute);
            } else if ($attribute->getAttributeCode() == 'enable_googlecheckout') {
                $altAttribute = $attribute;
            }
        }
        if ($altAttribute) {
            return $this->_setAfterAttribute($altAttribute);
        }
        return $this;
    }

    public function _setAfterAttribute($attribute) {
        $info = $attribute->getAttributeSetInfo();
        if (is_array($info)) {
            foreach ($info as $set) {
                if (isset($set['group_id'])) {
                    $this->setAfter('group_' . $set['group_id']);
                    break;
                }
            }
        }
        return $this;
    }


    public function getTabLabel() {
        return $this ->__('Price Formula');
    }

    public function getTabTitle() {
        return $this ->__('Price Formula');
    }

    public function canShowTab() {
        if($this->getDataHelper()->getSettings()->getEnabled() && $this->getDataHelper()->isAdminRegistered() && $this->getCurrentProduct()->getTypeId() != 'grouped') {
            return true;
        } else {
            return false;
        }
    }

    public function isHidden() {
        return false;
    }

    /**
     * @return Itoris_ProductPriceFormula_Helper_Data
     */
    public function getDataHelper() {
        return Mage::helper('itoris_productpriceformula');
    }

    public function getCurrentProduct() {
        return Mage::registry('current_product');
    }

    public function getFormulaSettingsForLoad() {
        $formulaCollection = Mage::getModel('itoris_productpriceformula/formula')->getCollection();
        $formulaCollection->addFieldToFilter('main_table.product_id', $this->getCurrentProduct()->getId());
        $formulaCollection->getSelect()->order('main_table.position DESC');
        $formulas = array();
        foreach ($formulaCollection as $model) {
            $formulas[] = array(
                'formula_id'  => $model->getFormulaId(),
                'name'        => $model->getName(),
                'position'    => $model->getPosition(),
                'status'      => $model->getStatus(),
                'active_from' => $model->getActiveFrom(),
                'active_to'   => $model->getActiveTo(),
                'group_id'    => $model->getGroupId(),
                'run_always'  => $model->getRunAlways(),
				'apply_to_total'  => $model->getApplyToTotal(),
				'frontend_total'  => $model->getFrontendTotal(),
				'disallow_criteria'  => (array)json_decode($model->getDisallowCriteria())
            );
        }

        return $formulas;
    }

    public function lastFormulaIdFromDb() {
        $resource = Mage::getSingleton('core/resource');
        $connection = $resource->getConnection('read');
        $table = $resource->getTableName('itoris_productpriceformula_settings_formula');
        return $connection->fetchOne("select max(formula_id) from {$table}");
    }

    public function lastConditionIdFromDb() {
        $resource = Mage::getSingleton('core/resource');
        $connection = $resource->getConnection('read');
        $table = $resource->getTableName('itoris_productpriceformula_conditions');
        return $connection->fetchOne("select max(condition_id) from {$table}");
    }

    public function getConditionsForLoad() {
        $conditionCollection = Mage::getModel('itoris_productpriceformula/condition')->getCollection();
        $conditionCollection->getSelect()->join(array('formula' => Mage::getSingleton('core/resource')->getTableName('itoris_productpriceformula_settings_formula')),
            'main_table.formula_id = formula.formula_id and formula.product_id = ' . $this->getCurrentProduct()->getId(), array('order' => 'formula.position', 'formula_id' => 'formula.formula_id')
        )->order('formula.position DESC');
        $conditions = array();
        foreach ($conditionCollection as $model) {
            if (array_key_exists($model->getFormulaId(), $conditions)) {
                $conditions[(int)$model->getFormulaId()][] = array(
                    'condition_id' 		=> $model->getConditionId(),
                    'formula_id'   		=> $model->getFormulaId(),
                    'condition'    		=> $model->getCondition(),
                    'position'     		=> $model->getPosition(),
                    'price'        		=> $model->getPrice(),
                    'override_weight'   => (int)$model->getOverrideWeight(),
                    'weight'        	=> $model->getWeight()
                );
            } else {
                $conditions[(int)$model->getFormulaId()] = array(
                     array(
                        'condition_id' 		=> $model->getConditionId(),
                        'formula_id'   		=> $model->getFormulaId(),
                        'condition'    		=> $model->getCondition(),
                        'position'     		=> $model->getPosition(),
                        'price'        		=> $model->getPrice(),
						'override_weight'   => (int)$model->getOverrideWeight(),
						'weight'        	=> $model->getWeight()
                     )
                );
            }

        }
        return $conditions;
    }

    public function operatorsTable() {
        return array(
            array('>', 'Left part is greater than right part', '{width} > 100'),
            array('<', 'Right part is greater than left part', '{year} < 2013'),
            array('==', 'Left part is equal to the right part', '{color} == "Red"'),
            array('!=', 'Left part is not equal to the right part', '{color} != "Red"'),
            array('>=', 'Left part greater or equal to the right part', '{weight} >= 50'),
            array('<=', 'Right part greater or equal to the left part', '{qty} <= 5'),
            array('&&', 'The "AND" operator', '{width} > 100 && {height} > 100'),
            array('||', 'The "OR" operator', '{year} < 2000 || {year} > 2013'),
            array('()', 'Sub condition', '( {a} > 10 || {b} > 10 ) && {c} > 10'),
            array('+', 'Addition', '{a} + {b} > 10'),
            array('-', 'Subtraction', '{a} - {b} > 10'),
            array('*', 'Multiplication', '{width} * {height} > 1000'),
            array('/', 'Division', '{a} < {b} / PI'),
        );
    }

    public function operatorsTableForPrice() {
        return array(
            array('()', 'Sub condition', '( {sku1} + {sku2} ) / PI'),
            array('+', 'Addition', '{sku1} + 10'),
            array('-', 'Subtraction', '{sku1} - 10'),
            array('*', 'Multiplication', '2 * PI * {sku_radius}'),
            array('/', 'Division', '{sku1} / 1.5'),
        );
    }

    public function mathFunctions() {
        return array(
            array('abs(x)', 'Returns the absolute value of x'),
            array('acos(x)', 'Returns the arccosine of x, in radians'),
            array('asin(x)', 'Returns the arcsine of x, in radians'),
            array('atan(x)', 'Returns the arctangent of x as a numeric value between -PI/2 and PI/2 radians'),
            array('atan2(y,x)', 'Returns the arctangent of the quotient of its arguments'),
            array('ceil(x)', 'Returns x, rounded upwards to the nearest integer'),
            array('cos(x)', 'Returns the cosine of x (x is in radians)'),
            array('exp(x)', 'Returns the value of Ex'),
            array('floor(x)', 'Returns x, rounded downwards to the nearest integer'),
            array('log(x)', 'Returns the natural logarithm (base E) of x'),
            array('max(x,y,z,...,n)', 'Returns the number with the highest value'),
            array('min(x,y,z,...,n)', 'Returns the number with the lowest value'),
            array('pow(x,y)', 'Returns the value of x to the power of y'),
            array('random()', 'Returns a random number between 0 and 1'),
            array('round(x)', 'Rounds x to the nearest integer'),
            array('sin(x)', 'Returns the sine of x (x is in radians)'),
            array('sqrt(x)', 'Returns the square root of x'),
            array('tan(x)', 'Returns the tangent of an angle'),
        );
    }

    public function constantTable() {
        return array(
            array('E', 'Returns Euler\'s number (approx. 2.718)'),
            array('LN2', 'Returns the natural logarithm of 2 (approx. 0.693)'),
            array('LN10', 'Returns the natural logarithm of 10 (approx. 2.302)'),
            array('LOG2E', 'Returns the base-2 logarithm of E (approx. 1.442)'),
            array('LOG10E', 'Returns the base-10 logarithm of E (approx. 0.434)'),
            array('PI', 'Returns PI (approx. 3.14)'),
            array('SQRT1_2', 'Returns the square root of 1/2 (approx. 0.707)'),
            array('SQRT2', 'Returns the square root of 2 (approx. 1.414)'),
        );
    }

    public function variablesTable() {
        return array(
            array('{configured_price}', 'Price after product options selected'),
            array('{initial_price}', 'Price before options selected'),
            array('{price}', 'Price after all calculations applied'),
            array('{special_price}', 'Special price configured in the product'),
            array('{any_attrbute_name}', 'Any product attribute name enclosed into {}'),
            array('{qty}', 'Product quantity selected'),
            array('{any_option_sku}', 'Call any product option by its SKU enclosed into {}'),
        );
    }

}

?>