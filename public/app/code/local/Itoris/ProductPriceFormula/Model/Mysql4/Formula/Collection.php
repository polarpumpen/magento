<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_PRODUCTPRICEFORMULA
 * @copyright  Copyright (c) 2013 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

  

class Itoris_ProductPriceFormula_Model_Mysql4_Formula_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract {

    protected $tableGroup = 'itoris_productpriceformula_group';
    protected $tableFormula = 'itoris_productpriceformula_settings_formula';

    protected function _construct() {
        $this->_init('itoris_productpriceformula/formula');
        $this->tableGroup = $this->getTable('group');
        $this->tableFormula = $this->getTable('formula');
    }

    protected function _initSelect() {
        parent::_initSelect();
        $this->getSelect()->joinLeft(
            array('group' => $this->tableGroup),
            'group.formula_id = main_table.formula_id',
            array('group_id' => 'group_concat(distinct group.group_id)')
        )->group('main_table.formula_id');

        return $this;
    }

    public function addGroupFilter($groupId) {
        $this->_select->having("group_id IS NULL OR FIND_IN_SET('" . intval($groupId) . "', group_id)");
        return $this;
    }
}
?>