<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_PRODUCTPRICEFORMULA
 * @copyright  Copyright (c) 2013 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

 

class Itoris_ProductPriceFormula_Model_Rewrite_Quote_Item extends Mage_Sales_Model_Quote_Item {

    public function calcRowTotal() {
        $helper = Mage::helper('itoris_productpriceformula/price');
        if ($helper->isRegisteredAutonomous() && $helper->getSettings()->getEnabled()) {
            $finalPrice = $helper->getProductFinalPrice($this);
            if (!is_null($finalPrice)) {
				$baseCurrency = Mage::app()->getWebsite()->getBaseCurrency();
				$currentCurrency = Mage::app()->getStore()->getCurrentCurrency();
				$currencyRate = $baseCurrency->getRate($currentCurrency);
				$currencyRate = $currencyRate > 0 ? $currencyRate : 1;
                $this->setPrice($finalPrice)
                    ->setBaseOriginalPrice($finalPrice);
				
				$this->setRowTotal($this->getQty() * $finalPrice * $currencyRate)
					->setBaseRowTotal($this->getQty() * $finalPrice * $currencyRate);
				return $this;
            }
        }
        return parent::calcRowTotal();
    }
}
?>