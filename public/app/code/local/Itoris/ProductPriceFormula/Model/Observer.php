<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_PRODUCTPRICEFORMULA
 * @copyright  Copyright (c) 2013 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

 
class Itoris_ProductPriceFormula_Model_Observer {
	
	protected $isEnabledFlag = false;
	
	public function __construct() {
		try {
			$helper = Mage::helper('itoris_productpriceformula/price');
			$this->isEnabledFlag =  $helper->isAdminRegistered() && $helper->getSettings()->getEnabled();
		} catch (Exception $e) {/** save store model */}
	}

    public function saveProduct($obj) {
		if (!$this->isEnabledFlag) return;
		
		//copy formulas if admin duplicates the product
		$request = Mage::app()->getRequest();
		if ($request->getActionName() == 'duplicate' && (int)$request->getParam('id') && (int)$obj->getDataObject()->getId()) {
			$this->duplicateProduct((int)$request->getParam('id'), (int)$obj->getDataObject()->getId());
			return;
		}
		
        $products = $obj->getDataObject();
        $productId = $products->getId();
        $settingsParam = Mage::app()->getRequest()->getParam('itoris_productpriceformula_settings');
        $conditionsParam = Mage::app()->getRequest()->getParam('itoris_productpriceformula_conditions');
        if (is_array($settingsParam)) {
            ksort($settingsParam);
        }
        $formulaIdsNotForDelete = array();
        $resource = Mage::getSingleton('core/resource');
        $connection = $resource->getConnection('read');
        $tableFormula = $resource->getTableName('itoris_productpriceformula_settings_formula');
        $tableCondition = $resource->getTableName('itoris_productpriceformula_conditions');
        if (!empty($settingsParam)) {
            foreach ($settingsParam as $param) {
                $conditionNotForDelete = array();
                $settingsModel = Mage::getModel('itoris_productpriceformula/formula');
                $formulaIdDb = (int)$param['formula_id_db'];
                $settingsModel->load($formulaIdDb);
                $settingsModel->setName($param['name']);
                $settingsModel->setProductId((int)$productId);
                $settingsModel->setStatus((int)$param['status']);
				$settingsModel->setApplyToTotal((int)$param['apply_to_total']);
				$settingsModel->setFrontendTotal((int)$param['frontend_total']);
				$disallowCriteria = array();
				foreach($param['disallow_formula'] as $key => $formula) $disallowCriteria[] = array('formula' => $formula, 'message' => $param['disallow_message'][$key]);
				$settingsModel->setDisallowCriteria(json_encode($disallowCriteria));
                if (isset($param['position'])) {
                    $settingsModel->setPosition((int)$param['position']);
                } else {
                    $settingsModel->setPosition(0);
                }
                if (isset($param['active_from']) && !empty($param['active_from'])) {
                    $prepareStartDate = explode('/', $param['active_from']);
                    $startDate = $prepareStartDate[2] . '-' . $prepareStartDate[0] . '-' . $prepareStartDate[1];
                    $settingsModel->setActiveFrom($startDate);
                } else {
                    $settingsModel->setActiveFrom(null);
                }
                if (isset($param['active_to']) && !empty($param['active_to'])) {
                    $prepareEndDate = explode('/', $param['active_to']);
                    $endDate = $prepareEndDate[2] . '-' . $prepareEndDate[0] . '-' . $prepareEndDate[1];
                    $settingsModel->setActiveTo($endDate);
                } else {
                    $settingsModel->setActiveTo(null);
                }
                if (isset($param['run_always'])) {
                    $settingsModel->setRunAlways((int)$param['run_always']);
                } else {
                    $settingsModel->setRunAlways(0);
                }
                $settingsModel->save();
                $formulaId = (int)$settingsModel->getId();
                $formulaIdsNotForDelete[] = $formulaId;
                $tableGroup = $resource->getTableName('itoris_productpriceformula_group');
                $valueUserGroup = $param['group'];
                $connection->query("delete from {$tableGroup} where formula_id={$formulaId}");
                foreach ($valueUserGroup as $group) {
                    if ($group != '') {
                        $connection->query("insert into {$tableGroup} (formula_id, group_id) values ({$formulaId}, {$group})");
                    }
                }
                if (!empty($conditionsParam)) {
                    if ($formulaIdDb != $formulaId) {
                        $conditionsParamByFormulaId = $conditionsParam[$formulaIdDb];
                    } else {
                        $conditionsParamByFormulaId = $conditionsParam[$formulaId];
                    }
                    foreach ($conditionsParamByFormulaId as $conditionId => $conditionData) {
                        $conditionModel = Mage::getModel('itoris_productpriceformula/condition');
                        $conditionModel->load($conditionId);
                        $conditionModel->setFormulaId($formulaId);
                        if (isset($conditionData['condition'])) {
                            $conditionModel->setCondition($conditionData['condition']);
                        } else {
                            $conditionModel->setCondition(null);
                        }
                        $conditionModel->setPrice($conditionData['price']);
                        $conditionModel->setPosition($conditionData['position']);
                        $conditionModel->setOverrideWeight(isset($conditionData['override_weight']) && trim($conditionData['weight']) != "" ? 1 : 0);
                        $conditionModel->setWeight($conditionData['weight']);
                        $conditionModel->save();
                        $conditionNotForDelete[] = $conditionModel->getId();
                    }
                }
                if (!empty($conditionNotForDelete)) {
                    $conditionNotForDelete = implode(',', $conditionNotForDelete);
                    $connection->query("delete from {$tableCondition} where `condition_id` not in ({$conditionNotForDelete}) and formula_id={$formulaId}");
                } else {
                    $connection->query("delete from {$tableCondition} where formula_id={$formulaId}");
                }
            }
        }

        if (!empty($formulaIdsNotForDelete)) {
            $formulaIdsNotForDelete = implode(',', $formulaIdsNotForDelete);
            $connection->query("delete from {$tableFormula} where `formula_id` not in ({$formulaIdsNotForDelete}) and product_id={$productId}");
        } else {
            $connection->query("delete from {$tableFormula} where product_id={$productId}");
        }


    }
	
	public function checkErrors($observer) {
		$quoteItem = $observer->getEvent()->getItem();
		$price = Mage::helper('itoris_productpriceformula/price')->getProductFinalPrice($quoteItem);
		if ($quoteItem->getPriceFormulaError()) {
			Mage::throwException($quoteItem->getPriceFormulaError());
		}
	}
	
	public function duplicateProduct($oldProductId, $newProductId) {
        $resource = Mage::getSingleton('core/resource');
        $connection = $resource->getConnection('read');
        $tableFormula = $resource->getTableName('itoris_productpriceformula_settings_formula');
        $tableCondition = $resource->getTableName('itoris_productpriceformula_conditions');
		$formulas = $connection->fetchCol("select `formula_id` from {$tableFormula} where `product_id` = {$oldProductId}");
		foreach($formulas as $formulaId) {
			$formula = Mage::getModel('itoris_productpriceformula/formula')->load($formulaId);
			$formula->setId(null)->setProductId($newProductId)->save();
			$conditions = $connection->fetchCol("select `condition_id` from {$tableCondition} where `formula_id` = {$formulaId}");
			foreach($conditions as $conditionId) {
				$condition = Mage::getModel('itoris_productpriceformula/condition')->load($conditionId);
				$condition->setId(null)->setFormulaId($formula->getId())->save();
			}
		}
	}
	
    public function addMassactionToProductGrid($observer) {
        if ($this->isEnabledFlag
            && $observer->getBlock() instanceof Mage_Adminhtml_Block_Catalog_Product_Grid
            && $observer->getBlock()->getMassactionBlock()
        ) {
            $observer->getBlock()->getMassactionBlock()->addItem('copy_price_formulas', array(
                'label'=> Mage::helper('itoris_productpriceformula')->__('Copy Price Formulas 1 to Many'),
                'url'  => $observer->getBlock()->getUrl('adminhtml/productpriceformula_formula/massCopy', array('_current'=>true)),
                'additional' => array(
                    'visibility' => array(
                        'name' => 'from_product_id',
                        'type' => 'text',
                        'class' => 'required-entry',
                        'label' => Mage::helper('catalog')->__('From Product Id'),
                    )
                )
            ));
        }
    }

}
?>