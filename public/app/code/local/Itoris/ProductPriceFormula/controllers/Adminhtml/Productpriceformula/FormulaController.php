<?php
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_PRODUCTPRICEFORMULA
 * @copyright  Copyright (c) 2013 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */


class Itoris_ProductPriceFormula_Adminhtml_Productpriceformula_FormulaController extends Itoris_ProductPriceFormula_Controller_Admin_Controller {

    public function massCopyAction() {
        $productIds = $this->getRequest()->getParam('product');
        $fromProductId = $this->getRequest()->getParam('from_product_id');
        if (is_array($productIds)) {
			$resource = Mage::getSingleton('core/resource');
			$connection = $resource->getConnection('read');
			$tableFormula = $resource->getTableName('itoris_productpriceformula_settings_formula');
			$tableCondition = $resource->getTableName('itoris_productpriceformula_conditions');
			$formulas = $connection->fetchCol("select `formula_id` from {$tableFormula} where `product_id` = {$fromProductId}");
			if (count($formulas) > 0) {
				$saved = 0;
				foreach ($productIds as $newProductId) {
					//delete all old formulas
					$connection->query("delete from {$tableFormula} where `product_id` = {$newProductId}");
					//create new
					foreach($formulas as $formulaId) {
						$formula = Mage::getModel('itoris_productpriceformula/formula')->load($formulaId);
						$formula->setId(null)->setProductId($newProductId)->save();
						$conditions = $connection->fetchCol("select `condition_id` from {$tableCondition} where `formula_id` = {$formulaId}");
						foreach($conditions as $conditionId) {
							$condition = Mage::getModel('itoris_productpriceformula/condition')->load($conditionId);
							$condition->setId(null)->setFormulaId($formula->getId())->save();
						}
					}
					$saved++;
				}
				$this->_getSession()->addSuccess($this->__('Price formulas have been copied to %s products', $saved));
			} else {
				$this->_getSession()->addError($this->__('Product doesn\'t have price formulas'));
			}
        } else {
            $this->_getSession()->addError($this->__('Please select product ids'));
        }

        $this->_redirect('adminhtml/catalog_product/', array('_current' => true));
    }
}
?>