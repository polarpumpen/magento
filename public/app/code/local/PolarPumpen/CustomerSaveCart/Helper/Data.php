<?php

/* 

 */

class PolarPumpen_CustomerSaveCart_Helper_Data extends Mage_Core_Helper_Abstract
{

    /**
     * Creates a customer out of a customer base info array containing
     * firstname, lastname and email.
     *
     * $customerBaseInfo['firstname']
     * $customerBaseInfo['lastname']
     * $customerBaseInfo['email']
     * @param array $customerBaseInfo
     * @return bool
     *
     *
     */


    public function createCustomer($customerBaseInfo)
    {
        try {
            if (!is_array($customerBaseInfo) || empty($customerBaseInfo['email'])) {
                throw new InvalidArgumentException('Customer data was empty, customer could not be created. Input customerBadeInfo : '.gettype($customerBaseInfo));
            }
                $customer = Mage::getModel("customer/customer");
                //$customer->setWebsiteId(1);
                $customer->setWebsiteId($customer->setWebsiteId(Mage::app()->getWebsite()->getId()));
                $customer->setFirstname($customerBaseInfo['firstname']);
                $customer->setLastname($customerBaseInfo['lastname']);
                $customer->setEmail($customerBaseInfo['email']);
                $customer->setPassword($customer->generatePassword(8));

                $customer->save();
                $customer->setConfirmation(null);

                if(!$customer->save()) {
                    throw new Exception("Error when trying to save the customer. input Email: " . $customerBaseInfo['email']);
                }
                //$customer->sendNewAccountEmail();

        } catch (Exception $ex) {
            $saveCartLog = Mage::getModel('polarpumpen_customersavecart/savecartlog');
            $saveCartLog->logError("Exception", "createCustomer", $ex->getMessage(), $ex->getTraceAsString());
            $saveCartLog->save();
            unset($saveCartLog);

        } finally {
            if(!empty($ex)) {
                return false;
            }

            $saveCartLog = Mage::getModel('polarpumpen_customersavecart/savecartlog');
            $saveCartLog->logNotice("Notice", "createCustomer", "Customer with email " . $customerBaseInfo['email'] .
                " did not exist and has been created with ID " . $customer->getId());
            $saveCartLog->save();
            unset($saveCartLog);

            return true;
        }

    }

    /**
     * @param Mage_Customer_Model_Customer $customerObj
     * @param array $inputAddress
     * @return bool
     */

    public function getAddressIdIfExists(Mage_Customer_Model_Customer $customerObj, $inputAddress = null)
    {

        $customer = Mage::getModel("customer/customer");
        $customer->setWebsiteId(Mage::app()->getWebsite()->getId());
        $customer->load($customerObj->getId());

        foreach ($customer->getAddresses() as $address) {

            if (!(strtolower($address->getFirstname()) === strtolower($inputAddress['firstname']))) {
                continue;
            }

            if (!(strtolower($address->getLastname()) === strtolower($inputAddress['lastname']))) {
                continue;
            }
            if (!(strtolower($address->getStreet(1)) === strtolower($inputAddress['street'][0]))) {
                continue;
            }

            if (!(strtolower($address->getCity()) === strtolower($inputAddress['city']))) {
                continue;
            }
            if (!((int)$address->getPostcode() == (int)trim($inputAddress['postcode']))) {
                continue;
            }

            return $address;
        }
        return false;
    }


    /**
     * @param Mage_Customer_Model_Customer $customer
     * @param $customerAddress
     * @param $type
     * @param int $sameDeliveryAsBilling
     * @return bool
     */

    public function setCustomerAddress(Mage_Customer_Model_Customer $customer, $customerAddress, $type, $sameDeliveryAsBilling = 1) {

        $existingAddress = Mage::helper('polarpumpen_customersavecart')->getAddressIdIfExists($customer, $customerAddress);

        if(!empty($existingAddress) && $existingAddress->getId() > 0) {


            //Kolla så inte telephone är tomt
            if(Mage::helper('polarpumpen_customersavecart')->matchTelephoneOnAddress($existingAddress, $customerAddress['telephone'])) {

                switch($type) {
                    case "billing":
                        $existingAddress->setIsDefaultBilling('1');

                        if($sameDeliveryAsBilling == 1) {
                            $existingAddress->setIsDefaultShipping('1');
                        } else {
                            $existingAddress->setIsDefaultShipping('0');
                        }
                        $existingAddress->save();


                        return true;

                    case "shipping":
                        $existingAddress->setIsDefaultShipping('1');
                        $existingAddress->save();


                        return true;

                }
            } else {
                $existingAddress->setTelephone($customerAddress['telephone']);
                $existingAddress->save();
            }

        } else {

            $newAddress = Mage::getModel('customer/address');
            $newAddress->setData($customerAddress);
            $newAddress->setCustomerId($customer->getId());

            switch($type) {
                case "billing":
                    $newAddress->setIsDefaultBilling('1');
                    if($sameDeliveryAsBilling == 1) {
                        $newAddress->setIsDefaultShipping('1');
                    } else {
                        $newAddress->setIsDefaultShipping('0');
                    }
                    $newAddress->setSaveInAddressBook('1');
                    $newAddress->save();

                    return true;

                case "shipping":

                    $newAddress->setIsDefaultBilling('0');
                    $newAddress->setIsDefaultShipping('1');
                    $newAddress->setSaveInAddressBook('1');
                    $newAddress->save();

                    return true;
            }

        }

    }

    /**
     * @param $address
     * @param $phoneNumberToMatch
     * @return bool
     */

    public function matchTelephoneOnAddress($address, $phoneNumberToMatch) {

        if((int)$address->getTelephone() == (int)$phoneNumberToMatch) {
            return true;
        }
        return false;

    }

    public function createNewQuoteFromQuote(Mage_Sales_Model_Resource_Quote $sourceQuote)
    {
        try {

            $newQuote = Mage::getModel('sales/quote');
            $newQuote->merge($sourceQuote);

            if(!$newQuote->collectTotals()->save()) {
                throw new Exception("Could not copy source quote into a new quote. Provided sourceQuote ID = " . $sourceQuote->getId());
            }

            //Merge of quotes does not include Payment, Shipping, or Billing data so we nee dto copy these from the old quote.
            //Payment info
            $newPaymentQuote = $newQuote->getPayment();
            $sourcePaymentQuote = $sourceQuote->getPayment();

            //Billing info
            $newBillingAddress = $newQuote->getBillingAddress();
            $sourceBillingAddress = $sourceQuote->getBillingAddress();

            //Delivery info
            $newShippingAddress = $newQuote->getShippingAddress();
            $sourceShippingAddress = $sourceQuote->getShippingAddress();

            if(!Mage::helper('polarpumpen_customersavecart')->copyQuoteFlatData($sourceQuote, $newQuote)) {
                throw new Exception("copyQuoteFlatData returned false. ");
            }
            if(!Mage::helper('polarpumpen_customersavecart')->copyQuotePaymentData($sourcePaymentQuote, $newPaymentQuote)) {
                throw new Exception("copyQuotePaymentData returned false. ");
            }
            if(!Mage::helper('polarpumpen_customersavecart')->copyQuoteAdressData($sourceBillingAddress, $newBillingAddress)) {
                throw new Exception("copyQuoteAdressData(billing) returned false. ");
            }
            if(!Mage::helper('polarpumpen_customersavecart')->copyQuoteAdressData($sourceShippingAddress, $newShippingAddress, false, "shipping")) {
                throw new Exception("copyQuoteAdressData(shipping) returned false. ");
            }

        } catch (Exception $ex) {
            $saveCartLog = Mage::getModel('polarpumpen_customersavecart/savecartlog');
            $saveCartLog->logError("Exception", "createNewQuoteFromQuote",
                $ex->getMessage(), $ex->getTraceAsString());
            $saveCartLog->save();
            unset($saveCartLog);
        } finally {
            if(!empty($ex)) {
                return false;
            }
            return $newQuote;
        }

    }

    /**
     * @param $address
     * @return bool
     */

    public function checkIfCustomerHaveDefaultPolarpumpenAddress($address) {

        $polarpumpenDefaultAddress = array(
            "FirstName" => "Polarpumpen",
            "LastName" => "AB",
            "Street" => "Datavägen14a",
            "Postcode" =>"43632",
            "City" => "Askim",
            "Phone" => "0770777600"
        );


        $addressWithoutWhiteSpace = str_replace(" ", "", $address->getStreet(1));
        $phoneWithoutWhiteSpace = str_replace(" ", "", $address->getTelephone());
        $firstnameWithoutWhiteSpace = str_replace(" ", "", $address->getFirstname());
        /* Check if addresses matches. If we got a match return true */

        if(strtolower($addressWithoutWhiteSpace) === strtolower($polarpumpenDefaultAddress["Street"])) {
            return true;
        }

        if(strtolower($phoneWithoutWhiteSpace) === strtolower($polarpumpenDefaultAddress["Phone"])) {
            /* We got a match */
            return true;
        }

        if(strtolower($firstnameWithoutWhiteSpace) === strtolower($polarpumpenDefaultAddress["FirstName"]) &&
            strtolower($phoneWithoutWhiteSpace) === strtolower($polarpumpenDefaultAddress["Phone"])) {

            return true;

        }

        if(strtolower($addressWithoutWhiteSpace) === strtolower($polarpumpenDefaultAddress["Street"]) &&
            strtolower($phoneWithoutWhiteSpace) === strtolower($polarpumpenDefaultAddress["Phone"])) {
            /* We got a match */

            return true;
        }

        return false;

    }

    /**
     * @param Mage_Customer_Model_Customer $customer
     * @param int $quoteIdToInactivate
     * @return bool
     */

    public function deleteCustomersQuote(Mage_Customer_Model_Customer $customer, $quoteIdToInactivate = 0)
    {
        try {
            $quoteCollection = Mage::getModel('sales/quote')
                ->getCollection()
                ->addFieldToFilter('customer_id', $customer->getId());

            $notExpiredCustomerToQuote = array();

            /*
             * see if customer have previous CustomersToQuotes which has not expired, which we will not delete,
             * only set them to inactive if customers are clicking the buy button again.
            */

            $customerToQuotes = Mage::getModel('polarpumpen_customersavecart/customertoquote')->loadByCustomerId($customer->getId());

            foreach($customerToQuotes as $quote) {
                $notExpiredCustomerToQuote[] = $quote->getQuoteId();
            }

            if(count($quoteCollection) > 0 ) {
                foreach ($quoteCollection as $item) {
                    if(in_array($item->getId(), $notExpiredCustomerToQuote)) {
                        $item->setIsActive(0);
                        $item->save();
                    } else {
                        $item->delete();
                    }

                    /*
                    if($item->getId() == $quoteIdToInactivate) {
                        $item->setIsActive(0);
                        $item->save();
                    } else {
                        $item->delete();
                    }*/
                }
            }

            return true;
        }catch (Exception $ex) {
            $saveCartLog = Mage::getModel('polarpumpen_customersavecart/savecartlog');
            $saveCartLog->logError("Exception", "deleteCustomersQuote",
                $ex->getMessage(), $ex->getTraceAsString());
            $saveCartLog->save();
            unset($saveCartLog);

            return false;
        }

    }

    /**
     * @param Mage_Sales_Model_Quote $quoteToUpdate
     * @param $productsSKUToKeep
     * @return bool
     */

    public function removeProductsFromQuote(Mage_Sales_Model_Quote $quoteToUpdate, $productsSKUToKeep)
    {
        try {
            foreach ($quoteToUpdate->getAllItems() as $item) {
                $itemSKU = $item->getSku();

                if($item->getParentItemId() === null) {

                    if (!in_array($itemSKU, $productsSKUToKeep)) {
                        $quoteToUpdate->removeItem($item->getId());
                    }
                }
            }

            $quoteToUpdate->collectTotals()->save();

        }catch (Exception $ex) {
            $saveCartLog = Mage::getModel('polarpumpen_customersavecart/savecartlog');
            $saveCartLog->logError("Exception", "removeProductsFromQuote",
                $ex->getMessage(), $ex->getTraceAsString());
            $saveCartLog->save();
            unset($saveCartLog);
            return false;
        }
        return true;

    }

    /**
     * @param $product
     * @param string $customOptionToFind
     * @return string
     */

    public function getCustomOption($product, $customOptionToFind = "") {

        $_customOptions = $product->getProduct()->getTypeInstance(true)->getOrderOptions($product->getProduct());
        $customOption = "";

        //Each custom option loop

        foreach($_customOptions['options'] as $_option){

            if($_option['label'] === $customOptionToFind) {
                $customOption = $_option['label'] .':'. $_option['value'];
            }
        }
        return $customOption;

    }

    /**
     * @param Mage_Sales_Model_Quote $quoteToCopyFrom
     * @param Mage_Sales_Model_Quote $quoteToCopyTo
     * @param bool $copyCustomerId
     * @return bool
     */

    public function copyQuoteFlatData(Mage_Sales_Model_Quote $quoteToCopyFrom, Mage_Sales_Model_Quote $quoteToCopyTo, $copyCustomerId = false)
    {

        try {
            if (empty($quoteToCopyFrom) || empty($quoteToCopyTo)) {
                throw new Exception("Either quoteToCopyFrom or quoteToCopyTo is empty");
            }

            $quoteToCopyTo->setCreatedAt($quoteToCopyFrom->getCreatedAt());
            $quoteToCopyTo->setUpdatedAt($quoteToCopyFrom->getUpdatedAt());
            $quoteToCopyTo->setConvertedAt($quoteToCopyFrom->getConvertedAt());
            $quoteToCopyTo->setStoreId($quoteToCopyFrom->getStoreId());
            $quoteToCopyTo->setIsActive($quoteToCopyFrom->getIsActive());
            $quoteToCopyTo->setIsVirtual($quoteToCopyFrom->getIsVirtual());
            $quoteToCopyTo->setIsMultiShipping($quoteToCopyFrom->getIsMultiShipping());
            $quoteToCopyTo->setItemsCount($quoteToCopyFrom->getItemsCount());
            $quoteToCopyTo->setItemsQty($quoteToCopyFrom->getItemsQty());
            $quoteToCopyTo->setOrigOrderId($quoteToCopyFrom->getOrigOrderId());
            $quoteToCopyTo->setStoreToBaseRate($quoteToCopyFrom->getStoreToBaseRate());
            $quoteToCopyTo->setStoreToQuoteRate($quoteToCopyFrom->getStoreToQuoteRate());
            $quoteToCopyTo->setBaseCurrencyCode($quoteToCopyFrom->getBaseCurrencyCode());
            $quoteToCopyTo->setStoreCurrencyCode($quoteToCopyFrom->getStoreCurrencyCode());
            $quoteToCopyTo->setQuoteCurrencyCode($quoteToCopyFrom->getQuoteCurrencyCode());
            $quoteToCopyTo->setGrandTotal($quoteToCopyFrom->getGrandTotal());
            $quoteToCopyTo->setBaseGrandTotal($quoteToCopyFrom->getBaseGrandTotal());
            $quoteToCopyTo->setCheckoutMethod($quoteToCopyFrom->getCheckoutMethod());

            if($copyCustomerId == true) {
                $quoteToCopyTo->setCustomerId($quoteToCopyFrom->getCustomerId());
            }
            $quoteToCopyTo->setCustomerTaxClassId($quoteToCopyFrom->getCustomerTaxClassId());
            $quoteToCopyTo->setCustomerGroupId($quoteToCopyFrom->getCustomerGroupId());
            $quoteToCopyTo->setCustomerEmail($quoteToCopyFrom->getCustomerEmail());
            $quoteToCopyTo->setCustomerPrefix($quoteToCopyFrom->getCustomerPrefix());
            $quoteToCopyTo->setCustomerFirstname($quoteToCopyFrom->getCustomerFirstname());
            $quoteToCopyTo->setCustomerLastname($quoteToCopyFrom->getCustomerLastname());
            $quoteToCopyTo->setCustomerSuffix($quoteToCopyFrom->getCustomerSuffix());
            $quoteToCopyTo->setCustomerDob($quoteToCopyFrom->getCustomerDob());
            $quoteToCopyTo->setCustomerNote($quoteToCopyFrom->getCustomerNote());
            $quoteToCopyTo->setCustomerNoteNotify($quoteToCopyFrom->getCustomerNoteNotify());
            $quoteToCopyTo->setCustomerIsGuest($quoteToCopyFrom->getCustomerIsGuest());
            $quoteToCopyTo->setRemoteIp($quoteToCopyFrom->getRemoteIp());
            $quoteToCopyTo->setAppliedRuleIds($quoteToCopyFrom->getAppliedRuleIds());
            $quoteToCopyTo->setReservedOrderId($quoteToCopyFrom->getReservedOrderId());
            $quoteToCopyTo->setPasswordHash($quoteToCopyFrom->getPasswordHash());
            $quoteToCopyTo->setCouponCode($quoteToCopyFrom->getCouponCode());
            $quoteToCopyTo->setGlobalCurrencyCode($quoteToCopyFrom->getGlobalCurrencyCode());
            $quoteToCopyTo->setBaseToGlobalRate($quoteToCopyFrom->getBaseToGlobalRate());
            $quoteToCopyTo->setBaseToQuoteRate($quoteToCopyFrom->getBaseToQuoteRate());
            $quoteToCopyTo->setCustomerTaxvat($quoteToCopyFrom->getCustomerTaxvat());
            $quoteToCopyTo->setCustomerGender($quoteToCopyFrom->getCustomerGender());
            $quoteToCopyTo->setSubtotal($quoteToCopyFrom->getSubtotal());
            $quoteToCopyTo->setSubtotalWithDiscount($quoteToCopyFrom->getSubtotalWithDiscount());
            $quoteToCopyTo->setBaseSubtotalWithDiscount($quoteToCopyFrom->getBaseSubtotalWithDiscount());
            $quoteToCopyTo->setIsChanged($quoteToCopyFrom->getIsChanged());
            $quoteToCopyTo->setTriggerRecollect($quoteToCopyFrom->getTriggerRecollect());
            $quoteToCopyTo->setExtShippingInfo($quoteToCopyFrom->getExtShippingInfo());
            $quoteToCopyTo->setGiftMessageId($quoteToCopyFrom->getGiftMessageId());
            $quoteToCopyTo->setIsPersistent($quoteToCopyFrom->getIsPersistent());
            $quoteToCopyTo->setFirecheckoutDeliveryDate($quoteToCopyFrom->getFirecheckoutDeliveryDate());
            $quoteToCopyTo->setFirecheckoutDeliveryTimerange($quoteToCopyFrom->getFirecheckoutDeliveryTimerange());
            $quoteToCopyTo->setFirecheckoutCustomerComment($quoteToCopyFrom->getFirecheckoutCustomerComment());
            $quoteToCopyTo->setTmField1($quoteToCopyFrom->getTmField1());
            $quoteToCopyTo->setTmField2($quoteToCopyFrom->getTmField2());
            $quoteToCopyTo->setTmField3($quoteToCopyFrom->getTmField3());
            $quoteToCopyTo->setTmField4($quoteToCopyFrom->getTmField4());
            $quoteToCopyTo->setTmField5($quoteToCopyFrom->getTmField5());
            $quoteToCopyTo->setKlarnaCheckoutId($quoteToCopyFrom->getKlarnaCheckoutId());
            $quoteToCopyTo->setKlarnaCheckoutNewsletter($quoteToCopyFrom->getKlarnaCheckoutNewsletter());
            $quoteToCopyTo->setCodFee($quoteToCopyFrom->getCodFee());
            $quoteToCopyTo->setBaseCodFee($quoteToCopyFrom->getBaseCodFee());
            $quoteToCopyTo->setCodTaxAmount($quoteToCopyFrom->getCodTaxAmount());
            $quoteToCopyTo->setBaseCodTaxAmount($quoteToCopyFrom->getBaseCodTaxAmount());


            if(!$quoteToCopyTo->collectTotals()->save()) {
                throw new Exception("Error when trying to save the quote flat data. Quote to copy from id " .  $quoteToCopyFrom->getId() . " quote to copy to Id " . $quoteToCopyTo->getId());
            }

        }catch (Exception $ex) {
            $saveCartLog = Mage::getModel('polarpumpen_customersavecart/savecartlog');
            $saveCartLog->logError("Exception", "copyQuoteFlatData",
                $ex->getMessage(), $ex->getTraceAsString());
            $saveCartLog->save();
            unset($saveCartLog);
        } finally {
            if(!empty($ex)) {
                return false;
            }
            return true;
        }
    }

    /**
     * @param Mage_Sales_Model_Quote_Payment $quoteToCopyFrom
     * @param Mage_Sales_Model_Quote_Payment $quoteToCopyTo
     * @return bool
     */

    public function copyQuotePaymentData(Mage_Sales_Model_Quote_Payment $quoteToCopyFrom, Mage_Sales_Model_Quote_Payment $quoteToCopyTo)
    {
        try {

            if (empty($quoteToCopyFrom) || empty($quoteToCopyTo)) {
                throw new Exception("Either quoteToCopyFrom or quoteToCopyTo is empty");
            }

            $quoteToCopyTo->setCreatedAt($quoteToCopyFrom->getCreatedAt());
            $quoteToCopyTo->setUpdatedAt($quoteToCopyFrom->getUpdatedAt());
            $quoteToCopyTo->setMethod($quoteToCopyFrom->getMethod());
            $quoteToCopyTo->setCcType($quoteToCopyFrom->getCcType());
            $quoteToCopyTo->setCcNumberEnc($quoteToCopyFrom->getCcNumberEnc());
            $quoteToCopyTo->setCcLast4($quoteToCopyFrom->getCcLast4());
            $quoteToCopyTo->setCcCidEnc($quoteToCopyFrom->getCcCidEnc());
            $quoteToCopyTo->setCcOwner($quoteToCopyFrom->getCcOwner());
            $quoteToCopyTo->setCcExpMonth($quoteToCopyFrom->getCcExpMonth());
            $quoteToCopyTo->setCcExpYear($quoteToCopyFrom->getCcExpYear());
            $quoteToCopyTo->setCcSsOwner($quoteToCopyFrom->getCcSsOwner());
            $quoteToCopyTo->setCcSsStartMonth($quoteToCopyFrom->getCcSsStartMonth());
            $quoteToCopyTo->setCcSsStartYear($quoteToCopyFrom->getCcSsStartYear());
            $quoteToCopyTo->setPoNumber($quoteToCopyFrom->getPoNumber());
            $quoteToCopyTo->setAdditionalData($quoteToCopyFrom->getAdditionalData());
            $quoteToCopyTo->setCcSsIssue($quoteToCopyFrom->getCcSsIssue());
            $quoteToCopyTo->setAdditionalInformation($quoteToCopyFrom->getAdditionalInformation());
            $quoteToCopyTo->setPaypalPayerId($quoteToCopyFrom->getPaypalPayerId());
            $quoteToCopyTo->setPaypalPayerStatus($quoteToCopyFrom->getPaypalPayerStatus());
            $quoteToCopyTo->setPaypalCorrelationId($quoteToCopyFrom->getPaypalCorrelationId());

            if(!$quoteToCopyTo->save()) {
                throw new Exception("Error when trying to save the quote payment data. Quote to copy from id" .  $quoteToCopyFrom->getId() . " quote to copy to Id " . $quoteToCopyTo->getId());
            }


        }catch (Exception $ex) {
            $saveCartLog = Mage::getModel('polarpumpen_customersavecart/savecartlog');
            $saveCartLog->logError("Exception", "copyQuotePaymentData",
                $ex->getMessage(), $ex->getTraceAsString());
            $saveCartLog->save();
            unset($saveCartLog);
        } finally {
            if(!empty($ex)) {
                return false;
            }

            return true;
        }
    }

    /**
     * @param Mage_Sales_Model_Quote_Address $quoteToCopyFrom
     * @param Mage_Sales_Model_Quote_Address $quoteToCopyTo
     * @param bool $copyCustomerId
     * @param string $adressType
     * @return bool
     */

    public function copyQuoteAdressData(Mage_Sales_Model_Quote_Address $quoteToCopyFrom, Mage_Sales_Model_Quote_Address $quoteToCopyTo, $copyCustomerId = false, $adressType = "")
    {

        try {
            if(empty($quoteToCopyFrom) || empty($quoteToCopyTo)) {
                throw new Exception("Either quoteToCopyFrom or quoteToCopyTo is empty");
            }

            $quoteToCopyTo->setCreatedAt($quoteToCopyFrom->getCreatedAt());
            $quoteToCopyTo->setUpdatedAt($quoteToCopyFrom->getUpdatedAt());

            if($copyCustomerId == true) {
                $quoteToCopyTo->setCustomerId($quoteToCopyFrom->getCustomerId());
            }

            $quoteToCopyTo->setSaveInAddressBook($quoteToCopyFrom->getSaveInAddressBook());
            $quoteToCopyTo->setCustomerAddressId($quoteToCopyFrom->getCustomerAddressId());
            $quoteToCopyTo->setAddressType($quoteToCopyFrom->getAddressType());
            $quoteToCopyTo->setEmail($quoteToCopyFrom->getEmail());
            $quoteToCopyTo->setPrefix($quoteToCopyFrom->getPrefix());
            $quoteToCopyTo->setFirstname($quoteToCopyFrom->getFirstname());
            $quoteToCopyTo->setMiddlename($quoteToCopyFrom->getMiddlename());
            $quoteToCopyTo->setLastname($quoteToCopyFrom->getLastname());
            $quoteToCopyTo->setSuffix($quoteToCopyFrom->getSuffix());
            $quoteToCopyTo->setCompany($quoteToCopyFrom->getCompany());
            $quoteToCopyTo->setStreet($quoteToCopyFrom->getStreet());
            $quoteToCopyTo->setCity($quoteToCopyFrom->getCity());
            $quoteToCopyTo->setRegion($quoteToCopyFrom->getRegion());
            $quoteToCopyTo->setRegionId($quoteToCopyFrom->getRegionId());
            $quoteToCopyTo->setPostcode($quoteToCopyFrom->getPostcode());
            $quoteToCopyTo->setCountryId($quoteToCopyFrom->getCountryId());
            $quoteToCopyTo->setTelephone($quoteToCopyFrom->getTelephone());

            $quoteToCopyTo->setFax($quoteToCopyFrom->getFax());
            $quoteToCopyTo->setSameAsBilling($quoteToCopyFrom->getSameAsBilling());
            $quoteToCopyTo->setFreeShipping($quoteToCopyFrom->getFreeShipping());
            $quoteToCopyTo->setCollectShippingRates($quoteToCopyFrom->getCollectShippingRates());
            $quoteToCopyTo->setShippingMethod($quoteToCopyFrom->getShippingMethod());
            $quoteToCopyTo->setShippingDescription($quoteToCopyFrom->getShippingDescription());
            $quoteToCopyTo->setWeight($quoteToCopyFrom->getWeight());
            $quoteToCopyTo->setSubtotal($quoteToCopyFrom->getSubtotal());
            $quoteToCopyTo->setBaseSubtotal($quoteToCopyFrom->getBaseSubtotal());
            $quoteToCopyTo->setSubtotalWithDiscount($quoteToCopyFrom->getSubtotalWithDiscount());
            $quoteToCopyTo->setBaseSubtotalWithDiscount($quoteToCopyFrom->getBaseSubtotalWithDiscount());
            $quoteToCopyTo->setTaxAmount($quoteToCopyFrom->getTaxAmount());
            $quoteToCopyTo->setBaseTaxAmount($quoteToCopyFrom->getBaseTaxAmount());
            $quoteToCopyTo->setShippingAmount($quoteToCopyFrom->getShippingAmount());
            $quoteToCopyTo->setBaseShippingAmount($quoteToCopyFrom->getBaseShippingAmount());
            $quoteToCopyTo->setShippingTaxAmount($quoteToCopyFrom->getShippingTaxAmount());
            $quoteToCopyTo->setBaseShippingTaxAmount($quoteToCopyFrom->getBaseShippingTaxAmount());
            $quoteToCopyTo->setDiscountAmount($quoteToCopyFrom->getDiscountAmount());
            $quoteToCopyTo->setBaseDiscountAmount($quoteToCopyFrom->getBaseDiscountAmount());
            $quoteToCopyTo->setGrandTotal($quoteToCopyFrom->getGrandTotal());
            $quoteToCopyTo->setBaseGrandTotal($quoteToCopyFrom->getBaseGrandTotal());
            $quoteToCopyTo->setCustomerNotes($quoteToCopyFrom->getCustomerNotes());
            $quoteToCopyTo->setAppliedTaxes($quoteToCopyFrom->getAppliedTaxes());
            $quoteToCopyTo->setDiscountDescription($quoteToCopyFrom->getDiscountDescription());
            $quoteToCopyTo->setShippingDiscountAmount($quoteToCopyFrom->getShippingDiscountAmount());
            $quoteToCopyTo->setBaseShippingDiscountAmount($quoteToCopyFrom->getBaseShippingDiscountAmount());
            $quoteToCopyTo->setSubtotalInclTax($quoteToCopyFrom->getSubtotalInclTax());
            $quoteToCopyTo->setBaseSubtotalTotalInclTax($quoteToCopyFrom->getBaseSubtotalTotalInclTax());
            $quoteToCopyTo->setHiddenTaxAmount($quoteToCopyFrom->getHiddenTaxAmount());
            $quoteToCopyTo->setBaseHiddenTaxAmount($quoteToCopyFrom->getBaseHiddenTaxAmount());
            $quoteToCopyTo->setShippingHiddenTaxAmount($quoteToCopyFrom->getShippingHiddenTaxAmount());
            $quoteToCopyTo->setBaseShippingHiddenTaxAmnt($quoteToCopyFrom->getBaseShippingHiddenTaxAmnt());
            $quoteToCopyTo->setShippingInclTax($quoteToCopyFrom->getShippingInclTax());
            $quoteToCopyTo->setBaseShippingInclTax($quoteToCopyFrom->getBaseShippingInclTax());
            $quoteToCopyTo->setVatId($quoteToCopyFrom->getVatId());
            $quoteToCopyTo->setVatIsValid($quoteToCopyFrom->getVatIsValid());
            $quoteToCopyTo->setVatRequestId($quoteToCopyFrom->getVatRequestId());
            $quoteToCopyTo->setVatRequestDate($quoteToCopyFrom->getVatRequestDate());
            $quoteToCopyTo->setVatRequestSuccess($quoteToCopyFrom->getVatRequestSuccess());
            $quoteToCopyTo->setGiftMessageId($quoteToCopyFrom->getGiftMessageId());
            $quoteToCopyTo->setCareOf($quoteToCopyFrom->getCareOf());
            $quoteToCopyTo->setCodFee($quoteToCopyFrom->getCodFee());
            $quoteToCopyTo->setBaseCodFee($quoteToCopyFrom->getBaseCodFee());
            $quoteToCopyTo->setCodTaxAmount($quoteToCopyFrom->getCodTaxAmount());
            $quoteToCopyTo->setBaseCodTaxAmount($quoteToCopyFrom->getBaseCodTaxAmount());

            if($adressType === "shipping") {
                //$quoteToCopyTo->save()->collectShippingRates()->save();
                $quoteToCopyTo->setCollectShippingRates(1);
            }

            if(!$quoteToCopyTo->save()) {
                throw new Exception("Error when trying to save the quote address data. Quote to copy from id" .  $quoteToCopyFrom->getId() . " quote to copy to Id " . $quoteToCopyTo->getId());
            }

        } catch (Exception $ex) {
            $saveCartLog = Mage::getModel('polarpumpen_customersavecart/savecartlog');
            $saveCartLog->logError("Exception", "copyQuoteAdressData",
                $ex->getMessage(), $ex->getTraceAsString());
            $saveCartLog->save();
            unset($saveCartLog);

        } finally {
            if(!empty($ex)) {
                return false;
            }
            return true;
        }
    }

    /**
     * @param $customerGroupIdToCheckAgainst
     * @return bool
     */

    public function checkIfSalesAreLoggedIn($customerGroupIdToCheckAgainst) {
        $isLoggedIn = Mage::getSingleton('customer/session')->isLoggedIn();
        /* If customer is logged in */
        if($isLoggedIn) {
            $customerGroupId = Mage::getSingleton('customer/session')->getCustomerGroupId();
            if($customerGroupId == $customerGroupIdToCheckAgainst) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param $daysToAdd
     * @param string $dateSource
     * @return bool|string
     */


    public function addDaysToDate($daysToAdd, $dateSource = "NOW") {
        try {

            if($date = new DateTime($dateSource)) {
                $date->modify("+". (int)$daysToAdd . " day");

                return $date->format("Y-m-d H:i");
            }
        } catch(Exception $ex) {
            $saveCartLog = Mage::getModel('polarpumpen_customersavecart/savecartlog');
            $saveCartLog->logError("Exception", "addDaysToDate",
                $ex->getMessage(), $ex->getTraceAsString());
            $saveCartLog->save();
            unset($saveCartLog);
            return false;
        }
    }

    public function hashRequestParameters($customerID, $cartID, $emailaddress) {
        $hash = hash('sha256', "!Rävlåda12543!%#__ " . $customerID . " " . $cartID . " " . strtolower($emailaddress));

        return $hash;
    }
}