<?php
/**
 * Created by PhpStorm.
 * User: danielpettersson
 * Date: 2016-11-10
 * Time: 15:42
 */

class PolarPumpen_CustomerSaveCart_Model_Mysql4_Savecartlog_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        $this->_init('polarpumpen_customersavecart/savecartlog');
        //parent::_construct();
    }
}