<?php

/* 
 * 
 * 
 * Should we keep a connection table which holds saved quotes to get them where the customer actually have saved them.
 * To allow saved cart from guest, maybe we should have: 
 * id, quote_id, customer_email_address, markedAssave(bool)
 * Where a guest can save cart as guest(by providing his/hers emailadress). Then we could have a function in cart retrieve saved cart(and customer(guest/logged in) enter 
 * the email and cart will be loaded
 * 
 */

class PolarPumpen_CustomerSaveCart_Model_Customertoquote extends Mage_Core_Model_Abstract
{

    public function _construct()
    {
        $this->_init('polarpumpen_customersavecart/customertoquote');
        parent::_construct();
   }
	 
	 public function createCustomerToQuote(Mage_Customer_Model_Customer $customer, Mage_Sales_Model_Quote $quote, $expireDate, $quoteAlternative = 1) {
		 
		 if(!empty($customer) && !empty($quote)) {
			
			try {
				$this->setCustomerId($customer->getId());
				$this->setWebsiteId($customer->getWebsiteId());
				$this->setQuoteId($quote->getId());
                $this->setExpireDate($expireDate);
                $this->setQuoteAlternative($quoteAlternative);
			 
			} catch (Exception $ex) {
				Zend_Debug::dump($ex->getMessage());
                return false;
			}
		}
		//Check if it shoudl return null as well
		return $this;
	}

	public function loadBasedOnQuoteId($cartId) {

        $id = $this->_getResource()->loadByQuoteID('quote_id', $cartId);

        return $id;

    }

	public function setClicked() {

        $currentClicks = $this->getClicks();
        $currentClicks += 1;

        $this->setClicks($currentClicks);
        $this->save();

    }

    public function setWonQuote() {

        $this->setWon(1);
        $this->save();

        /*
        //$customerToQuote = Mage::getModel('polarpumpen_customersavecart/customertoquote');
        if($customerToQuote->load((int)$quoteID, 'quote_id')) {
            $customerToQuote->setWon(1);
            $customerToQuote->save();
        }*/

    }

    public function loadByQuoteId($quoteID){
        $collection = $this->getCollection()
            ->addFieldToFilter('quote_id', $quoteID);
        return $collection->getFirstItem();
    }


    public function loadByCustomerId($customerId) {

        $collection = $this->getCollection()
            ->addFieldToFilter('customer_id', $customerId)
           // ->addFieldToFilter('quote_id', array('neq'=>$quoteToExclude))
            ->addFieldToFilter('expire_date', array(gt =>date('Y-m-d H:i:s')));


        return $collection->load();

    }

}
