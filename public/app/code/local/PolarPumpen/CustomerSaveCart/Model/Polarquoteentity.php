<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class PolarPumpen_CustomerSaveCart_Model_Polarquoteentity extends Mage_Core_Model_Abstract {

    /**
     *  var string
     */

	public $Type = "";

    /**
     * @var string
     */

	public $Classification = "";

    /**
     * @var int
     */

	public $CustomerId = 0;

    /**
     * @var int
     */

    public $UserId = 0;

    /**
     * @var string
     */

	public $FirstName = "";

    /**
     * @var string
     */

	public $LastName = "";

    /**
     * @var string
     */

	public $PhoneNumber = "";

    /**
     * @var string
     */

	public $Email = "";

    /**
     * @var string
     */

	public $PersonalMessage = "";

    /**
     * @var int
     */

    public $HeatingArea = 0;

    /**
     * @var string
     */

    public $GeographicArea = "";

    /**
     * @var string
     */

    public $Other = "";

    /**
     * @var string
     */

    public $QuestionAndAnswer = "";

    /**
     * @var int
     */
	public $Proposal1Id = 0;

    /**
     * @var array
     *
     * 	'Sku'=>'',
        'Name'=>'',
        'Quantity'=>0,
        'Price'=>0,
        'RecommendedPrice'=>0,
        'SpecialPrice'=>0,
        'CustomOptions'=>[]
        'WarrantyText'=>'',
        'Url'=>'',

     */

	public $Proposal1 = array();

	public $Proposal2Id = 0;

    /**
     * @var array
     *  'Sku'=>'',
        'Name'=>'',
        'Quantity'=>0,
        'Price'=>0,
        'RecommendedPrice'=>0,
        'SpecialPrice'=>0,
        'CustomOptions'=>[],
        'WarrantyText'=>'',
        'Url'=>'',
     *
     */
	
	public $Proposal2 =array();

    /**
     * Constructor
     */
	
	protected function _construct()
    {
        $this->_init('polarpumpen_customersavecart/polarquoteentity');
        parent::_construct();
    }

    /**
     * @param Mage_Customer_Model_Customer $customer
     * @param Mage_Sales_Model_Quote $quoteAlternative1
     * @param Mage_Sales_Model_Quote|NULL $quoteAlternative2
     * @param null $additionalFormData
     * @param null $customDiscount
     * @return $this
     */

    public function createPolarQuoteEntity(Mage_Customer_Model_Customer $customer, Mage_Sales_Model_Quote $quoteAlternative1, Mage_Sales_Model_Quote $quoteAlternative2 = NULL, $additionalFormData = NULL, $customDiscount = NULL) {


        $temporarySalesId = array(
            2112,
            2118,
            2115,
            2116,
            2117,
            2119,
            2120,
            2113
        );

        $fixedFreightCosts = array(
            "amtable_amtable2" =>790,
            "amtable_amtable3" => 1290
        );



        //Just to have an empty array to use with freight sku.
        $customOptionArrayHolder = array();
        //counter for freight sku
        $i = 0;
        $shippingDiscount = 0;
        $specialPriceFreight = -1;

        $itemUrl = "";
        $cartPrice = 0;
        $recomendedPrice = 0;
        $specialPrice = 0;
        $productsCustomOption = array();
        $parentProduct = false;
        $productHasSpecialPrice = 0;

        $currentLoggedInSalesPersonId = Mage::getSingleton('customer/session')->getCustomer()->getId();


        $this->Proposal1Id = $quoteAlternative1->getEntityId();
        $this->CustomerId = $customer->getId();
        $this->FirstName = $quoteAlternative1->getCustomerFirstname();
        $this->LastName = $quoteAlternative1->getCustomerLastname();
        $this->PhoneNumber = $quoteAlternative1->getBillingAddress()->getTelephone();
        $this->Email = $quoteAlternative1->getCustomerEmail();
        $this->PersonalMessage = $quoteAlternative1->getFirecheckoutCustomerComment();

        //$this->UserId = 1898;


        if(!empty((int)$currentLoggedInSalesPersonId) && $currentLoggedInSalesPersonId > 0) {
            if(in_array($currentLoggedInSalesPersonId, $temporarySalesId)) {
                $this->UserId = $currentLoggedInSalesPersonId;
            } else {
                $this->UserId = 1898;
            }
        } else {
            $this->UserId = 0;
        }

        $this->PersonalMessage = $quoteAlternative1->getFirecheckoutCustomerComment();

        if(!empty($additionalFormData["offerType"])) {
            $this->Type = $additionalFormData["offerType"];
        } else {
            $this->Type = "Air";
        }

        if(!empty($additionalFormData["heatingArea"])) {
            $this->HeatingArea = intval($additionalFormData["heatingArea"]);
        } else {
            $this->HeatingArea = 0;
        }

        if(!empty($additionalFormData["geographicArea"])) {
            $this->GeographicArea = $additionalFormData["geographicArea"];
        } else {
            $this->GeographicArea = "";
        }

        if(!empty($additionalFormData["other"])) {
            $this->Other = $additionalFormData["other"];
        } else {
            $this->Other = "";
        }

        if(!empty($additionalFormData["offerClassification"])) {
            $this->Classification = $additionalFormData["offerClassification"];
        } else {
            $this->Classification = "Cold";
        }

        if(!empty($additionalFormData["questionAndAnswer"])) {
            $this->QuestionAndAnswer = $additionalFormData["questionAndAnswer"];
        }


        $customOptionHelper = Mage::helper('catalog/product_configuration');

        $cartItemsQuote1 = $quoteAlternative1->getAllItems();

        $bundleID = 0;
        foreach ($cartItemsQuote1 as $key =>$item) {
            $options = $customOptionHelper->getCustomOptions($item);


            $itemUrl = "";
            $cartPrice = 0;
            $recomendedPrice = 0;
            $specialPrice = -1;
            $productsCustomOption = array();
            $parentProduct = false;
            $productHasSpecialPrice = 0;


            if($item->getProductType() === "bundle" && is_null($item->getParentId())) {
                $bundleID = $item->getItemId();


            }

            if($item->getParentItemId() !== null && $item->getParentItemId() == $bundleID) {

                //Set price, special price and custom discount.

                if($item->getProduct()->getPrice() > $item->getPriceInclTax()) {
                    //$regularPrice = $item->getProduct()->getPrice();
                    $recomendedPrice = (int)$item->getProduct()->getPrice();
                    $specialPrice = 0;
                }


            } else {
                if(empty($options)) {
                    if((int)$item->getProduct()->getPrice() > (int)$item->getPriceInclTax()) {

                        $specialPrice = (int) $item->getPriceInclTax();
                        $cartPrice = (int)$item->getProduct()->getPrice();
                        $productHasSpecialPrice = 1;
                    }
                }
            }

            if($productHasSpecialPrice == 0) {
                $cartPrice = $item->getPriceInclTax();
            }


            if(empty($item->getParentItemId())) {
                $itemUrl= $item->getProduct()->getUrlPath();
                $parentProduct = true;
            }



            if(!empty($options)) {
                foreach($options as $option) {
                    $productsCustomOption[] = $option['value'];
                }
            }


                $this->Proposal1[$key] = array(
                    'SKU'=>$item->getSku(),
                    'Name'=>$item->getName(),
                    'Quantity'=>$item->getQty(),
                    'Price' => (int)$cartPrice,
                    'RecommendedPrice' => (int)$recomendedPrice,
                    'SpecialPrice' => (int)$specialPrice,
                    'CustomOptions' =>$productsCustomOption,
                    //'WarrantyText' => ' ',
                    'WarrantyText' => "",
                    'Url' => $itemUrl,
                    'CustomDiscount'=>"0",
                    'Parent'=>$parentProduct
                );

            if(is_array($customDiscount) && array_key_exists($item->getSku(), $customDiscount)) {
                $floatDiscount = floatval($customDiscount["" . $item->getSku() . ""]);

                if (is_float($floatDiscount) && $floatDiscount > 0) {
                    $this->Proposal1[$key]["CustomDiscount"] = $floatDiscount;
                }
            }


            unset($productsCustomOption);
            unset($options);
            unset($specialPrice);
            unset($itemUrl);
            unset($floatDiscount);

            $i++;

        }


        if(!empty($quoteAlternative1->getShippingAddress()->getShippingDescription())) {



            $shippingDiscount = abs($quoteAlternative1->getShippingAddress()->getDiscountAmount());

            if($shippingDiscount > $quoteAlternative1->getShippingAddress()->getShippingInclTax()
            || (int)$quoteAlternative1->getShippingAddress()->getShippingInclTax() == 0) {
                $specialPriceFreight = 0;
                $installationFreightCost = $fixedFreightCosts[$quoteAlternative1->getShippingAddress()->getShippingMethod()];
                
            } else {
                $installationFreightCost = (int)$quoteAlternative1->getShippingAddress()->getShippingInclTax();
            }

            $this->Proposal1[$i] = array(
                'SKU'=>"Frakt",
                'Name'=>$quoteAlternative1->getShippingAddress()->getShippingDescription(),
                'Quantity'=>"1",
                'Price' => $installationFreightCost,
                'RecommendedPrice' => "0",
                'SpecialPrice' => $specialPriceFreight,
                'CustomOptions' =>$customOptionArrayHolder,
                //'WarrantyText' => ' ',
                'WarrantyText' => "",
                'Url' => "",
                'CustomDiscount'=>"0",
                'Parent'=>true
            );


        }


        $i = 0;
        $bundleID = 0;
        if(!empty($quoteAlternative2)) {
            $cartItemsQuote2 = $quoteAlternative2->getAllItems();
            $this->Proposal2Id = $quoteAlternative2->getEntityId();

            foreach ($cartItemsQuote2 as $key =>$item) {

                $options = $customOptionHelper->getCustomOptions($item);

                $itemUrl = "";
                $cartPrice = 0;
                $recomendedPrice = 0;
                $specialPrice = -1;
                $productsCustomOption = array();
                $parentProduct = false;
                $productHasSpecialPrice = 0;


                if($item->getProductType() === "bundle" && is_null($item->getParentId())) {
                    $bundleID = $item->getItemId();


                }

                if($item->getParentItemId() !== null && $item->getParentItemId() == $bundleID) {

                    //Set price, special price and custom discount.

                    if($item->getProduct()->getPrice() > $item->getPriceInclTax()) {
                        //$regularPrice = $item->getProduct()->getPrice();
                        $recomendedPrice = (int)$item->getProduct()->getPrice();
                        $specialPrice = -1;
                    }

                }else {
                    if(empty($options)) {
                        if((int)$item->getProduct()->getPrice() > (int)$item->getPriceInclTax()) {
                            $specialPrice = (int)$item->getPriceInclTax();
                            $cartPrice = (int)$item->getProduct()->getPrice();
                            $productHasSpecialPrice = 1;
                        }
                    }
                }

                if($productHasSpecialPrice == 0) {
                    $cartPrice = $item->getPriceInclTax();
                }

                //$cartPrice = $item->getPriceInclTax();

                if(empty($item->getParentItemId())) {
                    $itemUrl= $item->getProduct()->getUrlPath();
                    $parentProduct = true;
                }



                if(!empty($options)) {
                    foreach($options as $option) {
                        $productsCustomOption[] = $option['value'];
                    }
                }

                    /*
                }else {
                    //If product has custom option prices could be really different
                    $specialPrice = (int)$item->getProduct()->getSpecialPrice();

                }*/

                /*

                if($specialPrice == 0 && !empty($item->getProduct()->getSpecialPrice())) {
                    //Try to get special price
                    $specialPrice = (int)$item->getProduct()->getSpecialPrice();

                }*/

                $this->Proposal2[$key] = array(
                    'SKU'=>$item->getSku(),
                    'Name'=>$item->getName(),
                    'Quantity'=>$item->getQty(),
                    'Price' => (int)$cartPrice,
                    'RecommendedPrice' => (int)$recomendedPrice,
                    'SpecialPrice' => (int)$specialPrice,
                    'CustomOptions' =>$productsCustomOption,
                    //'WarrantyText' => ' ',
                    'WarrantyText' => "",
                    'Url' => $itemUrl,
                    'CustomDiscount'=>"0",
                    'Parent'=>$parentProduct
                );

                if(is_array($customDiscount) && array_key_exists($item->getSku(), $customDiscount)) {
                    $floatDiscount = floatval($customDiscount["" . $item->getSku() . ""]);

                    if (is_float($floatDiscount) && $floatDiscount > 0) {
                        $this->Proposal2[$key]["CustomDiscount"] = $floatDiscount;
                    }
                }

                unset($productsCustomOption);
                unset($options);
                unset($specialPrice);
                unset($itemUrl);
                unset($floatDiscount);

                $i++;


            }

            if(!empty($quoteAlternative2->getShippingAddress()->getShippingDescription())) {
                unset($shippingDiscount);

                $specialPriceFreight = -1;

                $shippingDiscount = abs($quoteAlternative2->getShippingAddress()->getDiscountAmount());

                if ($shippingDiscount > $quoteAlternative2->getShippingAddress()->getShippingInclTax()
                    || (int)$quoteAlternative2->getShippingAddress()->getShippingInclTax() == 0) {

                    $specialPriceFreight = 0;
                    $installationFreightCost = $fixedFreightCosts[$quoteAlternative2->getShippingAddress()->getShippingMethod()];

                } else {
                    $installationFreightCost = (int)$quoteAlternative2->getShippingAddress()->getShippingInclTax();
                }


                $this->Proposal2[$i] = array(
                    'SKU' => "Frakt",
                    'Name' => $quoteAlternative2->getShippingAddress()->getShippingDescription(),
                    'Quantity' => "1",
                    'Price' => $installationFreightCost,
                    'RecommendedPrice' => "0",
                    'SpecialPrice' => $specialPriceFreight,
                    'CustomOptions' => $customOptionArrayHolder,
                    //'WarrantyText' => ' ',
                    'WarrantyText' => "",
                    'Url' => "",
                    'CustomDiscount' => "0",
                    'Parent' => true
                );
            }

        } else {
            $this->Proposal2Id = "";
            $this->Proposal2 = array();

        }
        return $this;
    }
}

