<?php
/**
 * Created by PhpStorm.
 * User: danielpettersson
 * Date: 2016-11-10
 * Time: 15:42
 */

class PolarPumpen_CustomerSaveCart_Model_Savecartlog extends Mage_Core_Model_Abstract
{

    public function _construct()
    {
        $this->_init('polarpumpen_customersavecart/savecartlog');
        parent::_construct();
    }

    /**
     * @param $type
     * @param $methodName
     * @param $message
     * @param null $stackTrace
     * @param null $quoteOption1Id
     * @param null $quoteOption2Id
     * @param null $customerEmail
     * @param null $jsonPayload
     * @return $this
     */

    public function log($type, $methodName, $message, $stackTrace = null, $quoteOption1Id = null, $quoteOption2Id = null, $customerEmail = null, $jsonPayload = null) {

        $dt = new DateTime();


        $this->setType($type);
        $this->setMethodName($methodName);
        $this->setMessage($message);
        $this->setStackTrace($stackTrace);
        $this->setQuote1Id($quoteOption1Id);
        $this->setQuote2Id($quoteOption2Id);
        $this->setCustomerEmail($customerEmail);
        $this->setJsonPayload($jsonPayload);
        $this->setDateCreated($dt->format('Y-m-d H:i:s'));

        return $this;
    }

    /**
     * @param $type
     * @param $methodName
     * @param $message
     * @param $quoteOption1Id
     * @param $customerEmail
     * @param null $quoteOption2Id
     * @return $this
     */

    public function logSuccess($type, $methodName, $message, $quoteOption1Id, $customerEmail, $quoteOption2Id = null) {
        $dt = new DateTime();


        $this->setType($type);
        $this->setMethodName($methodName);
        $this->setMessage($message);
        $this->setQuote1Id($quoteOption1Id);
        $this->setCustomerEmail($customerEmail);
        $this->setQuote2Id($quoteOption2Id);
        $this->setDateCreated($dt->format('Y-m-d H:i:s'));

        return $this;

    }

    /**
     * @param $type
     * @param $methodName
     * @param $message
     * @param $stackTrace
     * @return $this
     */

    public function logError($type, $methodName, $message, $stackTrace) {
        $dt = new DateTime();


        $this->setType($type);
        $this->setMethodName($methodName);
        $this->setMessage($message);
        $this->setStackTrace($stackTrace);
        $this->setDateCreated($dt->format('Y-m-d H:i:s'));

        return $this;

    }

    /**
     * @param $type
     * @param $methodName
     * @param $message
     * @param $jsonPayload
     * @return $this
     */

    public function logJsonPayload($type, $methodName, $message, $jsonPayload) {

        $dt = new DateTime();

        $this->setType($type);
        $this->setMethodName($methodName);
        $this->setMessage($message);
        $this->setJsonPayload($jsonPayload);
        $this->setDateCreated($dt->format('Y-m-d H:i:s'));

        return $this;

    }

    /**
     * @param $type
     * @param $methodName
     * @param $message
     * @return $this
     */

    public function logNotice($type, $methodName, $message) {

        $dt = new DateTime();

        $this->setType($type);
        $this->setMethodName($methodName);
        $this->setMessage($message);
        $this->setDateCreated($dt->format('Y-m-d H:i:s'));

        return $this;

    }



}