<?php
/**
 * Created by PhpStorm.
 * User: danielpettersson
 * Date: 2016-11-07
 * Time: 09:36
 */

class PolarPumpen_CustomerSaveCart_Model_Observer extends Varien_Event_Observer
{
    public function afterSavingOrder($observer) {

        $order = $observer->getOrder();

        $customerToQuote = Mage::getModel('polarpumpen_customersavecart/customertoquote')->loadByQuoteId((int)$order->getQuoteId());
        if(!empty($customerToQuote->getCustomerToQuoteId())) {
            $customerToQuote->setWonQuote();

        }

        if($order->getCustomerGroupId() == (int)Mage::getStoreConfig('polarpumpen_customersavecart/general/PolarPumpenSalesCustomerGroupId')) {

            //if($order->getCustomerGroupId() == (int)Mage::getStoreConfig('polarpumpen_customersavecart/general/PolarPumpenSalesCustomerGroupId')) {

            $params = Mage::app()->getRequest()->getParams();
            $customer = Mage::getModel("customer/customer");
            $customer->setWebsiteId(Mage::app()->getWebsite()->getId());


            if($customer->loadByEmail($params['billing']['email'])) {

                if($customer->getId() !== $order->getCustomerId()) {

                    $order->setCustomerId($customer->getId());
                    $order->setCustomerGroupId($customer->getGroupId());
                    $order->setCustomerEmail($params['billing']['email']);
                    $order->setCustomerFirstname($params['billing']['firstname']);
                    $order->setCustomerLastname($params['billing']['lastname']);


                    $billingAddress = $order->getBillingAddress();
                    $billingAddress->setCustomerId($customer->getId());
                    $billingAddress->setEmail($params['billing']['email']);

                    $orderQuote = $order->getQuote();
                    $orderQuote->setCustomerId($customer->getId());
                    $orderQuote->setCustomerGroupId($customer->getGroupId());
                    $orderQuote->setCustomerEmail($params['billing']['email']);

                    $order->setCustomer($customer);

                    //$orderQuote->save();
                    $shippingAddress = $order->getShippingAddress();

                    //sett både billing och shipping mail
                    if (!empty($params['amorderattr']['ship_fname'])) {

                        $shippingAddress->setFirstname($params['amorderattr']['ship_fname']);
                        $shippingAddress->setLastname($params['amorderattr']['ship_sname']);
                        $shippingAddress->setStreet($params['amorderattr']['ship_address']);
                        $shippingAddress->setCity($params['amorderattr']['shipping_city']);
                        $shippingAddress->setPostcode($params['amorderattr']['ship_zipcode']);
                        $shippingAddress->setCountryId($params['billing']['country_id']);
                        $shippingAddress->setTelephone($params['billing']['telephone']);

                    }

                    $shippingAddress->setEmail($params['billing']['email']);
                    $shippingAddress->setCustomerId($customer->getId());
                }

                if($order->save()) {
                    $saveCartLog = Mage::getModel('polarpumpen_customersavecart/savecartlog');
                    $saveCartLog->logNotice("Info", "Event afterSavingOrder()",
                        "Order by sales with ID X and order with ID " . $order->getId() . " is now connected " .
                        "to customer with ID " . $customer->getId());
                    $saveCartLog->save();
                    unset($saveCartLog);

                } else {

                    $saveCartLog = Mage::getModel('polarpumpen_customersavecart/savecartlog');
                    $saveCartLog->logNotice("Error", "Event afterSavingOrder()",
                        "Customer could not assigned to the order ID. Order ID:  " . $order->getId() . "
                      customer Email: " . $params['billing']['email']);
                    $saveCartLog->save();
                    unset($saveCartLog);

                }

            } else {

                $saveCartLog = Mage::getModel('polarpumpen_customersavecart/savecartlog');
                $saveCartLog->logNotice("Error", "Event afterSavingOrder()",
                    "Customer could not been loaded with provided email address and order could not been assigned to the customer ID. Order ID:  " . $order->getId() . "
                      customer Email: " . $params['billing']['email']);
                $saveCartLog->save();
                unset($saveCartLog);

                //Mail to sales about the issue
            }

            //Add to set the customers address
        }
    }

    public function beforeSavingOrder($observer) {

        $order = $observer->getOrder();
        $params = Mage::app()->getRequest()->getParams();

        if($order->getCustomerGroupId() == (int)Mage::getStoreConfig('polarpumpen_customersavecart/general/PolarPumpenSalesCustomerGroupId')) {

            if (!empty($params['billing']['email'])) {


                $sameShippingAsBillng = true;
                //Check f customer exists.
                $customer = Mage::getModel("customer/customer");
                $customer->setWebsiteId(Mage::app()->getWebsite()->getId());
                $customer->loadByEmail($params['billing']['email']);

                if (empty($customer->getId())) {

                    $customerBaseInfo = array(
                        'firstname' => $params['billing']['firstname'],
                        'lastname' => $params['billing']['lastname'],
                        'email' => $params['billing']['email'],

                    );

                    Mage::helper('polarpumpen_customersavecart')->createCustomer($customerBaseInfo);

                }

                $customer->setWebsiteId(Mage::app()->getWebsite()->getId());
                $customer->loadByEmail($params['billing']['email']);

                $billingAddress = array(
                    'firstname' => $params['billing']['firstname'],
                    'lastname' => $params['billing']['lastname'],
                    'street' => array(
                        '0' => $params['billing']['street'][0],
                    ),

                    'city' => $params['billing']['city'],
                    'region_id' => '',
                    'region' => '',
                    'postcode' => $params['billing']['postcode'],
                    'country_id' => $params['billing']['country_id'],
                    'telephone' => $params['billing']['telephone'],

                );

                if (!empty($params['amorderattr']['ship_fname'])) {

                    $sameShippingAsBillng = false;

                    $shippingAddress = array(
                        'firstname' => $params['amorderattr']['ship_fname'],
                        'lastname' => $params['amorderattr']['ship_sname'],
                        'street' => array(
                            '0' => $params['amorderattr']['ship_address'],
                        ),
                        'city' => $params['amorderattr']['shipping_city'],
                        'region_id' => '',
                        'region' => '',
                        'postcode' => $params['amorderattr']['ship_zipcode'],
                        'country_id' => $params['billing']['country_id'],
                        'telephone' => $params['billing']['telephone'],

                    );
                }

                if (!empty($customer->getId())) {
                    if (!empty($billingAddress)) {
                        if ($sameShippingAsBillng) {
                            Mage::helper('polarpumpen_customersavecart')->setCustomerAddress($customer, $billingAddress, "billing", 1);
                        } else {
                            Mage::helper('polarpumpen_customersavecart')->setCustomerAddress($customer, $billingAddress, "billing", 0);
                            if (!empty($shippingAddress)) {
                                Mage::helper('polarpumpen_customersavecart')->setCustomerAddress($customer, $shippingAddress, "shipping", 0);
                            }
                        }
                    }
                } else {

                    $saveCartLog = Mage::getModel('polarpumpen_customersavecart/savecartlog');
                    $saveCartLog->logNotice("Warning", "Event beforeSavingOrder()",
                        "Can't get customer ID, this means that the customer have not been created or another issue have arisen. " .
                        "Adresses have not been added to the customer. Order Id : " . $order->getId() . " Email address: " . $params['billing']['email']);
                    $saveCartLog->save();
                    unset($saveCartLog);

                }

            } else {
                $saveCartLog = Mage::getModel('polarpumpen_customersavecart/savecartlog');
                $saveCartLog->logNotice("Warning", "Event beforeSavingOrder()",
                    "params['billing']['email'] is empty so the order cant be connected to the customer. " .
                    "Order Id : " . $order->getId() . " ");
                $saveCartLog->save();
                unset($saveCartLog);
            }
        }
    }

    public function customerLogout($observer) {

        if(!empty(Mage::getSingleton('customer/session')->getIsSalesBuying())) {
            Mage::getSingleton('customer/session')->unsIsSalesBuying();
        }
    }

    public function onepageCheckoutSuccess($observer) {

        if(!empty(Mage::getSingleton('customer/session')->getIsSalesBuying())) {
            Mage::getSingleton('customer/session')->logout();
        }
    }
}