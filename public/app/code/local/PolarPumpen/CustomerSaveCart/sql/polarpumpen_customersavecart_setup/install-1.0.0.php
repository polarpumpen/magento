<?php

$this->startSetup();

$customerToQuoteTable = $this->getTable('polarpumpen_customersavecart/customertoquote');
$saveCartLogTable = $this->getTable('polarpumpen_customersavecart/savecartlog');

$sql = "

DROP TABLE IF EXISTS `polarpumpen_customer_to_quote`;

DROP TABLE IF EXISTS `" . $this->getTable('polarpumpen_customersavecart/customertoquote') . "`;

CREATE TABLE `" . $this->getTable('polarpumpen_customersavecart/customertoquote') . "` (
  `customer_to_quote_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `quote_id` int(11) DEFAULT NULL,
  `customer_id` int(8) DEFAULT NULL,
  `website_id` smallint(3) DEFAULT NULL,
  `expire_date` datetime DEFAULT NULL,
  `clicks` int(4) NOT NULL DEFAULT '0',
  `won` tinyint(1) NOT NULL DEFAULT '0',
  `quote_alternative` tinyint(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`customer_to_quote_id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `" . $this->getTable('polarpumpen_customersavecart/savecartlog') . "`;


CREATE TABLE `" . $this->getTable('polarpumpen_customersavecart/savecartlog') . "` (
  `savecart_log_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(60) NOT NULL,
  `method_name` varchar(80) DEFAULT NULL,
  `message` text NOT NULL,
  `stack_trace` text,
  `quote1_id` int(11) DEFAULT NULL,
  `quote2_id` int(11) DEFAULT NULL,
  `customer_email` varchar(150) DEFAULT NULL,
  `json_payload` text,
  `date_created` datetime DEFAULT NULL,
  PRIMARY KEY (`savecart_log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




";

$this->run($sql);
$this->endSetup();