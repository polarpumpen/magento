<?php

class PolarPumpen_CustomerSaveCart_IndexController extends Mage_Core_Controller_Front_Action
{

    public function testParseIndexAction() {


       echo Mage::helper('polarpumpen_customersavecart')->hashRequestParameters(1902, 50243, "pettersson.daniel1@gmail.com");
    }

    public function loadCartBasedOnIdAction() {

        try {
            $cartID = $this->getRequest()->getParam('cartID');
            $customerID = $this->getRequest()->getParam('customerID');
            $customerEmail = $this->getRequest()->getParam('customerEmail');
            $checksum = $this->getRequest()->getParam('checksum');
            $isSalesBuying = $this->getRequest()->getParam('isSales');

            if(empty($checksum)) {
                throw new InvalidArgumentException('Checksum is empty: ');
            }

            if(empty($cartID) || !is_int((int)$cartID)) {
                throw new InvalidArgumentException("cartID is either null or not an int. cartID value: ".$cartID);
            }
            if(empty($customerID) || !is_int((int)$customerID)) {
                throw new InvalidArgumentException("customerID is either null or not an int. customerID value: ".$customerID);
            }
            if(empty($customerEmail) || !is_string($customerEmail)) {
                throw new InvalidArgumentException("customerEmail is either null or not an string. customerEmail value: ".$customerEmail);
            }

            if(strtolower($checksum) !== strtolower(Mage::helper('polarpumpen_customersavecart')->hashRequestParameters($customerID, $cartID, $customerEmail))) {
                throw new InvalidArgumentException("Checksum is invalid. Parameters : Customer Id " . $customerID .
                    " Cart Id " . $cartID . " Customer Email " .$customerEmail . " Checksum " . $checksum);

            }


        }catch (Exception $ex) {
            $saveCartLog = Mage::getModel('polarpumpen_customersavecart/savecartlog');
            $saveCartLog->log("Exception","loadCartAction()", $ex->getMessage(), $ex->getTraceAsString());
            $saveCartLog->save();
            unset($saveCartLog);
        } finally {

            if(!empty($ex)) {
                //Mage::getSingleton('core/session')->addError('Något gick tyvärr fel vid inladdning av varukorg. Vänligen kontakta Polarpumpen support');
                $this->_redirect('offer-404');
                return false;
            }

            //Check if it is the sales that are buying, if it is set IsSalesBuying session variable to 1



            if(!empty($isSalesBuying) && strtolower($isSalesBuying) === "true") {
                Mage::getSingleton('customer/session')->setIsSalesBuying(1);
            }

            $this->loadCart($cartID, $customerID, $customerEmail);
            return true;

        }

    }

    private function loadCart($cartID, $customerID, $customerEmail)
    {
        try {

            $customer = Mage::getModel("customer/customer");
            $customer->setWebsiteId(Mage::app()->getWebsite()->getId());
            $customer->load((int)$customerID);

            if(!($customer instanceof Mage_Customer_Model_Customer) || empty($customer->getId())) {
                throw new Exception('Provided customer does not match any customers. Customer id: ' . $customerID);
            }
            //$customerToQuote = Mage::getModel('polarpumpen_customersavecart/customertoquote');

            $customerToQuote = Mage::getModel('polarpumpen_customersavecart/customertoquote')->loadByQuoteId((int)$cartID);

            if(empty($customerToQuote->getCustomerToQuoteId())) {
                throw new Exception('Cant load CustomerToQuote based on cart id. Cart id: ' . $cartID);
            }

            if($customerToQuote->getCustomerId() !== $customer->getId()) {
                throw new Exception('Customer does not match the one on the quote. Customer ID: ' .$customer->getId()
                    . ' Customer Id on quote' . $customerToQuote->getCustomerId() );
            }

            if (!(new DateTime() < new DateTime($customerToQuote->getExpireDate()))) {
                throw new Exception('Offer has Expired. Expiration date ' . $customerToQuote->getExpireDate());
            }

            if (!$customerSelectedCart = Mage::getModel('sales/quote')->load((int)$cartID)) {
                throw new Exception('Customers selected cart could not been loaded.
                                    Cart id tried to load ' . $cartID);
            }

            //Delete customers previous carts
            if (!Mage::helper('polarpumpen_customersavecart')->deleteCustomersQuote($customer, $cartID)) {
                throw new Exception('Could not delete customers previous carts. Customer ID: ' . $customer->getId()
                    . ' Cart Id: ' . $cartID);
            }

            $customerSelectedCart->setCustomerId($customer->getId());
            $customerSelectedCart->setIsActive(1);
            $customerSelectedCart->save();

            $customerSelectedCart->assignCustomerWithAddressChange($customer, $customerSelectedCart->getBillingAddress(),
                $customerSelectedCart->getShippingAddress());

            if(!$customerSelectedCart->save()) {
                throw new Exception('Could not assign quote to the customer: ' . $customer->getId()
                    . ' customerSelectedCart Id: ' . $customerSelectedCart->getId());
            }

            Mage::getSingleton('checkout/session')->setQuoteId($customerSelectedCart->getId());

            if(!Mage::getSingleton('customer/session')->loginById($customer->getId())) {
                throw new Exception('Could not login the customer automatically: ' . $customer->getId());
            }

        } catch (Exception $ex) {

            $saveCartLog = Mage::getModel('polarpumpen_customersavecart/savecartlog');
            $saveCartLog->logError("Exception","loadCartBasedOnIdAction", $ex->getMessage(), $ex->getTraceAsString());
            $saveCartLog->save();
            unset($saveCartLog);

        } finally {

            if(!empty($ex)) {
                //Mage::getSingleton('core/session')->addError('Något gick tyvärr fel vid inladdning av varukorg. Vänligen kontakta Polarpumpen support');
                $this->_redirect('offer-404');
                return false;
            }

            Mage::getSingleton('core/session')->addSuccess('Din varukorg laddades in utan problem');

            $this->_redirect('firecheckout/index/index');

            $saveCartLog = Mage::getModel('polarpumpen_customersavecart/savecartlog');
            $saveCartLog->log("Success","loadCartAction()", "Customers cart was loaded without problem. Customer Id " . $customerID
                . " Cart ID " . $customerSelectedCart->getId() . " Email " . $customerEmail);
            $saveCartLog->save();
            unset($saveCartLog);

            //If check
            $customerToQuote->setClicked();

            return true;
        }

    }

    private function postQuotes(Mage_Customer_Model_Customer $customer, Mage_Sales_Model_Quote $quoteAlternative1, Mage_Sales_Model_Quote $quoteAlternative2 = NULL, $additonalFormData = NULL, $customDiscount = NULL)
    {
        try {
            if(!($customer instanceof Mage_Customer_Model_Customer) || empty($customer->getId())) {
                throw new Exception("Customer object is null. Input parameter: " . $customer);
            }
            if(!($quoteAlternative1 instanceof Mage_Sales_Model_Quote) || empty($quoteAlternative1)) {
                throw new InvalidArgumentException("Quote option 1 is empty or of wrong type and it have not been parsed to a JSON object.
                        Provided input quoteAlternative1: " . $quoteAlternative1);
            }
            if(!empty($quoteAlternative2) && $quoteAlternative2 instanceof Mage_Sales_Model_Quote) {
                $polarQuoteEntity = Mage::getModel('polarpumpen_customersavecart/polarquoteentity')->createPolarQuoteEntity($customer, $quoteAlternative1, $quoteAlternative2, $additonalFormData, $customDiscount);
            } else {
                $polarQuoteEntity = Mage::getModel('polarpumpen_customersavecart/polarquoteentity')->createPolarQuoteEntity($customer, $quoteAlternative1, NULL, $additonalFormData, $customDiscount);
            }

            if(!($polarQuoteEntity instanceof PolarPumpen_CustomerSaveCart_Model_Polarquoteentity) ||
                empty($polarQuoteEntity)) {
                throw new Exception("PolarQuote object could not be created and have not been parsed to JSON.
                            Input QuoteAlternative1: " . $quoteAlternative1 . " Input PolarQuoteObject" . $polarQuoteEntity);
            }

            if(!$jsonEncodedQuote = json_encode($polarQuoteEntity)) {
                throw new Exception("PolarQuoteEntity object could not been parsed into a JSON object.
                                Input polarQuoteEntity: " . $polarQuoteEntity);
            }
            $saveCartLog = Mage::getModel('polarpumpen_customersavecart/savecartlog');
            $saveCartLog->logJsonPayload("Success", "JSON Payload",
                "PolarQuoteEntity object JSON was sucessfully parsed into JSON object", $jsonEncodedQuote);

            $saveCartLog->save();
            unset($saveCartLog);


            $ch = curl_init(Mage::getStoreConfig('polarpumpen_customersavecart/general/PolarPumpenCRMUrl'));

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type:  application/json"));
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonEncodedQuote);
            curl_setopt($ch, CURLOPT_FAILONERROR,true);

            // execute!
            curl_exec($ch);


            if(curl_error($ch)) {
                throw new Exception("Error when posting to CRM. Quote 1 id: ". $quoteAlternative1->getId() .
                    "Curl Error ". curl_error($ch));
            }
        } catch (Exception $ex) {

            Mage::getSingleton('core/session')->addError($ex->getMessage());
            $saveCartLog = Mage::getModel('polarpumpen_customersavecart/savecartlog');
            $saveCartLog->logError("Exception", "postQuotes", $ex->getMessage(), $ex->getTraceAsString());
            $saveCartLog->save();
            unset($saveCartLog);

        } finally {
            if(!empty($ch)) {
                curl_close($ch);
            }

            if(!empty($ex)) {
                return false;
            }

            //Log Success

            $saveCartLog = Mage::getModel('polarpumpen_customersavecart/savecartlog');
            if(!empty($quoteAlternative2)) {
                $saveCartLog->logSuccess("Success", "postQuotes", "Everything went well and quotes have been posted to CRM",
                    $quoteAlternative1->getId(), $customer->getEmail(), $quoteAlternative2->getId());
            }else {
                $saveCartLog->logSuccess("Success", "postQuotes", "Everything went well and quote have been posted to CRM",
                    $quoteAlternative1->getId(), $customer->getEmail(), null);
            }
            $saveCartLog->save();
            unset($saveCartLog);

            return true;
        }

    }

    public function createQuotesFromCheckoutAction()
    {
        try {
            if(!Mage::helper('polarpumpen_customersavecart')->checkIfSalesAreLoggedIn(Mage::getStoreConfig('polarpumpen_customersavecart/general/PolarPumpenSalesCustomerGroupId'))) {
                throw new Exception('Either the module CustomerSaveCart is not activated or an unauthrosied user tried to use the function');
            }

            /* Initialize variables */

            $customerBaseInfo = null;
            $billingAddress = null;
            $shippingAddress = null;
            $additionalOfferFormData = array();
            $newQuote2 = null;

            $offerExpireDate = Mage::helper('polarpumpen_customersavecart')->addDaysToDate(Mage::getStoreConfig('polarpumpen_customersavecart/general/PolarPumpenOfferExpireDays'));


            $session = Mage::getSingleton('checkout/session');
            $quote_id = $session->getQuoteId();

            /* Check if there is a valid quote in current session */

            if (empty($currentQuote = Mage::getModel('sales/quote')->load($quote_id))) {
                //$errors[] = "Quote for sessions could not be loaded";
                throw new Exception('Quote for sessions could not be loaded. Input Quote id from session: ' . $quote_id);
            }

            if((int)$currentQuote->getItemsQty() <= 0) {
                throw new Exception("Current quote have no items in cart. Quote id : " . $currentQuote->getId() .
                    " Quote item qty: " . (int)$currentQuote->getItemsQty());
            }

            /* Get post request from the checkout form */

            $params = $this->getRequest()->getParams();

            $additionalOfferFormData["offerClassification"] = $params["offerClassification"];
            $additionalOfferFormData["offerType"] = $params["offerType"];
            $additionalOfferFormData["heatingArea"] = $params["heatingArea"];
            $additionalOfferFormData["geographicArea"] = $params["geographicArea"];
            $additionalOfferFormData["other"] = $params["other"];
            $additionalOfferFormData["questionAndAnswer"] = $params["questionAndAnswer"];





            if (empty($params['billing']['email'] || $params['billing']['telephone'] )) {
                throw new Exception('Billing params is empty, cant proceed with offer creation.');
            }


            /* Set the current quote data with provided customer data */

            $currentQuote->setCustomerFirstname($params['billing']['firstname']);
            $currentQuote->setCustomerLastname($params['billing']['lastname']);
            $currentQuote->setFirecheckoutCustomerComment($params['order-comment']);
            $currentQuote->setTmField1($params['amorderattr']['persnr_orgnr']);
            $currentQuote->setCustomerEmail($params['billing']['email']);

            $currentQuote->setCustomerId(NULL);
            $currentQuote->setCustomerGroupId(NULL);
            $currentQuote->save();


            /* Set billing address for the quote. Table : sales_flat_quote_address */

            $billingQuote = $currentQuote->getBillingAddress();
            $billingQuote->setEmail($params['billing']['email']);

            $billingQuote->setFirstname($params['billing']['firstname']);
            $billingQuote->setLastname($params['billing']['lastname']);
            $billingQuote->setTelephone($params['billing']['telephone']);
            $billingQuote->setStreet($params['billing']['street'][0]);
            $billingQuote->setPostcode($params['billing']['postcode']);
            $billingQuote->setCity($params['billing']['city']);
            $billingQuote->setCountryId($params['billing']['country_id']);
            $billingQuote->setSaveInAddressBook($params['billing']['save_in_address_book']);
            $billingQuote->save();

            /* If not shipping firstname is empty, we know that the shipping address is different from the delivery.
            This is a workaround to solve the issue with  firecheckout hardcoded that shipping always is the same as billing.

            Table : sales_flat_quote_address */

            if (!empty($params['amorderattr']['ship_fname'])) {
                //User is another shipping adress, get that data
                $shippingQuote = $currentQuote->getShippingAddress();

                $shippingQuote->setEmail($params['billing']['email']);
                $shippingQuote->setFirstname($params['amorderattr']['ship_fname']);
                $shippingQuote->setLastname($params['amorderattr']['ship_sname']);
                $shippingQuote->setTelephone($params['billing']['telephone']);
                $shippingQuote->setStreet($params['amorderattr']['ship_address']);
                $shippingQuote->setPostcode($params['amorderattr']['ship_zipcode']);
                $shippingQuote->setCity($params['amorderattr']['shipping_city']);
                $shippingQuote->setCountryId($params['billing']['country_id']);
                $shippingQuote->setSaveInAddressBook($params['billing']['save_in_address_book']);
                $shippingQuote->setSameAsBilling(0);
                $shippingQuote->save();

            } else {
                /* Same shipping as billing address. Table sales_flat_quote_address */

                $shippingQuote = $currentQuote->getShippingAddress();

                $shippingQuote->setEmail($params['billing']['email']);
                $shippingQuote->setFirstname($params['billing']['firstname']);
                $shippingQuote->setLastname($params['billing']['lastname']);
                $shippingQuote->setTelephone($params['billing']['telephone']);
                $shippingQuote->setStreet($params['billing']['street'][0]);
                $shippingQuote->setPostcode($params['billing']['postcode']);
                $shippingQuote->setCity($params['billing']['city']);
                $shippingQuote->setCountryId($params['billing']['country_id']);
                $shippingQuote->setSaveInAddressBook($params['billing']['save_in_address_book']);
                $shippingQuote->setSameAsBilling(1);
                $shippingQuote->save();
            }

            /* Copy the current quote to a new quote */
            $newQuote = Mage::helper('polarpumpen_customersavecart')->createNewQuoteFromQuote($currentQuote);

            if(empty($newQuote)) {
                throw new Exception("newQuote could not been created. Trying to create quote from current quote. currentQuote ID:  " . $currentQuote->getId());
            }

            /* If sales have marked option 2, we create a new copy of the current quote to hold option 2. */
            if(!empty($params['option2'])) {
                $newQuote2 = Mage::helper('polarpumpen_customersavecart')->createNewQuoteFromQuote($currentQuote);
                //add check here as well.
            }
            /* This is a dumb workaround to update quote with correct items. So the quote needs to be loaded again.  */
            if (!empty($params['option1'])) {
                $option1ShoppingCart = Mage::getModel('sales/quote')->load($newQuote->getId());

                /* Remove the products which is not marked as option 1 */
                if (Mage::helper('polarpumpen_customersavecart')->removeProductsFromQuote($option1ShoppingCart, $params['option1'])) {
                    unset($newQuote);
                } else {
                    throw new Exception('Error when creating option 1 cart');
                }
            }
            /* Check if $newquote2 is not empty, then we know it has option 2 have been selected */
            if (!empty($newQuote2)) {

                $option2ShoppingCart = Mage::getModel('sales/quote')->load($newQuote2->getId());

                /* Remove products to create option 2 cart */

                if (Mage::helper('polarpumpen_customersavecart')->removeProductsFromQuote($option2ShoppingCart, $params['option2'])) {
                    unset($newQuote2);
                } else {
                    throw new Exception('Error when creating option 2 cart');
                }

            }

            /* Create Billing addresse */

            $billingAddress = array(
                'firstname' => $params['billing']['firstname'],
                'lastname' => $params['billing']['lastname'],
                'street' => array(
                    '0' => $params['billing']['street'][0],
                ),

                'city' => $params['billing']['city'],
                'region_id' => '',
                'region' => '',
                'postcode' => $params['billing']['postcode'],
                'country_id' => $params['billing']['country_id'],
                'telephone' => $params['billing']['telephone'],

            );

            if (!empty($params['amorderattr']['ship_fname'])) {

                $shippingAddress = array(
                    'firstname' => $params['amorderattr']['ship_fname'],
                    'lastname' => $params['amorderattr']['ship_sname'],
                    'street' => array(
                        '0' => $params['amorderattr']['ship_address'],
                    ),
                    'city' => $params['amorderattr']['shipping_city'],
                    'region_id' => '',
                    'region' => '',
                    'postcode' => $params['amorderattr']['ship_zipcode'],
                    'country_id' => $params['billing']['country_id'],
                    'telephone' => $params['billing']['telephone'],

                );
            }

            //Get customer object ( or create it)

            $customer = Mage::getModel("customer/customer");
            $customer->setWebsiteId(Mage::app()->getWebsite()->getId());
            $customer->loadByEmail($params['billing']['email']);

            if (empty($customer->getId())) {

                $customerBaseInfo = array(
                    'firstname' => $params['billing']['firstname'],
                    'lastname' => $params['billing']['lastname'],
                    'email' => $params['billing']['email'],

                );

                if (!Mage::helper('polarpumpen_customersavecart')->createCustomer($customerBaseInfo)) {
                    throw new Exception('Customer could not been saved. Provided input 
                                customerBaseInfo type: ' . gettype($customerBaseInfo));
                }

                $customer->setWebsiteId(Mage::app()->getWebsite()->getId());
                $customer->loadByEmail($params['billing']['email']);

            } else {
                //Notice logging that customer is about to be created
            }

            if (!($customer instanceof Mage_Customer_Model_Customer) || empty($customer->getId())) {
                throw new InvalidArgumentException("Customer object is of wrong type. 
                            Could not load customer. Input customer type: " . gettype($customer));
            }
            //Sett billing and shipping address
            if(!empty($shippingAddress)) {
                Mage::helper('polarpumpen_customersavecart')->setCustomerAddress($customer, $billingAddress, "billing", 0);
                Mage::helper('polarpumpen_customersavecart')->setCustomerAddress($customer, $shippingAddress, "shipping", 0);

            } else {
                Mage::helper('polarpumpen_customersavecart')->setCustomerAddress($customer, $billingAddress, "billing");
            }


            $customerToQuote = Mage::getModel('polarpumpen_customersavecart/customertoquote');

            if (!empty($newQuote) && empty($option1ShoppingCart)) {
                $customerToQuote->createCustomerToQuote($customer, $newQuote, $offerExpireDate);
            } else {
                $customerToQuote->createCustomerToQuote($customer, $option1ShoppingCart, $offerExpireDate);
            }

            if (!$customerToQuote->save()) {
                throw new Exception('Could not save CustomerToQuote object. Customer ID: '
                    . $customer->getId());
            }

            if (!empty($option2ShoppingCart)) {
                $customerToQuote2 = Mage::getModel('polarpumpen_customersavecart/customertoquote');
                $customerToQuote2->createCustomerToQuote($customer, $option2ShoppingCart, $offerExpireDate, 2);
                if (!$customerToQuote2->save()) {
                    throw new Exception('Could not save second CustomerToQuote object. Customer ID: '
                        . $customer->getId() . ", Option2ShoppingCart type: " . gettype($option2ShoppingCart));
                }
            }


        } catch (Exception $ex) {

            Mage::getSingleton('core/session')->addError($ex->getMessage());
            $saveCartLog = Mage::getModel('polarpumpen_customersavecart/savecartlog');
            $saveCartLog->logError("Exception message", "createQuotesFromCheckoutAction",
                $ex->getMessage(), $ex->getTraceAsString());
            $saveCartLog->save();
            unset($saveCartLog);

        } finally {
            if(!empty($ex)) {
                //Mage::getSingleton('core/session')->addError('Unfortunately something went wrong. Please contact Polarpumpen IT');
                $this->_redirect('offer-404');
                return false;
            }
        }

        try {
            if(!empty($option1ShoppingCart) && !empty($option2ShoppingCart)) {
                if (!$this->postQuotes($customer, $option1ShoppingCart, $option2ShoppingCart, $additionalOfferFormData, $params["customDiscount"])) {
                    throw new Exception('Could not post quotes alternatives to postQuotes method. 
                        Input data: Customer : ' . gettype($customer) . ", option1ShoppingCart type: " . gettype($option1ShoppingCart)
                        . ", option2ShoppingCart type ". gettype($option2ShoppingCart)  .
                        ", additionalOfferFormData type " . gettype($additionalOfferFormData));
                }


            } elseif(!empty($option1ShoppingCart)) {

                if (!$this->postQuotes($customer, $option1ShoppingCart, null, $additionalOfferFormData, $params["customDiscount"])) {
                    throw new Exception('Could not post option1ShoppingCart1  to postQuotes method. 
                        Input data: Customer : ' . gettype($customer) . ", option1ShoppingCart type: " . gettype($option1ShoppingCart)
                        . ", additionalOfferFormData type " . gettype($additionalOfferFormData));
                }

            } elseif(!empty($option2ShoppingCart)) {

                if (!$this->postQuotes($customer, $option2ShoppingCart, null, $additionalOfferFormData, $params["customDiscount"])) {
                    throw new Exception('Could not post option1ShoppingCart1  to postQuotes method. 
                        Input data: Customer : ' . gettype($customer) . ", option1ShoppingCart type: " . gettype($option2ShoppingCart)
                        . ", additionalOfferFormData type " . gettype($additionalOfferFormData));
                }
            } else {
                if (!$this->postQuotes($customer, $newQuote, NULL, $additionalOfferFormData, $params["customDiscount"])) {
                    throw new Exception('Could not post newQuote to postQuotes method. 
                        Input data: Customer : ' . gettype($customer) . ", newQuote type: " . gettype($newQuote)
                        . ", additionalOfferFormData type " . gettype($additionalOfferFormData));

                }

            }

        } catch (Exception $ex) {
            $saveCartLog = Mage::getModel('polarpumpen_customersavecart/savecartlog');
            $saveCartLog->logError("Exception message", "createQuotesFromCheckoutAction",
                $ex->getMessage(), $ex->getTraceAsString());

            $saveCartLog->save();
            unset($saveCartLog);

        } finally {
            if(!empty($ex)) {
                Mage::getSingleton('core/session')->addNotice('Något gick tyvärr fel, vänligen kontakta Polarpumpen IT');
                $this->_redirect('offer-403');
                return false;
            }

            $checkout = Mage::getSingleton("checkout/session");
            $checkout->setData('amasty_order_attributes',
                array('ship_fname'=>null,
                    'ship_sname'=>null,
                    'ship_address'=>null,
                    'ship_zipcode'=>null,
                    'shipping_city'=>null));

            //Clear cart
            Mage::getSingleton('checkout/cart')->truncate();
            Mage::getSingleton('checkout/cart')->save();

            Mage::getSingleton('core/session')->addSuccess("Offert skickades till CRM utan problem. En kopia har skickats till offert@polarpumpen.se.");
            $this->_redirect('/');

            return true;
        }

    }

}