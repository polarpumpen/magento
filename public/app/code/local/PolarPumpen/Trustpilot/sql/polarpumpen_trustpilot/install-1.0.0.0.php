<?php

/*
 * @var $installer Mage_Core_Model_Resource_Setup 
 */
$this->startSetup();

$sql = "

CREATE TABLE `" . $this->getTable('polarpumpen_trustpilot/user') . "` (
   `id` int(10) NOT NULL AUTO_INCREMENT,
   `name` varchar(50) NOT NULL,
   `key` varchar(70) NOT NULL,
   `secret` varchar(70) NOT NULL,
   `username` varchar(70) NOT NULL,
   `password` varchar(70) NOT NULL,
   PRIMARY KEY (`id`),
   UNIQUE KEY `name_UNIQUE` (`name`)
 ) ENGINE=InnoDB;

CREATE TABLE `" . $this->getTable('polarpumpen_trustpilot/business') . "` (
   `id` int(10) NOT NULL AUTO_INCREMENT,
   `user_id` int(10) NOT NULL,
   `name` varchar(50) NOT NULL,
   `business_unit_id` varchar(70) NOT NULL,
   `review_invitation_store_id` smallint(5) DEFAULT NULL,
   `invitation_template` varchar(24) DEFAULT NULL,
   `invitation_locale` varchar(12) DEFAULT NULL,
   `invitation_sender_email` varchar(60) DEFAULT 'noreply.invitations@trustpilot.com',
   `invitation_sender_reply_to` varchar(60) DEFAULT 'noreply.invitations@trustpilot.com',
   `invitation_sender_name` varchar(45) DEFAULT NULL,
   `invitation_preferred_send_add_time_d` int(10) unsigned NOT NULL DEFAULT '90',
   `invitation_preferred_send_add_time_t` varchar(8) DEFAULT '10:00:00',
   PRIMARY KEY (`id`),
   UNIQUE KEY `name_UNIQUE` (`name`),
   KEY `ix_business_user_id` (`user_id`),
   CONSTRAINT `fk_business_user_id` FOREIGN KEY (`user_id`) REFERENCES `" . $this->getTable('polarpumpen_trustpilot/user') . "` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
 ) ENGINE=InnoDB;

CREATE TABLE `" . $this->getTable('polarpumpen_trustpilot/log') . "` (
   `id` int(10) NOT NULL AUTO_INCREMENT,
   `user_id` int(10) DEFAULT NULL,
   `business_id` int(10) DEFAULT NULL,
   `increment_id` varchar(50) DEFAULT NULL,
   `store_id` smallint(5) UNSIGNED DEFAULT NULL,
   `text` varchar(3400) NOT NULL,
   `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
   PRIMARY KEY (`id`),
   KEY `fk_logs_user_id` (`user_id`),
   KEY `fk_logs_business_id` (`business_id`),
   CONSTRAINT `fk_log_user_id` FOREIGN KEY (`user_id`) REFERENCES `" . $this->getTable('polarpumpen_trustpilot/user') . "` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
   CONSTRAINT `fk_log_business_id` FOREIGN KEY (`business_id`) REFERENCES `" . $this->getTable('polarpumpen_trustpilot/business') . "` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
 ) ENGINE=InnoDB;

";

$this->run($sql);
$this->endSetup();
