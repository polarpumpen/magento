<?php

class PolarPumpen_Trustpilot_Model_User extends Mage_Core_Model_Abstract {

    protected $_eventPrefix = 'polarpumpen_trustpilot_user';
    protected $_eventObject = 'user';
    protected $http;

    /** @var string */
    protected $accessToken;

    /** @var string */
    protected $clientId;

    /** @var string */
    protected $refreshToken;

    /** @var int */
    protected $accessTokenExpiresIn;

    /**
     * @param string $val
     * @return string 
     */
    function name($val = null) {
        if ($val) $this->_data[__FUNCTION__] = $val;
        return $this->_data[__FUNCTION__];
    }

    /**
     * @param string $val
     * @return string 
     */
    function key($val = null) {
        if ($val) $this->_data[__FUNCTION__] = $val;
        return $this->_data[__FUNCTION__];
    }

    /**
     * @param string $val
     * @return string 
     */
    function secret($val = null) {
        if ($val) $this->_data[__FUNCTION__] = $val;
        return $this->_data[__FUNCTION__];
    }

    /**
     * @param string $val
     * @return string 
     */
    function username($val = null) {
        if ($val) $this->_data[__FUNCTION__] = $val;
        return $this->_data[__FUNCTION__];
    }

    /**
     * @param string $val
     * @return string 
     */
    function password($val = null) {
        if ($val) $this->_data[__FUNCTION__] = $val;
        return $this->_data[__FUNCTION__];
    }

    protected function _construct() {
        $this->_init('polarpumpen_trustpilot/user');
        $this->http = Mage::helper('polarpumpen_trustpilot/Http');
    }

    function login() {
        $url = $this->getBasUrl('oauth/oauth-business-users-for-applications/accesstoken');

        $postfields = array(
            'grant_type' => 'password',
            'username' => $this->username(),
            'password' => $this->password()
        );

        $responseJs = json_decode($this->http->post($url, $postfields, $this->getHeaders())->body, true);

        $this->accessToken = $responseJs['access_token'];
        $this->refreshToken = $responseJs['refresh_token'];
        $this->clientId = $responseJs['client_id'];
        $this->accessTokenExpiresIn = $responseJs['expires_in'];
    }

    function refresh() {
        $url = $this->getBasUrl('oauth/oauth-business-users-for-applications/refresh');

        $postfields = array(
            'grant_type' => 'refresh_token',
            'refresh_token' => $this->refreshToken
        );

        $responseJs = json_decode($this->http->post($url, $postfields, $this->getHeaders())->body, true);

        $this->accessToken = $$responseJs['access_token'];
        $this->refreshToken = $$responseJs['refresh_token'];
        $this->clientId = $$responseJs['client_id'];
        $this->accessTokenExpiresIn = $$responseJs['expires_in'];
    }

    function loginTest() {
        if ($this->accessToken === null) {
            $this->login();
        }
    }

    function getBasUrl($addUrl = '', $extra = '') {
        return 'https://' . $extra . 'api.trustpilot.com/v1/' . $addUrl;
    }

    function getBasUrlAndaccessToken($addUrl, $extra = '') {
        return $this->getBasUrl($addUrl, $extra) . '?token=' . $this->accessToken;
    }

    function getAuthorizationHeader() { // avänds ej 
        return 'Authorization: Basic ' . base64_encode($this->clientId . ':' . $this->user->secret());
    }

    function getHeaders() {
        return array(
            'Authorization: Basic ' . base64_encode($this->key() . ':' . $this->secret()),
            'Content-Type: application/x-www-form-urlencoded'
        );
    }

    function bearerHeader() {
        return 'Authorization: Bearer ' . $this->accessToken;
    }

    protected function getUrlPadd() {
        return '?apikey=' . base64_encode($this->key());
    }

    function __toString() {
        return $this->name;
    }

}
