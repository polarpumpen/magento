<?php

class PolarPumpen_Trustpilot_Model_Business extends Mage_Core_Model_Abstract {

    protected $_eventPrefix = 'polarpumpen_trustpilot_business';
    protected $_eventObject = 'business';
    protected $http;

    /**
     * @param string $val
     * @return string 
     */
    function name($val = null) {
        if ($val) $this->_data[__FUNCTION__] = $val;
        return $this->_data[__FUNCTION__];
    }

    /**
     * @param string $val
     * @return string 
     */
    function businessUnitID($val = null) {
        $name = 'business_unit_id';
        if ($val) $this->_data[$name] = $val;
        return $this->_data[$name];
    }

    /**
     * @param int $val
     * @return int 
     */
    function reviewInvitationStoreId($val = null) {
        $name = 'review_invitation_store_id';
        if ($val) $this->_data[$name] = $val;
        return intval($this->_data[$name]);
    }

    /**
     * @param PolarPumpen_Trustpilot_Model_User $val
     * @return PolarPumpen_Trustpilot_Model_User 
     */
    function user() {
        $user = Mage::getModel('polarpumpen_trustpilot/User');
        $user->load($this->_data['user_id']);
        return $user;
    }

    /**
     * @return string 
     */
    function getTemplateName() {
        if ($this->invitationTemplate === null) return "not set";
        $api = Mage::helper('polarpumpen_trustpilot/trustpilotApi'); /* @var $api PolarPumpen_Trustpilot_Helper_TrustpilotApi */
        $api->initViaBusinessId($this->id);
        $list = $api->getListOfInvitationTemplates();
        foreach ($list['templates'] as $value)
                if ($this->invitationTemplate == $value['id']) return$value['name'];
        return "not found !";
    }

    protected function _construct() {
        $this->_init('polarpumpen_trustpilot/business');
        $this->http = Mage::helper('polarpumpen_trustpilot/Http');
    }

    protected function _beforeSave() {
        if ($this->_data['review_invitation_store_id'] == '') $this->_data['review_invitation_store_id'] = null;
    }

    function getListOfInvitationTemplates($user) {
        $user->loginTest();
        $url = $user->getBasUrlAndaccessToken('private/business-units/' . $this->businessUnitID() . '/templates', 'invitations-');
        $responseJs = $this->http->jsGet($url);
        return $responseJs;
    }

    function createProductReviewInvitationLink($reviewInvitationLinkDataArr, $user) {
        $user->loginTest();
        if (!is_array($reviewInvitationLinkDataArr)) throw new Exception('reviewInvitationLinkDataArr is not a array!');
        $url = $user->getBasUrlAndaccessToken('private/product-reviews/business-units/' . $this->businessUnitID() . '/invitation-links');
        $responseJs = $this->http->jsPost($url, $reviewInvitationLinkDataArr);
        return $responseJs;
    }

    function createNewInvitation($dataToSendJs, $user) {
        $user->loginTest();
        $url = $user->getBasUrlAndaccessToken('private/business-units/' . $this->businessUnitID() . '/invitations', 'invitations-');
        $responseJs = $this->http->jsPost($url, $dataToSendJs);
        return $responseJs;
    }

    function __toString() {
        return $this->name;
    }

}
