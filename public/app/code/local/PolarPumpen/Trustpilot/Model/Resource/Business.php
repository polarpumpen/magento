<?php

class PolarPumpen_Trustpilot_Model_Resource_Business extends Mage_Core_Model_Resource_Db_Abstract {

    protected function _construct() {
        $this->_init('polarpumpen_trustpilot/business', 'id');
    }

}
