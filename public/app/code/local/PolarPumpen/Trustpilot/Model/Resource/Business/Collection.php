<?php

class PolarPumpen_Trustpilot_Model_Resource_Business_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract {

    public function _construct() {
        $this->_init('polarpumpen_trustpilot/business');
    }

}