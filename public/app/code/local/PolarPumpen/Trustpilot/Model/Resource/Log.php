<?php

class PolarPumpen_Trustpilot_Model_Resource_Log extends Mage_Core_Model_Resource_Db_Abstract {

    protected function _construct() {
        $this->_init('polarpumpen_trustpilot/log', 'id');
    }

}
