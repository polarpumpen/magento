<?php

class PolarPumpen_Trustpilot_Model_Observer {

    /** @var PolarPumpen_Trustpilot_Helper_TrustpilotApi */
    private $api;
    private $order;
    private $storeId;

    function salesOrderPlaceAfter($observer) {
        try {
            $this->order = $observer->getEvent()->getData('order'); /* $order $api Mage_ */
            $this->storeId = $this->order->StoreId;

            $this->api = Mage::helper('polarpumpen_trustpilot/trustpilotApi'); /* @var $api PolarPumpen_Trustpilot_Helper_TrustpilotApi */
            if ($this->api->initViaStoreId($this->storeId) == false) throw new Exception("no Business");

            $invitationLinkData = $this->createProductReviewInvitationLink();
            $reviewLinkId = $invitationLinkData['reviewLinkId'];
            $reviewUrl = $invitationLinkData['reviewUrl'];

            $responseJs = $this->createNewInvitation($reviewUrl);

            $this->api->log($responseJs, $this->order, $this->storeId);
        } catch (Exception $ex) {
            if ($this->api !== null) $this->api->log($ex, $this->order, $this->storeId);
        }
    }

    private function createProductReviewInvitationLink() {
        $email = $this->order->CustomerEmail;
        $name = ucfirst($this->order->CustomerFirstname) . " " . ucfirst($this->order->CustomerLastname);
        $referenceId = $this->order->incrementId;

        $redirectUri = "https://www.polarpumpen.se"; // <----------------------------------------------------------------------------------------------------------------

        $dataReviewInvitationLinkData = $this->api->getNewReviewInvitationLinkDataArr($email, $name, $referenceId, $redirectUri);
        foreach ($this->order->getAllVisibleItems() as $item) {
            $productId = $item->getProductId();
            $product = Mage::getModel('catalog/product')->load($productId);
            
            $productUrl = $product->getProductUrl();
            $imageUrl = (string)Mage::helper('catalog/image')->init($product, 'image')->resize(150);
            $name = $product->getName();
            $sku = $product->getSku();
            $brand = $product->getAttributeText('td_marke');

            $this->api->addProductToReviewInvitationLinkDataArr($dataReviewInvitationLinkData, $productUrl, $imageUrl, $name, $sku, $brand);
        }
        return $this->api->createProductReviewInvitationLink($dataReviewInvitationLinkData);
    }

    private function createNewInvitation($reviewUrl) {
        $recipientEmail = $this->order->CustomerEmail;
        $recipientName = ucfirst($this->order->CustomerFirstname) . " " . ucfirst($this->order->CustomerLastname);
        $referenceId = $this->order->incrementId;
        $redirectUri = $reviewUrl;
        $createNewInvitationDataArr = $this->api->getCreateNewInvitationArr($recipientEmail, $recipientName, $referenceId, $redirectUri);
        return $this->api->createNewInvitation($createNewInvitationDataArr);
    }

}
