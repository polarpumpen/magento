<?php

class PolarPumpen_Trustpilot_Model_log extends Mage_Core_Model_Abstract {

    protected $_eventPrefix = 'polarpumpen_trustpilot_log';
    protected $_eventObject = 'log';

    protected function _construct() {
        $this->_init('polarpumpen_trustpilot/log');
    }

    function __toString() {
        return 'text ' . nl2br($this->text) . '<br>' .
                'userId ' . (($this->userId === null) ? 'null' : $this->userId) . '<br>' .
                'businessID ' . (($this->businessID === null) ? 'null' : $this->businessID) . '<br>' .
                'incrementId ' . (($this->incrementId === null) ? 'null' : $this->incrementId) . '<br>' .
                'storeId ' . (($this->storeId === null) ? 'null' : $this->storeId);
    }

}
