<?php

class PolarPumpen_Trustpilot_Block_Adminhtml_User_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    public function __construct() {
        parent::__construct();
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection() {
        $this->setCollection(Mage::getModel("polarpumpen_trustpilot/user")->getCollection());
        return parent::_prepareCollection();
    }

    protected function _prepareColumns() {

        $this->addColumn('id', array(
            'header' => 'ID',
            'align' => 'right',
            'width' => '50px',
            'index' => 'id',
        ));

        $this->addColumn('name', array(
            'header' => 'Name',
            'align' => 'left',
            'index' => 'name',
        ));

        $this->addColumn('action', array(
            'header' => 'Action',
            'width' => '100',
            'type' => 'action',
            'getter' => 'getId',
            'actions' => array(
                array(
                    'caption' => 'Show',
                    'url' => array('base' => '*/*/show'),
                    'field' => 'id'
                )),
            'filter' => false,
            'sortable' => false,
            'index' => 'stores',
            'is_system' => true,
        ));

        return parent::_prepareColumns();
    }

    public function getRowUrl($row) {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

}
