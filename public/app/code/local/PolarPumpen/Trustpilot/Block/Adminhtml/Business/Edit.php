<?php

class PolarPumpen_Trustpilot_Block_Adminhtml_Business_Edit extends Mage_Adminhtml_Block_Widget_Form_Container {

    function __construct() {
        parent::__construct();
        $this->_blockGroup = 'polarpumpen_trustpilot';
        $this->_controller = 'Adminhtml_Business';
        $this->_mode = 'edit';
    }

    function getHeaderText() {
        return 'Edit Business Unit Id ( ' . Mage::registry("PolarPumpen_trustpilot_model")->name . ' )';
    }

    function getBackUrl() {
        return $this->getUrl('*/*/edit/', array('id' => Mage::registry("PolarPumpen_trustpilot_model")->userId));
    }

    function getDeleteUrl() {
        return $this->getUrl('*/*/delete', array('id' => Mage::registry("PolarPumpen_trustpilot_model")->userId, 'type' => 'Business', 'target' => Mage::registry("PolarPumpen_trustpilot_model")->Id));
    }

}
