<?php

class PolarPumpen_Trustpilot_Block_Adminhtml_User extends Mage_Adminhtml_Block_Widget_Grid_Container {

    function __construct() {
        $this->_controller = 'Adminhtml_User';
        $this->_blockGroup = 'polarpumpen_trustpilot';
        $this->_headerText = "Trustpilot Manager";
        $this->_addButtonLabel = "Add Trustpilot User";
        parent::__construct();
    }

}

?>