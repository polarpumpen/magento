<?php

class PolarPumpen_Trustpilot_Block_Adminhtml_User_Edit extends Mage_Adminhtml_Block_Widget_Form_Container {

    function __construct() {
        parent::__construct();
        $this->_objectId = 'id';
        $this->_blockGroup = 'polarpumpen_trustpilot';
        $this->_controller = 'Adminhtml_User';
        $this->_mode = 'edit';
    }

    function getHeaderText() {
        if (Mage::registry("PolarPumpen_trustpilot_model")->Id !== null) {
            return 'Edit User ( ' . Mage::registry("PolarPumpen_trustpilot_model")->name . ' )';
        } else {
            return 'New User';
        }
    }

    function getBackUrl() {
        return $this->getUrl('*/*/');
    }

}
