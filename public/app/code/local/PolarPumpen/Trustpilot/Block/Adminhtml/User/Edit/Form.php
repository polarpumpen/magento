<?php

class PolarPumpen_Trustpilot_Block_Adminhtml_User_Edit_Form extends Mage_Adminhtml_Block_Widget_Form {

    private $user;
    public $businessCollection;
    public $logs;
    private $formkey;
    private $buffer;

    function __construct(array $args = array()) {
        parent::__construct($args);
        $this->user = Mage::registry("PolarPumpen_trustpilot_model");
        $this->buffer = Mage::helper('polarpumpen_trustpilot/HtmlBuffer');

        if ($this->user->id !== null) {

            $this->businessCollection = Mage::getModel("polarpumpen_trustpilot/Business")->
                    getCollection()->
                    addFilter('user_id', $this->user->Id);

            $this->logs = Mage::getModel("polarpumpen_trustpilot/Log")->
                    getCollection()->
                    addFilter('user_id', $this->user->Id);

            if ($this->businessCollection === null) throw new Exception('$this->businessCollection === null');

            if ($this->logs === null) throw new Exception('$this->logs === null');

            $this->formkey = Mage::getSingleton('core/session')->getFormKey();
        }
    }

    function _prepareForm() {

        $form = new Varien_Data_Form(array(
            'id' => "edit_form",
            'action' => $this->getUrl('*/*/save'),
            'method' => 'post',
            'enctype' => 'multipart/form-data'
        ));

        $fieldset = $form->addFieldset('base_fieldset', array(
            'legend' => 'User',
            'class' => 'fieldset-wide',
        ));

        if ($this->user->id !== null) {
            $fieldset->addField('id', 'hidden', array(
                'name' => 'id',
            ));
        }

        $fieldset->addField('name', 'text', array(
            'name' => 'name',
            'label' => 'Name',
            'title' => 'Name',
            'required' => true,
                //      'after_element_html' => '<small>Comments</small>',
        ));

        $fieldset->addField('key', 'text', array(
            'name' => 'key',
            'label' => 'Key',
            'title' => 'Key',
            'required' => true,
        ));


        $fieldset->addField('secret', 'password', array(
            'name' => 'secret',
            'label' => 'Secret',
            'title' => 'Secret',
            'required' => true,
        ));


        $fieldset->addField('username', 'text', array(
            'name' => 'username',
            'label' => 'User Name',
            'title' => 'User Name',
            'required' => true,
        ));

        $fieldset->addField('password', 'password', array(
            'name' => 'password',
            'label' => 'Password',
            'title' => 'Password',
            'required' => true,
        ));

        if ($this->user->Id != null) {
            $form->setValues($this->user->getData());
        } else {
            $form->setValues(Mage::getSingleton('adminhtml/session')->getFormData());
        }$form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }

    function _afterToHtml($html) {
        if ($this->user->id !== null) {
            try {
                $api = Mage::helper('polarpumpen_trustpilot/trustpilotApi'); /* @var $api PolarPumpen_Trustpilot_Helper_TrustpilotApi */
                $api->initViaUserId($this->user->id);
                $api->login();
            } catch (Exception $ex) {
                $this->buffer->messages($ex->getMessage());
                return $this->buffer->getContents() . $html;
            }
            $this->buffer = Mage::helper('polarpumpen_trustpilot/HtmlBuffer');
            $this->businessHtml();
            $this->businessAddHtml();
            $this->logsHtml();
        }
        return $html . $this->buffer->getContents();
    }

    private function businessHtml() {
        $buffer = $this->buffer; /* @var $buffer PolarPumpen_Trustpilot_Helper_HtmlBuffer */
        $that = $this;
        $buffer->
                ifElse($this->businessCollection->count() === 0, function()use($buffer) {
                    $buffer->
                    magentoFieldsetWideStart('Business Unit Ids')->
                    addToBuffer("No Business To Show")->
                    magentoFieldsetWideEnd();
                }, function()use($buffer, $that) {
                    $buffer->
                    magentoCollapseableStart('Business Unit Ids', 'businessUnitIdsCollapseable')->
                    tableStart()->
                    each($that->businessCollection, function ($item)use($buffer, $that) {
                        $buffer->
                        row2Start()->
                        magentoCollapseableStart($item->Name, 'businessUnitIdsCollapseable' . $item->Id)->
                        tableStart()->
                        magentoLabelRow('Business Unit Id', $item->businessUnitID())->
                        ifElse($item->reviewInvitationStoreId === null, function ()use($buffer) {
                            $buffer->
                            magentoLabelRow('Review Invitation Store Id', 'Null');
                        }, function ()use($buffer, $item) {
                            $buffer->
                            magentoLabelRow('Review Invitation Store Id', Mage::getModel('core/store')->load($item->reviewInvitationStoreId)->Name);
                        })->
                        magentoLabelRow('Invitation Template Name', $item->getTemplateName())->
                        magentoLabelRow('Locale', $item->invitationLocale)->
                        magentoLabelRow('Sender Email', $item->invitationSenderEmail)->
                        magentoLabelRow('Sender Reply To', $item->invitationSenderReplyTo)->
                        magentoLabelRow('Sender Name', $item->invitationSenderName)->
                        magentoLabelRow('Email Delay , Days', $item->invitationPreferredSendAddTimeD)->
                        magentoLabelRow('Email Delay , Send Time', $item->invitationPreferredSendAddTimeT)->
                        row1Start()->
                        magentoButtonSetLocation('Edit', 'save', $that->getUrl('*/*/edit_business', array('id' => $item->Id)))->
                        addToBuffer("       ")->
                        magentoDeleteButton($that->getUrl('*/*/delete', array('id' => $that->user->Id, 'type' => 'Business', 'target' => $item->Id)))->
                        row1End()->
                        tableEnd()->
                        magentoCollapseableEnd('businessUnitIdsCollapseable' . $item->Id)->
                        row2End();
                    })->
                    tableEnd()->
                    magentoCollapseableEnd('businessUnitIdsCollapseable');
                });
    }

    private function businessAddHtml() {
        $buffer = $this->buffer; /* @var $buffer PolarPumpen_Trustpilot_Helper_HtmlBuffer */
        $buffer->
                magentoCollapseableStart('Add Business Unit Id', 'businessUnitIdsAddCollapseable')->
                formStart('formAddBusinessUnitId', $this->getUrl('*/index/add', array('id' => $this->user->Id)))->
                inputHiddenFormKey()->
                inputHidden('user_id', $this->user->Id)->
                tableStart()->
                magentoTextRow('Name', 'name', null, true)->
                magentoTextRow('Business Unit Id', 'business_unit_id', null, true)->
                row1Start()->
                magentoButton("Add", 'save', 'document.getElementById(\'formAddBusinessUnitId\').submit();')->
                row1End()->
                tableEnd()->
                formEnd()->
                magentoCollapseableEnd('businessUnitIdsCollapseable');
    }

    private function logsHtml() {
        $buffer = $this->buffer;  /* @var $buffer PolarPumpen_Trustpilot_Helper_HtmlBuffer */
        $that = $this;
        $buffer->
                ifElse($this->logs->count() === 0, function()use($buffer) {
                    $buffer->
                    magentoFieldsetWideStart('Logs')->
                    addToBuffer("No Log Entries To Show")->
                    magentoFieldsetWideEnd();
                }, function()use($buffer, $that) {
                    $buffer->
                    magentoCollapseableStart('Logs', 'logCollapseable')->
                    each($that->logs, function ($item)use($buffer) {
                        $buffer->addToBuffer($item . '<br><br><br>');
                    })->
                    magentoCollapseableEnd('logCollapseable');
                });
    }

}
