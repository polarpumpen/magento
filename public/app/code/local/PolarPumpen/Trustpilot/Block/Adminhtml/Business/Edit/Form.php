<?php

class PolarPumpen_Trustpilot_Block_Adminhtml_Business_Edit_Form extends Mage_Adminhtml_Block_Widget_Form {

    private $business;
    private $buffer;
    private $templatesList;
    private $messagesText;

    function __construct(array $args = array()) {
        parent::__construct($args);
        $this->business = Mage::registry("PolarPumpen_trustpilot_model");
        $this->buffer = Mage::helper('polarpumpen_trustpilot/HtmlBuffer');

        if ($this->business->id !== null) {
            $this->formkey = Mage::getSingleton('core/session')->getFormKey();
        }

        $api = Mage::helper('polarpumpen_trustpilot/trustpilotApi'); /* @var $api PolarPumpen_Trustpilot_Helper_TrustpilotApi */
        $api->initViaBusinessId($this->business->id);

        try {
            $list = $api->getListOfInvitationTemplates();
        } catch (Exception $ex) {
            $this->messagesText = $ex->getMessage();
            return;
        }
        $this->messagesText = null;
        $this->templatesList = array();
        foreach ($list['templates'] as $value) {
            $this->templatesList[$value['id']] = $value['name'];
        }
    }

    function _prepareForm() {

        $form = new Varien_Data_Form(array(
            'id' => "edit_form",
            'action' => $this->getUrl('*/*/add', array('id' => $this->id, 'goto' => $this->userId)),
            'method' => 'post',
            'enctype' => 'multipart/form-data'
        ));

        $fieldset = $form->addFieldset('base_fieldset', array(
            'legend' => 'Business Unit Id',
            'class' => 'fieldset-wide',
        ));

        if ($this->business->id !== null) {
            $fieldset->addField('id', 'hidden', array(
                'name' => 'id',
            ));
        }

        $fieldset->addField('name', 'text', array(
            'name' => 'name',
            'label' => 'Name',
            'title' => 'Name',
            'required' => true,
                //      'after_element_html' => '<small>Comments</small>',
        ));

        $fieldset->addField('business_unit_id', 'text', array(
            'name' => 'business_unit_id',
            'label' => 'Business Unit Id',
            'title' => 'Business Unit Id',
            'required' => true,
        ));

        if ($this->messagesText == null) {

            $options = array('' => 'No Store');
            foreach (Mage::app()->getWebsites() as $website) {
                foreach ($website->getGroups() as $group) {
                    $stores = $group->getStores();
                    foreach ($stores as $store) {
                        $options [$store->getId()] = $store->getName();
                    }
                }
            }

            $fieldset->addField('review_invitation_store_id', 'select', array(
                'label' => 'Review Invitation Store Id',
                'title' => 'Review Invitation Store Id',
                'required' => true,
                'name' => 'review_invitation_store_id',
                'values' => $options,
            ));

            $fieldset->addField('invitation_template', 'select', array(
                'label' => 'Template',
                'title' => 'Template',
                'required' => false,
                'name' => 'invitation_template',
                'values' => $this->templatesList,
            ));


            $fieldset->addField('invitation_locale', 'select', array(
                'label' => 'Locale',
                'title' => 'Locale',
                'required' => false,
                'name' => 'invitation_locale',
                'values' => $this->getLocals()
            ));

            $fieldset->addField('invitation_sender_email', 'text', array(
                'name' => 'invitation_sender_email',
                'label' => 'Sender Email',
                'title' => 'Sender Email',
                'required' => false,
            ));

            $fieldset->addField('invitation_sender_reply_to', 'text', array(
                'name' => 'invitation_sender_reply_to',
                'label' => 'Sender Reply To',
                'title' => 'Sender Reply To',
                'required' => false,
            ));

            $fieldset->addField('invitation_sender_name', 'text', array(
                'name' => 'invitation_sender_name',
                'label' => 'Sender Name',
                'title' => 'Sender Name',
                'required' => false,
            ));

            $values = array();
            for ($i = 0; $i < 401; $i++) {
                $values[$i] = $i;
            }

            $fieldset->addField('invitation_preferred_send_add_time_d', 'select', array(
                'label' => 'Email Delay , Days',
                'title' => 'Email Delay , Days',
                'required' => false,
                'name' => 'invitation_preferred_send_add_time_d',
                'values' => $values,
            ));

            $values = array();
            for ($i = 0; $i < 25; $i++) {
                $str = str_pad($i, 2, '0', STR_PAD_LEFT) . ':00';
                $values[$str] = $str;
                $str = str_pad($i, 2, '0', STR_PAD_LEFT) . ':30';
                $values[$str] = $str;
            }

            $fieldset->addField('invitation_preferred_send_add_time_t', 'select', array(
                'label' => 'Email delay , Send Time',
                'title' => 'Email delay , Send Time',
                'required' => false,
                'name' => 'invitation_preferred_send_add_time_t',
                'values' => $values,
            ));
        }

        if ($this->business->Id != null) $form->setValues($this->business->getData());
        else $form->setValues(Mage::getSingleton('adminhtml/session')->getFormData());

        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }

    function _afterToHtml($html) {
        if ($this->messagesText !== null) {
            $buffer = Mage::helper('polarpumpen_trustpilot/HtmlBuffer');
            $buffer->messages($this->messagesText);
            return $buffer->getContents() . $html;
        }
        return $html;
    }

    function getLocals() {
        return array('af-ZA' => 'Afrikaans - South Africa',
            'sq-AL' => 'Albanian - Albania',
            'ar-DZ' => 'Arabic - Algeria',
            'ar-BH' => 'Arabic - Bahrain',
            'ar-EG' => 'Arabic - Egypt',
            'ar-IQ' => 'Arabic - Iraq',
            'ar-JO' => 'Arabic - Jordan',
            'ar-KW' => 'Arabic - Kuwait',
            'ar-LB' => 'Arabic - Lebanon',
            'ar-LY' => 'Arabic - Libya',
            'ar-MA' => 'Arabic - Morocco',
            'ar-OM' => 'Arabic - Oman',
            'ar-QA' => 'Arabic - Qatar',
            'ar-SA' => 'Arabic - Saudi Arabia',
            'ar-SY' => 'Arabic - Syria',
            'ar-TN' => 'Arabic - Tunisia',
            'ar-AE' => 'Arabic - United Arab Emirates',
            'ar-YE' => 'Arabic - Yemen',
            'hy-AM' => 'Armenian - Armenia',
            'Cy-az-AZ' => 'Azeri (Cyrillic) - Azerbaijan',
            'Lt-az-AZ' => 'Azeri (Latin) - Azerbaijan',
            'eu-ES' => 'Basque - Basque',
            'be-BY' => 'Belarusian - Belarus',
            'bg-BG' => 'Bulgarian - Bulgaria',
            'ca-ES' => 'Catalan - Catalan',
            'zh-CN' => 'Chinese - China',
            'zh-HK' => 'Chinese - Hong Kong SAR',
            'zh-MO' => 'Chinese - Macau SAR',
            'zh-SG' => 'Chinese - Singapore',
            'zh-TW' => 'Chinese - Taiwan',
            'zh-CHS' => 'Chinese (Simplified)',
            'zh-CHT' => 'Chinese (Traditional)',
            'hr-HR' => 'Croatian - Croatia',
            'cs-CZ' => 'Czech - Czech Republic',
            'da-DK' => 'Danish - Denmark',
            'div-MV' => 'Dhivehi - Maldives',
            'nl-BE' => 'Dutch - Belgium',
            'nl-NL' => 'Dutch - The Netherlands',
            'en-AU' => 'English - Australia',
            'en-BZ' => 'English - Belize',
            'en-CA' => 'English - Canada',
            'en-CB' => 'English - Caribbean',
            'en-IE' => 'English - Ireland',
            'en-JM' => 'English - Jamaica',
            'en-NZ' => 'English - New Zealand',
            'en-PH' => 'English - Philippines',
            'en-ZA' => 'English - South Africa',
            'en-TT' => 'English - Trinidad and Tobago',
            'en-GB' => 'English - United Kingdom',
            'en-US' => 'English - United States',
            'en-ZW' => 'English - Zimbabwe',
            'et-EE' => 'Estonian - Estonia',
            'fo-FO' => 'Faroese - Faroe Islands',
            'fa-IR' => 'Farsi - Iran',
            'fi-FI' => 'Finnish - Finland',
            'fr-BE' => 'French - Belgium',
            'fr-CA' => 'French - Canada',
            'fr-FR' => 'French - France',
            'fr-LU' => 'French - Luxembourg',
            'fr-MC' => 'French - Monaco',
            'fr-CH' => 'French - Switzerland',
            'gl-ES' => 'Galician - Galician',
            'ka-GE' => 'Georgian - Georgia',
            'de-AT' => 'German - Austria',
            'de-DE' => 'German - Germany',
            'de-LI' => 'German - Liechtenstein',
            'de-LU' => 'German - Luxembourg',
            'de-CH' => 'German - Switzerland',
            'el-GR' => 'Greek - Greece',
            'gu-IN' => 'Gujarati - India',
            'he-IL' => 'Hebrew - Israel',
            'hi-IN' => 'Hindi - India',
            'hu-HU' => 'Hungarian - Hungary',
            'is-IS' => 'Icelandic - Iceland',
            'id-ID' => 'Indonesian - Indonesia',
            'it-IT' => 'Italian - Italy',
            'it-CH' => 'Italian - Switzerland',
            'ja-JP' => 'Japanese - Japan',
            'kn-IN' => 'Kannada - India',
            'kk-KZ' => 'Kazakh - Kazakhstan',
            'kok-IN' => 'Konkani - India',
            'ko-KR' => 'Korean - Korea',
            'ky-KZ' => 'Kyrgyz - Kazakhstan',
            'lv-LV' => 'Latvian - Latvia',
            'lt-LT' => 'Lithuanian - Lithuania',
            'mk-MK' => 'Macedonian (FYROM)',
            'ms-BN' => 'Malay - Brunei',
            'ms-MY' => 'Malay - Malaysia',
            'mr-IN' => 'Marathi - India',
            'mn-MN' => 'Mongolian - Mongolia',
            'nb-NO' => 'Norwegian (Bokmål) - Norway',
            'nn-NO' => 'Norwegian (Nynorsk) - Norway',
            'pl-PL' => 'Polish - Poland',
            'pt-BR' => 'Portuguese - Brazil',
            'pt-PT' => 'Portuguese - Portugal',
            'pa-IN' => 'Punjabi - India',
            'ro-RO' => 'Romanian - Romania',
            'ru-RU' => 'Russian - Russia',
            'sa-IN' => 'Sanskrit - India',
            'Cy-sr-SP' => 'Serbian (Cyrillic) - Serbia',
            'Lt-sr-SP' => 'Serbian (Latin) - Serbia',
            'sk-SK' => 'Slovak - Slovakia',
            'sl-SI' => 'Slovenian - Slovenia',
            'es-AR' => 'Spanish - Argentina',
            'es-BO' => 'Spanish - Bolivia',
            'es-CL' => 'Spanish - Chile',
            'es-CO' => 'Spanish - Colombia',
            'es-CR' => 'Spanish - Costa Rica',
            'es-DO' => 'Spanish - Dominican Republic',
            'es-EC' => 'Spanish - Ecuador',
            'es-SV' => 'Spanish - El Salvador',
            'es-GT' => 'Spanish - Guatemala',
            'es-HN' => 'Spanish - Honduras',
            'es-MX' => 'Spanish - Mexico',
            'es-NI' => 'Spanish - Nicaragua',
            'es-PA' => 'Spanish - Panama',
            'es-PY' => 'Spanish - Paraguay',
            'es-PE' => 'Spanish - Peru',
            'es-PR' => 'Spanish - Puerto Rico',
            'es-ES' => 'Spanish - Spain',
            'es-UY' => 'Spanish - Uruguay',
            'es-VE' => 'Spanish - Venezuela',
            'sw-KE' => 'Swahili - Kenya',
            'sv-FI' => 'Swedish - Finland',
            'sv-SE' => 'Swedish - Sweden',
            'syr-SY' => 'Syriac - Syria',
            'ta-IN' => 'Tamil - India',
            'tt-RU' => 'Tatar - Russia',
            'te-IN' => 'Telugu - India',
            'th-TH' => 'Thai - Thailand',
            'tr-TR' => 'Turkish - Turkey',
            'uk-UA' => 'Ukrainian - Ukraine',
            'ur-PK' => 'Urdu - Pakistan',
            'Cy-uz-UZ' => 'Uzbek (Cyrillic) - Uzbekistan',
            'Lt-uz-UZ' => 'Uzbek (Latin) - Uzbekistan',
            'vi-VN' => 'Vietnamese - Vietnam');
    }

}
