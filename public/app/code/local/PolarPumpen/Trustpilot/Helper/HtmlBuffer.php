<?php

class PolarPumpen_Trustpilot_Helper_HtmlBuffer extends Mage_Core_Helper_Abstract {

    private static $formkey;
    private $buffer;

    public function __construct() {
        if (PolarPumpen_Trustpilot_Helper_HtmlBuffer::$formkey == null) PolarPumpen_Trustpilot_Helper_HtmlBuffer::$formkey = Mage::getSingleton('core/session')->getFormKey();
        $this->buffer = "";
    }

    /**
     * @return  string
     */
    function getFormKey() {
        return PolarPumpen_Trustpilot_Helper_HtmlBuffer::$formkey;
    }

    /**
     * @return  string
     */
    public function getContentsAndCleanBuffer() {
        $temp = $this->buffer;
        $this->buffer = "";
        return $temp;
    }

    /**
     * @return  string
     */
    public function getContents() {
        return $this->buffer;
    }

    /**
     * @return  PolarPumpen_Trustpilot_Helper_AdminHtml
     */
    public function addToBuffer($strOrMef) {
        if (is_string($strOrMef)) {
            $this->buffer .= $strOrMef;
        } else {
            ob_start();
            $strOrMef();
            $this->buffer .= ob_get_contents();
            ob_end_clean();
        }
        return $this;
    }

    /**
     * @return  PolarPumpen_Trustpilot_Helper_HtmlBuffer
     */
    public function script($strOrMef) {
        return $this->
                        addToBuffer('<script>')->
                        addToBuffer($strOrMef)->
                        addToBuffer('</script>');
    }

    /**
     * @return  PolarPumpen_Trustpilot_Helper_HtmlBuffer
     */
    function magentoButton($title, $classCss, $onclick) {
        $this->buffer .= '<button title = "' . $title . '" type = "button" class = "scalable ' . $classCss . '" onclick = "' . $onclick . '" style = ""><span><span><span>' . $title . '</span></span></span></button>';
        return $this;
    }

    /**
     * @return  PolarPumpen_Trustpilot_Helper_HtmlBuffer
     */
    function magentoButtonSetLocation($title, $classCss, $url) {
        return $this->magentoButton($title, $classCss, 'setLocation(\'' . $url . '\')');
    }

    /**
     * @return  PolarPumpen_Trustpilot_Helper_HtmlBuffer
     */
    function magentoDeleteButton($url, $text = 'Are you sure you want to do this?') {
        return $this->magentoButton('Delete', 'delete', 'deleteConfirm(\'' . $text . '\',\'' . $url . '\')"');
    }

    /**
     * @return  PolarPumpen_Trustpilot_Helper_HtmlBuffer
     */
    function magentoFieldsetWideStart($titel) {
        $this->buffer .=
                '<div class = "entry-edit">' .
                '<div class = "entry-edit-head">' .
                '<h4 class = "icon-head head-edit-form fieldset-legend">' . $titel . '</h4>' .
                '<div class = "form-buttons"></div>' .
                '</div>' .
                '<div class = "fieldset fieldset-wide" >' .
                '<div class = "hor-scroll">';
        return $this;
    }

    /**
     * @return  PolarPumpen_Trustpilot_Helper_HtmlBuffer
     */
    function magentoFieldsetWideEnd() {
        $this->buffer .='</div></div></div>';
        return $this;
    }

    /**
     * @return  PolarPumpen_Trustpilot_Helper_HtmlBuffer
     */
    function magentoCollapseableStart($titel, $id) {
        $this->buffer .=
                '<div class = "entry-edit">' .
                '<div class = "section-config" style = "margin-bottom: 15px;" >' .
                '<div class = "entry-edit-head collapseable">' .
                '<a id = "' . $id . '-head" href = "#" onclick = "Fieldset.toggleCollapse(\'' . $id . '\');' .
                'return false;" class = "">' . $titel . '</a>' .
                '</div>' .
                '<input id = "' . $id . '-state" name = "config_state[' . $id . ']" type = "hidden" value = "0">' .
                '<fieldset class = "config collapseable" id = "' . $id . '" style = "display: none;">' .
                '<legend>' . $titel . '</legend>';

        return $this;
    }

    /**
     * @return  PolarPumpen_Trustpilot_Helper_HtmlBuffer
     */
    function magentoCollapseableEnd($id) {
        $this->buffer .=
                '</fieldset>' .
                '<script type = "text/javascript" > //<![CDATA[Fieldset.applyCollapse(\'' . $id . '\');//]]></script>' .
                '</div>' .
                '</div>';
        return $this;
    }

    /**
     * @return  PolarPumpen_Trustpilot_Helper_HtmlBuffer
     */
    function selectNumbers($name, $max = 31) {
        $html = '<select name="' . $name . '" class="select">';
        for ($i = 1; $i < $max; $i++) {
            $html .='<option value="' . $i . '">' . $i . '</option>';
        }
        $this->buffer .= $html . '</select>';
        return $this;
    }

    /**
     * @return  PolarPumpen_Trustpilot_Helper_HtmlBuffer
     */
    function select($name, $array, $atrebut = '') {
        $html = '<select name="' . $name . '"' . $atrebut . '>';
        foreach ($array as $key => $value) {
            $html .='<option value="' . $key . '">' . $value . '</option>';
        }
        $this->buffer .= $html . '</select>';
        return $this;
    }

    /**
     * @return  PolarPumpen_Trustpilot_Helper_HtmlBuffer
     */
    function selectOptions($arr) {
        $html = '';
        foreach ($arr as $key => $value)
            $html.= '<option value="' . $key . '">' . $value . '</option>';
        $this->buffer .= $html;
        return $this;
    }

    /**
     * @return  PolarPumpen_Trustpilot_Helper_HtmlBuffer
     */
    function questionsOptions($name = "question_id") {
        $html = '<select name="' . $name . '" class="select">';
        foreach ($this->questions as $value)
            $html .='<option value="' . $value->Id . '">' . $value->Name . '</option>';
        $this->buffer .= $html . '</select>';
        return $this;
    }

    /**
     * @return  PolarPumpen_Trustpilot_Helper_HtmlBuffer
     */
    function pipelinesOptions($name = "pipeline_id") {
        $html = '<select name="' . $name . '" class="select">';
        foreach ($this->pipelines as $value)
            $html .='<option value="' . $value->Id . '">' . $value->Name . '</option>';
        $this->buffer .= $html . '</select>';
        return $this;
    }

    /**
     * @return  PolarPumpen_Trustpilot_Helper_HtmlBuffer
     */
    function formStart($id, $postUrl) {
        $this->buffer .= '<form id="' . $id . '" action="' . $postUrl . '" method="post" enctype="multipart/form-data" >';
        return $this;
    }

    /**
     * @return  PolarPumpen_Trustpilot_Helper_HtmlBuffer
     */
    function formEnd() {
        $this->buffer .= '</form>';
        return $this;
    }

    /**
     * @return  PolarPumpen_Trustpilot_Helper_HtmlBuffer
     */
    function input($type, $name, $value = null, $cssClass = "", $atrebut = '') {
        if ($value == null) $this->buffer .= '<input type="' . $type . '" name="' . $name . '" class="' . $cssClass . '" ' . $atrebut . '/>';
        else $this->buffer .= '<input type="' . $type . '" name="' . $name . '" value="' . $value . '" class="' . $cssClass . '" ' . $atrebut . '/>';
        return $this;
    }

    /**
     * @return  PolarPumpen_Trustpilot_Helper_HtmlBuffer
     */
    function text($name, $value = null, $cssClass = "", $atrebut = '') {
        return $this->input('text', $name, $value, $cssClass, $atrebut);
    }

    /**
     * @return  PolarPumpen_Trustpilot_Helper_HtmlBuffer
     */
    function inputHidden($name, $value) {
        return $this->input('hidden', $name, $value);
    }

    /**
     * @return  PolarPumpen_Trustpilot_Helper_HtmlBuffer
     */
    function inputHiddenFormKey() {
        return $this->input('hidden', 'form_key', PolarPumpen_Trustpilot_Helper_HtmlBuffer::$formkey);
    }

    /**
     * @return  PolarPumpen_Trustpilot_Helper_HtmlBuffer
     */
    function tableStart() {
        $this->buffer.= '<table cellspacing = "0" class = "form-list" style = "width: 80%;"><tbody>';
        return $this;
    }

    /**
     * @return  PolarPumpen_Trustpilot_Helper_HtmlBuffer
     */
    function tableEnd() {
        $this->buffer.= '</tbody></table>';
        return $this;
    }

    /**
     * @return  PolarPumpen_Trustpilot_Helper_HtmlBuffer
     */
    function row1Start($label = "", $required = false) {
        if ($required) $label .=' <span class="required">*</span>';
        $this->buffer.=
                '<tr>' .
                '<td class = "label"><label>' . $label . '</label></td>' .
                '<td class = "value">';
        return $this;
    }

    /**
     * @return  PolarPumpen_Trustpilot_Helper_HtmlBuffer
     */
    function row2Start() {
        $this->buffer.=
                '<tr>' .
                '<td class = "value" colspan = "2" >';
        return $this;
    }

    /**
     * @return  PolarPumpen_Trustpilot_Helper_HtmlBuffer
     */
    function row1End() {
        $this->buffer.=
                '</td>' .
                '</tr>';
        return $this;
    }

    /**
     * @return  PolarPumpen_Trustpilot_Helper_HtmlBuffer
     */
    function row2End() {
        return $this->row1End();
    }

    /**
     * @return  PolarPumpen_Trustpilot_Helper_HtmlBuffer
     */
    function magentoLabelRow($label, $value, $required = false) {
        if ($required) $label .=' <span class="required">*</span>';
        $this->buffer.=
                '<tr>' .
                '<td class = "label"><label>' . $label . '</label></td>' .
                '<td class = "value">' . $value . '</td>' .
                '</tr>';
        return $this;
    }

    /**
     * @return  PolarPumpen_Trustpilot_Helper_HtmlBuffer
     */
    function magentoTextRow($label, $name, $value = null, $required = false) {
        return $this->row1Start($label, $required)->input('text', $name, $value, 'input-text')->row1End();
    }

    /**
     * @return  PolarPumpen_Trustpilot_Helper_HtmlBuffer
     */
    function magentoNumberRow($label, $name, $value = null) {
        return $this->row1Start($label)->input('number', $name, $value, 'input-text')->row1End();
    }

    /**
     * @return  PolarPumpen_Trustpilot_Helper_HtmlBuffer
     */
    function magentoSelectRow($label, $name, $options, $value = null, $atrebut = '') {
        $this->row1Start($label);
        $this->buffer .= '<select name = "' . $name . '" class = "select" ' . $atrebut . '>';
        foreach ($options as $key => $value)
            $this->buffer .= '<option value = "' . $key . '">' . $value . '</option>';
        $this->buffer .= '</select>';
        return $this->row1End();
    }

    /**
     * @return  PolarPumpen_Trustpilot_Helper_HtmlBuffer
     */
    function magentoSelectNumbersRow($label, $name, $value = null, $max = 31) {
        $this->row1Start($label);
        $this->buffer .= '<select name = "' . $name . '" class = "select">';
        for ($i = 1; $i < $max; $i++) {
            $this->buffer .= '<option value = "' . $i . '">' . $i . '</option>';
        }
        $this->buffer .= '</select>';
        return $this->row1End();
    }

    /**
     * @return  PolarPumpen_Trustpilot_Helper_HtmlBuffer
     */
    function ifElse($bool, $mef1, $mef2 = null) {
        if ($bool) $mef1();
        elseif ($mef2) $mef2();
        return $this;
    }

    /**
     * @return  PolarPumpen_Trustpilot_Helper_HtmlBuffer
     */
    function run($mef) {
        $mef();
        return $this;
    }

    /**
     * @return  PolarPumpen_Trustpilot_Helper_HtmlBuffer
     */
    function each($collection, $mef) {
        foreach ($collection as $item)
            $mef($item);
        return $this;
    }

    /**
     * @return  PolarPumpen_Trustpilot_Helper_HtmlBuffer
     */
    function divSpace($space = '10px') {
        $this->buffer .='<div style = "width: ' . $space . ';display: inline-block;"></div>';
        return $this;
    }

    /**
     * @return  PolarPumpen_Trustpilot_Helper_HtmlBuffer
     */
    function region($arg1, $arg2 = null, $arg3 = null) {
        if ($arg1 != null) {
            if (is_string($arg1)) $this->addToBuffer($arg1);
            else $arg1();
        }

        if ($arg2 != null) {
            if (is_string($arg2)) $this->addToBuffer($arg2);
            else $arg2();
        }

        if ($arg3 != null) {
            if (is_string($arg3)) $this->addToBuffer($arg3);
            else $arg3();
        }

        return $this;
    }

    function messages($text) {
        $this->buffer .='<div id = "messages"><ul class = "messages"><li class = "error-msg"><ul><li><span>' . $text . ' </span></li></ul></li></ul></div>';
        return $this;
    }

}
