<?php

class PolarPumpen_Trustpilot_Helper_Http extends Mage_Core_Helper_Abstract {

    public function get($url, $header = array()) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');

        return $this->endGetOrPost($ch, $header, $url);
    }

    public function post($url, $fields = array(), $header = array()) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');

        if (is_array($fields)) {
            $fields_string = "";
            foreach ($fields as $key => $value) {
                $fields_string .= $key . '=' . urlencode($value) . '&';
            }
            rtrim($fields_string, '&');
            curl_setopt($ch, CURLOPT_POST, count($fields));
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
        } else if (is_string($fields)) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        }
        return $this->endGetOrPost($ch, $header, $url);
    }

    private function endGetOrPost($ch, $header, $url) {
        // farlig !!!! mend för dev !!!
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0); // On dev server only!

        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        $response = curl_exec($ch);
        if ($response === false) throw new Exception(curl_error($ch));

        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $responseHeader = substr($response, 0, $header_size);
        $responseBody = substr($response, $header_size);

        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if ($code != 200) throw new Exception($url . ' ' . $code . " " . $responseBody);

        return (Object) array('body' => $responseBody, 'header' => $responseHeader);
    }

    public function jsPost($url, $fields = array(), $header = array('Content-Type: application/json')) {
        return json_decode($this->post($url, json_encode($fields), $header)->body, true);
    }

    public function jsGet($url, $header = array('Content-Type: application/json')) {
        return json_decode($this->get($url, $header)->body, true);
    }

}
