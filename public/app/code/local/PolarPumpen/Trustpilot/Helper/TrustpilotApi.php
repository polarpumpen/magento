<?php

class PolarPumpen_Trustpilot_Helper_TrustpilotApi extends Mage_Core_Helper_Abstract {

    /** @var PolarPumpen_Trustpilot_Model_User */
    protected $user;

    /** @var PolarPumpen_Trustpilot_Model_Business */
    protected $business;

    function initViaUserId($id) {
        $this->user = null;
        $this->business = null;
        if ($id === null) throw new Exception('id is null!');
        $idSafe = intval($id);
        $modelTemp = Mage::getModel('polarpumpen_trustpilot/User');
        $modelTemp->load($idSafe);
        if ($modelTemp->id === null) throw new Exception('$modelTemp->id is null');
        $this->user = $modelTemp;
        return ($this->user != null);
    }

    function initViaStoreId($id) {
        $this->user = null;
        $this->business = null;
        if ($id === null) throw new Exception('id is null!');
        $idSafe = intval($id);
        $modelTemp = Mage::getModel('polarpumpen_trustpilot/Business');
        $modelTemp->load($idSafe, 'review_invitation_store_id');
        if ($modelTemp->id === null) throw new Exception('$modelTemp->id is null');
        $this->business = $modelTemp;
        $this->user = $modelTemp->user();
        return ($this->user != null);
    }

    function initViaBusinessId($id) {
        $this->user = null;
        $this->business = null;
        if ($id === null) throw new Exception('id is null!');
        $idSafe = intval($id);
        $modelTemp = Mage::getModel('polarpumpen_trustpilot/Business');
        $modelTemp->load($idSafe);
        if ($modelTemp->id === null) throw new Exception('$modelTemp->id is null');
        $this->business = $modelTemp;
        $this->user = $modelTemp->user();
        return ($this->user != null);
    }

    function createProductReviewInvitationLink($reviewInvitationLinkDataArr) {
        return $this->business->createProductReviewInvitationLink($reviewInvitationLinkDataArr, $this->user);
    }

    function createNewInvitation($createNewInvitationDataArr) {
        return $this->business->createNewInvitation($createNewInvitationDataArr, $this->user);
    }

    function getNewReviewInvitationLinkDataArr($email, $name, $referenceId, $redirectUri, $locale = null) {
        if ($locale == null) $locale = $this->business->invitationLocale;
        return array(
            'consumer' => array('email' => $email, 'name' => $name),
            'referenceId' => $referenceId,
            'locale' => $locale,
            'redirectUri' => $redirectUri,
            'products' => array()
        );
    }

    function getCreateNewInvitationArr($recipientEmail, $recipientName, $referenceId, $redirectUri, $templateId = null, $locale = null, $senderEmail = null, $senderName = null, $replyTo = null, $preferredSendTime = null, $tags = array()) {

        if ($templateId == null) $templateId = $this->business->invitationTemplate;

        if ($locale == null) $locale = $this->business->invitationLocale;

        if ($senderEmail == null) $senderEmail = $this->business->invitationSenderEmail;

        if ($senderName == null) $senderName = $this->business->invitationSenderName;

        if ($replyTo == null) $replyTo = $this->business->invitationSenderReplyTo;

        if ($preferredSendTime === null) {
            date_default_timezone_set('UTC');
            $d = date("d") + intval($this->business->invitationPreferredSendAddTimeD);
            if (substr_count($this->business->invitationPreferredSendAddTimeT, ':') == 1) {
                $t = $this->business->invitationPreferredSendAddTimeT . ':00';
            } else {
                $t = $this->business->invitationPreferredSendAddTimeT;
            }
            $t = $this->business->invitationPreferredSendAddTimeT;
            $preferredSendTime = date("Y-m-d\T" . $t, mktime(0, 0, 0, date("m"), $d, date("Y")));
        }

        return array(
            'recipientEmail' => $recipientEmail,
            'recipientName' => $recipientName,
            'referenceId' => $referenceId,
            'templateId' => $templateId,
            'locale' => $locale,
            'senderEmail' => $senderEmail,
            'senderName' => $senderName,
            'replyTo' => $replyTo,
            'preferredSendTime' => $preferredSendTime,
            'tags' => $tags,
            'redirectUri' => $redirectUri,
        );
    }

    function addProductToReviewInvitationLinkDataArr(&$reviewInvitationLinkDataArr, $productUrl, $imageUrl, $name, $sku, $brand) {
        $products = &$reviewInvitationLinkDataArr['products'];
        $products[] = array(
            'productUrl' => $productUrl,
            'imageUrl' => $imageUrl,
            'name' => $name,
            'sku' => $sku,
            'brand' => $brand
        );
    }

    function getListOfInvitationTemplates() {
        return $this->business->getListOfInvitationTemplates($this->user);
    }

    function getListOfBusinessUnits() {
        return $this->user->getListOfBusinessUnits();
    }

    function login() {
        $this->user->login();
    }

    function log($str, $order = null, $storeId = null) {

        if (is_a($str, 'Exception')) {
            $text = $str->getMessage();
            $traceArr = $str->getTrace();
            $count = min(5, count($traceArr));
            for ($index = 0; $index < $count; $index++) {
                $arr = $traceArr[$index];
                $text .= $index . ' ' . $arr["file"] . ' ' . $arr["line"] . "\n";
            }
        } else {
            $text = json_encode($str, true);
        }
        $textExtra = 'user ' . (($this->user === null) ? 'null' : $this->user->name()) . "\n" .
                'business ' . (($this->business === null) ? 'null' : $this->business->name()) . "\n" .
                'order ' . (($order === null) ? 'null' : $order->incrementId . ' , ' . $order->CustomerEmail ) . "\n" .
                'storeId ' . (($storeId === null) ? 'null' : $storeId ) . "\n";
        Mage::log($textExtra . "\n" . $text, null, "polarpumpen_trustpilot.log");

        try {
            $model = Mage::getModel('polarpumpen_trustpilot/Log');
            $model->text = $text;
            if ($this->user !== null) $model->userId = $this->user->id;
            if ($this->business !== null) $model->businessId = $this->business->id;
            if ($order !== null && $order->incrementId !== null) $model->incrementId = $order->incrementId;
            if ($storeId !== null) $model->storeId = $storeId;
            $model->save();
        } catch (Exception $exc) {
            Mage::log($exc, null, "polarpumpen_trustpilot.log");
        }
    }

}
