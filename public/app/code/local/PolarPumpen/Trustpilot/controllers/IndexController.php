<?php

class PolarPumpen_Trustpilot_IndexController extends Mage_Adminhtml_Controller_Action {

    function varDump($var) {
        ob_start();
        var_dump($var);
        return ob_get_clean();
    }

    function indexAction() {
        $this->loadLayout()->_setActiveMenu('cms/page');
        $this->getLayout()->getBlock('content')->insert($this->getLayout()->createBlock('polarpumpen_trustpilot/Adminhtml_User'));
        $this->renderLayout();
    }

    function editAction() {
        $this->loadLayout()->_setActiveMenu('cms/page');
        $user = Mage::getModel('polarpumpen_trustpilot/user');
        $id = $this->getRequest()->getParam('id');

        if ($id != NULL) { 
            $user->load($id);
        }

        Mage::register('PolarPumpen_trustpilot_model', $user);

        if ($id != NULL && $user->Id != NULL) {
            $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
            if (!empty($data)) {
                $user->setData($data);
            }
            $this->_addContent($this->getLayout()->createBlock("polarpumpen_trustpilot/Adminhtml_User_Edit"));
            $this->renderLayout();
        } else {
            //Mage::getSingleton('adminhtml/session')->addError('user does not exist');
            $this->_redirect('*/*/');
        }
    }

    function edit_businessAction() {
        $this->loadLayout()->_setActiveMenu('cms/page');
        $business = Mage::getModel('polarpumpen_trustpilot/business');
        $id = $this->getRequest()->getParam('id');
        if ($id != NULL) $business->load($id);

        Mage::register('PolarPumpen_trustpilot_model', $business);

        if ($id != NULL && $business->Id != NULL) {
            $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
            if (!empty($data)) $business->setData($data);
            $this->_addContent($this->getLayout()->createBlock("polarpumpen_trustpilot/Adminhtml_Business_Edit"));
            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError('Business does not exist');
            $this->_redirect('*/*/');
        }
    }

    function newAction() {
        $this->loadLayout()->_setActiveMenu('cms/page');
        Mage::register('PolarPumpen_trustpilot_model', Mage::getModel('polarpumpen_trustpilot/user'));
        $this->_addContent($this->getLayout()->createBlock("polarpumpen_trustpilot/Adminhtml_User_Edit"));
        $this->renderLayout();
    }

    function saveAction() {
        $data = $this->getRequest()->getPost();
        $user = Mage::getModel('polarpumpen_trustpilot/user');
        if ($data == null) {
            Mage::getSingleton('adminhtml/session')->addError('Unable to find User to save');
            $this->_redirect('*/*/');
        }
        try {
            $user->setData($data);
            $user->save();
            Mage::getSingleton('adminhtml/session')->addSuccess('User was successfully saved');
            Mage::getSingleton('adminhtml/session')->setFormData(false);
            $this->_redirect("*/*/edit", array('id' => $user->Id));
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            Mage::getSingleton('adminhtml/session')->setFormData($data);
            $parmId = $this->getRequest()->getParam('id');
            if ($parmId != NULL) $this->_redirect("*/*/edit", array('id' => $parmId));
            else $this->_redirect("*/*/new");
        }
    }

    function addAction() {
        $data = $this->getRequest()->getPost();
        $business = Mage::getModel('polarpumpen_trustpilot/Business');
        try {
            $business->setData($data);
            $business->save();
            Mage::getSingleton('adminhtml/session')->addSuccess('Business was successfully saved');
            Mage::getSingleton('adminhtml/session')->setFormData(false);
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            Mage::getSingleton('adminhtml/session')->setFormData($data);
        }
        $parmId = $this->getRequest()->getParam('goto', null);
        if ($parmId == null) $parmId = $this->getRequest()->getParam('id');
        $this->_redirect("*/*/edit", array('id' => $parmId));
    }

    function deleteAction() {
        try {
            if ($this->getRequest()->getParam('id') == NULL) throw new Exception("id == null");
            $type = ucfirst($this->getRequest()->getParam('type', 'User'));

            if ($type == "Business") $model = Mage::getModel('polarpumpen_trustpilot/Business');
            else If ($type === "User") $model = Mage::getModel('polarpumpen_trustpilot/user');
            else throw new Exception("bad type");
            $target = $this->getRequest()->getParam('target', $this->getRequest()->getParam('id'));
            $model->load($target);
            $model->delete();

            Mage::getSingleton('adminhtml/session')->addSuccess($type . ' was deleted');
            Mage::getSingleton('adminhtml/session')->setFormData(false);
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        }
        if ($type == 'Selector') {
            $this->_redirect("*/*/");
        } else {
            $parmId = $this->getRequest()->getParam('id');
            $this->_redirect("*/*/edit", array('id' => $parmId));
        }
    }

    /*
      function orderAction() {
      $productids = array(4, 5, 6);
      $websiteId = Mage::app()->getWebsite()->getId();
      $store = Mage::app()->getStore();
      // Start New Sales Order Quote
      $quote = Mage::getModel('sales/quote')->setStoreId(1);

      // $email = "robin.l.osborne@gmail.com";
      // $email = "johan@polson.se";

      $email = $this->getRequest()->getParam("email", null);

      if ($email === null) {
      $this->loadLayout();
      $block = $this->getLayout()
      ->createBlock('core/text', 'example-block')
      ->setText('<h1> no email</h1>');
      $this->_addContent($block);
      $this->renderLayout();

      return;
      }

      // Set Sales Order Quote Currency
      $quote->setCurrency($order->AdjustmentAmount->currencyID);
      $customer = Mage::getModel('customer/customer')
      ->setWebsiteId($websiteId)
      ->loadByEmail($email);
      if ($customer->getId() == "") {
      $customer = Mage::getModel('customer/customer');
      $customer->setWebsiteId($websiteId)
      ->setStore($store)
      ->setFirstname('johan')
      ->setLastname('polson')
      ->setEmail($email)
      ->setPassword("password");
      $customer->save();
      }

      // Assign Customer To Sales Order Quote
      $quote->assignCustomer($customer);

      // Configure Notification
      $quote->setSendCconfirmation(1);
      foreach ($productids as $id) {
      $product = Mage::getModel('catalog/product')->load($id);
      $quote->addProduct($product, new Varien_Object(array('qty' => 1)));
      }

      // Set Sales Order Billing Address
      $billingAddress = $quote->getBillingAddress()->addData(array(
      'customer_address_id' => '',
      'prefix' => '',
      'firstname' => 'john',
      'middlename' => '',
      'lastname' => 'Deo',
      'suffix' => '',
      'company' => '',
      'street' => array(
      '0' => 'Noida',
      '1' => 'Sector 64'
      ),
      'city' => 'Noida',
      'country_id' => 'IN',
      'region' => 'UP',
      'postcode' => '201301',
      'telephone' => '78676789',
      'fax' => 'gghlhu',
      'vat_id' => '',
      'save_in_address_book' => 1
      ));

      // Set Sales Order Shipping Address
      $shippingAddress = $quote->getShippingAddress()->addData(array(
      'customer_address_id' => '',
      'prefix' => '',
      'firstname' => 'john',
      'middlename' => '',
      'lastname' => 'Deo',
      'suffix' => '',
      'company' => '',
      'street' => array(
      '0' => 'Noida',
      '1' => 'Sector 64'
      ),
      'city' => 'Noida',
      'country_id' => 'IN',
      'region' => 'UP',
      'postcode' => '201301',
      'telephone' => '78676789',
      'fax' => 'gghlhu',
      'vat_id' => '',
      'save_in_address_book' => 1
      ));
      if ($shipprice == 0) {
      $shipmethod = 'freeshipping_freeshipping';
      }

      // Collect Rates and Set Shipping & Payment Method
      $shippingAddress->setCollectShippingRates(true)
      ->collectShippingRates()
      ->setShippingMethod('flatrate_flatrate')
      ->setPaymentMethod('checkmo');

      // Set Sales Order Payment
      $quote->getPayment()->importData(array('method' => 'checkmo'));

      // Collect Totals & Save Quote
      $quote->collectTotals()->save();

      // Create Order From Quote
      $service = Mage::getModel('sales/service_quote', $quote);
      $service->submitAll();
      $increment_id = $service->getOrder()->getRealOrderId();

      ob_start();
      var_dump($service->getOrder()->getData());
      $text = ob_get_clean();
      $quote = $customer = $service = null;

      $this->loadLayout();
      $block = $this->getLayout()
      ->createBlock('core/text', 'example-block')
      ->setText('<h1> id ' . $increment_id . '</h1>' . $text);

      $this->_addContent($block);
      $this->renderLayout();
      }


      function indexAction() {

      $out = "";

      $api = Mage::helper('polarpumpen_trustpilot/TrustpilotApi'); / @var $api PolarPumpen_Trustpilot_Helper_TrustpilotApi /
      $api->initViaId(1);

      $data = $api->getNewReviewInvitationLinkDataArr(
      'johan@polson.se', 'johan', '-1', 'sv-SE', 'http://www.fz.se');

      $api->addProductToReviewInvitationLinkDataArr($data, 'https://plus.google.com/109234483876419570722/about', 'https://lh3.googleusercontent.com/-_hnMQOeMVcw/AAAAAAAAAAI/AAAAAAAAAFE/gNQYeqHZMxE/s180-c-k-no/photo.jpg', 'name', 'sku', '01234567890', 'mpn', 'brand');

      $r = $api->createProductReviewInvitationLink($data);

      $out .= $this->varDump($r);

      $data = $api->getNewReviewInvitationLinkDataArr(
      'johan@polson.se', 'johan', '-1', 'sv-SE', 'http://www.fz.se');

      $api->addProductToReviewInvitationLinkDataArr($data, 'https://plus.google.com/109234483876419570722/about', 'https://lh3.googleusercontent.com/-_hnMQOeMVcw/AAAAAAAAAAI/AAAAAAAAAFE/gNQYeqHZMxE/s180-c-k-no/photo.jpg', 'name', 'sku', '01234567890', 'mpn', 'brand');
      $api->addProductToReviewInvitationLinkDataArr($data, 'https://plus.google.com/109234483876419570722/about', 'https://lh3.googleusercontent.com/-_hnMQOeMVcw/AAAAAAAAAAI/AAAAAAAAAFE/gNQYeqHZMxE/s180-c-k-no/photo.jpg', 'name', 'sku2', '01234567890', 'mpn', 'brand');

      $r = $api->createProductReviewInvitationLink($data);

      $out .= $this->varDump("new order");

      $out .= $this->varDump($r);

      $this->loadLayout();
      $block = $this->getLayout()
      ->createBlock('core/text', 'example-block')
      ->setText('<h1>' . $out . '</h1>');

      $this->_addContent($block);

      $this->renderLayout();
      }
     */
}
