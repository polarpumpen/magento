<?php

class PolarPumpen_AttributeTooltip_Model_Observer
{
    /**
     * Hook that allows us to edit the form that is used to create and/or edit attributes.
     * @param Varien_Event_Observer $observer
     */
    public function addFieldToAttributeEditForm($observer)
    {
        // Add the extra fields to the base fieldset:
        $fieldset = $observer->getForm()->getElement('base_fieldset');
        
        $fieldset->addField('tooltip_text', 'text', array(
            'name' => 'tooltip_text',
            'label' => Mage::helper('core')->__('Tooltip text'),
            'title' => Mage::helper('core')->__('Tooltip text')
        ));
        
        $fieldset->addField('tooltip_url', 'text', array(
            'name' => 'tooltip_url',
            'label' => Mage::helper('core')->__('Tooltip Url'),
            'title' => Mage::helper('core')->__('Tooltip Url')
        ));
    }
}