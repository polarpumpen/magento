<?php

class PolarPumpen_Core_Helper_Data extends Mage_Core_Helper_Abstract {

    function infoEx($order, &$r) {

        if ($order == NULL) {
            throw new Exception('$order == NULL');
        }

        $oa = $this->___amorderattr($order);

        $this->___clone("persnr_orgnr", $r, $oa);
        $this->___clone("fastighet", $r, $oa);
        $this->___clone("pers_comp", $r, $oa);

        $this->___clone("ship_address", $r, $oa);
        $this->___clone("ship_zipcode", $r, $oa);
        $this->___clone("shipping_city", $r, $oa);
        $this->___clone("ship_country", $r, $oa);

        $this->___clone("ship_fname", $r, $oa);
        $this->___clone("ship_sname", $r, $oa);

        $this->___clone("customer_client_email", $r, $oa);

        switch ($r["ship_country"]) {
            case "Sverige":
                $r["ship_country"] = "SE";
                break;
            case "Norge":
                $r["ship_country"] = "NO";
                break;
            case "Finland":
                $r["ship_country"] = "FI";
                break;
            case "Danmark":
                $r["ship_country"] = "DK";
                break;
        }

        if (
                $r['ship_address'] != null &&
                $r['ship_zipcode'] != null &&
                $r['shipping_city'] != null &&
                $r['ship_fname'] != null &&
                $r['ship_sname'] != null
        ) {
            $r['shipping_address']["street"] = $r['ship_address'];
            $r['shipping_address']["postcode"] = $r['ship_zipcode'];
            $r['shipping_address']["city"] = $r['shipping_city'];
            $r['shipping_address']["country_id"] = $r['ship_country'];
            $r['shipping_address']["firstname"] = $r['ship_fname'];
            $r['shipping_address']["lastname"] = $r['ship_sname'];
        }
        $fV = reset($r['status_history']);
        foreach ($r['status_history'] as $key => &$value) {
            if ($fV === $value) $value["comment"] = str_pad($r['persnr_orgnr'], 60) . str_pad($r['fastighet'], 60);
            else unset($r['status_history'][$key]);
        }
    }

    private function ___clone($key, &$arrT, &$arrS) {
        $arrT[$key] = $arrS[$key];
    }

    private function ___amorderattr($order) {
        $orderAttributes = Mage::getModel('amorderattr/attribute');

        if ($orderAttributes == NULL) {
            throw new Exception('$orderAttributes == NULL');
        }

        $orderAttributes->load($order->getId(), 'order_id');
        if ($orderAttributes->getId() == NULL) {
            throw new Exception('$orderAttributes->getId() == NULL');
        }

        $custom = array();

        $collection = Mage::getModel('eav/entity_attribute')->getCollection();
        $collection->addFieldToFilter('entity_type_id', Mage::getModel('eav/entity')->setType('order')->getTypeId());
        $collection->getSelect()->order('checkout_step');
        $attributes = $collection->load();

        if ($attributes->getSize()) {

            foreach ($attributes as $attribute) {
                $value = '';
                switch ($attribute->getFrontendInput()) {
                    case 'select':
                    case 'boolean':
                    case 'radios':
                        $options = $attribute->getSource()->getAllOptions(true, true);
                        foreach ($options as $option) {
                            if ($option['value'] == $orderAttributes->getData($attribute->getAttributeCode())) {
                                $value = $option['label'];
                                break;
                            }
                        }
                        break;
                    default:
                        $value = $orderAttributes->getData($attribute->getAttributeCode());
                        break;
                }
                $custom[$attribute->getAttributeCode()] = $value;
            }
        }
        return $custom;
    }

}
