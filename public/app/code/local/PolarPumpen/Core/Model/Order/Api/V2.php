<?php

class PolarPumpen_Core_Model_Order_Api_V2 extends Mage_Sales_Model_Order_Api {

    function info($orderIncrementId) {
        $result = parent::info($orderIncrementId);

        try {
            Mage::helper("polarpumpen_core")->infoEx($this->_initOrder($orderIncrementId), $result);
        } catch (Exception $e) {
            Mage::log($e->getMessage(), null, 'polarpumpen_core.log');
        }

        return $result;
    }

}
