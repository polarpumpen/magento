<?php

/**
 * @author Adnan Asani <adnan@netconsult.se>
 */
class NetConsult_Polarpumpen_Observer
{
    /**
     * @param $event
     */
    public function onCoreBlockAbstractToHtmlBefore($event)
    {
        if ($event->getBlock() instanceof NetConsult_Polarpumpen_Block_Page_Html_Breadcrumbs) {
            /** @var NetConsult_Polarpumpen_Block_Page_Html_Breadcrumbs $block */
            $block = $event->getBlock();

            if (Mage::app()->getFrontController()->getRequest()->getRouteName() == 'cms') {
                $page = Mage::getSingleton('cms/page');
                $identifiers = explode('/', $page->getIdentifier());

                if (count($identifiers) && in_array('lokalt', $identifiers) && in_array('varmepumpar', $identifiers)) {
                    array_pop($identifiers);
                    foreach($identifiers as $identifier) {
                        /** @var Xonu_SBE_Model_Category $category */
                        $category = Mage::getResourceModel('catalog/category_collection')
                            ->addAttributeToFilter('url_key', $identifier)
                            ->addAttributeToSelect('name')
                            ->getFirstItem();

                        $block->addCrumbBefore($identifier, array(
                            'label' => $category->getName(),
                            'title' => $category->getName(),
                            'link' => $category->getUrl(),
                        ));
                    }
                }
            }
        }
    }
}