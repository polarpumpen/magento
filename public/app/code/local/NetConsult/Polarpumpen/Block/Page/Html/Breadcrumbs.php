<?php

/**
 * @author Adnan Asani <adnan@netconsult.se>
 */
class NetConsult_Polarpumpen_Block_Page_Html_Breadcrumbs
    extends Mage_Page_Block_Html_Breadcrumbs
{
    /**
     * Adding items before the last crumb while preserving keys
     *
     * @param $crumbName
     * @param array $crumbInfo
     * @return $this
     */
    public function addCrumbBefore($crumbName, array $crumbInfo)
    {
        $last = null;
        $this->_prepareArray($crumbInfo, array('label', 'title', 'link', 'first', 'last', 'readonly'));
        if (!in_array($crumbName, array_keys($this->_crumbs))) {
            // Remove the last element
            $last = array_splice($this->_crumbs, count($this->_crumbs) - 1, 1);

            // Add an item
            $this->_crumbs[$crumbName] = $crumbInfo;

            // Push back the last item
            $this->_crumbs = array_merge($this->_crumbs, $last);
        }
        return $this;
    }

    /**
     * @return array
     */
    public function getCrumbs()
    {
        return $this->_crumbs;
    }
}