<?php

/**
 * This is a general class for the section tabs.
 * @author Adnan Asani <adnan@netconsult.se>
 */
class NetConsult_Polarpumpen_Block_Tabs extends Mage_Core_Block_Template
{
    protected function _construct()
    {
        $this->setTemplate("polarpumpen/tabs/section-tabs.phtml");
        parent::_construct();
    }

    protected $tabs = array();

    /**
     * @return array
     */
    public function getTabs()
    {
        return $this->tabs;
    }

    /**
     * @param $alias
     * @param $title
     */
    public function addBlockTab($alias, $title)
    {
        $this->tabs[] = array(
            'alias' => $alias,
            'title' => $title,
        );
    }
}