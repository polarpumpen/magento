<?php

class NetConsult_Polarpumpen_Block_List extends Mage_Core_Block_Text
{

    protected function _toHtml()
    {
        $this->setText('');
        $n = 0;
        foreach ($this->getSortedChildren() as $name) {
            $n++;
            $block = $this->getLayout()->getBlock($name);
            if (!$block) {
                Mage::throwException(Mage::helper('core')->__('Invalid block: %s', $name));
            }
            $this->addText(
                '<div class="block-item block-item-' . $n . '">' .
                    $block->toHtml() .
                '</div>'
            );
        }
        return parent::_toHtml();
    }

}