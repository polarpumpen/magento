<?php

/**
 * @author Adnan Asani <adnan@netconsult.se>
 */
class NetConsult_Polarpumpen_Block_Submenu extends Mage_Core_Block_Abstract
{
    /**
     * @return string
     */
    protected function _toHtml()
    {
        /** @var Mage_Catalog_Model_Product $product */
        $product 	= Mage::registry('product');
        $category 	= Mage::registry('current_category');
        $store		= Mage::app()->getStore()->getId();

        if ($product) {
            $category = $product->getCategory(); /* @var $category Mage_Catalog_Model_Category */
        }

        if (!$category) {
            return '';
        }

        $parentCategories = $category->getParentCategories();
        $tree = array();
        $html = "";

        /**
         * First we get all possible combinations, but then we reverse them and get the most speciifc menu.
         */
        $listToTraverse = array();
        foreach ($parentCategories as $parentCategory) { /* @var $category Mage_Catalog_Model_Category */
            if($parentCategory->getUrlKey() == 'guide') {
                continue;
            }
            
            $tree[] = $parentCategory->getUrlKey(); 

            //$blockName = 'pp_menu_' . join('_', $tree);
            switch ($store) {
                case '1':
                    $blockName = 'pp_menu_' . join('_', $tree);
                    break;
                case '2':
                    $blockName = 'pl_menu_' . join('_', $tree);
                    break;
                case '3':
                    $blockName = 'fk_menu_' . join('_', $tree);
                    break;
                case '4':
                    $blockName = 'kvt_menu_' . join('_', $tree);
                        break;
            }

            $listToTraverse[$blockName]  = $parentCategory;
        }

        foreach (array_reverse($listToTraverse, true) as $blockName => $parentCategory) {
            $block = $this->getLayout()->createBlock('Mage_Cms_Block_Block', $blockName, array('block_id' => $blockName)); /* @var $block Mage_Cms_Block_Block */

            $blockHtml = $block->toHtml();
            if ($blockHtml) {
                $submenuActive = "";
                if ($category->getUrlKey() == $parentCategory->getUrlKey()) {
                    $submenuActive = 'block-item-active';
                }

                $baseClass = array($blockName, $submenuActive);
                if (substr($blockName, -6) == '_guide') {
                    $baseClass[] = 'guide-block';
                } else {
                    $baseClass[] = 'block-items';
                }
                $html .= sprintf('<section class="%s">%s</section>', join(' ', $baseClass), $blockHtml);
                break;
            }
        }

        //if (!in_array('guide', $tree)) {
            foreach($listToTraverse as $blockName => $parentCategory) {
                $blockName = $blockName . '_guide';
                $block = $this->getLayout()->createBlock('Mage_Cms_Block_Block', $blockName, array('block_id' => $blockName)); /* @var $block Mage_Cms_Block_Block */
                if ($block->toHtml()) {
                    $html .= sprintf('<section class="%s">%s</section>', join(' ', array($blockName, 'guide-block')), $block->toHtml());
                }
            }
        //}

        if($store == '1') {
            $html .= "<div class='guide-block guide-block-blogpost'>".$this->getLayout()->createBlock('cms/block')->setBlockId('pp_blogpost_below_nav')->toHtml() . "</div>";
        }
            
        if ($html) {
            $html = "<nav id=\"left-submenu\">" . $html . "</nav>";
        }

        return $html;
    }
}