<?php

/**
 * @author Adnan Asani <adnan@netconsult.se>
 */
class NetConsult_Polarpumpen_Block_Cms_Page_Content extends Mage_Core_Block_Abstract
{
    protected $pageId;

    /**
     * @return mixed
     */
    public function getPageId()
    {
        return $this->pageId;
    }

    /**
     * @param $pageId
     * @return $this
     */
    public function setPageId($pageId)
    {
        $this->pageId = $pageId;
        return $this;
    }

    /**
     * @return string
     * @throws Exception
     */
    protected function _toHtml()
    {
        if (!$this->getPageId()) {
            return '';
        }

        /** @var Mage_Cms_Model_Page $page */
        $page = Mage::getModel('cms/page');
        /** @var Mage_Cms_Helper_Data $helper */
        $helper = Mage::helper('cms');

        $page->setStoreId(Mage::app()->getStore()->getId());
        $page->load($this->getPageId());

        if (!$page) {
            return '';
        }


        $processor = $helper->getPageTemplateProcessor();
        $html = $processor->filter($page->getContent());
        /* $page->load($pageIdentifier,'identifier');*/

        return $html;
    }
}