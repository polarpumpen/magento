<?php

/**
 * @author Adnan Asani <adnan@netconsult.se>
 */
class NetConsult_Polarpumpen_Block_Tabs_Vertical extends Mage_Core_Block_Template
{
    protected function _construct()
    {
        $this->setTemplate("polarpumpen/tabs/vertical-tabs.phtml");

        parent::_construct();
    }

    protected $tabs = array();

    /**
     * @return array
     */
    public function getTabs()
    {
        return $this->tabs;
    }

    /**
     * @param $alias
     * @param $title
     */
    public function addBlockTab($alias, $title)
    {
        $this->tabs[] = array(
            'alias' => $alias,
            'title' => $title,
        );
    }
}