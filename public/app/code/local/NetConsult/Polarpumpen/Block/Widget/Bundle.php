<?php

class NetConsult_Polarpumpen_Block_Widget_Bundle extends Mage_Core_Block_Template
{

    protected $_product;

    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('polarpumpen/widget/bundle.phtml');
    }

    public function setProductId($productId)
    {
        $this->_product = Mage::getModel('catalog/product')->load($productId);
        return $this;
    }

    public function getProduct()
    {
        return $this->_product;
    }

    /**
     * @param Mage_Bundle_Model_Selection $selection
     * @return string
     */
    public function getSelectionPriceFormatted(Mage_Bundle_Model_Selection $selection)
    {
        /** @var Mage_Core_Helper_Data $coreHelper */
        $coreHelper = $this->helper('core');
        $selectionPrice = $selection->getData('price');
        if ((int)$selectionPrice) {
            return $coreHelper->formatPrice($selectionPrice);
        }
        return "";
    }
}