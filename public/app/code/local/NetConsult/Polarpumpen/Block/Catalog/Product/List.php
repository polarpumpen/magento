<?php

class NetConsult_Polarpumpen_Block_Catalog_Product_List extends Mage_Catalog_Block_Product_List
{
    protected $categoryId;
    protected $showCategoryTitle;
    protected $showCategoryDescription;

    /**
     * @return mixed
     */
    public function getCategoryId()
    {
        return $this->categoryId;
    }

    /**
     * @param mixed $categoryId
     */
    public function setCategoryId($categoryId)
    {
        $this->categoryId = $categoryId;
    }

    /**
     * @return mixed
     */
    public function getShowCategoryDescription()
    {
        return $this->showCategoryDescription;
    }

    /**
     * @param mixed $showCategoryDescription
     */
    public function setShowCategoryDescription($showCategoryDescription)
    {
        $this->showCategoryDescription = $showCategoryDescription;
    }

    /**
     * @return mixed
     */
    public function getShowCategoryTitle()
    {
        return $this->showCategoryTitle;
    }

    /**
     * @param mixed $showCategoryTitle
     */
    public function setShowCategoryTitle($showCategoryTitle)
    {
        $this->showCategoryTitle = $showCategoryTitle;
    }
}
