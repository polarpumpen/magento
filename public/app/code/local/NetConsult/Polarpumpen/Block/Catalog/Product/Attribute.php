<?php

/**
 * @author Adnan Asani <adnan@netconsult.se>
 */
class NetConsult_Polarpumpen_Block_Catalog_Product_Attribute extends Mage_Catalog_Block_Product_View
{
    protected $attributeCode;

    /**
     * @return mixed
     */
    public function getAttributeCode()
    {
        return $this->attributeCode;
    }

    /**
     * @param $attributeCode
     * @return $this
     */
    public function setAttributeCode($attributeCode)
    {
        $this->attributeCode = $attributeCode;
        return $this;
    }

    /**
     * @return string
     */
    protected function _toHtml()
    {
        $data = $this->getProduct()->getData($this->getAttributeCode());

        $templateFilter = Mage::getModel('cms/template_filter');
        return $templateFilter->filter($data);

    }
}