<?php

/**
 * @author Adnan Asani <adnan@netconsult.se>
 */
class NetConsult_Polarpumpen_Block_Catalog_Product_View_Tabs extends Mage_Catalog_Block_Product_View_Tabs
{
    /** @var  Mage_Catalog_Model_Product */
    protected $product;

    public function _construct()
    {
        /** @var Mage_Catalog_Model_Product $product */
        $this->product = Mage::registry('current_product');
    }

    /**
     * @param $alias
     * @param $title
     */
    public function addBlockTab($alias, $title)
    {
        $this->_tabs[] = array(
            'alias' => $alias,
            'title' => $title
        );
    }

    /**
     * If there is any content in the Extra tab attribute group for the product:
     * 1 - Load the attribute
     * 2 - Create a tab block for each attribute
     * 3 - Append as a child to this block
     *
     * @return string
     */
    public function _toHtml()
    {
        if ($this->product) {
            $extraTabs = array();
            $product = $this->product;

            /** @var Mage_Cms_Helper_Data $helper */
            $helper = Mage::helper('cms');

            $setId = $product->getAttributeSetId(); // Attribute set Id

            /** @var Mage_Eav_Model_Resource_Entity_Attribute_Group_Collection $groups */
            $groups = Mage::getModel('eav/entity_attribute_group')
                ->getResourceCollection()
                ->setAttributeSetFilter($setId)
                ->setSortOrder()
                ->load();

            $attributeGroup = null;
            /** @var Mage_Eav_Model_Resource_Entity_Attribute_Group $group */
            foreach($groups as $group) {
                if (strtolower($group->getAttributeGroupName()) == 'extra tabbar') {
                    $attributeGroup = $group;
                    break;
                }
            }

            if ($attributeGroup) {
                $attributes = Mage::getResourceModel('catalog/product_attribute_collection')
                    ->setAttributeGroupFilter($group->getId())
                    ->addVisibleFilter()
                    ->checkConfigurableProducts()
                    ->load();

                if ($attributes->getSize() > 0) {
                    /** @var Mage_Eav_Model_Entity_Attribute $attribute */
                    foreach ($attributes->getItems() as $attribute) {
                        $extraTabs[] = $attribute->getAttributeCode();
                    }
                }
            }

            foreach($extraTabs as $attributeField) {
                if ($product->getData($attributeField) != null && strlen($product->getData($attributeField)) > 0) {
                    /** @var Mage_Eav_Model_Entity_Attribute $attribute */
                    $attribute =  Mage::getModel('eav/entity_attribute')->loadByCode('catalog_product', $attributeField);
                    $this->addBlockTab($attributeField, $attribute->getData('frontend_label'));

                    /** @var NetConsult_Polarpumpen_Block_Catalog_Product_View_Tab $block */
                    $block = $this->getLayout()->createBlock('polarpumpen/catalog_product_view_tab', $attributeField);
                    $block->setContent($helper->getBlockTemplateProcessor()->filter($product->getData($attributeField)));
                    $this->setChild($attributeField, $block);
                }
            }
        }

        return parent::_toHtml();
    }

    function getTabs()
    {
        return $this->_tabs;
    }
}