<?php

/**
 * @author Adnan Asani <adnan@netconsult.se>
 */
class NetConsult_Polarpumpen_Block_Catalog_Category_Content extends Mage_Core_Block_Template
{
    protected $categoryId;

    /**
     * @param mixed $categoryId
     */
    public function setCategoryId($categoryId)
    {
        $this->categoryId = $categoryId;
    }

    /**
     * @return mixed
     */
    public function getCategoryId()
    {
        return $this->categoryId;
    }

    /**
     * @return string
     * @throws Exception
     */
    public function _toHtml()
    {
        /** @var Mage_Catalog_Model_Category $category */
        $category = Mage::getModel('catalog/category')
            ->setStoreId(Mage::app()->getStore()->getId())
            ->load($this->getCategoryId());

        /** @var Mage_Cms_Helper_Data $helper */
        $helper = Mage::helper('cms');

        if (!$category) {
            return '';
        }

        $processor = $helper->getPageTemplateProcessor();
        $html = $processor->filter($category->getData('description'));
        return $html;
    }
}