<?php

class NetConsult_Polarpumpen_Block_Catalog_Category_TopBlock extends Mage_Core_Block_Template
{
    protected $blockType;
    protected $targetUrl;
    protected $imageUrl;
    protected $title;
    protected $description;
    protected $description2;
    protected $description3;
    protected $buttonLabel;
    protected $installationUrl;

    /**
     * @return mixed
     */
    public function getInstallationUrl()
    {
        return $this->installationUrl;
    }

    /**
     * @param mixed $installationUrl
     */
    public function setInstallationUrl($installationUrl)
    {
        $this->installationUrl = $installationUrl;
    }

    /**
     * @return Mage_Core_Block_Abstract
     */
    public function getParent()
    {
        return $this->_parent;
    }

    /**
     * @param Mage_Core_Block_Abstract $parent
     */
    public function setParent($parent)
    {
        $this->_parent = $parent;
    }

    /**
     * @return mixed
     */
    public function getDescription2()
    {
        return $this->description2;
    }

    /**
     * @param mixed $description2
     */
    public function setDescription2($description2)
    {
        $this->description2 = $description2;
    }

    /**
     * @return mixed
     */
    public function getDescription3()
    {
        return $this->description3;
    }

    /**
     * @param mixed $description3
     */
    public function setDescription3($description3)
    {
        $this->description3 = $description3;
    }

    /**
     * @return mixed
     */
    public function getBlockType()
    {
        return $this->blockType;
    }

    /**
     * @param $blockType
     */
    public function setBlockType($blockType)
    {
        $this->blockType = $blockType;
    }

    /**
     * @return mixed
     */
    public function getTargetUrl()
    {
        return $this->targetUrl;
    }

    /**
     * @param mixed $targetUrl
     */
    public function setTargetUrl($targetUrl)
    {
        $this->targetUrl = $targetUrl;
    }

    /**
     * @return mixed
     */
    public function getImageUrl()
    {
        return $this->imageUrl;
    }

    /**
     * @param mixed $imageUrl
     */
    public function setImageUrl($imageUrl)
    {
        $this->imageUrl = $imageUrl;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getButtonLabel()
    {
        return $this->buttonLabel;
    }

    /**
     * @param mixed $buttonLabel
     */
    public function setButtonLabel($buttonLabel)
    {
        $this->buttonLabel = $buttonLabel;
    }
}