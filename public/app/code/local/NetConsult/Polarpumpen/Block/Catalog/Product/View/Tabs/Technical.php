<?php

/**
 * @author Adnan Asani <adnan@netconsult.se>
 */
class NetConsult_Polarpumpen_Block_Catalog_Product_View_Tabs_Technical
    extends Mage_Catalog_Block_Product_View
{

    /**
     * Should be in this exact order.
     * Generellt
     * Värme
     * Uppvärmning av vatten
     * Kyla
     * Utomhusdel
     * Inomhusdel
     * Enhet
     * Varmvattenberedare
     * El
     * Köldmedium
     * Till tekniker
     * Övrigt data
     */
    protected $technicalAttributes = array(
        'generellt',
        'värme',
        'uppvärmning av vatten',
        'kyla',
        'utomhusdel',
        'inomhusdel',
        'enhet',
        'varmvattenberedare',
        'el',
        'köldmedium',
        'till tekniker',
        'övrig data',
        'väv',
        'stativ',
        'styrning',

    );

    /**
     * @return array
     */
    protected function getAttributeConfigs()
    {
        return array(
            // td_v
            'td_v_tot_forbrukning' => array('unit' => 'kW'),
            'td_v_tot_scop' => array('unit' => 'm²'),
            'td_v_kvmin' => array('unit' => 'm²'),
            'td_v_kvmax' => array('unit' => 'm²'),
            'td_v_kapnom' => array('unit' => 'kW'),
            'td_v_kapmin' => array('unit' => 'kW'),
            'td_v_kapmax' => array('unit' => 'kW'),
            'td_v_extinfo' => array('unit' => '-'),
            'td_v_energimarkning' => array('unit' => '"STEM"'),
            'td_v_cop' => array('unit' => 'η'),
            'td_v_scop' => array('unit' => 'η'),
            'td_v_arbeomrade_ute' => array('unit' => '°C'),

            // td_vvb
            'td_vvb_volym' => array('unit' => 'liter'),
            'td_vvb_vattenkap' => array('unit' => 'liter'),
            'td_vvb_till_driftstryck' => array('unit' => 'kPa'),
            'td_vvb_temp_vvatten' => array('unit' => '°C'),
            'td_vvb_korrskydd' => array('unit' => 'ja/nej'),
            'td_vvb_expansionskarl' => array('unit' => 'liter'),
            'td_vvb_buffertnk' => array('unit' => 'liter'),

            // td_k
            'td_k_eer' => array('unit' => 'η'),
            'td_k_energimarkning' => array('unit' => '"STEM"'),
            'td_k_kvmax' => array('unit' => 'm²'),
            'td_k_kvmin' => array('unit' => 'm²'),
            'td_k_kapmax' => array('unit' => 'kW'),
            'td_k_kapnom' => array('unit' => 'kW'),
            'td_k_kapmin' => array('unit' => 'kW'),
            'td_k_seer' => array('unit' => 'η'),
            'td_k_tot_forbrukning' => array('unit' => 'kW'),

            // td_el
            'td_el_stromforsorjning' => array('unit' => 'Ø, V, Hz'),
            'td_el_stromforsorjning_inne' => array('unit' => 'Ø, V, Hz'),
            'td_el_stromforsorjning_ute' => array('unit' => 'Ø, V, Hz'),

            'td_el_stromforbrukning' => array('unit' => 'A'),
            'td_el_stromforbr_inne' => array('unit' => 'A'),
            'td_el_stromforbr_ute' => array('unit' => 'A'),

            'td_el_avsakr_elpatron' => array('unit' => 'A'),
            'td_el_rek_sakring' => array('unit' => 'A'),
            'td_el_rek_sakr_inne' => array('unit' => 'A'),
            'td_el_rek_sakr_ute' => array('unit' => 'A'),

            // td_ute
            'td_ute_vikt' => array('unit' => 'kg'),
            'td_ute_luftstrom' => array('unit' => 'm³/h'),
            'td_ute_ljudniva' => array('unit' => 'dB(A)'),
            'td_ute_dimensioner' => array('unit' => 'mm'),

            // td_inne el. enhet
            'td_inne_diameter' => array('unit' => 'mm'),
            'td_inne_dimensioner' => array('unit' => 'mm'),
            'td_inne_ljudniva' => array('unit' => 'dB(A)'),
            'td_inne_luftflode' => array('unit' => 'l/h'),
            'td_inne_luftstrom' => array('unit' => 'm³/h'),
            'td_inne_reshojd' => array('unit' => 'mm'),
            'td_inne_vikt' => array('unit' => 'kg'),

            // td_km
            'td_km_anslut_ror_g_v' => array('unit' => 'tum'),
            'td_km_mangd' => array('unit' => 'g'),
            'td_km_max_hojd' => array('unit' => 'm'),
            'td_km_pafyllning' => array('unit' => 'g/m'),
            'td_km_ror' => array('unit' => 'm'),
            'td_km_typ' => array('unit' => 'typ'),

            // td_tt
            'td_tt_ansl_koldbarare' => array('unit' => 'mm Cu'),
            'td_tt_ansl_varme' => array('unit' => 'mm Cu'),
            'td_tt_ansl_vatten' => array('unit' => 'mm Cu'),
            'td_tt_ansl_ventilation' => array('unit' => 'mm'),

            // td_uav
            'td_uav_circ_pump_varme' => array('unit' => 'typ'),
            'td_uav_prebryvar_hp' => array('unit' => 'bar'),
            'td_uav_prebryvar_lp' => array('unit' => 'bar'),
            'td_uav_till_driftstryck' => array('unit' => 'bar (kPa)'),
            'td_uav_tryckfall' => array('unit' => 'bar'),
            'td_uav_utg_va_temp' => array('unit' => '°C'),
            'td_uav_utg_va_temp_el' => array('unit' => '°C'),
            'td_uav_vacirkulation' => array('unit' => 'l/min'),

            // td_markis
            'td_markis_vav_vikt' => array('unit' => 'g/m²'),
            'td_markis_vav_ljushardighet' => array('unit' => 'skala 1-8'),
            'td_markis_stativ_kulor' => array('unit' => 'pulverlackerat'),
            'td_markis_stativ_armar_antal' => array('unit' => 'st'),
            'td_markis_bredd' => array('unit' => 'cm'),
            'td_markis_utfall' => array('unit' => 'cm'),

        );
    }

    protected function getUnitByAttributeCode($attributeCode)
    {
        $config = $this->getAttributeConfigs();
        if (array_key_exists($attributeCode, $config)) {
            return array_key_exists('unit', $config[$attributeCode]) ? $config[$attributeCode]['unit'] : '';
        }

        return '';
    }

    /**
     * Return the attribute details of the product
     * @see catalog/product/view/technical_details_tab.phtml
     *
     * @return array
     */
    public function getAttributeList()
    {
        $product = $this->getProduct();
        $attributeGroupCollection = Mage::getResourceModel('eav/entity_attribute_group_collection')
            ->setAttributeSetFilter($product->getAttributeSetId())
            ->load();

        $data = array();
        foreach ($attributeGroupCollection as $attributeGroup) {
            $attributeGroupName = $attributeGroup->getAttributeGroupName();
            if (in_array(mb_strtolower($attributeGroupName, "UTF-8"), $this->technicalAttributes)) {
                $attributesCollection = Mage::getResourceModel('catalog/product_attribute_collection');
                $attributesCollection->setAttributeGroupFilter($attributeGroup->getId());

                $attributes = array();

                foreach ($attributesCollection as $attribute) { /* @var $attribute Mage_Catalog_Model_Resource_Eav_Attribute */
                    if (!$attribute->getIsVisible()) {
                        continue;
                    }

                    $attributeCode = $attribute->getAttributeCode();
                    $attributeValue = null;

                    switch ($attribute->getFrontend()->getInputType()) {
                        case 'multiselect':
                        case 'select':
                        case 'dropdown':
                            $attributeValue = $product->getAttributeText($attributeCode);
                            break;
                        default:
                            $attributeValue = $product->getData($attributeCode);
                    }

                    if ($attributeValue == null) {
                        continue;
                    }

                    if (!in_array($attribute->getFrontend()->getInputType(), array('multiselect', 'select', 'dropdown'))) {
                        switch ($attribute->getData('backend_type')) {
                            case 'decimal':
                                $attributeValue = floatval($attributeValue);
                                break;
                            case 'int':
                                $attributeValue = intval($attributeValue);
                                break;
                        }
                    }

                    $templateFilter = Mage::getModel('cms/template_filter');
                    $attributeValue = $templateFilter->filter($attributeValue);

                    $attributes[$attributeCode] = array(
                        'label' => $attribute->getStoreLabel(),
                        'code' => $attributeCode,
                        'value' => $attributeValue,
                        'unit' => $this->getUnitByAttributeCode($attributeCode),
                    );
                }

                if (count($attributes)) {
                    $attributeGroupIndex = array_search(strtolower($attributeGroupName), $this->technicalAttributes);
                    $data[$attributeGroupIndex] = array(
                        'id' => $attributeGroup->getId(),
                        'label' => $attributeGroupName,
                        'attributes' => $attributes
                    );
                }
            }
        }

        // Handle concatenations
        $attributeConcatenations = array(
            array(
                'label' => 'Kvadratmeter (Min - Max)',
                'attributes' => array(
                    'td_v_kvmin',
                    'td_v_kvmax'
                )
            ),
            array(
                'label' => 'Kapacitet (Max / Nom / Min)',
                'attributes' => array(
                    "td_v_kapmax",
                    "td_v_kapnom",
                    "td_v_kapmin"
                )
            ),
            array(
                'label' => 'Kvadratmeter (Min - Max)',
                'attributes' => array(
                    'td_k_kvmin',
                    'td_k_kvmax'
                )
            ),
            array(
                'label' => 'Kapacitet (Max / Nom / Min)',
                'attributes' => array(
                    "td_k_kapmax",
                    "td_k_kapnom",
                    "td_k_kapmin"
                )
            )
        );

        foreach ($attributeConcatenations as $attributeConcatenation) {
            $label = $attributeConcatenation['label'];
            $attributeCombinations = $attributeConcatenation['attributes'];

            foreach ($attributeCombinations as $attributeCode) {
                foreach ($data as $id => $config) {
                    if (array_key_exists($attributeCode, $config['attributes'])) {
                        $values = array();
                        $invalid = 0;
                        foreach ($attributeCombinations as $attributeCode2) {
                            $value = floatval($config['attributes'][$attributeCode2]['value']);
                            if (!$value) {
                                $value = "-";
                                $invalid++;
                            }
                            $values[] = $value;

                            if ($attributeCode != $attributeCode2) {
                                unset($data[$id]['attributes'][$attributeCode2]);
                            }
                        }

                        if ($invalid == count($values)) {
                            unset($data[$id]['attributes'][$attributeCode]);
                        } else {
                            $data[$id]['attributes'][$attributeCode]['label'] = $label;
                            $data[$id]['attributes'][$attributeCode]['value'] = join(' / ', $values);
                        }
                        break 2;
                    }
                }
            }
        }

        $attributeKeys = $this->technicalAttributes;
        usort($data, function($previousItem, $currentItem) use ($attributeKeys) {
            $previousIndex = array_search(strtolower($previousItem['label']), $attributeKeys);
            $currentIndex  = array_search(strtolower($currentItem['label']), $attributeKeys);
            return ($previousIndex < $currentIndex) ? -1 : 1;
        });
        return $data;
    }
}