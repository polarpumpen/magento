<?php

/**
 * Class NetConsult_Polarpumpen_Block_Catalog_Product_View_Tab
 * @author Adnan Asani <adnan@netconsult.se>
 */
class NetConsult_Polarpumpen_Block_Catalog_Product_View_Tab extends Mage_Catalog_Block_Product_View
{
    /** @var string */
    protected $content;

    public function _construct()
    {
        $this->setTemplate("catalog/product/view/tab.phtml");
        parent::_construct();
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }
}