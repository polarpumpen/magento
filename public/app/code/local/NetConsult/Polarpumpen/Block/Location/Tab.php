<?php

/**
 * @author Adnan Asani <adnan@netconsult.se>
 */
class NetConsult_Polarpumpen_Block_Location_Tab extends Mage_Core_Block_Template
{
    protected function _construct()
    {
        $this->setTemplate("polarpumpen/location/tab.phtml");
        parent::_construct();
    }
}