<?php

/**
 * Class NetConsult_Polarpumpen_Helper_Downloads_File
 */
class NetConsult_Polarpumpen_Helper_Downloads_File extends Mage_Core_Helper_Abstract
{
    /**
     * @param MageWorx_Downloads_Model_Files $item
     * @return bool
     */
    public function isNewTab(MageWorx_Downloads_Model_Files $item)
    {
        if (strlen($item->getUrl()) < 1) {
            return false;
        }

        if ($item->getType() == 'pdf') {
            return true;
        }

        if (substr($item->getUrl(), 0, 4) == 'http') {
            return true;
        }

        return false;
    }
}