<?php

class NetConsult_Polarpumpen_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * Get HTML of all child blocks with given ID
     *
     * @param $block Current block object
     * @param string $staticBlockId ID of static blocks
     * @param bool $auto Automatically align static blocks vertically
     * @return string HTML output
     *
     */
    public function getFormattedBlocks($block, $staticBlockId, $auto = true)
    {
        $colHtml = array();
        $html = '';
        $largerBlockIdentifier = sprintf("%s%u", $staticBlockId, 2);
        $gridClassBase = 'grid12-';
        $defaultSize = 2;

        for ($i = 1; $i < 7; $i++) {
            $blockId = sprintf("%s%u", $staticBlockId, $i);
            if ($tmp = $block->getChildHtml($blockId)) {
                $colHtml[$blockId] = array(
                    'size' => $defaultSize,
                    'content' => $tmp,
                );
            }
        }

        $colCount = count($colHtml);
        if ($colCount) {
            // Default Size
            $size = $auto ? (int)(12 / $colCount) : $defaultSize;
            if (array_key_exists($largerBlockIdentifier, $colHtml)) {
                $largerBlockSize = $auto ? (int)($size * 2) : $defaultSize;
                if ($largerBlockSize > 8) {
                    $largerBlockSize = 8;
                }
                $colHtml[$largerBlockIdentifier]['size'] = $largerBlockSize;
            }

            foreach($colHtml as $blockId => $blockInfo) {
                if ($blockId !== $largerBlockIdentifier) {
                    if (array_key_exists($largerBlockIdentifier, $colHtml) && $auto) {
                        $autoSize = (int)((12 - $colHtml[$largerBlockIdentifier]['size']) / ($colCount - 1));
                        if ($autoSize <= $defaultSize ) {
                            $autoSize = $defaultSize;
                        }
                        $blockInfo['size'] = $autoSize;
                    } else {
                        $blockInfo['size'] = $defaultSize;
                    }
                }

                $gridClass = sprintf("%s%u", $gridClassBase, $blockInfo['size']);
                $html .= '<div class="' . $gridClass. '">';
                $html .= '	<div class="std">' . $blockInfo['content'] . '</div>';
                $html .= '</div>';
            }

        }
        return $html;
    }
}