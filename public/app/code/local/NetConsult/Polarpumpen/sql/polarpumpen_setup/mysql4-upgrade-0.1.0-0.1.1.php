<?php

$installer = $this;
/* @var $installer NetConsult_Marketplace_Model_Resource_Setup */

$installer->startSetup();

$installer->addAttribute(Mage_Catalog_Model_Category::ENTITY, 'more_information', array(
    'group'            => 'Other',
    'input'            => 'textarea',
    'type'             => 'text',
    'label'            => 'More Information',
    'visible'          => true,
    'required'         => false,
    'user_defined'     => true,
    'default'          => "",
    'global'           => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
));

$installer->updateAttribute(Mage_Catalog_Model_Category::ENTITY, 'more_information', 'is_wysiwyg_enabled', 1);
$installer->updateAttribute(Mage_Catalog_Model_Category::ENTITY, 'more_information', 'is_html_allowed_on_front', 1);

$installer->endSetup();