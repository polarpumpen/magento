<?php
$installer = $this;
/* @var $installer Mage_Eav_Model_Entity_Setup */

$installer->startSetup();

/**
 * Product categories
 */


/**
 * @param $labelText
 * @param $attributeCode
 * @param $values
 * @param Mage_Eav_Model_Entity_Setup $installer
 * @param null $productTypes
 * @param string $attributeGroupName
 * @throws Exception
 */
function createAttribute($labelText, $attributeCode, $values, Mage_Eav_Model_Entity_Setup $installer, $productTypes = null, $attributeGroupName = 'Polarpumpen')
{
    $labelText = trim($labelText);
    $attributeCode = trim($attributeCode);

    if ($labelText == '' || $attributeCode == '') {
        throw new Exception("Can't import the attribute with an empty label or code.  LABEL= [$labelText]  CODE= [$attributeCode]");
    }

    // Default options that can be overridden
    $data = array(
        'type' => 'varchar',
        'input' => 'text',
        'source' => null,
        'unique' => 0,
        'required' => 0,
        'searchable' => 0,
        'visible_on_front' => 0,
        'visible_in_advanced_search' => 0,
        'default' => '',
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
        'frontend_class' => '',
        'note' => '',
        'position' => 0,
        'backend' => null,
        'is_html_allowed_on_front' => 0
    );

    if (array_key_exists('frontend_input_renderer', $values)) {
        $data['frontend_input_renderer'] = $values['frontend_input_renderer'];
    }

    foreach ($values as $key => $newValue) {
        if (!array_key_exists($key, $data)) {
            throw new Exception("Attribute feature [$key] is not valid.");
        } else {
            $data[$key] = $newValue;
        }
    }

    if ($productTypes === null) {
        $productTypes = array(
            'simple','configurable','grouped','bundle'
        );
    }

    $readOnlyData = array(
        'apply_to' => join(',', $productTypes),
        'label' => $labelText,
        'user_defined' => 0,
    );

    $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, $attributeCode, array_merge($data, $readOnlyData));

    $attribute = $installer->getAttribute('catalog_product', $attributeCode);
    if (!$attribute) {
        throw new Exception("Cant find the attribute when it should have been created");
    }
    $attributeId = $attribute['attribute_id'];

    // Need to have this to get the catalog entity specific attributes to work correctly
    $installer->updateAttribute(Mage_Catalog_Model_Product::ENTITY, $attributeCode, array_merge($data, $readOnlyData));

    $allAttributeSetIds = $installer->getAllAttributeSetIds('catalog_product');
    foreach ($allAttributeSetIds as $attributeSetId) {
        $attributeGroup = $installer->getAttributeGroup(
            'catalog_product',
            $attributeSetId,
            $attributeGroupName
        );

        if (!$attributeGroup) {
            $installer->addAttributeGroup('catalog_product', $attributeSetId, $attributeGroupName);
            $attributeGroup = $installer->getAttributeGroup(
                'catalog_product',
                $attributeSetId,
                $attributeGroupName
            );

            if (!$attributeGroup) {
                throw new Exception('Unable to create new attribute group');
            }
        }

        $attributeGroupId = $attributeGroup['attribute_group_id'];
        $installer->addAttributeToSet(
            'catalog_product',
            $attributeSetId,
            $attributeGroupId,
            $attributeId
        );
    }
}

/**
 * Create attribute sets
 */
$installer->addAttributeSet(Mage_Catalog_Model_Product::ENTITY, 'Luft-Luft');
$installer->addAttributeSet(Mage_Catalog_Model_Product::ENTITY, 'Luft-Vatten');
$installer->addAttributeSet(Mage_Catalog_Model_Product::ENTITY, 'Frånluft');
$installer->addAttributeSet(Mage_Catalog_Model_Product::ENTITY, 'Bergvärme');
$installer->addAttributeSet(Mage_Catalog_Model_Product::ENTITY, 'Poolvärme');
$installer->addAttributeSet(Mage_Catalog_Model_Product::ENTITY, 'Varmvattenberedare');

/**
 * Create attributes
 */
createAttribute('Arbetsområde utomhustemperatur', 'arbetsomrade_utomhustemperatur', array(
    'type'     => 'varchar',
    'input'    => 'text',
    'is_html_allowed_on_front' => 1,
    'position' => 10
), $installer, null, 'Värme');

createAttribute('Kapacitet (Max/Nom./Min)', 'kapacitet_kw', array(
    'type'     => 'varchar',
    'input'    => 'text',
    'is_html_allowed_on_front' => 1,
    'position' => 20
), $installer, null, 'Värme');

createAttribute('Totalförbrukning', 'totalforbrukning', array(
    'type'     => 'varchar',
    'input'    => 'text',
    'is_html_allowed_on_front' => 1,
    'position' => 30
), $installer, null, 'Värme');

createAttribute('Garanterad värmedrift', 'garanterad_varmedrift', array(
    'type'     => 'varchar',
    'input'    => 'text',
    'is_html_allowed_on_front' => 1,
    'position' => 40
), $installer, null, 'Värme');

createAttribute('COP', 'cop', array(
    'type'     => 'varchar',
    'input'    => 'text',
    'is_html_allowed_on_front' => 1,
    'position' => 50
), $installer, null, 'Värme');

createAttribute('SCOP', 'scop', array(
    'type'     => 'varchar',
    'input'    => 'text',
    'is_html_allowed_on_front' => 1,
    'position' => 60
), $installer, null, 'Värme');

createAttribute('Energimärkning värme', 'energimarkning_varme', array(
    'type'     => 'varchar',
    'input'    => 'text',
    'is_html_allowed_on_front' => 1,
    'position' => 70
), $installer, null, 'Värme');

/**
 * Left out for the moment until clarified.
 */
//createAttribute('Extra info kapacitet värme', 'extra_info_kapacitet_varme', array(
//    'type'     => 'varchar',
//    'input'    => 'text',
//    'is_html_allowed_on_front' => 1,
//    'position' => 80
//), $installer, null, 'Värme');


$installer->endSetup();