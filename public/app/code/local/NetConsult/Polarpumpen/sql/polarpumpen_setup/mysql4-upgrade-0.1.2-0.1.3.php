<?php

$installer = $this;
/* @var $installer NetConsult_Marketplace_Model_Resource_Setup */

$installer->startSetup();

function updateFrontendClassesForPriceFields() {
    $connection = _getConnection('core_write');
    $sql = "SELECT * FROM " . _getTableName('eav_attribute') . " WHERE attribute_code LIKE 'td_%' AND frontend_input = 'price' AND frontend_class = 'validate-digits'";
    $rows = $connection->fetchAll($sql);
    foreach ($rows as $row) {
        // Change frontend-class
        $sql = 'UPDATE ' . _getTableName('eav_attribute') . ' SET frontend_class=? WHERE attribute_code=?';
        $connection->query($sql, array(
            'validate-number',
            $row['attribute_code']
        ));
    }
}

function _getTableName($tableName) {
    return Mage::getSingleton('core/resource')->getTableName($tableName);
}

function _getConnection($type = 'core_read') {
    return Mage::getSingleton('core/resource')->getConnection($type);
}

#simply call as:
updateFrontendClassesForPriceFields();

$installer->endSetup();