<?php

$installer = $this;
/* @var $installer NetConsult_Marketplace_Model_Resource_Setup */

$installer->startSetup();

function copyVarcharToDecimal() {
    $connection = _getConnection('core_write');
    $attributeIds = (string)implode(',', _getAttributeIds());
    $entityTypeId = (int)_getEntityTypeId();
    $sql = 'SELECT * FROM ' . _getTableName('catalog_product_entity_varchar') . ' WHERE attribute_id IN (' . $attributeIds . ') AND entity_type_id = ' . $entityTypeId;
    $rows = $connection->fetchAll($sql);
    foreach ($rows as $row) {
        $checkIfDecimalValueExists = _checkIfDecimalValueExists($row);
        if (!$checkIfDecimalValueExists) {
            $sql = 'INSERT INTO ' . _getTableName('catalog_product_entity_decimal') . ' (`entity_type_id`,`attribute_id`,`store_id`,`entity_id`,`value`)
                    VALUES (?,?,?,?,?)';
            $price = $row['value'];
            $price = trim(str_replace(',', '.', $price));  //in case if values are stored in dutch format
            $connection->query($sql, array($row['entity_type_id'], $row['attribute_id'], $row['store_id'], $row['entity_id'], $price));
        }

        $sql = 'DELETE FROM ' . _getTableName('catalog_product_entity_varchar') . ' WHERE value_id = ?';
        $connection->query($sql, $row['value_id']);

        // Change attribute type
        $sql = 'UPDATE ' . _getTableName('eav_attribute') . ' SET backend_model=?, backend_type=?, frontend_input=? WHERE attribute_id=?';
        $connection->query($sql, array(
            'catalog/product_attribute_backend_price',
            'decimal',
            'price',
            $row['attribute_id']
        ));
    }
}

function _getTableName($tableName) {
    return Mage::getSingleton('core/resource')->getTableName($tableName);
}

function _getConnection($type = 'core_read') {
    return Mage::getSingleton('core/resource')->getConnection($type);
}

function _getAttributeIds() {
    $_attributeCodes = array(
        "'td_v_kvmax'",
        "'td_v_kvmin'",
        "'td_v_kapmax'",
        "'td_v_kapnom'",
        "'td_v_kapmin'",
        "'td_v_scop'",
        "'td_k_kvmax'",
        "'td_k_kvmin'",
        "'td_k_kapmin'",
        "'td_k_kapnom'",
        "'td_k_kapmax'",
        "'td_k_seer'",

    );
    $attributeCodes = (string)implode(',', $_attributeCodes);

    $connection = _getConnection('core_read');
    $sql = "SELECT attribute_id
            FROM " . _getTableName('eav_attribute') . "
            WHERE attribute_code
            IN (
                " . $attributeCodes . "
            )";
    return $connection->fetchCol($sql);
}

function _getEntityTypeId()
{
    $connection = _getConnection('core_read');
    $sql = "SELECT entity_type_id FROM " . _getTableName('eav_entity_type') . " WHERE entity_type_code = 'catalog_product'";
    return $connection->fetchOne($sql);
}

function _checkIfDecimalValueExists($row)
{
    $connection = _getConnection('core_write');
    $sql = 'SELECT COUNT(*) FROM ' . _getTableName('catalog_product_entity_decimal') . ' WHERE attribute_id = ? AND entity_type_id = ? AND store_id = ? AND entity_id = ?';
    $result = $connection->fetchOne($sql, array($row['attribute_id'], $row['entity_type_id'], $row['store_id'], $row['entity_id']));
    return $result > 0 ? true : false;
}

#simply call as:
copyVarcharToDecimal();

$installer->endSetup();