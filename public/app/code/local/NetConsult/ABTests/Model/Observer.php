<?php

class NetConsult_ABTests_Model_Observer
{
    /**
     * @param Varien_Event_Observer $observer
     */
    public function controllerActionLayoutLoadBefore(Varien_Event_Observer $observer)
    {
        if (!$this->getModuleHelper()->isEnabled()) {
            return;
        }

        /** @var $layout Mage_Core_Model_Layout */
        $layout = $observer->getEvent()->getLayout();

        $chosenVariation = $this->getModuleHelper()->getChosenVariation();

        $chosenVariationType = var_export($chosenVariation, true);
        Mage::app()->getResponse()->setHeader('X-Experiment', 'Variation: ' . $chosenVariationType);
        $layout->getUpdate()->addHandle('abtest_case_'.$chosenVariation);
    }

    /**
     * @return NetConsult_ABTests_Helper_Data
     */
    protected function getModuleHelper()
    {
        return Mage::helper('abtests');
    }
}
