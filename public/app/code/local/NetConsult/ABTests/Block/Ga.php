<?php

class NetConsult_ABTests_Block_Ga extends Mage_GoogleAnalytics_Block_Ga
{

    public function getCacheKeyInfo()
    {
        $helper = $this->getModuleHelper();

        return array(
            self::class,
            Mage::app()->getStore()->getId(),
            (int)Mage::app()->getStore()->isCurrentlySecure(),
            Mage::getDesign()->getPackageName(),
            Mage::getDesign()->getTheme('template'),
            $helper->getExperimentId(),
            $helper->getChosenVariation(),
        );
    }


    /**
     * Render regular page tracking javascript code
     * The custom "page name" may be set from layout or somewhere else. It must start from slash.
     *
     * @param string $accountId
     * @return string
     */
    protected function _getPageTrackingCodeUniversal($accountId)
    {
        return "\n".
        "ga('create', '{$this->jsQuoteEscape($accountId)}', 'auto'); "."\n".
        $this->getExperimentationCode()."\n".
        $this->_getAnonymizationCode()."\n".
        "ga('send', 'pageview'); "."\n";
    }

    /**
     * @return string
     */
    protected function getExperimentationCode()
    {
        $helper = $this->getModuleHelper();

        if (!$this->getModuleHelper()->isEnabled()) {
            return '';
        }

        // The Id of the experiment running on the page
        $experimentId = $helper->getExperimentId();

        // The variation chosen for the user
        $chosenVariation = $helper->getChosenVariation();

        return "
        ga('set', 'expId', '$experimentId');
        ga('set', 'expVar', '$chosenVariation');
        ";
    }

    /**
     * @return NetConsult_ABTests_Helper_Data
     */
    protected function getModuleHelper()
    {
        return Mage::helper('abtests');
    }
}
