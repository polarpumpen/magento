<?php

class NetConsult_ABTests_Helper_Data extends Mage_Core_Helper_Abstract
{
    const SESSION_KEY_CHOSEN_VARIATION = 'abtest_chosenvariation';
    const CONFIG_KEY_ENABLED           = 'netconsult_abtests/general/enabled';
    const CONFIG_KEY_EXPERIMENT_ID     = 'netconsult_abtests/general/experiment_id';
    const CONFIG_KEY_VARIATIONS        = 'netconsult_abtests/general/variations';
    const CONFIG_KEY_ALWAYS_SHOW_VAR   = 'netconsult_abtests/general/always_show_variation';

    /**
     * @return bool
     */
    public function isEnabled()
    {
        return (bool) Mage::getStoreConfig(static::CONFIG_KEY_ENABLED);
    }

    /**
     * @return mixed
     */
    public function getExperimentId()
    {
        return Mage::getStoreConfig(static::CONFIG_KEY_EXPERIMENT_ID);
    }

    /**
     * @return mixed
     */
    public function getAlwaysShowVariation()
    {
        return Mage::getStoreConfig(static::CONFIG_KEY_ALWAYS_SHOW_VAR);

    }

    /**
     * @return int
     */
    public function getChosenVariation()
    {
        // Check if we always should show a specific variation
        if ($this->getAlwaysShowVariation() !== null && $this->getAlwaysShowVariation() !== "") {
            return $this->getAlwaysShowVariation();
        }

        $session = $this->getSession();
        $sessionKeySuffix = $this->getExperimentId();

        $variation = $session->getData(static::SESSION_KEY_CHOSEN_VARIATION.$sessionKeySuffix);

        if ($variation === null) {
            $variation = $this->getRandomVariationFromConfig();
            $session->setData(static::SESSION_KEY_CHOSEN_VARIATION.$sessionKeySuffix, $variation);
        }

        return $variation;
    }

    /**
     * @return bool|string
     */
    protected function getRandomVariationFromConfig()
    {
        $configValue = Mage::getStoreConfig(static::CONFIG_KEY_VARIATIONS);
        $variations = explode(',', $configValue);

        if (!$variations) {
            return false;
        }

        return $variations[array_rand($variations)];
    }

    /**
     * @return Mage_Core_Model_Session
     */
    protected function getSession()
    {
        return Mage::getSingleton("core/session", array("name" => "frontend"));
    }

}
