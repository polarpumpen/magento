<?php
$this->startSetup();


$fieldsSql = 'SHOW COLUMNS FROM ' . $this->getTable('salesrule/rule');
$cols = $this->getConnection()->fetchCol($fieldsSql);

if (!in_array('netconsult_promo_sku', $cols)){
    $this->run("ALTER TABLE `{$this->getTable('salesrule/rule')}` ADD `netconsult_promo_sku` TEXT");
}

if (!in_array('netconsult_promo_type', $cols)){
    $this->run("ALTER TABLE `{$this->getTable('salesrule/rule')}` ADD `netconsult_promo_type` SMALLINT(4) NOT NULL AFTER `netconsult_promo_sku`");
}

$this->endSetup();