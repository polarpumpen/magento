<?php

class NetConsult_Promo_Model_Admin_Observer
{

    public function salesRulePrepareForm(Varien_Event_Observer $observer)
    {
        $actionsSelect = $observer->getForm()->getElement('simple_action');
        if ($actionsSelect) {
            $values = $actionsSelect->getValues();
            $values[] = array(
                'value' => 'netconsult_promo_items',
                'label' => Mage::helper('ncpromo')->__('Auto add promo items with products'),

            );

            $actionsSelect->setValues($values);


            $fieldset = $observer->getForm()->getElement('action_fieldset');

            /*$fieldset->addField('netconsult_promo_type', 'select', array(
                'name' => 'netconsult_promo_type',
                'label' => Mage::helper('ncpromo')->__('Promo Type'),
                'values' => array(
                    0 => Mage::helper('ncpromo')->__('All SKUs below'),
                    1 => Mage::helper('ncpromo')->__('One of the SKUs below')
                ),
            ),
                'discount_amount'
            );
            */
            $fieldset->addField('netconsult_promo_sku', 'text', array(
                'name' => 'netconsult_promo_sku',
                'label' => Mage::helper('ncpromo')->__('Promo Items'),
                'note' => Mage::helper('ncpromo')->__('Comma separated list of the SKUs (,)'),
            ),
                'discount_amount'
            );

        }

        return $this;
    }

}