<?php

class NetConsult_Promo_Model_Observer
{

    protected $_rules;
    protected $_productsToAdd = array();
    protected $_salesRuleIsHandled = array();

    protected function _setItemPrefix($items)
    {
        foreach ($items as $item) {
            $buyRequest = $item->getBuyRequest();

            if (isset($buyRequest['options']['netconsult_promo_rule_id'])) {
                $rule = $this->_loadRule($buyRequest['options']['netconsult_promo_rule_id']);
                $ruleLabel = $rule->getStoreLabel();
                $rulePrefix = !empty($ruleLabel) ? $ruleLabel : '';
                $rulePrefix = "Is Promo: ";
                $item->setName($rulePrefix . ' ' . $item->getName());
            }
        }
    }

    public function onCartItemUpdateBefore(Varien_Event_Observer $observer)
    {
        $request = Mage::app()->getRequest();

        $id = (int)$request->getParam('id');
        $item = Mage::getSingleton('checkout/cart')->getQuote()->getItemById($id);

        if ($item->getId() && $this->_quoteItemisPromo($item)) {
            $options = $request->getParam('options');
            $options['netconsult_promo_rule_id'] = $this->_quoteItemSalesRuleId($item);
            $request->setParam('options', $options);
        }
    }

    public function onAddressCollectTotalsAfter($observer)
    {
        $quote = $observer->getQuoteAddress()->getQuote();

        $items = $quote->getAllItems();

        foreach ($items as $item) {
            if ($this->_quoteItemIsPromo($item)) {
                $item->isDeleted(false);
                $this->_resetWeee($item);
            }
        }

        $productsToAdd = $this->_getPromoItems();
        if (is_array($productsToAdd)) {
            unset($productsToAdd['_groups']);

            foreach ($items as $item) {
                /** @var $item Mage_Sales_Model_Quote_Item */

                $sku = $item->getProduct()->getData('sku');

                if (!isset($productsToAdd[$sku])) {
                    continue;
                }

                $qtyIncreased = isset($productsToAdd[$sku]['qtyIncreased']) ? $productsToAdd[$sku]['qtyIncreased'] : false;

                if ($this->_quoteItemIsPromo($item)) {
                    if (!$qtyIncreased) {
                        unset($productsToAdd[$sku]); // to allow to decrease qty
                    } else {
                        $productsToAdd[$sku]['qty'] -= $item->getQty();
                    }
                }
            }

            $deletedProducts = $this->_getDeletedItems();

            $this->_productsToAdd = array();
            foreach ($productsToAdd as $sku => $item) {
                if ($item['qty'] > 0 && $item['auto_add'] && !isset($deletedProducts[$sku])) {
                    $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $sku);

                    if (isset($this->_productsToAdd[$product->getId()])) {
                        $this->_productsToAdd[$product->getId()]['qty'] += $item['qty'];
                    } else {
                        $this->_productsToAdd[$product->getId()] = array(
                            'product' => $product,
                            'qty' => $item['qty']
                        );
                    }
                }
            }
        }
    }

    public function onCollectTotalsAfter($observer)
    {
        if (!Mage::getSingleton('checkout/session')->hasQuote()) {
            return;
        }

        $allowedItems = $this->_getPromoItems();
        $cart = Mage::getSingleton('checkout/cart');
        /** @var $cart Mage_Checkout_Model_Cart */

        foreach ($this->_productsToAdd as $item) {
            $product = $item['product'];
            $ruleId = $allowedItems[$product->getSku()] ? $allowedItems[$product->getSku()]['rule_id'] : null;

            Mage::helper('ncpromo/product')->addProduct(
                $product,
                false, false, false, $ruleId,
                $item['qty']
            );
        }

        $this->_productsToAdd = array();
        foreach ($observer->getQuote()->getAllItems() as $item) {
            if ($this->_quoteItemisPromo($item)) {

                $ruleLabel = $this->_quoteItemSalesRule($item)->getStoreLabel();

                if ($item->getParentItemId())
                    continue;

                $sku = $item->getProduct()->getData('sku');

                if (isset($allowedItems['_groups'][$this->_quoteItemSalesRuleId($item)])) { // Add one of
                    if ($allowedItems['_groups'][$this->_quoteItemSalesRuleId($item)]['qty'] <= 0) {
                        $this->_removeQuoteItem($cart, $item);
                    } else if ($item->getQty() > $allowedItems['_groups'][$this->_quoteItemSalesRuleId($item)]['qty']) {
                        $item->setQty($allowedItems['_groups'][$this->_quoteItemSalesRuleId($item)]['qty']);
                    }

                    if ($ruleLabel) {
                        $item->setMessage($ruleLabel);
                    }

                    $allowedItems['_groups'][$this->_quoteItemSalesRuleId($item)]['qty'] -= $item->getQty();
                } else if (isset($allowedItems[$sku])) {  // Add all of
                    if ($allowedItems[$sku]['qty'] <= 0) {
                        $this->_removeQuoteItem($cart, $item);
                    } else if ($item->getQty() > $allowedItems[$sku]['qty']) {
                        $item->setQty($allowedItems[$sku]['qty']);
                    }

                    if ($ruleLabel) {
                        $item->setMessage($ruleLabel);
                    }

                    $allowedItems[$sku]['qty'] -= $item->getQty();
                } else {
                    $this->_removeQuoteItem($cart, $item);
                }
            }
        }

        $this->_updateQuoteTotalQty($observer->getQuote());

        if (!$cart->getItemsQty()) {
            $cart->truncate();
        }
    }

    protected function _removeQuoteItem(Mage_Checkout_Model_Cart $cart, Mage_Sales_Model_Quote_Item $item)
    {
        $cart->removeItem($item->getId());
        $cart->getQuote()->setTotalsCollectedFlag(false);
        $cart->getQuote()->getShippingAddress()->unsetData('cached_items_nonnominal');
        $cart->getQuote()->collectTotals();
        $cart->getQuote()->save();
    }

    public function onCheckoutCartUpdateItemsBefore()
    {
        Mage::getSingleton('checkout/session')->setNetConsultPromoItemsBefore(
            Mage::getSingleton('checkout/session')->getNetConsultPromoItems()
        );
    }

    public function onCollectTotalsBefore()
    {
        $this->_reset();
    }

    /**
     * Process quote item validation and discount calculation
     * @param   Varien_Event_Observer $observer
     * @return $this
     */
    public function handleSalesRuleValidation($observer)
    {
        $rule = $observer->getEvent()->getRule();

        if (isset($this->_salesRuleIsHandled[$rule->getId()])) {
            return $this;
        }


        $address = $observer->getEvent()->getAddress();

        if (!in_array($rule->getSimpleAction(), array('netconsult_promo_items'))) {
            return $this;
        }

        $this->_salesRuleIsHandled[$rule->getId()] = true;

        $promoSku = $rule->getNetconsultPromoSku();
        if (!$promoSku) {
            return $this;
        }

        $quote = $observer->getEvent()->getQuote();

        $qty = $this->_getFreeItemsQty($rule, $quote, $address);

        $promoSku = explode(',', $promoSku);
        foreach ($promoSku as $sku) {
            $sku = trim($sku);
            if (!$sku) {
                continue;
            }

            Mage::helper('ncpromo/product')->addPromoItem($sku, $qty, $rule->getId());
        }

        return $this;
    }

    /**
     * Mark item as deleted to prevent it's auto-addition
     * @param $observer
     */
    public function onQuoteRemoveItem($observer)
    {
        $action = Mage::app()->getRequest()->getActionName();
        if (!in_array($action, array('delete', 'ajaxDelete'))) {
            return;
        }

        $id = (int)Mage::app()->getRequest()->getParam('id');

        $item = $observer->getEvent()->getQuoteItem();

        if (($item->getId() == $id) && $this->_quoteItemisPromo($item) && !$item->getParentId()) {
            Mage::helper('ncpromo/product')->deleteProduct($item->getProduct()->getData('sku'));
        }
    }

    // find qty
    // (for the whole cart it is $rule->getDiscountQty()
    // for items it is (qty * (number of matched non-free items) / step)
    protected function _getFreeItemsQty($rule, $quote, $address)
    {
        $amount = max(1, $rule->getDiscountAmount());

        $qty = 0;

        $step = max(1, $rule->getDiscountStep());
        foreach ($quote->getItemsCollection() as $item) {
            if (!$item)
                continue;

            if ($this->_quoteItemisPromo($item))
                continue;

            if ($this->_skip($item, $address)) {
                continue;
            }

            if (!$rule->getActions()->validate($item)) {
                continue;
            }
            if ($item->getParentItemId()) {
                continue;
            }
            if ($item->getProduct()->getParentProductId()) {
                continue;
            }

            $qty = $qty + $item->getQty();
        }

        $qty = floor($qty / $step) * $amount;
        $max = $rule->getDiscountQty();
        if ($max) {
            $qty = min($max, $qty);
        }
        return $qty;
    }

    /**
     * A place to indicate different way to skip an item to be evaluated
     *
     * @param Mage_Sales_Model_Quote_Item $item
     * @param $address
     *
     * @return bool
     */
    protected function _skip(Mage_Sales_Model_Quote_Item $item, $address)
    {
        return false;
    }

    protected function _updateQuoteTotalQty(Mage_Sales_Model_Quote $quote)
    {
        $quote->setItemsCount(0);
        $quote->setItemsQty(0);
        $quote->setVirtualItemsQty(0);

        foreach ($quote->getAllVisibleItems() as $item) {
            if ($item->getParentItem()) {
                continue;
            }

            $children = $item->getChildren();
            if ($children && $item->isShipSeparately()) {
                foreach ($children as $child) {
                    if ($child->getProduct()->getIsVirtual()) {
                        $quote->setVirtualItemsQty($quote->getVirtualItemsQty() + $child->getQty() * $item->getQty());
                    }
                }
            }

            if ($item->getProduct()->getIsVirtual()) {
                $quote->setVirtualItemsQty($quote->getVirtualItemsQty() + $item->getQty());
            }
            $quote->setItemsCount($quote->getItemsCount() + 1);
            $quote->setItemsQty((float)$quote->getItemsQty() + $item->getQty());
        }
    }

    protected function _resetWeee(&$item)
    {
        Mage::helper('weee')->setApplied($item, array());

        $item->setBaseWeeeTaxDisposition(0);
        $item->setWeeeTaxDisposition(0);

        $item->setBaseWeeeTaxRowDisposition(0);
        $item->setWeeeTaxRowDisposition(0);

        $item->setBaseWeeeTaxAppliedAmount(0);
        $item->setBaseWeeeTaxAppliedRowAmount(0);

        $item->setWeeeTaxAppliedAmount(0);
        $item->setWeeeTaxAppliedRowAmount(0);
    }

    protected function _getPromoItems()
    {
        return Mage::helper('ncpromo/product')->getPromoItems();
    }

    protected function _getDeletedItems()
    {
        $deletedItems = Mage::getSingleton('checkout/session')->getNetConsultPromoDeletedItems();

        if (!$deletedItems) {
            $deletedItems = array();
        }

        return $deletedItems;
    }

    /**
     * Don't apply any discounts to free items
     * @param $observer
     */
    public function onProductAddAfter($observer)
    {
        $items = $observer->getItems();

        $this->_setItemPrefix($items);

        foreach ($items as $item) {
            /** @var $item Mage_Sales_Model_Quote_Item */
            if ($this->_quoteItemisPromo($item)) {
                $item->setNoDiscount(true);
                $item->getProduct()->setIsSuperMode(true);
                $item->setCustomPrice(0);
                $item->setOriginalCustomPrice(0);
            }
        }
    }

    protected function _getAllItems($address)
    {
        $items = $address->getAllNonNominalItems();
        if (!$items) { // CE 1.3 version
            $items = $address->getAllVisibleItems();
        }
        if (!$items) { // cart has virtual products
            $cart = Mage::getSingleton('checkout/cart');
            $items = $cart->getItems();
        }
        return $items;
    }

    protected function _loadRule($id)
    {
        if (!isset($this->_rules[$id])) {
            $this->_rules[$id] = Mage::getModel('salesrule/rule')->load($id);
        }
        return $this->_rules[$id];
    }

    protected function _reset()
    {
        Mage::helper('ncpromo/product')->reset();
    }

    public function quoteItemIsPromo(Mage_Sales_Model_Quote_Item $item)
    {
        return $this->_quoteItemIsPromo($item);
    }

    /**
     * Check if quote item is promo
     *
     * @param Mage_Sales_Model_Quote_Item $item
     * @return bool
     */
    protected function _quoteItemIsPromo(Mage_Sales_Model_Quote_Item $item)
    {
        if (Mage::app()->getStore()->isAdmin()) {
            return false;
        }

        $ruleId = $this->_quoteItemSalesRuleId($item);
        return $ruleId !== null;
    }

    protected function _quoteItemSalesRuleId(Mage_Sales_Model_Quote_Item $item)
    {
        $buyRequest = $item->getBuyRequest();
        $ruleId = isset($buyRequest['options']['netconsult_promo_rule_id']) ? $buyRequest['options']['netconsult_promo_rule_id'] : null;
        return $ruleId;
    }

    protected function _quoteItemSalesRule(Mage_Sales_Model_Quote_Item $item)
    {
        $rule = Mage::getModel('salesrule/rule');
        $rule->load($this->_quoteItemSalesRuleId($item));
        return $rule;
    }


}