<?php

class NetConsult_Promo_Helper_Data extends Mage_Core_Helper_Abstract
{

    public function isPromoItem(Mage_Sales_Model_Quote_Item $item)
    {
        return Mage::getSingleton('ncpromo/observer')->quoteItemIsPromo($item);
    }

}