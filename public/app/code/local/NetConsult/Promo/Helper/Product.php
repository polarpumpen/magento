<?php

class NetConsult_Promo_Helper_Product extends Mage_Core_Helper_Abstract
{
    protected $_locked;
    protected $_hasItems = false;

    public function addProduct($product, $super = false, $options = false, $bundleOptions = false, $ruleId = false, $qty = 1, $downloadableLinks = array())
    {
        /**
         * @var Mage_Checkout_Model_Cart $cart
         */
        $cart = Mage::getModel('checkout/cart');

        $qty = $this->checkAvailableQty($product, $qty);

        if ($qty <= 0) {
            Mage::throwException('Unable to process request because promo item was not in stock!');
        }

        $requestInfo = array(
            'qty' => $qty,
            'options' => array()
        );

        if ($super) {
            $requestInfo['super_attribute'] = $super;
        }

        if ($options) {
            $requestInfo['options'] = $options;
        }

        if ($bundleOptions) {
            $requestInfo['bundle_option'] = $bundleOptions;
        }

        if (count($downloadableLinks) > 0 && $product->getTypeId() == 'downloadable') {
            $requestInfo['links'] = $downloadableLinks;
        }

        $requestInfo['options']['netconsult_promo_rule_id'] = $ruleId;

        $cart->addProduct(+$product->getId(), $requestInfo);
        $cart->getQuote()->setTotalsCollectedFlag(false);
        $cart->getQuote()->getShippingAddress()->unsetData('cached_items_nonnominal');
        $cart->getQuote()->collectTotals();
        $cart->getQuote()->save();
        $this->restore($product->getData('sku'));
    }

    public function deleteProduct($sku)
    {
        $deletedItems = Mage::getSingleton('checkout/session')->getNetConsultDeletedItems();

        if (!$deletedItems) {
            $deletedItems = array();
        }

        $deletedItems[$sku] = true;

        Mage::getSingleton('checkout/session')->setNetConsultDeletedItems($deletedItems);
    }

    public function addPromoItem($sku, $qty, $ruleId)
    {
        if ($this->_locked) {
            return;
        }

        if (!$this->_hasItems) {
            $this->reset();
        }

        $this->_hasItems = true;

        $items = Mage::getSingleton('checkout/session')->getNetConsultPromoItems();
        if ($items === null) {
            $items = array('_groups' => array());
        }

        $autoAdd = false;

        if (!is_array($sku)) {
            $collection = Mage::getResourceModel('catalog/product_collection')
                ->addAttributeToSelect(array('status', 'required_options'))
                ->addAttributeToFilter('sku', $sku)
                ->setPage(1, 1);

            $product = $collection->getFirstItem();

            $currentWebsiteId = Mage::app()->getStore()->getWebsiteId();
            if (!is_array($product->getWebsiteIds())
                || !in_array($currentWebsiteId, $product->getWebsiteIds())
            ) {
                // Ignore products from other websites
                return;
            }

            if (!$product || ($product->getStatus() != Mage_Catalog_Model_Product_Status::STATUS_ENABLED) || !$product->isSalable()) {
                Mage::throwException('We apologize, but your free promo is not available at the moment');
            } else {
                if (in_array($product->getTypeId(), array('simple', 'virtual')) && !$product->getTypeInstance(true)->hasRequiredOptions($product))
                    $autoAdd = true;
            }

            if (isset($items[$sku])) {
                $items[$sku]['qty'] += $qty;
            } else {
                $items[$sku] = array(
                    'sku' => $sku,
                    'qty' => $qty,
                    'auto_add' => $autoAdd,
                    'rule_id' => $ruleId
                );
            }
        } else {
            $items['_groups'][$ruleId] = array(
                'sku' => $sku,
                'qty' => $qty,
                'rule_id' => $ruleId
            );
        }

        $this->_checkChanges($items);

        Mage::getSingleton('checkout/session')->setNetConsultPromoItems($items);
    }

    public function getPromoItems()
    {
        return Mage::getSingleton('checkout/session')->getNetConsultPromoItems();
    }

    public function reset()
    {
        if ($this->_hasItems) {
            $this->_locked = true;
            return;
        }

        Mage::getSingleton('checkout/session')->setNetConsultPromoItems(array('_groups' => array()));
    }

    protected function checkAvailableQty($product, $qtyRequested)
    {
        $cart = Mage::getModel('checkout/cart');
        /** @var $cart Mage_Checkout_Model_Cart $cart */

        $stockItem = Mage::getModel('cataloginventory/stock_item')
            ->assignProduct($product);

        if (!$stockItem->getManageStock())
            return $qtyRequested;

        $qtyAdded = 0;
        foreach ($cart->getItems() as $item) {
            if ($item->getProductId() == $product->getId()) {
                $qtyAdded += $item->getQty();
            }
        }

        $qty = $stockItem->getStockQty() - $qtyAdded;

        return min($qty, $qtyRequested);
    }

    protected function restore($sku)
    {
        $deletedItems = Mage::getSingleton('checkout/session')->getNetConsultPromoDeletedItems();

        if (!$deletedItems || !isset($deletedItems[$sku])) {
            return;
        }

        unset($deletedItems[$sku]);

        Mage::getSingleton('checkout/session')->setNetConsultPromoDeletedItems($deletedItems);
    }

    protected function _checkChanges(&$items)
    {
        $beforeItems = Mage::getSingleton('checkout/session')->getNetConsultPromoItemsBefore();

        if (is_array($items) && is_array($beforeItems)) {
            foreach ($items as $sku => &$item) {
                if (!empty($sku) && ($sku !== '_groups')) {
                    if (isset($beforeItems[$sku]) && $beforeItem = $beforeItems[$sku]) {
                        $item['qtyIncreased'] = $beforeItem['qty'] < $item['qty'];
                    } else {
                        $item['qtyIncreased'] = true;
                    }
                }
            }
        }
    }

}