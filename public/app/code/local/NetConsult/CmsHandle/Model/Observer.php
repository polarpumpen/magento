<?php

class NetConsult_CmsHandle_Model_Observer
{

    public function onCmsPageRender(Varien_Event_Observer $observer)
    {
        $action = $observer->getEvent()->getControllerAction();

        $actionName = strtolower($action->getFullActionName());
        $action->getLayout()->getUpdate()
            ->addHandle($actionName);

        return $this;
    }
}