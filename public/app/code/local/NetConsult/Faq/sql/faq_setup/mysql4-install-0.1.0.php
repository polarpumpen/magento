<?php
$installer = $this;

$installer->startSetup();

$installer->run("
    CREATE TABLE IF NOT EXISTS `{$this->getTable("faq/item")}` (
    `item_id` int(11) NOT NULL AUTO_INCREMENT,
    `question` TEXT NOT NULL,
    `answer` TEXT NOT NULL,
    `meta_description` TEXT NOT NULL,
    `meta_keywords` TEXT NOT NULL,
    `own_page` int(1) DEFAULT 0,
    `created` int(12) NOT NULL,
    PRIMARY KEY (`item_id`)
    )
");

$installer->run("
        CREATE TABLE IF NOT EXISTS `{$this->getTable("faq/item_store")}` (
          `item_id` int(11) NOT NULL,
          `store_id` int(11) NOT NULL,
          PRIMARY KEY (`item_id`,`store_id`)
        )
    ");

$installer->run("
        CREATE TABLE IF NOT EXISTS `{$this->getTable("faq/item_product")}` (
          `item_id` int(11) NOT NULL,
          `product_id` int(11) NOT NULL,
          PRIMARY KEY (`item_id`,`product_id`)
        )
    ");

$installer->run("
        CREATE TABLE IF NOT EXISTS `{$this->getTable("faq/item_category")}` (
          `item_id` int(11) NOT NULL,
          `category_id` int(11) NOT NULL,
          PRIMARY KEY (`item_id`,`category_id`)
        )
    ");

$installer->run("
        CREATE TABLE IF NOT EXISTS `{$this->getTable("faq/item_tag")}` (
          `item_id` int(11) NOT NULL,
          `tag` varchar(200) NOT NULL,
          PRIMARY KEY (`item_id`,`tag`)
        )
    ");



$installer->endSetup();