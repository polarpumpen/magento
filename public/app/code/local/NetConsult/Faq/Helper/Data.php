<?php

class NetConsult_Faq_Helper_Data extends Mage_Core_Helper_Abstract
{

    /** @var Algolia_Algoliasearch_Helper_Config */
    protected $config;

    /** @var Algolia_Algoliasearch_Helper_Algoliahelper */
    protected $algoliaHelper;

    /** @var Algolia_Algoliasearch_Helper_Logger */
    protected $logger;

    public function __construct()
    {
        $this->config = Mage::helper('algoliasearch/config');
        $this->algoliaHelper = Mage::helper('algoliasearch/algoliahelper');
        $this->logger = Mage::helper('algoliasearch/logger');
    }


    public function getAllCategoriesArray()
    {
        $categoriesArray = Mage::getModel('catalog/category')
            ->getCollection()
            ->addAttributeToSelect('name')
            ->addAttributeToSort('path', 'asc')
            ->load()
            ->toArray();

        $categories = array();
        foreach ($categoriesArray as $categoryId => $category) {
            if (isset($category['name']) && isset($category['level'])) {
                $categories[] = array(
                    'label' => str_repeat("- ", $category['level']) . " " . $category['name'],
                    'level'  =>$category['level'],
                    'value' => $categoryId
                );
            }
        }

        return $categories;
    }

    public function getIndexEntry(NetConsult_Faq_Model_Item $item, $storeId)
    {
        if ($item->getOwnPage()) {
            $url = Mage::app()->getStore($storeId)->getUrl('faq/question/view', array('id' => $item->getId()));
        } else {
            $url = Mage::app()->getStore($storeId)->getUrl('faq/question/list') . "#faq-" . $item->getId();
        }

        $object = array(
            'objectID' => $item->getId(),
            'value' => $item->getQuestion(),
            'content' => $this->strip($item->getAnswer()),
            'url' => $url,
        );

        return $object;
    }

    /**
     * Index a new entry
     *
     * @param NetConsult_Faq_Model_Item $item
     * @param $storeId
     */
    public function indexEntry(NetConsult_Faq_Model_Item $item, $storeId)
    {
        $indexName = Mage::helper('faq')->getIndexName($storeId);
        $this->logger->log('Indexing ' . $item->getId() . ' (store: ' . $storeId . '), index: ' . $indexName);
        $object = $this->getIndexEntry($item, $storeId);
        $this->algoliaHelper->addObjects(array($object), $indexName);
    }

    /**
     * Remove entry from database
     *
     * @param $id
     * @param $storeId
     */
    public function removeEntry($id, $storeId)
    {
        $indexName = Mage::helper('faq')->getIndexName($storeId);
        $this->logger->log('Remove from index ' . $id . ' (store: ' . $storeId . '), index: ' . $indexName);
        $this->algoliaHelper->deleteObjects(array($id), $indexName);
    }

    protected function strip($s)
    {
        $s = trim(preg_replace('/\s+/', ' ', $s));
        $s = preg_replace('/&nbsp;/', ' ', $s);
        $s = preg_replace('!\s+!', ' ', $s);
        $s = preg_replace('/\{\{[^}]+\}\}/', ' ', $s);

        return trim(strip_tags($s));
    }

    public function getIndexName($storeId)
    {
        $indexName = (string)$this->getBaseIndexName($storeId) . '_section_faq';
        return $indexName;
    }

    protected function getBaseIndexName($storeId = null)
    {
        return (string)$this->config->getIndexPrefix($storeId) . Mage::app()->getStore($storeId)->getCode();
    }

    public function getIndexSettings($storeId)
    {
        return array(
            'attributesToIndex'   => array('value', 'content'),
            'attributesToSnippet' => array('content:7'),
        );
    }


}