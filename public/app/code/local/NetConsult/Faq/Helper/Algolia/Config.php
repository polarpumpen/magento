<?php

class NetConsult_Faq_Helper_Algolia_Config extends Algolia_Algoliasearch_Helper_Config
{

    public function getAutocompleteSections($storeId = null)
    {
        $autoCompleteSection = parent::getAutocompleteSections($storeId);
        foreach ($autoCompleteSection as $key => $section) {
            $autoCompleteSection[$key]['label'] = Mage::helper('algoliasearch')->__($section['label']);
        }

        $autoCompleteSection[] = array(
            'name' => 'faq',
            'label' => 'Frågor och Svar',
            'hitsPerPage' => 4,
        );

        return $autoCompleteSection;
    }


}