<?php

class NetConsult_Faq_Helper_Algolia_Data extends Algolia_Algoliasearch_Helper_Data
{

    public function rebuildStoreAdditionalSectionsIndex($storeId)
    {
        if ($this->config->isEnabledBackend($storeId) === false) {
            $this->logger->log('INDEXING IS DISABLED FOR '.$this->logger->getStoreName($storeId));

            return;
        }

        $additionnal_sections = $this->config->getAutocompleteSections();

        foreach ($additionnal_sections as $section) {
            if ($section['name'] === 'products' || $section['name'] === 'categories' || $section['name'] === 'pages' || $section['name'] === 'suggestions' || $section['name'] == 'faq') {
                continue;
            }

            $index_name = $this->additionalsections_helper->getIndexName($storeId).'_'.$section['name'];

            $attribute_values = $this->additionalsections_helper->getAttributeValues($storeId, $section);

            foreach (array_chunk($attribute_values, 100) as $chunk) {
                $this->algolia_helper->addObjects($chunk, $index_name.'_tmp');
            }

            $this->algolia_helper->moveIndex($index_name.'_tmp', $index_name);

            $this->algolia_helper->setSettings($index_name,
                $this->additionalsections_helper->getIndexSettings($storeId));
        }
    }
}