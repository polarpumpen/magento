<?php

class NetConsult_Faq_Block_List extends Mage_Core_Block_Template
{

    public function getItems()
    {
        return Mage::registry('faq_items');
    }

}