<?php

class NetConsult_Faq_Block_Catalog_List extends Mage_Core_Block_Template
{

    protected $_items;

    public function getItems()
    {
        if ($this->_items) {
            return $this->_items;
        }

        $collection = Mage::getModel('faq/item')->getCollection(); /* @var $collection NetConsult_Faq_Model_Resource_Item_Collection */
        $collection->addStoreFilter();

        $category = Mage::registry('current_category');

        if (Mage::registry('current_product')) {
            $collection->addProductsFilter(array(
                Mage::registry('current_product')->getId()
            ), $category ? array($category->getId()) : null);
        } elseif ($category) {
            $collection->addCategoriesFilter(array($category->getId()));
        }

        $collection->getSelect()->order(new Zend_Db_Expr('RAND()'));
        $collection->getSelect()->limit(10);

        $this->_items = $collection;
        return $collection;
    }

}