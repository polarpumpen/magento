<?php

class NetConsult_Faq_Block_Adminhtml_Faq_Item_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('faq_item_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('faq')->__('FAQ'));
    }

    protected function _beforeToHtml()
    {
        $this->addTab('form_section', array(
            'label'     => Mage::helper('faq')->__('FAQ'),
            'title'     => Mage::helper('faq')->__('FAQ'),
            'content'   => $this->getLayout()->createBlock('faq/adminhtml_faq_item_edit_tab_form')->toHtml(),
        ));


        return parent::_beforeToHtml();
    }
}