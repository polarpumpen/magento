<?php

class NetConsult_Faq_Block_Adminhtml_Faq_Item extends Mage_Adminhtml_Block_Widget_Grid_Container
{

    public function __construct()
    {
        $this->_controller = 'adminhtml_faq_item';
        $this->_blockGroup = 'faq';
        $this->_headerText = Mage::helper('faq')->__('FAQ: List');
        $this->_addButtonLabel = Mage::helper('faq')->__('Create FAQ');

        parent::__construct();
    }

}