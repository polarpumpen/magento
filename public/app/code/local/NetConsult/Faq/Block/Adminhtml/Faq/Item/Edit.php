<?php

class NetConsult_Faq_Block_Adminhtml_Faq_Item_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{

    public function __construct()
    {
        parent::__construct();
        $this->_objectId = 'id';
        $this->_blockGroup = 'faq';
        $this->_controller = 'adminhtml_faq_item';

        $this->_updateButton('save', 'label', Mage::helper('faq')->__('Save FAQ'));
        $this->_updateButton('delete', 'label', Mage::helper('faq')->__('Delete FAQ'));

    }

    public function getHeaderText()
    {
        if( Mage::registry('faq_item') && Mage::registry('faq_item')->getId() ) {
            return Mage::helper('faq')->__("Edit FAQ");
        } else {
            return Mage::helper('faq')->__('Add FAQ');
        }
    }

}