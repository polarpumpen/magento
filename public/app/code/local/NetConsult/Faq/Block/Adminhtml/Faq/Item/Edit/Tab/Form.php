<?php

class NetConsult_Faq_Block_Adminhtml_Faq_Item_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{

    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);

        $fieldset = $form->addFieldset('faq_form',
            array('legend' => Mage::helper('faq')->__('Details'))
        );
        /* @var $fieldset Varien_Data_Form_Element_Fieldset */

        $fieldset->addField('question', 'textarea', array(
            'label' => Mage::helper('faq')->__('Question'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'question',
        ));

        $fieldset->addField('answer', 'textarea', array(
            'label' => Mage::helper('faq')->__('Answer'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'answer',
        ));

        $fieldsetVisibility = $form->addFieldset('faq_form_visibility',
            array('legend' => Mage::helper('faq')->__('Visibility'))
        );
        /* @var $fieldsetVisibility Varien_Data_Form_Element_Fieldset */

        $fieldsetVisibility->addType('tags', 'NetConsult_Faq_Block_Adminhtml_Form_Tags');
        $fieldsetVisibility->addField('tags', 'tags', array(
            'label' => Mage::helper('faq')->__('Tags'),
            'required' => false,
            'name' => 'tags',
        ));
//
//        $fieldsetVisibility->addField('product_ids', 'tags', array(
//            'label' => Mage::helper('faq')->__('Products'),
//            'required' => false,
//            'name' => 'product_ids',
//        ));

        $fieldsetVisibility->addField('store_ids', 'multiselect', array(
            'name'      => 'store_ids[]',
            'label'     => Mage::helper('cms')->__('Store View'),
            'title'     => Mage::helper('cms')->__('Store View'),
            'required'  => true,
            'values'    => Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(false, true),
        ));

        $fieldsetVisibility->addField('category_ids', 'multiselect', array(
            'name'      => 'category_ids[]',
            'label'     => Mage::helper('faq')->__('Categories'),
            'title'     => Mage::helper('faq')->__('Categories'),
            'required'  => false,
            'values'    => Mage::helper('faq')->getAllCategoriesArray()
        ));



        $fieldsetPage = $form->addFieldset('faq_form_page',
            array('legend' => Mage::helper('faq')->__('Page Settings'))
        );
        /* @var $fieldsetPage Varien_Data_Form_Element_Fieldset */

        $fieldsetPage->addField('own_page', 'select', array(
            'label'     => Mage::helper('faq')->__('Own Page'),
            'name'      => 'own_page',
            'onclick' => "",
            'onchange' => "",
            'value'  => '0',
            'values' => array(
                array('value'=>'0','label'=>'No'),
                array('value'=>'1','label'=>'Yes'),
            ),
            'disabled' => false,
            'readonly' => false,
        ));



        $fieldsetPage->addField('meta_description', 'textarea', array(
            'label' => Mage::helper('faq')->__('Meta Description'),
            'required' => false,
            'name' => 'meta_description',
        ));

        $fieldsetPage->addField('meta_keywords', 'textarea', array(
            'label' => Mage::helper('faq')->__('Meta Keywords'),
            'required' => false,
            'name' => 'meta_keywords',
        ));



        if (Mage::getSingleton('adminhtml/session')->getFaqItem()) {
            $form->setValues(Mage::getSingleton('adminhtml/session')->getFaqItem());
            Mage::getSingleton('adminhtml/session')->setFaqItem(null);
        } elseif (Mage::registry('faq_item')) {
            $data = Mage::registry('faq_item')->getData();
            $form->setValues($data);
        }
        return parent::_prepareForm();
    }

}