<?php

class NetConsult_Faq_Block_Adminhtml_Faq_Item_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    public function __construct()
    {
        parent::__construct();
        $this->setId('faqItems');
        $this->setDefaultSort('question');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('faq/item')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('item_id', array(
            'header' => Mage::helper('faq')->__('ID'),
            'align' => 'right',
            'width' => '50px',
            'index' => 'item_id',
        ));

        $this->addColumn('question', array(
            'header' => Mage::helper('faq')->__('Question'),
            'align' => 'left',
            'index' => 'question',
        ));

        $this->addColumn('own_page', array(
            'header' => Mage::helper('faq')->__('Own Page'),
            'align' => 'left',
            'index' => 'own_page',
            'type'      => 'options',
            'options'   => array(
                1 => Mage::helper('faq')->__('Yes'),
                0 => Mage::helper('faq')->__('No')
            ),
        ));

        $this->addColumn('store_ids', array(
            'header' => Mage::helper('faq')->__('Store Views'),
            'align' => 'left',
            'index' => 'store_ids',
            'type' => 'options',
            'renderer' => 'faq/adminhtml_storeViewsRenderer',
            'filter' => false,
            'sortable' => false,
        ));

        return parent::_prepareColumns();
    }

    protected function _prepareMassaction()
    {

        return $this;
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

}