<?php

/**
 * Class NetConsult_Faq_Block_Adminhtml_Form_Tags
 *
 * Tags built from https://github.com/aehlke/tag-it
 */
class NetConsult_Faq_Block_Adminhtml_Form_Tags extends Varien_Data_Form_Element_Abstract
{
    public function getElementHtml()
    {
        $value = $this->getValue();

        $html = '<ul class="tags" data-target="' . $this->getName() . '">';

        if (is_array($value)) {
            foreach ($value as $text) {
                $html .= "<li>" . $this->_escape($text) . "</li>";
            }
        }

        $html .= '</ul>';

        return $html;

    }

}