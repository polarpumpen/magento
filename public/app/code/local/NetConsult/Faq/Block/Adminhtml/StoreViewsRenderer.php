<?php

class NetConsult_Faq_Block_Adminhtml_StoreViewsRenderer
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $options = Mage::getSingleton('adminhtml/system_store')->getStoreOptionHash();
        
        $values = $row->getData($this->getColumn()->getIndex());
        if (is_array($values) && count($values)) {
            if (count($values) >= 1 && in_array(0, $values)) {
                return Mage::helper('faq')->__('(all)');
            } else {
                $result = array();
                foreach ($values as $value) {
                    if (array_key_exists($value, $options)) {
                        $result[] = $options[$value];
                    }
                }
                if (!count($result)) {
                    return null;
                }
                sort($result);
                return join("<br>", $result);
            }
        }
        return Mage::helper('faq')->__('?');
    }


}