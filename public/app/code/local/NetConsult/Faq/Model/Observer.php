<?php

class NetConsult_Faq_Model_Observer
{

    /** @var Algolia_Algoliasearch_Helper_Config */
    protected $config;

    /** @var Algolia_Algoliasearch_Helper_Logger */
    protected $logger;

    /** @var Algolia_Algoliasearch_Helper_Algoliahelper */
    protected $algoliaHelper;

    public function __construct()
    {
        $this->config = Mage::helper('algoliasearch/config');
        $this->logger = Mage::helper('algoliasearch/logger');
        $this->algoliaHelper = Mage::helper('algoliasearch/algoliahelper');

    }


    public function rebuildFaqIndex(Varien_Object $event)
    {
        $storeId = $event->getStoreId();
        if ($this->config->isEnabledBackend($storeId) === false) {
            $this->logger->log('INDEXING IS DISABLED FOR ' . $this->logger->getStoreName($storeId));
            return;
        }

        $this->logger->log('Start FAQ rebuilding index');

        $indexName = Mage::helper('faq')->getIndexName($storeId);

        $objects = array();
        $collection = Mage::getModel('faq/item')->getCollection();
        foreach ($collection as $item) { /* @var $item NetConsult_Faq_Model_Item */
            if (!(in_array($storeId, $item->getStoreIds()) || in_array(0, $item->getStoreIds()))) {
                continue;
            }

            $object = Mage::helper('faq')->getIndexEntry($item, $storeId);
            $objects[] = $object;
        }


        $this->algoliaHelper->addObjects($objects, $indexName.'_tmp');
        $this->algoliaHelper->moveIndex($indexName.'_tmp', $indexName);

        $this->algoliaHelper->setSettings($indexName, Mage::helper('faq')->getIndexSettings($storeId));

        $this->logger->log('Stop FAQ rebuilding index');
    }



}