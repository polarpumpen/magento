<?php

class NetConsult_Faq_Model_Resource_Item_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{

    protected function _construct()
    {
        $this->_init('faq/item');
    }

    public function _afterLoad()
    {
        parent::_afterLoad();
        foreach ($this->_items as $item) {
            $item->getResource()->load($item, $item->getId());
        }
    }



    public function addStoreFilter()
    {
        $this->getSelect()->join(
            array('store' => $this->getTable('faq/item_store')),
            'store.item_id=main_table.item_id'
        )->where('store.store_id IN (?)', array(0, Mage::app()->getStore()->getId()));
    }

    public function addProductsFilter(array $productIds, array $categoryIds = null)
    {
        $select = $this->getSelect();

        $select->joinLeft(
            array('product' => $this->getTable('faq/item_product')),
            'product.item_id=main_table.item_id'
        );

        $categoryIds = array(1,2,3);
        if ($categoryIds) {
            $select->joinLeft(
                array('category' => $this->getTable('faq/item_category')),
                'category.item_id=main_table.item_id'
            )->where($this->getSelect()->getAdapter()->quoteInto('product.product_id IN (?)', $productIds) . ' OR ' .
                $this->getSelect()->getAdapter()->quoteInto('category.category_id IN (?)', $categoryIds));
        } else {
            $select->where('product.product_id IN (?)', $productIds);
        }
    }

    public function addCategoriesFilter(array $categoryIds)
    {
        $select = $this->getSelect();

        $select->joinLeft(
            array('category' => $this->getTable('faq/item_category')),
            'category.item_id=main_table.item_id');
        $select->where('category.category_id IN (?)', $categoryIds);
    }


}