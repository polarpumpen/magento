<?php

class NetConsult_Faq_Model_Resource_Item extends Mage_Core_Model_Resource_Db_Abstract
{

    protected function _construct()
    {
        $this->_init('faq/item', 'item_id');
    }

    /**
     * @param Mage_Core_Model_Abstract $object
     * @return Mage_Core_Model_Resource_Db_Abstract
     */
    protected function _beforeSave(Mage_Core_Model_Abstract $object)
    {
        if (!$object->getCreated()) {
            $object->setCreated(time());
        }

        return parent::_beforeSave($object);
    }

    /**
     * @param Mage_Core_Model_Abstract $object
     * @return $this|Mage_Core_Model_Resource_Db_Abstract
     */
    protected function _beforeDelete(Mage_Core_Model_Abstract $object)
    {
        parent::_beforeDelete($object);

        $condition = $this->_getWriteAdapter()->quoteInto('item_id = ?', $object->getId());

        // Products
        $this->_getWriteAdapter()->delete($this->getTable('faq/item_product'), $condition);

        // Categories
        $this->_getWriteAdapter()->delete($this->getTable('faq/item_category'), $condition);

        // Stores
        $this->_getWriteAdapter()->delete($this->getTable('faq/item_store'), $condition);

        // Tags
        $this->_getWriteAdapter()->delete($this->getTable('faq/item_tag'), $condition);

        return $this;
    }

    /**
     * @param Mage_Core_Model_Abstract $object
     * @return Mage_Core_Model_Resource_Db_Abstract
     */
    protected function _afterSave(Mage_Core_Model_Abstract $object)
    {
        $condition = $this->_getWriteAdapter()->quoteInto('item_id = ?', $object->getId());

        // Stores
        $this->_getWriteAdapter()->delete($this->getTable('faq/item_store'), $condition);
        foreach ((array) $object->getData('store_ids') as $store) {
            $storeArray = array();
            $storeArray['item_id'] = $object->getId();
            $storeArray['store_id'] = $store;
            $this->_getWriteAdapter()->insert($this->getTable('faq/item_store'), $storeArray);
        }

        // Products
        $this->_getWriteAdapter()->delete($this->getTable('faq/item_product'), $condition);
        foreach ((array) $object->getData('product_ids') as $product) {
            $productArray = array();
            $productArray['item_id'] = $object->getId();
            $productArray['product_id'] = $product;
            $this->_getWriteAdapter()->insert($this->getTable('faq/item_product'), $productArray);
        }

        // Categories
        $this->_getWriteAdapter()->delete($this->getTable('faq/item_category'), $condition);
        foreach ((array) $object->getData('category_ids') as $category) {
            $categoryArray = array();
            $categoryArray['item_id'] = $object->getId();
            $categoryArray['category_id'] = $category;
            $this->_getWriteAdapter()->insert($this->getTable('faq/item_category'), $categoryArray);
        }

        // Tags
        $this->_getWriteAdapter()->delete($this->getTable('faq/item_tag'), $condition);
        foreach ((array) $object->getData('tags') as $tag) {
            $tagArray = array();
            $tagArray['item_id'] = $object->getId();
            $tagArray['tag'] = $tag;
            $this->_getWriteAdapter()->insert($this->getTable('faq/item_tag'), $tagArray);
        }
        
        return parent::_afterSave($object);
    }

    /**
     * @param Mage_Core_Model_Abstract $object
     * @return Mage_Core_Model_Resource_Db_Abstract
     */
    protected function _afterLoad(Mage_Core_Model_Abstract $object)
    {
        // Stores
        $select = $this->_getReadAdapter()->select()
            ->from($this->getTable('faq/item_store'), '*')
            ->where('item_id = ?', $object->getId());

        if ($data = $this->_getReadAdapter()->fetchAll($select)) {
            $storesArray = array();
            foreach ($data as $row) {
                $storesArray[] = $row['store_id'];
            }
            $object->setData('store_ids', $storesArray);
        } else {
            $object->setData('store_ids', array());
        }

        // Products
        $select = $this->_getReadAdapter()->select()
            ->from($this->getTable('faq/item_product'), '*')
            ->where('item_id = ?', $object->getId());

        if ($data = $this->_getReadAdapter()->fetchAll($select)) {
            $productsArray = array();
            foreach ($data as $row) {
                $productsArray[] = $row['product_id'];
            }
            $object->setData('product_ids', $productsArray);
        } else {
            $object->setData('product_ids', array());
        }
        
        // Categories
        $select = $this->_getReadAdapter()->select()
            ->from($this->getTable('faq/item_category'), '*')
            ->where('item_id = ?', $object->getId());

        if ($data = $this->_getReadAdapter()->fetchAll($select)) {
            $categoriesArray = array();
            foreach ($data as $row) {
                $categoriesArray[] = $row['category_id'];
            }
            $object->setData('category_ids', $categoriesArray);
        } else {
            $object->setData('category_ids', array());
        }

        // Tags
        $select = $this->_getReadAdapter()->select()
            ->from($this->getTable('faq/item_tag'), '*')
            ->where('item_id = ?', $object->getId());

        if ($data = $this->_getReadAdapter()->fetchAll($select)) {
            $tagsArray = array();
            foreach ($data as $row) {
                $tagsArray[] = $row['tag'];
            }
            $object->setData('tags', $tagsArray);
        } else {
            $object->setData('tags', array());
        }

        return parent::_afterLoad($object);
    }

    /**
     * @param string $field
     * @param mixed $value
     * @param Mage_Core_Model_Abstract $object
     * @return Zend_Db_Select
     */
    protected function _getLoadSelect($field, $value, $object)
    {
        $select = parent::_getLoadSelect($field, $value, $object);

        // Stores
        if ($object->getStoreId()) {
            $select->join(array('item_store' => $this->getTable('faq/item_store')), $this->getMainTable() . '.item_id = item_store.item_id')
                ->where('item_store.store_id in (0, ?) ', $object->getStoreId())
                ->order('store_id DESC')
                ->limit(1);
        }

        // Products
        if ($object->getProductId()) {
            $select->join(array('item_product' => $this->getTable('faq/item_product')), $this->getMainTable() . '.item_id = item_product.item_id')
                ->where('item_product.product_id = ?', $object->getProductId())
                ->order('product_id DESC')
                ->limit(1);
        }

        // Categories
        if ($object->getCategoryId()) {
            $select->join(array('item_category' => $this->getTable('faq/item_category')), $this->getMainTable() . '.item_id = item_category.item_id')
                ->where('item_category.category_id = ?', $object->getCategoryId())
                ->order('category_id DESC')
                ->limit(1);
        }

        // Tags
        if ($object->getTag()) {
            $select->join(array('item_tag' => $this->getTable('faq/item_tag')), $this->getMainTable() . '.item_id = item_tag.item_id')
                ->where('item_tag.tag in (0, ?) ', $object->getTag())
                ->order('category_id DESC')
                ->limit(1);
        }

        return $select;
    }

}