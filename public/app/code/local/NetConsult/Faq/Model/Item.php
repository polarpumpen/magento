<?php

/**
 * Class NetConsult_Faq_Model_Item
 *
 * @method NetConsult_Faq_Model_Item setId()
 *
 * @method string getQuestion()
 * @method NetConsult_Faq_Model_Item setQuestion($value)
 *
 * @method string getAnswer()
 * @method NetConsult_Faq_Model_Item setAnswer($value)
 *
 * @method int getOwnPage()
 * @method NetConsult_Faq_Model_Item setOwnPage($int)
 *
 * @method string getMetaDescription()
 * @method NetConsult_Faq_Model_Item setMetaDescription($string)
 *
 * @method string getMetaKeywords()
 * @method NetConsult_Faq_Model_Item setMetaKeywords($string)
 *
 * @method string[] getTags()
 * @method NetConsult_Faq_Model_Item setTags($value)
 *
 * @method int[] getStoreIds()
 * @method NetConsult_Faq_Model_Item setStoreIds($value)
 *
 * @method int[] getProductIds()
 * @method NetConsult_Faq_Model_Item setProductIds(array $value)
 *
 * @method int[] getCategoryIds()
 * @method NetConsult_Faq_Model_Item setCategoryIds($value)
 *
 * @method int getCreated()
 */
class NetConsult_Faq_Model_Item extends Mage_Core_Model_Abstract
{

    protected function _construct()
    {
        $this->_init("faq/item");
    }

}