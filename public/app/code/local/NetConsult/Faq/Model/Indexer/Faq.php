<?php

class NetConsult_Faq_Model_Indexer_Faq extends Algolia_Algoliasearch_Model_Indexer_Abstract
{
    const EVENT_MATCH_RESULT_KEY = 'algoliasearch_match_result';

    /** @var Algolia_Algoliasearch_Helper_Config */
    protected $config;

    /** @var  Algolia_Algoliasearch_Model_Queue */
    protected $queue;

    /** @var Algolia_Algoliasearch_Helper_Logger */
    protected $logger;

    protected static $credential_error = false;

    public function __construct()
    {
        parent::__construct();

        $this->config = Mage::helper('algoliasearch/config');
        $this->queue = Mage::getSingleton('algoliasearch/queue');
        $this->logger = Mage::helper('algoliasearch/logger');
    }

    public function getName()
    {
        return Mage::helper('algoliasearch')->__('NetConsult FAQ for Algolia');
    }

    public function getDescription()
    {
        return "Bygg om FAQ index";
    }

    public function matchEvent(Mage_Index_Model_Event $event)
    {
        return false;
    }

    protected function _registerEvent(Mage_Index_Model_Event $event)
    {
        return $this;
    }

    protected function _processEvent(Mage_Index_Model_Event $event)
    {
    }

    /**
     * Rebuild all index data.
     */
    public function reindexAll()
    {
        if (!$this->config->getApplicationID() || !$this->config->getAPIKey() || !$this->config->getSearchOnlyAPIKey()) {
            /** @var Mage_Adminhtml_Model_Session $session */
            $session = Mage::getSingleton('adminhtml/session');
            $session->addError('Algolia reindexing failed: You need to configure your Algolia credentials in System > Configuration > Algolia Search.');

            return;
        }

        $this->logger->log('Start FAQ reindex');

        foreach (Mage::app()->getStores() as $store) {
            if (!$this->config->isEnabledBackend($store->getId())) {
                continue;
            }

            $this->addToQueue('faq/observer', 'rebuildFaqIndex', array('store_id' => $store->getId()), 1);
        }

        $this->logger->log('Stop FAQ reindex');

        return $this;
    }

    public function addToQueue($observer, $method, $data, $data_size)
    {
        if ($this->config->isQueueActive()) {
            $this->queue->add($observer, $method, $data, $data_size);
        } else {
            Mage::getSingleton($observer)->$method(new Varien_Object($data));
        }
    }

}
