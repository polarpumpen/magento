<?php

class NetConsult_Faq_Adminhtml_Faq_ItemController extends Mage_Adminhtml_Controller_Action
{

    protected function _initAction()
    {
        $this->loadLayout();
        $this->_initLayoutMessages('adminhtml/session');
        $this->_setActiveMenu('cms/faq')
            ->_addBreadcrumb(
                Mage::helper('faq')->__('FAQ'),
                Mage::helper('faq')->__('FAQ')
            );
        $this->_title(Mage::helper('faq')->__('FAQ'));
        return $this;
    }

    public function indexAction()
    {
        $this->_initAction();
        $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

        $this->_addContent($this->getLayout()->createBlock('faq/adminhtml_faq_item'));

        $this->renderLayout();
    }

    public function newAction()
    {
        $this->_forward('edit');
    }

    public function editAction()
    {
        $this->_initAction();

        $layout = $this->getLayout();
        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('faq/item')->load($id);

        if ($model->getId() || $id == 0) {
            $data = Mage::getSingleton('adminhtml/session')->getFaqItem(true);
            if (!empty($data)) {
                $model->setData($data);
            }

            Mage::register('faq_item', $model);

            $this->loadLayout();

            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

            $editBlock = $layout->createBlock('faq/adminhtml_faq_item_edit');
            $this->_addContent($editBlock)
                ->_addLeft($layout->createBlock('faq/adminhtml_faq_item_edit_tabs'));

            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('faq')->__('Item does not exist'));
            $this->_redirect('*/*/');
        }
    }

    public function saveAction()
    {
        if ($this->getRequest()->getPost()) {
            try {
                $postData = $this->getRequest()->getPost();

                $tags = (isset($postData['tags']) && is_array($postData['tags'])) ? $postData['tags'] : array();
                $tags = array_filter($tags, 'trim');

                $categoryIds = isset($postData['category_ids']) ? $postData['category_ids'] : array();
                $productIds = isset($postData['product_ids']) ? $postData['product_ids'] : array();
                $storeIds = isset($postData['store_ids']) ? $postData['store_ids'] : array();


                $item = Mage::getModel('faq/item'); /** @var $item NetConsult_Faq_Model_Item */
                $id = $this->getRequest()->getParam('id');

                $allStores = array_keys(Mage::app()->getStores());

                $previousStores = array();
                if ($id) {
                    $item->load($id);
                    $previousStores = $item->getStoreIds();
                }

                if (in_array(0, $previousStores)) {
                    $previousStores = $allStores;
                }

                if (in_array(0, $storeIds)) {
                    $newStores = $allStores;
                } else {
                    $newStores = $storeIds;
                }

                $item->setId($id)
                    ->setQuestion($postData['question'])
                    ->setAnswer($postData['answer'])
                    ->setTags($tags)
                    ->setStoreIds($storeIds)
                    ->setCategoryIds($categoryIds)
                    ->setProductIds($productIds)
                    ->setOwnPage($postData['own_page'])
                    ->setMetaDescription($postData['meta_description'])
                    ->setMetaKeywords($postData['meta_keywords']);
                $item->save();

                // All stores that are really new
                foreach (array_diff($newStores, $previousStores) as $storeId) {
                    Mage::helper('faq')->indexEntry($item, $storeId);
                }

                // Stores that have been removed
                foreach (array_diff($previousStores, $newStores) as $storeId) {
                    Mage::helper('faq')->removeEntry($id, $storeId);
                }

                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('FAQ was successfully saved'));
                Mage::getSingleton('adminhtml/session')->setFaqItem(false);

                $this->_redirect('*/*/index');
                return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFaqItem($this->getRequest()->getPost());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * Check if current user has access to this controller.
     *
     * @return boolean
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')
            ->isAllowed('cms/faq');
    }

}