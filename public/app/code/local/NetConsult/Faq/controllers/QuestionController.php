<?php

class NetConsult_Faq_QuestionController extends Mage_Core_Controller_Front_Action
{

    public function viewAction()
    {
        $id = $this->getRequest()->get('id');
        if (!$id) {
            $this->norouteAction();
            return;
        }

        $item = Mage::getModel('faq/item')->load($id);
        if (!$item || !$item->getId()) {
            $this->norouteAction();
            return;
        }

        Mage::register('faq_item', $item);

        $this->loadLayout();
        $this->renderLayout();
    }

    public function listAction()
    {
        $collection = Mage::getModel('faq/item')->getCollection();
        $storeId = Mage::app()->getStore()->getId();

        $items = array();
        foreach ($collection as $item) {
            /* @var $item NetConsult_Faq_Model_Item */
            if (!(in_array($storeId, $item->getStoreIds()) || in_array(0, $item->getStoreIds()))) {
                continue;
            }

            if ($item->getOwnPage()) {
                continue;
            }

            $items[] = $item;
        }

        Mage::register('faq_items', $items);

        $this->loadLayout();
        $this->renderLayout();
    }

}