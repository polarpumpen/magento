<?php

class NetConsult_ProductConfigurator_Block_Cart_Item_Renderer_Configurator extends Mage_Checkout_Block_Cart_Item_Renderer
{

    public function getProductAdditionalInformationBlock()
    {
        return $this->getLayout()->createBlock('productconfigurator/cart_item_renderer_configurator_options');
    }

}