<?php

class NetConsult_ProductConfigurator_Block_Cart_Item_Renderer_Configurator_Options extends Mage_Core_Block_Template
{

    protected $_template = 'netconsult/productconfigurator/checkout/cart/item/configurator/options.phtml';
    protected $_item;

    public function setItem($item)
    {
        /** @var Mage_Sales_Model_Quote_Item $item */
        $this->_item = $item;
        return $this;
    }

    /**
     * @return Mage_Sales_Model_Quote_Item
     */
    public function getItem()
    {
        return $this->_item;
    }

    public function getConfiguration()
    {
        if (!$this->getItem()) {
            return array();
        }

        $productConfiguration = $this->getItem()->getOptionByCode('productconfiguration');
        if (!$productConfiguration) {
            return array();
        }

        $value = json_decode($productConfiguration->getValue(), true);
        return $value ? $value : array();
    }



}