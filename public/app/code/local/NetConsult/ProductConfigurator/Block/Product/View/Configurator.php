<?php

class NetConsult_ProductConfigurator_Block_Product_View_Configurator extends Mage_Core_Block_Template
{
    protected $_product = null;

    /**
     * @return Mage_Catalog_Model_Product
     */
    function getProduct()
    {
        if (!$this->_product) {
            $this->_product = Mage::registry('product');
        }

        return $this->_product;
    }

    /**
     * @return string
     */
    function getConfiguratorTemplateKey()
    {
        return $this->getProduct()->getData('productconfigurator_template');
    }

    /**
     * @return object|bool
     */
    function getConfiguratorTemplate()
    {
        $templates = $this->getModuleHelper()->getProductTemplates();
        $key = $this->getConfiguratorTemplateKey();

        if (!$key || empty($templates)) {
            return false;
        }

        foreach ($templates as $template) {
            if ($template->id == $key) {
                return $template;
            }
        }

        return false;
    }

    /**
     * @return NetConsult_ProductConfigurator_Helper_Data
     */
    protected function getModuleHelper()
    {
        return Mage::helper('productconfigurator');
    }
}
