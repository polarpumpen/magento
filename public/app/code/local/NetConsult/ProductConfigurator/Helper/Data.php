<?php

class NetConsult_ProductConfigurator_Helper_Data extends Mage_Core_Helper_Data
{
    const TEMPLATE_CONFIG_KEY = 'netconsult/productconfigurator/templates';

    /**
     * @return array
     */
    protected function getFromStorage()
    {
        $collection = Mage::getModel('productconfigurator/template')->getCollection()->setOrder('timestamp', 'desc');
        $collection->getSelect()->limit(1);
        if ($collection->count()) {
            return $collection->getFirstItem()->getValue();
        }

        return null;
    }

    /**
     * @param $value
     * @return $this
     * @throws Exception
     */
    protected function saveToStorage($value)
    {
        $model = Mage::getModel('productconfigurator/template');
        $model->setValue($value);
        $model->save();

        return $this;
    }

    /**
     * @return array
     */
    public function getProductTemplates()
    {
        if ($value = $this->getFromStorage()) {
            return unserialize($value);
        }

        return array();
    }

    /**
     * @param $key
     * @return null
     */
    public function getProductTemplate($key)
    {
        $templates = $this->getProductTemplates();
        foreach ($templates as $template) {
            if ($template->id == $key) {
                return $template;
            }
        }

        return null;
    }

    /**
     * @param $data
     */
    public function setProductTemplates($data)
    {
        $data = $this->beforeSaveProductTemplates($data);
        $this->saveToStorage(serialize($data));
    }

    /**
     * @param array $templates
     * @return array
     */
    private function beforeSaveProductTemplates($templates)
    {

        foreach ($templates as $template) {
            $this->prepareObjectForSave($template);
            $this->prepareObjectListForSave($template->columns);
            $this->prepareObjectListForSave($template->rows);
            foreach ($template->supplementGroups as $supplementGroup) {
                $this->prepareObjectForSave($supplementGroup);
                foreach ($supplementGroup->supplements as $supplement) {
                    $this->prepareObjectForSave($supplement);
                    $this->prepareObjectListForSave($supplement->priceRules);
                }
            }
        }

        return $templates;
    }

    /**
     * @param array $list
     */
    private function prepareObjectListForSave($list)
    {
        array_map(array($this, 'prepareObjectForSave'), $list);
    }

    /**
     * @param object $object
     */
    public function prepareObjectForSave($object)
    {
        unset($object->{'$$hashKey'});
        if (!isset($object->id)) {
            $object->id = uniqid('', true);
        }
    }
}
