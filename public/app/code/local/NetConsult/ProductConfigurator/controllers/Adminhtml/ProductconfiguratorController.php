<?php

class NetConsult_ProductConfigurator_Adminhtml_ProductconfiguratorController extends Mage_Adminhtml_Controller_Action
{
    public function indexAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('productconfigurator/index');

        $this->renderLayout();
    }

    public function saveTemplatesAction()
    {
        $request = json_decode(file_get_contents("php://input"));

        if (!isset($request->templates)) {
            die('no templates');
        }

        $this->getModuleHelper()->setProductTemplates($request->templates);

        $this->getResponse()->setHeader('Content-type', 'application/json');
        $this->getResponse()->setBody(json_encode((object) array(
            'success' => true,
            'message' => 'Templates saved'
        )));
    }

    public function loadTemplatesAction()
    {
        $templates = $this->getModuleHelper()->getProductTemplates();

        if (!$templates) {
            $templates = array();
        }

        $this->getResponse()->setHeader('Content-type', 'application/json');
        $this->getResponse()->setBody(json_encode($templates));
    }

    /**
     * @return NetConsult_ProductConfigurator_Helper_Data
     */
    protected function getModuleHelper()
    {
        return Mage::helper('productconfigurator');
    }

}
