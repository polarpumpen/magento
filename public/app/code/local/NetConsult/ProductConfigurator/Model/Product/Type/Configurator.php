<?php

class NetConsult_ProductConfigurator_Model_Product_Type_Configurator extends Mage_Catalog_Model_Product_Type_Abstract
{
    public function hasOptions($product = null)
    {
        return true;
    }

    public function canConfigure($product = null)
    {
        return false;
    }

    public function isComposite($product = null)
    {
        return false;
    }

    public function isSalable($product = null)
    {
        return true;
    }

    public function prepareForCartAdvanced(Varien_Object $buyRequest, $product = null, $processMode = null)
    {
        /** @var Mage_Catalog_Model_Product $product */

        $templateKey = $product->getProductconfiguratorTemplate();
        if (!$templateKey) {
            throw new \Exception('Invalid template');
        }

        $helper = Mage::helper('productconfigurator');
        $template = $helper->getProductTemplate($templateKey);
        if (!$template) {
            throw new \Exception('Template doesn\'t exists!');
        }

        $useMatrix = isset($template->useMatrix) ? $template->useMatrix : false;
        if ($useMatrix) {
            // Validate input
            $widths = $template->columns; // Width (:value)
            $depths = $template->rows; // Height (:value)

            $matrix = $template->matrix;

            // Get input data
            $width = (int)$buyRequest->getData('width');
            $depthIndex = (int)$buyRequest->getData('depth') - 1; // @todo

            // Check width
            $widthIndex = null;
            foreach ($widths as $index => $row) {
                if ($width <= $row->value) {
                    $widthIndex = $index;
                    break;
                }
            }

            if ($widthIndex === null) {
                throw new \Exception('Invalid width');
            }

            // Check depth
            if (!array_key_exists($depthIndex, $depths)) {
                throw new \Exception('Invalid depth');
            }
            $depth = $depths[$depthIndex]->value;

            // Check the combination of width and depth
            if (!isset($matrix[$depthIndex][$widthIndex])) {
                throw new \Exception('Invalid entry in matrix');
            }

            // Get the base price for this combination
            $basePrice = $matrix[$depthIndex][$widthIndex]->value;
        } else {
            $width = isset($template->width) ? (float)$template->width : null;
            $depth = isset($template->depth) ? (float)$template->depth : null;
            $basePrice = isset($template->basePrice) ? (float)$template->basePrice : 0;
        }

        // Get selections
        $selections = $buyRequest->getData('select');
        if (!is_array($selections)) {
            throw new \Exception('Invalid input for selections!');
        }

        // Setup the configuration that should be stored on order row
        $configuration = array(
            'basePrice' => (float)$basePrice,
            'price' => (float)$basePrice,
            'width' => $width,
            'depth' => $depth,
            'supplements' => array()
        );

        foreach ($template->supplementGroups as $group) {
            if (!array_key_exists($group->id, $selections)) {
                throw new \Exception('Missing required field in group: ' . $group->name);
            }

            $selectedId = $selections[$group->id];
            $selection = null;
            foreach ($group->supplements as $supplement) {
                if ($supplement->id == $selectedId) {
                    $selection = $supplement;
                    break;
                }
            }

            if (!$selection) {
                throw new \Exception('Invalid selection in group: ' . $group->name);
            }

            $supplementPrice = $this->getPriceForSupplement($selection, $width, $depth);
            if (!$supplementPrice) {
                $price = $selection->defaultPrice;
            } else {
                $price = $supplementPrice->price;
            }

            $configuration['supplements'][] = array(
                'name' => $group->name,
                'groupNotes' => $group->notes,
                'selection' => $selection->name,
                'selectionNotes' => $selection->notes,
                'price' => (float)$price,
                'priceNotes' => $supplementPrice->notes ? $supplementPrice->notes : null,
            );

            $configuration['price'] += $price;
        }

        // Here we now have a valid configuration
        $buyRequest->setData('productconfiguration', json_encode($configuration));
        $product->addCustomOption('productconfiguration', json_encode($configuration));

        $result = parent::prepareForCartAdvanced($buyRequest, $product, $processMode);
        return $result;
    }

    protected function getPriceForSupplement($supplement, $width, $depth)
    {
        // Apply price rules
        foreach ($supplement->priceRules as $rule) {
            switch ($rule->propertyKey) {
                case 'width':
                    $value = $width;
                    break;
                case 'depth':
                    $value = $depth;
                    break;
                default;
                    $value = 0;
            }

            $applyRule = false;
            switch ($rule->operator) {
                case '>':
                    $applyRule = $value > $rule->propertyValue;
                    break;
                case '=':
                    $applyRule = $value == $rule->propertyValue;
                    break;
                case '<':
                    $applyRule = $value < $rule->propertyValue;
                    break;
            }

            if ($applyRule) {
                // Apply rule
                return $rule;
            }
        }

        return null;
    }


}