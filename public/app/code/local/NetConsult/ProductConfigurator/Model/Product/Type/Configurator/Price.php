<?php

class NetConsult_ProductConfigurator_Model_Product_Type_Configurator_Price extends Mage_Catalog_Model_Product_Type_Price
{
    public function getPrice($product)
    {
        $templateKey = $product->getProductconfiguratorTemplate();
        if (!$templateKey) {
            return parent::getPrice($product);
        }

        $template = Mage::helper('productconfigurator')->getProductTemplate($templateKey);
        if (!$template) {
            return parent::getPrice($product);
        }

        $useMatrix = isset($template->useMatrix) ? $template->useMatrix : false;
        if ($useMatrix) {
            $minPrice = null;
            foreach ($template->matrix as $dim1) {
                foreach ($dim1 as $dim2) {
                    if (!$dim2) {
                        continue;
                    }

                    $value = intval(trim($dim2->value));
                    if (!$value) {
                        continue;
                    }

                    if ($minPrice === null || $dim2->value < $minPrice) {
                        $minPrice = (float)$dim2->value;
                    }
                }
            }
        } else {
            $minPrice = isset($template->basePrice) ? (float)$template->basePrice : 0;
        }

        return $minPrice;
    }


    protected function _applyOptionsPrice($product, $qty, $finalPrice)
    {
        $productConfiguration = $product->getCustomOption('productconfiguration');
        if (!$productConfiguration) {
            return parent::_applyOptionsPrice($product, $qty, $finalPrice);
        }

        $conf = json_decode($productConfiguration->getValue(), true);
        if (!$conf) {
            return parent::_applyOptionsPrice($product, $qty, $finalPrice);
        }

        $finalPrice = (float)$conf['price'];

        return $finalPrice;
    }


}