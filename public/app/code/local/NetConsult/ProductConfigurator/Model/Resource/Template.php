<?php

class NetConsult_ProductConfigurator_Model_Resource_Template extends Mage_Core_Model_Resource_Db_Abstract
{

    protected function _construct()
    {
        $this->_init('productconfigurator/template', 'template_id');
    }

}