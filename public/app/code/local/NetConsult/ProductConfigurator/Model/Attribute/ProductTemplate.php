<?php

class NetConsult_ProductConfigurator_Model_Attribute_ProductTemplate extends Mage_Eav_Model_Entity_Attribute_Source_Abstract
{
    /**
     * Retrieve All options
     *
     * @return array
     */
    public function getAllOptions()
    {
        if (!is_null($this->_options)) {
            return $this->_options;
        }

        $options = array();

        foreach ($this->getModuleHelper()->getProductTemplates() as $productTemplate) {
            $options[] = array(
                'label' => $productTemplate->name,
                'value' => $productTemplate->id,
            );
        }

        return $this->_options = $options;
    }

    /**
     * @return NetConsult_ProductConfigurator_Helper_Data
     */
    protected function getModuleHelper()
    {
        return Mage::helper('productconfigurator');
    }
}
