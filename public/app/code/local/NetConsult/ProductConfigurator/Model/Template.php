<?php

class NetConsult_ProductConfigurator_Model_Template extends Mage_Core_Model_Abstract
{

    protected function _construct()
    {
        $this->_init('productconfigurator/template');
    }

}