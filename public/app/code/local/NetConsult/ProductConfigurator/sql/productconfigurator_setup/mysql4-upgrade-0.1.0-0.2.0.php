<?php
/**
 * @var Mage_Core_Model_Resource_Setup $this
 * @var Mage_Catalog_Model_Resource_Setup $installer
 */

$installer = Mage::getResourceModel('catalog/setup', 'catalog_setup');

$installer->startSetup();

$installer->addAttribute('catalog_product', 'productconfigurator_template', array(
    'group'                   => 'General',
    'label'                   => 'Product Template',
    'note'                    => '',
    //'type'                    => 'varchar',    //backend_type
    'input'                   => 'select',    //frontend_input
    'source'                  => 'productconfigurator/attribute_productTemplate',
    'user_defined'            => false,
    'backend'                 => '',
    'frontend'                => '',
    'global'                  => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'required'                => false,
    'visible_on_front'        => false,
    'apply_to'                => 'configurator',
    'is_configurable'         => false,
    'used_in_product_listing' => false,
    'sort_order'              => 5,
));

$installer->endSetup();
