<?php
/**
 * @var Mage_Core_Model_Resource_Setup $this
 * @var Mage_Catalog_Model_Resource_Setup $installer
 */

$installer = $this;
$installer->startSetup();

$installer->run("
    CREATE TABLE `{$installer->getTable('productconfigurator/template')}` (
      `template_id` int(11) NOT NULL auto_increment,
      `value` longtext,
      `timestamp` timestamp NOT NULL default CURRENT_TIMESTAMP,
      PRIMARY KEY  (`template_id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

Mage::getConfig()->cleanCache();
if ($value = Mage::getStoreConfig(NetConsult_ProductConfigurator_Helper_Data::TEMPLATE_CONFIG_KEY)) {
    $model = Mage::getModel('productconfigurator/template');
    $model->setValue($value);
    $model->save();
}

$installer->endSetup();
