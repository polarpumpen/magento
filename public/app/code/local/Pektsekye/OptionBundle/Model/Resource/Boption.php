<?php

class Pektsekye_OptionBundle_Model_Resource_Boption extends Mage_Core_Model_Resource_Db_Abstract
{
    public function _construct()
    {
        $this->_init('optionbundle/boption', 'boption_id');
    }
 
 

	 
  
    public function getBoptions($productId, $storeId)
    {        
      $select = $this->_getReadAdapter()->select()
        ->from(array('main_table' => $this->getMainTable()), array('boption_id','layout','popup','use_product_image','use_product_description'))
        ->join(array('default_boption_note'=>$this->getTable('optionbundle/boption_note')),
            '`default_boption_note`.boption_id=`main_table`.boption_id AND `default_boption_note`.store_id=0',
            array('default_note'=>'note'))
        ->joinLeft(array('store_boption_note'=>$this->getTable('optionbundle/boption_note')),
            '`store_boption_note`.boption_id=`main_table`.boption_id AND '.$this->_getWriteAdapter()->quoteInto('`store_boption_note`.store_id=?', $storeId),
            array('store_note'=>'note',
            'note'=>new Zend_Db_Expr('IFNULL(`store_boption_note`.note,`default_boption_note`.note)')))      
        ->where("product_id=?", $productId); 
        
      return $this->_getReadAdapter()->fetchAssoc($select);                                 
    } 
    
    
    public function getUsedBoptionIds()
    {        
      $select = $this->_getReadAdapter()->select()
        ->from(array('ob' => $this->getMainTable()), 'product_id')
        ->join(array('cp' => $this->getTable('catalog/product')), 'cp.entity_id = ob.product_id', 'sku')  
        ->join(array('bo' => $this->getTable('bundle/option')), 'bo.parent_id=cp.entity_id', 'option_id')
        ->join(array('ot' => $this->getTable('bundle/option_value')), 'ot.option_id = bo.option_id AND store_id=0', array())            
        ->join(array('bs' => $this->getTable('bundle/selection')), 'bo.option_id=bs.option_id', 'selection_id')  
        ->join(array('scp' => $this->getTable('catalog/product')), 'scp.entity_id = bs.product_id', array())           
        ->distinct(true)                          
        ->order(array('bo.position','ot.title','bs.position','scp.sku')); 
        
      return $this->_getReadAdapter()->fetchAll($select);                                    
    }


    public function getStoreNotes($boptionId)
    {        
      $select = $this->_getReadAdapter()->select()
        ->from(array('cs' => $this->getTable('core/store')), 'code')        
        ->join(array('obn' => $this->getTable('optionbundle/boption_note')), 'obn.store_id = cs.store_id', 'note') 
        ->where('boption_id=?', $boptionId); 
               
      return $this->_getReadAdapter()->fetchPairs($select);                           
    }


    public function getBoptionIds($skus)
    {        
      $select = $this->_getReadAdapter()->select()
        ->join(array('cp' => $this->getTable('catalog/product')), 'cp.entity_id = ob.product_id',  array('product_sku' => 'sku','product_id' => 'entity_id'))  
        ->join(array('bo' => $this->getTable('bundle/option')), 'bo.parent_id=cp.entity_id', 'option_id')
        ->join(array('ot' => $this->getTable('bundle/option_value')), 'ot.option_id = bo.option_id AND store_id=0', array())            
        ->join(array('bs' => $this->getTable('bundle/selection')), 'bo.option_id=bs.option_id', 'selection_id')  
        ->join(array('scp' => $this->getTable('catalog/product')), 'scp.entity_id = bs.product_id', array())           
        ->where("cp.sku IN (?)", $skus)                          
        ->order(array('bo.position','ot.title','bs.position','scp.sku')); 
        
      return $this->_getReadAdapter()->fetchAll($select);                                
    }
    

    public function saveBoptions($productId, $storeId, $boptions)
    {
      $storeId = (int) $storeId;
      $read = $this->_getReadAdapter();
      $write = $this->_getWriteAdapter();
		          
      $oldOptions = $this->getBoptions($productId, $storeId);      

         
      foreach($boptions as $boptionId => $option){ 
        $boptionId = (int) $boptionId;        
                
        $note = '';    
        $layout = '';  
        $popup = 0;
        $useProductImage = 1;           
        $useProductDescription = 1;         
        if (isset($option['note']) || isset($option['scope'])){ // option images tab loaded
          $note = isset($option['note']) ? $option['note'] : '';
          $layout = isset($option['layout']) ? $option['layout'] : '';  
          $popup = isset($option['popup']) ? $option['popup'] : 0;  
          $useProductImage = isset($option['use_product_image']) ? $option['use_product_image'] : 0;           
          $useProductDescription = isset($option['use_product_description']) ? $option['use_product_description'] : 0;                    
        } elseif (isset($oldOptions[$boptionId])){
          $note = $oldOptions[$boptionId]['note'];
          $layout = $oldOptions[$boptionId]['layout'];  
          $popup = $oldOptions[$boptionId]['popup']; 
          $useProductImage = $oldOptions[$boptionId]['use_product_image'];           
          $useProductDescription = $oldOptions[$boptionId]['use_product_description'];                  
        } 
                   
        $data = array(
          'boption_id'              => $boptionId, 
          'product_id'              => $productId,           
          'layout'                  => $layout,   
          'popup'                   => $popup,
          'use_product_image'       => $useProductImage,
          'use_product_description' => $useProductDescription                                 
        );

        $statement = $read->select()
          ->from($this->getMainTable())
          ->where('boption_id=?', $boptionId);

        if ($read->fetchRow($statement)) {
            $write->update(
              $this->getMainTable(),
              $data,
              $write->quoteInto('boption_id=?', $boptionId)
            );
        } else {
          $write->insert($this->getMainTable(), $data);
        }
        
        if (isset($option['notes'])){ // csv import
          foreach($option['notes'] as $sId => $note){  error_log($productId.' '.$boptionId.' '.$sId.' '.$note);        
            $this->saveNote($note, $boptionId, $sId, false);
          }
        } else {        
          $scope = isset($option['scope']);
          $this->saveNote($note, $boptionId, $storeId, $scope); 
        }        
      }
 
                           
    }
 
 
 	   protected function saveNote($note, $boptionId, $storeId, $scope)
    {
		    $read = $this->_getReadAdapter();
		    $write = $this->_getWriteAdapter();
		    $noteTable = $this->getTable('optionbundle/boption_note');
		    		
        if (!$scope) {		
		      $statement = $read->select()
			      ->from($noteTable, array('note'))
			      ->where('boption_id = '.$boptionId.' AND store_id = ?', 0);
          $default = $read->fetchRow($statement);
		      if (!empty($default)) {
			      if ($storeId == '0' || $default['note'] == '') {
				      $write->update(
					      $noteTable,
						      array('note' => $note),
						      $write->quoteInto('boption_id='.$boptionId.' AND store_id=?', 0)
				      );
			      }
		      } else {
			      $write->insert(
				      $noteTable,
					      array(
						      'boption_id' => $boptionId,
						      'store_id' => 0,
						      'note' => $note
			      ));
		      }
        }
        
		    if ($storeId != '0' && !$scope) {
			    $statement = $read->select()
				    ->from($noteTable)
				    ->where('boption_id = '.$boptionId.' AND store_id = ?', $storeId);

			    if ($read->fetchRow($statement)) {
				    $write->update(
					    $noteTable,
						    array('note' => $note),
						    $write->quoteInto('boption_id='.$boptionId.' AND store_id=?', $storeId));
			    } else {
				    $write->insert(
					    $noteTable,
						    array(
							    'boption_id' => $boptionId,
							    'store_id' => $storeId,
							    'note' => $note
				    ));
			    }
		    } elseif ($scope){
            $write->delete(
                $noteTable,
                $write->quoteInto('boption_id = '.$boptionId.' AND store_id = ?', $storeId)
            );		    
		    }
	} 
    


    public function saveBoptionsOrder($boptionId, $order)
    {       		   
      $this->_getWriteAdapter()->update($this->getTable('bundle/option'), array('position' => $order), array('option_id = ?' => $boptionId));                          
    }  


    public function saveBoptionsDefault($boptionId, $isDefault, $default)
    { 
      $this->_getWriteAdapter()->update($this->getTable('bundle/selection'), array('is_default' => $isDefault), array('selection_id IN (?)' => $default));                          
    }

}
