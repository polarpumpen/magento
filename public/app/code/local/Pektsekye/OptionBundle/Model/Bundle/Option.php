<?php

class Pektsekye_OptionBundle_Model_Bundle_Option extends Mage_Bundle_Model_Option
{

    public function getRequired()
    {
        $ids = Mage::registry('ob_ids');
        if ($ids && isset($ids['b'][$this->getId()])){
          return false; 
        }

        return $this->getData('required');
    }

}
