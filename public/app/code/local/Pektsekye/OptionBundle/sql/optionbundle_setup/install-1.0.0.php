<?php


/** @var $installer Pektsekye_OptionBundle_Model_Resource_Setup */
$installer = $this;

/**
 * Prepare database for tables setup
 */
$installer->startSetup();


$installer->getConnection()->dropTable($installer->getTable('optionbundle/boption'));
$table = $installer->getConnection()
    ->newTable($installer->getTable('optionbundle/boption'))        
    ->addColumn('boption_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        'default'   => '0',
        ), 'Bundle Option ID') 
    ->addColumn('product_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        'default'   => '0',
        ), 'Product ID')                
    ->addColumn('layout', Varien_Db_Ddl_Table::TYPE_TEXT, '64k', array(
        ), 'Layout')   
    ->addColumn('popup', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'unsigned'  => true,
        'nullable'  => false,        
        'default'   => '0',
        ), 'Popup')
    ->addColumn('use_product_image', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'unsigned'  => true,
        'nullable'  => false,        
        'default'   => '1',
        ), 'Use Product Image') 
    ->addColumn('use_product_description', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'unsigned'  => true,
        'nullable'  => false,        
        'default'   => '1',
        ), 'Use Product Description')                                          
    ->addForeignKey($installer->getFkName('optionbundle/boption', 'product_id', 'catalog/product', 'entity_id'),
        'product_id', $installer->getTable('catalog/product'), 'entity_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)                              
    ->addForeignKey($installer->getFkName('optionbundle/boption', 'boption_id', 'bundle/option', 'option_id'),
        'boption_id', $installer->getTable('bundle/option'), 'option_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)                 
    ->setComment('OptionBundle Bundle Option Table');
$installer->getConnection()->createTable($table);


$installer->getConnection()->dropTable($installer->getTable('optionbundle/boption_note'));
$table = $installer->getConnection()
    ->newTable($installer->getTable('optionbundle/boption_note'))       
    ->addColumn('boption_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        'default'   => '0',
        ), 'Bundle Option ID')  
    ->addColumn('store_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        ), 'Store ID')           
    ->addColumn('note', Varien_Db_Ddl_Table::TYPE_TEXT, '64k', array(
        ), 'Note')                   
    ->addForeignKey($installer->getFkName('optionbundle/boption_note', 'boption_id', 'optionbundle/boption', 'boption_id'),
        'boption_id', $installer->getTable('optionbundle/boption'), 'boption_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)                              
    ->addForeignKey($installer->getFkName('optionbundle/boption_note', 'store_id', 'core/store', 'store_id'),
        'store_id', $installer->getTable('core/store'), 'store_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)                 
    ->setComment('OptionBundle Bundle Option Note Table');
$installer->getConnection()->createTable($table);


$installer->getConnection()->dropTable($installer->getTable('optionbundle/bvalue'));
$table = $installer->getConnection()
    ->newTable($installer->getTable('optionbundle/bvalue'))
    ->addColumn('bvalue_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        'default'   => '0',
        ), 'Bundle Value ID')       
    ->addColumn('product_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        'default'   => '0',
        ), 'Product ID')        
    ->addColumn('image', Varien_Db_Ddl_Table::TYPE_TEXT, '64k', array(
        ), 'Image')                              
    ->addForeignKey($installer->getFkName('optionbundle/bvalue', 'product_id', 'catalog/product', 'entity_id'),
        'product_id', $installer->getTable('catalog/product'), 'entity_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)                              
    ->addForeignKey($installer->getFkName('optionbundle/bvalue', 'bvalue_id', 'bundle/selection', 'selection_id'),
        'bvalue_id', $installer->getTable('bundle/selection'), 'selection_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)                 
    ->setComment('OptionBundle Bundle Value Table');
$installer->getConnection()->createTable($table);


$installer->getConnection()->dropTable($installer->getTable('optionbundle/bvalue_description'));
$table = $installer->getConnection()
    ->newTable($installer->getTable('optionbundle/bvalue_description'))       
    ->addColumn('bvalue_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        'default'   => '0',
        ), 'Bundle Value ID')  
    ->addColumn('store_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        ), 'Store ID')           
    ->addColumn('description', Varien_Db_Ddl_Table::TYPE_TEXT, '64k', array(
        ), 'Description')                   
    ->addForeignKey($installer->getFkName('optionbundle/bvalue_description', 'bvalue_id', 'optionbundle/bvalue', 'bvalue_id'),
        'bvalue_id', $installer->getTable('optionbundle/bvalue'), 'bvalue_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)                              
    ->addForeignKey($installer->getFkName('optionbundle/bvalue_description', 'store_id', 'core/store', 'store_id'),
        'store_id', $installer->getTable('core/store'), 'store_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)                 
    ->setComment('OptionBundle Bundle Value Description Table');
$installer->getConnection()->createTable($table);

$installer->getConnection()->dropTable($installer->getTable('optionbundle/option'));
$table = $installer->getConnection()
    ->newTable($installer->getTable('optionbundle/option'))      
    ->addColumn('option_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        'default'   => '0',
        ), 'Option ID')  
    ->addColumn('product_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        'default'   => '0',
        ), 'Product ID')                  
    ->addColumn('default', Varien_Db_Ddl_Table::TYPE_TEXT, '64k', array(
        ), 'Default') 
    ->addColumn('note', Varien_Db_Ddl_Table::TYPE_TEXT, '64k', array(
        ), 'Note') 
    ->addColumn('layout', Varien_Db_Ddl_Table::TYPE_TEXT, '64k', array(
        ), 'Layout')   
    ->addColumn('popup', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'unsigned'  => true,
        'nullable'  => false,        
        'default'   => '0',
        ), 'Popup')                              
    ->addForeignKey($installer->getFkName('optionbundle/option', 'product_id', 'catalog/product', 'entity_id'),
        'product_id', $installer->getTable('catalog/product'), 'entity_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)          
    ->addForeignKey($installer->getFkName('optionbundle/option', 'option_id', 'catalog/product_option', 'option_id'),
        'option_id', $installer->getTable('catalog/product_option'), 'option_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)                 
    ->setComment('OptionBundle Option Table');
$installer->getConnection()->createTable($table);


$installer->getConnection()->dropTable($installer->getTable('optionbundle/option_note'));
$table = $installer->getConnection()
    ->newTable($installer->getTable('optionbundle/option_note'))       
    ->addColumn('option_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        'default'   => '0',
        ), 'Option ID')  
    ->addColumn('store_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        ), 'Store ID')           
    ->addColumn('note', Varien_Db_Ddl_Table::TYPE_TEXT, '64k', array(
        ), 'Note')                   
    ->addForeignKey($installer->getFkName('optionbundle/option_note', 'option_id', 'optionbundle/option', 'option_id'),
        'option_id', $installer->getTable('optionbundle/option'), 'option_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)                              
    ->addForeignKey($installer->getFkName('optionbundle/option_note', 'store_id', 'core/store', 'store_id'),
        'store_id', $installer->getTable('core/store'), 'store_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)                 
    ->setComment('OptionBundle Option Note Table');
$installer->getConnection()->createTable($table);


$installer->getConnection()->dropTable($installer->getTable('optionbundle/value'));
$table = $installer->getConnection()
    ->newTable($installer->getTable('optionbundle/value'))       
    ->addColumn('value_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        'default'   => '0',
        ), 'Value ID')   
    ->addColumn('product_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        'default'   => '0',
        ), 'Product ID')                 
    ->addColumn('image', Varien_Db_Ddl_Table::TYPE_TEXT, '64k', array(
        ), 'Image')                                       
    ->addForeignKey($installer->getFkName('optionbundle/value', 'product_id', 'catalog/product', 'entity_id'),
        'product_id', $installer->getTable('catalog/product'), 'entity_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)          
    ->addForeignKey($installer->getFkName('optionbundle/value', 'value_id', 'catalog/product_option_type_value', 'option_type_id'),
        'value_id', $installer->getTable('catalog/product_option_type_value'), 'option_type_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)                 
    ->setComment('OptionBundle Value Table');
$installer->getConnection()->createTable($table);


$installer->getConnection()->dropTable($installer->getTable('optionbundle/value_description'));
$table = $installer->getConnection()
    ->newTable($installer->getTable('optionbundle/value_description'))        
    ->addColumn('value_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        'default'   => '0',
        ), 'Value ID')  
    ->addColumn('store_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        ), 'Store ID')           
    ->addColumn('description', Varien_Db_Ddl_Table::TYPE_TEXT, '64k', array(
        ), 'Description')                   
    ->addForeignKey($installer->getFkName('optionbundle/value_description', 'value_id', 'optionbundle/value', 'value_id'),
        'value_id', $installer->getTable('optionbundle/value'), 'value_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)                              
    ->addForeignKey($installer->getFkName('optionbundle/value_description', 'store_id', 'core/store', 'store_id'),
        'store_id', $installer->getTable('core/store'), 'store_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)                 
    ->setComment('OptionBundle Value Description Table');
$installer->getConnection()->createTable($table);


$installer->getConnection()->dropTable($installer->getTable('optionbundle/selection_to_boption'));
$table = $installer->getConnection()
    ->newTable($installer->getTable('optionbundle/selection_to_boption'))     
    ->addColumn('selection_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        'default'   => '0',
        ), 'Bundle Selection ID') 
    ->addColumn('product_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        'default'   => '0',
        ), 'Product ID')          
    ->addColumn('children_boption_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,        
        'default'   => '0',
        ), 'Children Bundle Option Id')        
    ->addIndex($installer->getIdxName('optionbundle/selection_to_boption', 'children_boption_id'),
        'children_boption_id')      
    ->addForeignKey($installer->getFkName('optionbundle/selection_to_boption', 'product_id', 'catalog/product', 'entity_id'),
        'product_id', $installer->getTable('catalog/product'), 'entity_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)           
    ->addForeignKey($installer->getFkName('optionbundle/selection_to_boption', 'selection_id', 'bundle/selection', 'selection_id'),
        'selection_id', $installer->getTable('bundle/selection'), 'selection_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE) 
    ->addForeignKey($installer->getFkName('optionbundle/selection_to_boption', 'children_boption_id', 'bundle/option', 'option_id'),
        'children_boption_id', $installer->getTable('bundle/option'), 'option_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)                 
    ->setComment('OptionBundle Bundle Selection To Bundle Option Table');
$installer->getConnection()->createTable($table);


$installer->getConnection()->dropTable($installer->getTable('optionbundle/selection_to_selection'));
$table = $installer->getConnection()
    ->newTable($installer->getTable('optionbundle/selection_to_selection'))
    ->addColumn('selection_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        'default'   => '0',
        ), 'Bundle Selection ID')  
    ->addColumn('product_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        'default'   => '0',
        ), 'Product ID')          
    ->addColumn('children_selection_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,        
        'default'   => '0',
        ), 'Children Bundle Selection Id')        
    ->addIndex($installer->getIdxName('optionbundle/selection_to_selection', 'children_selection_id'),
        'children_selection_id')  
    ->addForeignKey($installer->getFkName('optionbundle/selection_to_selection', 'product_id', 'catalog/product', 'entity_id'),
        'product_id', $installer->getTable('catalog/product'), 'entity_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)               
    ->addForeignKey($installer->getFkName('optionbundle/selection_to_selection', 'selection_id', 'bundle/selection', 'selection_id'),
        'selection_id', $installer->getTable('bundle/selection'), 'selection_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE) 
    ->addForeignKey($installer->getFkName('optionbundle/selection_to_selection', 'children_selection_id', 'bundle/selection', 'selection_id'),
        'children_selection_id', $installer->getTable('bundle/selection'), 'selection_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)                 
    ->setComment('OptionBundle Bundle Selection To Bundle Selection Table');
$installer->getConnection()->createTable($table);



$installer->getConnection()->dropTable($installer->getTable('optionbundle/selection_to_option'));
$table = $installer->getConnection()
    ->newTable($installer->getTable('optionbundle/selection_to_option'))  
    ->addColumn('selection_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        'default'   => '0',
        ), 'Bundle Selection ID')  
    ->addColumn('product_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        'default'   => '0',
        ), 'Product ID')         
    ->addColumn('children_option_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,        
        'default'   => '0',
        ), 'Children Option Id')        
    ->addIndex($installer->getIdxName('optionbundle/selection_to_option', 'children_option_id'),
        'children_option_id')  
    ->addForeignKey($installer->getFkName('optionbundle/selection_to_option', 'product_id', 'catalog/product', 'entity_id'),
        'product_id', $installer->getTable('catalog/product'), 'entity_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)               
    ->addForeignKey($installer->getFkName('optionbundle/selection_to_option', 'selection_id', 'bundle/selection', 'selection_id'),
        'selection_id', $installer->getTable('bundle/selection'), 'selection_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE) 
    ->addForeignKey($installer->getFkName('optionbundle/selection_to_option', 'children_option_id', 'catalog/product_option', 'option_id'),
        'children_option_id', $installer->getTable('catalog/product_option'), 'option_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)                 
    ->setComment('OptionBundle Bundle Selection To Option Table');
$installer->getConnection()->createTable($table);



$installer->getConnection()->dropTable($installer->getTable('optionbundle/selection_to_value'));
$table = $installer->getConnection()
    ->newTable($installer->getTable('optionbundle/selection_to_value'))       
    ->addColumn('selection_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        'default'   => '0',
        ), 'Bundle Selection ID')  
    ->addColumn('product_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        'default'   => '0',
        ), 'Product ID')          
    ->addColumn('children_value_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,        
        'default'   => '0',
        ), 'Children Option Value Id')        
    ->addIndex($installer->getIdxName('optionbundle/selection_to_value', 'children_value_id'),
        'children_value_id')  
    ->addForeignKey($installer->getFkName('optionbundle/selection_to_value', 'product_id', 'catalog/product', 'entity_id'),
        'product_id', $installer->getTable('catalog/product'), 'entity_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)               
    ->addForeignKey($installer->getFkName('optionbundle/selection_to_value', 'selection_id', 'bundle/selection', 'selection_id'),
        'selection_id', $installer->getTable('bundle/selection'), 'selection_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE) 
    ->addForeignKey($installer->getFkName('optionbundle/selection_to_value', 'children_value_id', 'catalog/product_option_type_value', 'option_type_id'),
        'children_value_id', $installer->getTable('catalog/product_option_type_value'), 'option_type_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)                 
    ->setComment('OptionBundle Bundle Selection To Option Value Table');
$installer->getConnection()->createTable($table);


$installer->getConnection()->dropTable($installer->getTable('optionbundle/value_to_boption'));
$table = $installer->getConnection()
    ->newTable($installer->getTable('optionbundle/value_to_boption'))  
    ->addColumn('value_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        'default'   => '0',
        ), 'Option Value ID')  
    ->addColumn('product_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        'default'   => '0',
        ), 'Product ID')             
    ->addColumn('children_boption_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,        
        'default'   => '0',
        ), 'Children Bundle Option Id')        
    ->addIndex($installer->getIdxName('optionbundle/value_to_boption', 'children_boption_id'),
        'children_boption_id')   
    ->addForeignKey($installer->getFkName('optionbundle/value_to_boption', 'product_id', 'catalog/product', 'entity_id'),
        'product_id', $installer->getTable('catalog/product'), 'entity_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)              
    ->addForeignKey($installer->getFkName('optionbundle/value_to_boption', 'value_id', 'catalog/product_option_type_value', 'option_type_id'),
        'value_id', $installer->getTable('catalog/product_option_type_value'), 'option_type_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE) 
    ->addForeignKey($installer->getFkName('optionbundle/value_to_boption', 'children_boption_id', 'bundle/option', 'option_id'),
        'children_boption_id', $installer->getTable('bundle/option'), 'option_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)                 
    ->setComment('OptionBundle Option Value To Bundle Option Table');
$installer->getConnection()->createTable($table);


$installer->getConnection()->dropTable($installer->getTable('optionbundle/value_to_selection'));
$table = $installer->getConnection()
    ->newTable($installer->getTable('optionbundle/value_to_selection')) 
    ->addColumn('value_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        'default'   => '0',
        ), 'Option Value ID')  
    ->addColumn('product_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        'default'   => '0',
        ), 'Product ID')         
    ->addColumn('children_selection_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,        
        'default'   => '0',
        ), 'Children Bundle Selection Id')        
    ->addIndex($installer->getIdxName('optionbundle/value_to_selection', 'children_selection_id'),
        'children_selection_id')  
    ->addForeignKey($installer->getFkName('optionbundle/value_to_selection', 'product_id', 'catalog/product', 'entity_id'),
        'product_id', $installer->getTable('catalog/product'), 'entity_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)               
    ->addForeignKey($installer->getFkName('optionbundle/value_to_selection', 'value_id', 'catalog/product_option_type_value', 'option_type_id'),
        'value_id', $installer->getTable('catalog/product_option_type_value'), 'option_type_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE) 
    ->addForeignKey($installer->getFkName('optionbundle/value_to_selection', 'children_selection_id', 'bundle/selection', 'selection_id'),
        'children_selection_id', $installer->getTable('bundle/selection'), 'selection_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)                 
    ->setComment('OptionBundle Option Value To Bundle Selection Table');
$installer->getConnection()->createTable($table);



$installer->getConnection()->dropTable($installer->getTable('optionbundle/value_to_option'));
$table = $installer->getConnection()
    ->newTable($installer->getTable('optionbundle/value_to_option'))     
    ->addColumn('value_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        'default'   => '0',
        ), 'Option Value ID')  
    ->addColumn('product_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        'default'   => '0',
        ), 'Product ID')          
    ->addColumn('children_option_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,        
        'default'   => '0',
        ), 'Children Option Id')        
    ->addIndex($installer->getIdxName('optionbundle/value_to_option', 'children_option_id'),
        'children_option_id') 
    ->addForeignKey($installer->getFkName('optionbundle/value_to_option', 'product_id', 'catalog/product', 'entity_id'),
        'product_id', $installer->getTable('catalog/product'), 'entity_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)                
    ->addForeignKey($installer->getFkName('optionbundle/value_to_option', 'value_id', 'catalog/product_option_type_value', 'option_type_id'),
        'value_id', $installer->getTable('catalog/product_option_type_value'), 'option_type_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE) 
    ->addForeignKey($installer->getFkName('optionbundle/value_to_option', 'children_option_id', 'catalog/product_option', 'option_id'),
        'children_option_id', $installer->getTable('catalog/product_option'), 'option_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)                 
    ->setComment('OptionBundle Option Value To Option Table');
$installer->getConnection()->createTable($table);



$installer->getConnection()->dropTable($installer->getTable('optionbundle/value_to_value'));
$table = $installer->getConnection()
    ->newTable($installer->getTable('optionbundle/value_to_value')) 
    ->addColumn('value_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        'default'   => '0',
        ), 'Option Value ID') 
    ->addColumn('product_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        'default'   => '0',
        ), 'Product ID')                 
    ->addColumn('children_value_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,        
        'default'   => '0',
        ), 'Children Option Value Id')        
    ->addIndex($installer->getIdxName('optionbundle/value_to_value', 'children_value_id'),
        'children_value_id')  
    ->addForeignKey($installer->getFkName('optionbundle/value_to_value', 'product_id', 'catalog/product', 'entity_id'),
        'product_id', $installer->getTable('catalog/product'), 'entity_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)              
    ->addForeignKey($installer->getFkName('optionbundle/value_to_value', 'value_id', 'catalog/product_option_type_value', 'option_type_id'),
        'value_id', $installer->getTable('catalog/product_option_type_value'), 'option_type_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE) 
    ->addForeignKey($installer->getFkName('optionbundle/value_to_value', 'children_value_id', 'catalog/product_option_type_value', 'option_type_id'),
        'children_value_id', $installer->getTable('catalog/product_option_type_value'), 'option_type_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)                 
    ->setComment('OptionBundle Option Value To Option Value Table');
$installer->getConnection()->createTable($table);




$prefix = (string) Mage::getConfig()->getTablePrefix();
$odOptionTable = $prefix . 'optiondependent_option';
$odValueTable  = $prefix . 'optiondependent_value'; 

if ($this->tableExists($odOptionTable) && $this->tableExists($odValueTable)){
  $conn = $installer->getConnection();

  $optionId = array();
  $select = $conn->select()->from($odOptionTable);      
  foreach ($conn->fetchAll($select) as $r){
    $optionId[$r['product_id']][$r['row_id']] = $r['option_id'];   
  }

  $valueRows = $conn->fetchAll($conn->select()->from($odValueTable));
  
  $valueId = array();
  $productIds = array();            
  foreach ($valueRows as $r){
    $valueId[$r['product_id']][$r['row_id']] = $r['option_type_id'];
    $productIds[] = $r['product_id'];         
  }    
 
  $data = array(
      'selection_to_boption' => array(),
      'selection_to_option'  => array(),
      'value_to_boption'  => array(),
      'value_to_option'   => array(),
      'selection_to_selection'  => array(),
      'selection_to_value'   => array(),
      'value_to_selection'   => array(),
      'value_to_value'    => array()     
  );    

  foreach ($valueRows as $r){
    $vId = $r['option_type_id'];
    $productId = $r['product_id'];
    if (!empty($r['children'])){
      foreach(explode(',', $r['children']) as $rowId){
        if (isset($optionId[$productId][$rowId])){
          $data['value_to_option'][$vId][] = $optionId[$productId][$rowId];         
        } elseif (isset($valueId[$productId][$rowId])){
          $data['value_to_value'][$vId][] = $valueId[$productId][$rowId];          
        }
      }
    }   
  }
  
  foreach ($productIds as $productId)   
    Mage::getResourceModel('optionbundle/relation')->saveRelationsData($productId, $data);   
}
/**
 * Prepare database after tables setup
 */
$installer->endSetup();
