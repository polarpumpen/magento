<?php
class Pektsekye_OptionBundle_Block_Ob_Options extends Mage_Adminhtml_Block_Widget
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{

    protected $_options;
    protected $_boptions;    
    protected $_relations;
    protected $_relationData;    

    
    public function __construct()
    {
      parent::__construct();
      $this->setId('optionbundle_options');
      $this->setSkipGenerateContent(true);        
      $this->setTemplate('optionbundle/ob/options.phtml');
    }



    public function getProduct()
    {
      return Mage::registry('current_product');
    }  
   

 
 
     public function getBoptions()
    {
      if (!$this->_boptions) {
          $product = $this->getProduct();
          $typeInstance = $product->getTypeInstance(true);
          $typeInstance->setStoreFilter($product->getStoreId(), $product);

          $optionCollection = $typeInstance->getOptionsCollection($product);

          $selectionCollection = $typeInstance->getSelectionsCollection(
              $typeInstance->getOptionsIds($product),
              $product
          );

          $this->_boptions = $optionCollection->appendSelections($selectionCollection);
      }

      return $this->_boptions;
    }



    public function getOptions()
    {
      if (!isset($this->_options)){
        $options = array();
       
        $hasOrder = false;            
        $rData = $this->getRelationData();
        
        if ($this->getProduct()->getTypeId() == Mage_Catalog_Model_Product_Type::TYPE_BUNDLE){
          $t = 'b';
          $obOptions = Mage::getModel('optionbundle/boption')->getBoptions($this->getProduct()->getId());         
          foreach ($this->getBoptions() as $_option) {
          
              if (!$_option->getSelections()) {
                  continue;
              }
                            
              $id = (int) $_option->getId();
              $option = array(
                  'type' => $t,
                  'id' => $id,
                  'title' => $this->htmlEscape($_option->getTitle()),
                  'titleNotEscaped' => $_option->getTitle(),                  
                  'sort_order' => $_option->getPosition(),
                  'required' => isset($obOptions[$id]['required']) ? (int) $obOptions[$id]['required'] : 0,                   
                  'has_parent' => isset($rData['pVIdsByOId'][$t][$id]), 
                  'selection_type' =>  in_array($_option->getType(), array('multi', 'checkbox')) ? 'multiple' : 'single',                    
                  'values' => array(),
                  'default' =>  ''                    
              );

          
              foreach ($_option->getSelections() as $selection) {
                $valueId = (int) $selection->getSelectionId(); 
                                                               
                $option['values'][] = array(
                    'id' => $valueId,
                    'title' => $this->htmlEscape($selection->getName()),
                    'price' => $this->getPriceValue($selection->getSelectionPriceValue(), $selection->getSelectionPriceType()),
                    'has_parent' => isset($rData['pVIdsByVId'][$t][$valueId]) || isset($rData['pVIdsByOId'][$t][$id]),   
                    'has_children' => isset($rData['cVIdsByVId'][$t][$valueId]) || isset($rData['cOIdsByVId'][$t][$valueId])                                                       
                    );
                    
                if ($selection->getIsDefault())   
                  $option['default'] .=  ($option['default'] == '' ? '' : ',') . $valueId;
              }
            
            
            
             $options[] = $option;
             $hasOrder |= $_option->getPosition() > 0;             
          }        
        }

        $t = 'o';
        $obOptions = Mage::getModel('optionbundle/option')->getOptions($this->getProduct()->getId());          
        foreach ($this->getProduct()->getOptions() as $_option) {
          $id = (int) $_option->getOptionId();
          $option = array(
            'type' => $t,            
            'id' => $id,
            'title' => $this->htmlEscape($_option->getTitle()),  
            'titleNotEscaped' => $_option->getTitle(),                     
            'sort_order' => $_option->getSortOrder(), 
            'required' => $_option->getIsRequire(),             
            'default' => isset($obOptions[$id]['default']) ? $obOptions[$id]['default'] : '', 
            'values' => array()
          );        


          if ($_option->getGroupByType() == Mage_Catalog_Model_Product_Option::OPTION_GROUP_SELECT) {
          
            $option['selection_type'] = $_option->getType() == Mage_Catalog_Model_Product_Option::OPTION_TYPE_DROP_DOWN || $_option->getType() == Mage_Catalog_Model_Product_Option::OPTION_TYPE_RADIO ? 'single' : 'multiple';
            
            foreach ($_option->getValues() as $value) {
              $valueId = (int) $value->getOptionTypeId();
              $option['values'][] = array(
                  'id' => $valueId,
                  'title' => $this->htmlEscape($value->getTitle()),
                  'price' => $this->getPriceValue($value->getPrice(), $value->getPriceType() == 'percent'),
                  'has_parent' => isset($rData['pVIdsByVId'][$t][$valueId]) || isset($rData['pVIdsByOId'][$t][$id]),   
                  'has_children' => isset($rData['cVIdsByVId'][$t][$valueId]) || isset($rData['cOIdsByVId'][$t][$valueId])                                                     
                  );
            }
            
           } else {
              $option['has_parent'] = isset($rData['pVIdsByOId'][$t][$id]);                
           }
           
           $options[] = $option;
        }       
       
        if ($hasOrder){   
          usort($options, array(Mage::helper('optionbundle'), "sortOptions"));
        }
         
        $this->_options = $options; 
     }
                        
     return $this->_options;                         
    }                            



    public function getRelations()
    {
      if (!isset($this->_relations))
        $this->_relations = Mage::getModel('optionbundle/relation')->getRelations($this->getProduct()->getId());
      return $this->_relations;                         
    } 



    public function getRelationData()
    {
      if (!isset($this->_relationData))
        $this->_relationData = Mage::getModel('optionbundle/relation')->getRelationData($this->getProduct());
      return $this->_relationData;                         
    }

    
    
    
    public function hasCustomOptions()
    {     
      return count($this->getProduct()->getOptions()) > 0;                         
    } 
    
    
           
    public function isBundle()
    {     
      return $this->getProduct()->getTypeId() == Mage_Catalog_Model_Product_Type::TYPE_BUNDLE;                         
    }     
    
    
    
    public function hasBoptions()
    {     
      return count($this->getBoptions()) > 0;                         
    }    
    
    
    
    public function getDataJson()
    {
      $rData = $this->getRelationData();
      $rData['options'] = $this->getOptions();
                    
      return Zend_Json::encode($rData);                         
    }



    public function getRelationsJson()
    {
      return Zend_Json::encode($this->getRelations());                         
    }    

	  
	  
    public function getPriceValue($value, $isPercent)
    {
      if ((int)$value == 0)
        return '';        
      return $isPercent ? (float) $value : number_format($value, 2, null, '');
    }	  


    public function getTabUrl()
    {
        return $this->getUrl('adminhtml/ob_options/index', array('_current'=>true));
    }

    public function getTabClass()
    {
        return 'ajax';
    }
        
    public function getTabLabel()
    {
        return $this->__('Option Relations');
    }
        
    public function getTabTitle()
    {
        return $this->__('Option Relations');
    }
        
    public function canShowTab()
    {
        return $this->getProduct()->getId() != null;
    }    
    
    public function isHidden()
    {
        return false;
    }

}
