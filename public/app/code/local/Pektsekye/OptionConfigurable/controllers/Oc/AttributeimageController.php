<?php

class Pektsekye_OptionConfigurable_Oc_AttributeimageController extends Mage_Adminhtml_Controller_Action
{

  protected function _initAttribute()
  {

    $ocAttributeId = $this->getRequest()->getParam('oc_attribute_id'); 
     
    $attribute = Mage::getModel('optionconfigurable/attributeimage');
    
    if ($ocAttributeId != null){
      $attribute->load($ocAttributeId);
    } else {
      $attribute->setLayout('picker');
    }
    
    Mage::register('current_attributeimage', $attribute);
    return $this;
  }
  
  
  public function indexAction()
  {   
    $this->_title($this->__('Attribute Images'));
    $this->loadLayout();
    $this->_setActiveMenu('catalog/attributes');
    $this->_addContent(
        $this->getLayout()->createBlock('optionconfigurable/oc_attributeimage', 'optionconfigurable')
    );
    $this->_addBreadcrumb($this->__('Attribute Images'), $this->__('Attribute Images'));
    $this->renderLayout();
  }


  public function editAction()
  {
    $this->_initAttribute();
    
    $attribute = Mage::registry('current_attributeimage');
    
    $this->loadLayout();    
    $this->_title($attribute->getId() ? $attribute->getTitle() : Mage::helper('adminhtml')->__('New Attribute'));
    $this->_setActiveMenu('catalog/optionconfigurable');      
    $this->renderLayout();
  }
 
 
	public function newAction() {
		$this->_forward('edit');
	}
	
	
	public function saveAction() {
	
		if ($post = $this->getRequest()->getPost()) {

      $this->_initAttribute();
      $attribute = Mage::registry('current_attributeimage');
      
      $oldAttributeId = $attribute->getAttributeId();      
			$attribute->setAttributeId($post['attribute_id']);
		
			$attribute->setLayout($post['layout']);
					
			$attribute->setPopup((int) isset($post['popup']));		
			  
			$images = array();  
      if (isset($post['values']) && $post['attribute_id'] == $oldAttributeId){        
        foreach ($post['values'] as $valueId => $value){
          if (preg_match("/.tmp$/i", $value['image'])) {
            $images[$valueId] = $this->_moveImageFromTmp($value['image']);
          }	elseif (isset($value['image_saved_as']) && $value['image_saved_as'] != '' && (!isset($value['delete_image']) || $value['delete_image'] == '')){
            $images[$valueId] = $value['image_saved_as'];
          }	        
        }  
      }
      			  
			$attribute->setData('images', $images);
		
			  
			try {

        $attribute->save();
				
				Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Attribute was successfully saved'));
				Mage::getSingleton('adminhtml/session')->setFormData(false);
				if ($this->getRequest()->getParam('back')) {
					$this->_redirect('*/*/edit', array('oc_attribute_id' => $attribute->getOcAttributeId(), '_current'=>true, 'back'=> null));
					return;
				}
				$this->_redirect('*/*/');
				return;
      } catch (Exception $e) {
        Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        $this->_redirect('*/*/edit', array('_current'=>true));
        return;
      }
    } 
    Mage::getSingleton('adminhtml/session')->addError($this->__('Unable to find attribute to save'));
    $this->_redirect('*/*/');
	}


   
	public function deleteAction() {
		if( $this->getRequest()->getParam('attribute_id') > 0 ) {
			try {
				$model = Mage::getModel('optionconfigurable/attribute');
				 
				$model->setId($this->getRequest()->getParam('attribute_id'))
					->delete();
					 
				Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Attribute was successfully deleted'));
				$this->_redirect('*/*/');
				return;				
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				$this->_redirect('*/*/');
				return;
			}
		}
		$this->_redirect('*/*/');
	}


	public function massDeleteAction() {
		$ids = $this->getRequest()->getParam('ids');
    if(!is_array($ids)) {
      Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
    } else {
        try {
            foreach ($ids as $id) {
              $model = Mage::getModel('optionconfigurable/attributeimage');				 
              $model->setId($id)->delete();                    
            }
            Mage::getSingleton('adminhtml/session')->addSuccess(
                Mage::helper('adminhtml')->__(
                    'Total of %d record(s) were successfully deleted', count($ids)
                )
            );
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        }
    }
    $this->_redirect('*/*/');
	}
	
	
  public function massStatusAction() {
    $ids = $this->getRequest()->getParam('ids');
    if (!is_array($ids)) {
      Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
    } else {
        try {
            foreach ($ids as $id) {
              $model = Mage::getModel('optionconfigurable/attribute')
                      ->load($id)
                      ->setStatus($this->getRequest()->getParam('status'));
              $model->save();                    
            }
            Mage::getSingleton('adminhtml/session')->addSuccess(
                Mage::helper('optionconfigurable')->__('Total of %d record(s) have been updated.', count($ids))
            );
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        }
    }
    $this->_redirect('*/*/');
  } 


  public function gridAction()
  {
    $this->loadLayout();   
    $this->getResponse()->setBody($this->getLayout()->createBlock('optionconfigurable/oc_attributeimage_grid')->toHtml());
  }          


  protected function _moveImageFromTmp($file)
  {

      $ioObject = new Varien_Io_File();
      $destDirectory = dirname($this->_getMadiaConfig()->getMediaPath($file));

      try {
          $ioObject->open(array('path'=>$destDirectory));
      } catch (Exception $e) {
          $ioObject->mkdir($destDirectory, 0777, true);
          $ioObject->open(array('path'=>$destDirectory));
      }

      if (strrpos($file, '.tmp') == strlen($file)-4) {
          $file = substr($file, 0, strlen($file)-4);
      }

      $destFile = dirname($file) . $ioObject->dirsep()
                . Varien_File_Uploader::getNewFileName($this->_getMadiaConfig()->getMediaPath($file));
                
      $ioObject->mv(
        $this->_getMadiaConfig()->getTmpMediaPath($file),
        $this->_getMadiaConfig()->getMediaPath($destFile)
      );	

      return str_replace($ioObject->dirsep(), '/', $destFile);
  }


  protected function _getMadiaConfig()
  {
      return Mage::getSingleton('catalog/product_media_config');
  }	
    
    
  	
}
