<?php
class Pektsekye_OptionConfigurable_Block_Oc_Attributeimage extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'oc_attributeimage';
    $this->_blockGroup = 'optionconfigurable';
    $this->_headerText = $this->__('Attribute Images');
    $this->_addButtonLabel = $this->__('Add Attribute');
    parent::__construct();

  }

}
