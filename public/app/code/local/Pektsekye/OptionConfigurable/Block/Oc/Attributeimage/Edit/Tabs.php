<?php

class Pektsekye_OptionConfigurable_Block_Oc_Attributeimage_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('oc_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle($this->__('Attribute Images'));
    }


    protected function _beforeToHtml()
    {
    
        $this->addTab('general', array(
            'label'     => Mage::helper('adminhtml')->__('General Information'),
            'content'   => $this->getLayout()->createBlock('optionconfigurable/oc_attributeimage_edit_tab_general')->toHtml(),
            'active'    => true
        ));    
        
        $attribute = Mage::registry('current_attributeimage');    
        $this->addTab('images', array(
            'label'     => Mage::helper('adminhtml')->__('Images'),
            'content'   => $this->getLayout()->createBlock('optionconfigurable/oc_attributeimage_edit_tab_images')->toHtml(),
            'is_hidden' => is_null($attribute->getId())            
        ));           

        $this->_updateActiveTab();
        return parent::_beforeToHtml();
    }


    protected function _updateActiveTab()
    {
        $tabId = $this->getRequest()->getParam('tab');
        if( $tabId ) {
            $tabId = preg_replace("#{$this->getId()}_#", '', $tabId);
            if($tabId) {
                $this->setActiveTab($tabId);
            }
        }
    }
    
    
}
