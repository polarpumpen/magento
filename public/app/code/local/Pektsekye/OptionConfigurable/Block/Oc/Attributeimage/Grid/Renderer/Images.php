<?php

class Pektsekye_OptionConfigurable_Block_Oc_Attributeimage_Grid_Renderer_Images extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {    
        $html = '';
        $c = 0;
        
        $images = Mage::getModel('optionconfigurable/attributeimage')->getImages($row->getData('attribute_id'));
        foreach($images as $image){
          if ($c == 8){
            $html .= '...';
            break;
          }         
        
          if ($image != ''){
            $product = Mage::getModel('catalog/product');
            $url = $this->helper('catalog/image')->init($product, 'thumbnail', $image)->keepFrame(true)->resize(40,40)->__toString();
            $html .= '<img src="'. $url .'" style="display:inline;">&nbsp;';
            $c++;
          }
 
        }
        
        return $html;
    }

}
