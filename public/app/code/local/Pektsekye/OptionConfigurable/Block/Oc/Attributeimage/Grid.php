<?php

class Pektsekye_OptionConfigurable_Block_Oc_Attributeimage_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
  public function __construct()
  {
      parent::__construct();
      $this->setId('optionconfigurable');
      $this->setUseAjax(true);    
  }


  protected function _prepareCollection()
  {
      $collection = Mage::getModel('optionconfigurable/attributeimage')->getCollection();       
      $this->setCollection($collection);      
      parent::_prepareCollection();
      return $this;
  }


  protected function _prepareColumns()
  {
  
      $this->addColumn('oc_attribute_id', array(
          'header'    => Mage::helper('catalog')->__('Id'),
          'align'     =>'left',
          'index'     => 'oc_attribute_id',
          'width'     => 45                      
      ));

      $this->addColumn('title', array(
          'header'    => Mage::helper('catalog')->__('Attribute'),
          'align'     =>'left',
          'index'     => 'title'            
      ));

      $this->addColumn('attribute_code', array(
          'header'    => Mage::helper('catalog')->__('Attribute Code'),
          'align'     =>'left',
          'index'     => 'attribute_code',
          'width'     => 300                      
      ));	  
      
      $this->addColumn('images', array(
          'header'    => Mage::helper('catalog')->__('Images'),
          'align'     =>'center', 	
          'width'     => 400,                 
          'renderer'  => 'optionconfigurable/oc_attributeimage_grid_renderer_images'                     
      )); 

      
      $this->addColumn('action',
          array(
              'header'    =>  Mage::helper('adminhtml')->__('Action'),
              'width'     => 40,               
              'type'      => 'action',
              'getter'    => 'getOcAttributeId',
              'actions'   => array(
                  array(
                      'caption'   => Mage::helper('adminhtml')->__('Edit'),
                      'url'       => array('base'=> '*/*/edit','params'=>array('_current'=>true)),
                      'field'     => 'oc_attribute_id'
                  )
              ),
              'calendar'    => false,
              'sortable'  => false,
              'is_system' => true,
      )); 

	  
      return parent::_prepareColumns();
  }

  protected function _prepareMassaction()
  {
      $this->setMassactionIdField('oc_attribute_id');
      $this->getMassactionBlock()->setFormFieldName('ids');

      $this->getMassactionBlock()->addItem('delete', array(
           'label'    => Mage::helper('adminhtml')->__('Delete Images'),
           'url'      => $this->getUrl('*/*/massDelete'),
           'confirm'  => Mage::helper('adminnotification')->__('Are you sure?')
      ));
      return $this;
  }
  
  public function getGridUrl()
  {
      return $this->getUrl('*/*/grid', array('_current'=>true));
  }
    
  public function getRowUrl($row)
  {
      return $this->getUrl('*/*/edit', array('oc_attribute_id' => $row->getOcAttributeId()));
  }

}
