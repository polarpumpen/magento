<?php

class Pektsekye_OptionConfigurable_Block_Oc_Attributeimage_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        $this->_objectId   = 'attribute_id';
        $this->_blockGroup = 'optionconfigurable';        
        $this->_controller = 'oc_attributeimage';
        
        parent::__construct();
        
        $this->_removeButton('reset');                                   

        $this->_addButton('save_and_edit_button', array(
            'label'   => Mage::helper('adminhtml')->__('Save and Continue Edit'),
            'onclick'   => 'saveAndContinueEdit(\''.$this->_getSaveAndContinueUrl().'\')',
            'class'   => 'save'
        ), 1);
        
        $this->_formScripts[] = 'function saveAndContinueEdit(urlTemplate){
        var template = new Template(urlTemplate, /(^|.|\r|\n)({{(\w+)}})/);  
        editForm.submit(template.evaluate({tab_id:oc_tabsJsTabs.activeTab.id}));
                                  }';    
    }


    public function getHeaderText()
    {
        if (!is_null(Mage::registry('current_attributeimage')->getId())) {
            return Mage::registry('current_attributeimage')->getTitle();
        } else {
            return $this->__('Add Attribute');
        }
    }


    protected function _getSaveAndContinueUrl()
    {
        return $this->getUrl('*/*/save', array(
            '_current'  => true,
            'back'      => 'edit',
            'tab'       => '{{tab_id}}'                                  
        ));
    }     
}
