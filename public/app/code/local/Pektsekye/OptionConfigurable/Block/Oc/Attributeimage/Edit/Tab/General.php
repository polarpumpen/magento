<?php

class Pektsekye_OptionConfigurable_Block_Oc_Attributeimage_Edit_Tab_General extends Mage_Adminhtml_Block_Widget_Form
{

  protected function _prepareForm()
  {
      $form = new Varien_Data_Form();
      $fieldset = $form->addFieldset('optionconfigurable_form', array('legend'=>Mage::helper('adminhtml')->__('General Information')));

      
      $fieldset->addField('attribute_id', 'select', array(
          'label'     => $this->__('Attribute'),
          'required'  => true,
          'name'      => 'attribute_id',
          'values'    => Mage::registry('current_attributeimage')->getAttributesAsOptions(),
          'note'      => $this->__('after selecting attribute save to update the Images tab')      
      ));       


      $fieldset->addField('layout', 'select', array(
          'name'      => 'layout',
          'label'     => Mage::helper('core')->__('Layout'),
          'options'   => array(
                           'above'      =>$this->__('Above Option'),
                           'before'     =>$this->__('Before Option'),
                           'below'      =>$this->__('Below Option'),
                           'swap'       =>$this->__('Main Image'),
                           'picker'     =>$this->__('Color Picker'),
                           'pickerswap' =>$this->__('Picker & Main')
                         ),
          'onchange' => 'optionConfigurableImage.changePopup();'                                                         
      ));                                   


      $fieldset->addField('popup', 'checkbox', array(
          'name'      => 'popup',
          'label'     => Mage::helper('core')->__('Popup'),
          'value'     => 1,
          'checked'   => Mage::registry('current_attributeimage')->getPopup() == 1     
      )); 
      

      $form->setValues(Mage::registry('current_attributeimage')->getData());
      $this->setForm($form);
      
      return parent::_prepareForm();
  }
   
}
