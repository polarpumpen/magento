<?php


/** @var $installer Pektsekye_OptionConfigurable_Model_Resource_Setup */
$installer = $this;

/**
 * Prepare database for tables setup
 */
$installer->startSetup();


$installer->getConnection()->addColumn($installer->getTable('optionconfigurable/attribute'), 'use_attribute_image', array(
    'type' => Varien_Db_Ddl_Table::TYPE_SMALLINT,
    'unsigned'  => true,
    'nullable'  => false,        
    'default'   => '1',
    'comment' => 'Use attribute image'
));

$installer->getConnection()->addColumn($installer->getTable('optionconfigurable/attribute'), 'use_product_image', array(
    'type' => Varien_Db_Ddl_Table::TYPE_SMALLINT,
    'unsigned'  => true,
    'nullable'  => false,        
    'default'   => '1',
    'comment' => 'Use product image'
));


$installer->getConnection()->dropTable($installer->getTable('optionconfigurable/attributeimage'));
$table = $installer->getConnection()
    ->newTable($installer->getTable('optionconfigurable/attributeimage')) 
    ->addColumn('oc_attribute_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,    
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true
        ), 'OptionConfigurable Attributeimage ID')  
    ->addColumn('attribute_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        'default'   => '0',
        ), 'Attribute ID')  
    ->addColumn('layout', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        ), 'Layout')   
    ->addColumn('popup', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'unsigned'  => true,
        'nullable'  => false,        
        'default'   => '0',
        ), 'Popup')                         
    ->addIndex($installer->getIdxName('optionconfigurable/attributeimage', array('attribute_id'), Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE),
        array('attribute_id'), array('type' => Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE))                                                                  
    ->addForeignKey($installer->getFkName('optionconfigurable/attributeimage', 'attribute_id', 'eav/attribute', 'attribute_id'),
        'attribute_id', $installer->getTable('eav/attribute'), 'attribute_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)                
    ->setComment('OptionConfigurable Attributeimage Table');
$installer->getConnection()->createTable($table);


$installer->getConnection()->dropTable($installer->getTable('optionconfigurable/attributeimage_images'));
$table = $installer->getConnection()
    ->newTable($installer->getTable('optionconfigurable/attributeimage_images'))   
    ->addColumn('oc_attribute_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        'default'   => '0',
        ), 'OptionConfigurable Attributeimage ID')     
    ->addColumn('option_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        'default'   => '0',
        ), 'Attribute Option ID')              
    ->addColumn('image', Varien_Db_Ddl_Table::TYPE_TEXT, '64k', array(
        ), 'Image')  
    ->addForeignKey($installer->getFkName('optionconfigurable/attributeimage_images', 'oc_attribute_id', 'optionconfigurable/attributeimage', 'oc_attribute_id'),
        'oc_attribute_id', $installer->getTable('optionconfigurable/attributeimage'), 'oc_attribute_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)                                                           
    ->addForeignKey($installer->getFkName('optionconfigurable/attributeimage_images', 'option_id', 'eav/attribute_option', 'option_id'),
        'option_id', $installer->getTable('eav/attribute_option'), 'option_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)                 
    ->setComment('OptionConfigurable Attributeimage Image Table');
$installer->getConnection()->createTable($table);


$installer->endSetup();
