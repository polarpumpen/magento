<?php

class Pektsekye_OptionConfigurable_Model_Resource_Attributeimage_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    /**
     * Resource collection initialization
     *
     */
    protected function _construct()
    {
        $this->_init('optionconfigurable/attributeimage');
    }


    public function _beforeLoad()
    {
        $this->getSelect()
          ->join(array('ea' => $this->getTable('eav/attribute')), "main_table.attribute_id = ea.attribute_id", array('attribute_code','title'=>'frontend_label'));
        
        return parent::_beforeLoad();
    }

}
