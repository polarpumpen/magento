<?php

class Pektsekye_OptionConfigurable_Model_Resource_Attributeimage extends Mage_Core_Model_Resource_Db_Abstract
{
    public function _construct()
    {
        $this->_init('optionconfigurable/attributeimage', 'oc_attribute_id');
    }
  
    public function getImages($attributeId)
    {        
      $select = $this->_getReadAdapter()->select()
        ->from(array('oa' => $this->getMainTable()), array())      
        ->join(array('ai' => $this->getTable('optionconfigurable/attributeimage_images')), 'ai.oc_attribute_id = oa.oc_attribute_id', array('option_id', 'image'))
        ->where("attribute_id=?", $attributeId);  
               
      return $this->_getReadAdapter()->fetchPairs($select);                                
    }   


    public function getImageData($attributeId)
    {        
      $select = $this->_getReadAdapter()->select()
        ->from(array('ea' => $this->getTable('eav/attribute')), array())
        ->join(array('ao' => $this->getTable('eav/attribute_option')), 'ao.attribute_id = ea.attribute_id', 'option_id') 
        ->join(array('ov' => $this->getTable('eav/attribute_option_value')), 'ov.option_id = ao.option_id AND store_id=0', array('label'=>'value'))         
        ->joinLeft(array('ai' => $this->getTable('optionconfigurable/attributeimage_images')), 'ai.option_id = ao.option_id', 'image') 
        ->order(array("ao.sort_order","label"))
        ->where("ea.attribute_id=?", $attributeId);  
               
      return $this->_getReadAdapter()->fetchAll($select);                                
    } 


    public function saveImages($ocAttributeId, $images)
    {         
   
      $this->deleteImages($ocAttributeId);  
       
      if (count($images) == 0)
        return $this;
                
      $data = array();      
      foreach ($images as $valueId => $image){
        $data[] = array(
          'oc_attribute_id' => $ocAttributeId,
          'option_id'       => $valueId,  
          'image'           => $image                   
        );      
      }      
        
      $this->_getWriteAdapter()->insertMultiple($this->getTable('optionconfigurable/attributeimage_images'), $data);                                
    }


    public function deleteImages($ocAttributeId)
    {        
      $this->_getWriteAdapter()->delete($this->getTable('optionconfigurable/attributeimage_images'), "oc_attribute_id='{$ocAttributeId}'");                                
    }   

  
}
