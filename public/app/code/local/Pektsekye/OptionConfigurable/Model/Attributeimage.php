<?php

class Pektsekye_OptionConfigurable_Model_Attributeimage extends Mage_Core_Model_Abstract
{	
    
    public function _construct()
    {
      parent::_construct();
      $this->_init('optionconfigurable/attributeimage');
    }


    public function getImages($attributeId)
    {        
      return $this->getResource()->getImages($attributeId);                            
    } 


    public function getImageData()
    {        
      return $this->getResource()->getImageData($this->getAttributeId());                            
    } 

 
    public function getAttributesAsOptions()
    {  
      $usedAttributeIds = array();
      foreach ($this->getCollection() as $item){
        if ($this->getAttributeId() !== $item->getAttributeId())
          $usedAttributeIds[$item->getAttributeId()] = 1;
      }
    
      $options = array();
      /** @var $attributesCollection Mage_Catalog_Model_Resource_Product_Attribute_Collection */
      $attributesCollection = Mage::getResourceModel('catalog/product_attribute_collection');
      foreach ($attributesCollection as $attribute) {
          if ($attribute->getIsConfigurable() && $attribute->getIsUserDefined() && $attribute->getIsVisible() && !isset($usedAttributeIds[$attribute->getAttributeId()])) {
            $options[] = array('value'=>$attribute->getAttributeId(), 'label'=>$attribute->getFrontendLabel());   
          }
      }
      return $options;    

    }	


    public function _afterSave()
    {    
      if ($this->getId() && $this->hasData('images')){
        $this->getResource()->saveImages($this->getId(), $this->getData('images'));
      }     
    }

    
}
