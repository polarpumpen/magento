(function($, window, undefined) {
    /* Unused code at the moment
        var removeActivatedClasses = function(domObj) {
            activatedClasses.forEach(function(v) {
                if (domObj.hasClass(v)) {
                domObj.removeClass(v);
                }
            });
        };*/

    $(document).ready(function() {
        var windowHref = window.location.href;
        var windowPath = window.location.pathname;

        var activatedClasses = ['active', 'current'];
        var parentIndicationClasses = ['nav-item--parent', 'parent'];

        var setActivatedClasses = function(domObj) {
            var $liParents = domObj.parents('li');
            var activatedClassesString = activatedClasses.join(' ');
            domObj.addClass(activatedClassesString);
            domObj.parent().addClass(activatedClassesString);

            var classSetter = function(domObj) {
                if (domObj) {
                    var classList = domObj.attr('class');
                    if (classList !== undefined) {
                        var classNames = classList.split(/\s+/);
                        classNames.forEach(function(className) {
                            if (parentIndicationClasses.indexOf(className) !== -1) {
                                domObj.addClass(activatedClassesString);
                                domObj.children('a').first().addClass(activatedClassesString);
                            }
                        });
                    }
                }
            };

            $($liParents).each(function() {
                var current = $(this);
                classSetter(current);
            });
        };

        var initMenuActivation = function(selector) {
            var $nav = $(selector);
            var $navLinks = $('a', $nav);
            $navLinks.each(function() {
                var current = $(this),
                    currentUrl = current.attr('href');
                if (currentUrl === windowPath || currentUrl === windowHref) {
                    setActivatedClasses(current);
                }
            });
        };

        ['ul#nav', 'nav#left-submenu'].forEach(function(selector) {
            initMenuActivation(selector);
        });
    });
})(jQuery, window);