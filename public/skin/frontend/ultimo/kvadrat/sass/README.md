# Polarpumpen theme settings

## COLORS

- general colors
    - link, titles
        - HEX: #1b57a6
        - RGB: 188,209,236
	- link:hover
		- HEX: #fa6632        
		- RGB: 250, 102, 50
    - background (body)
        - HEX: #f2f2f2
        - RGB: 242, 242, 242

- text
    - titles
        - HEX: #111
        - RGB: 17, 17, 17
    - paragraph
        - HEX: #000
        - RGB: 0, 0, 0
- footer
    - link
        - HEX: #fa6632
        - RGB: 250, 102, 50
    - background
        - HEX: #eeebe9
        - RGB: 238, 235, 233
