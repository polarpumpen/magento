jQuery(document).ready(function() {
    jQuery('.faq-list li').each(function(index, e) {
        var elm = jQuery(e);
        var link = elm.find('.readmore');
        var h = elm.find('h3');
        h.css('cursor', 'pointer');

        var open = function() {
            if (elm.is('.closed')) {
                var question = h.text();
                enhancedEcommerceFAQ(question);
                elm.removeClass('closed');
            } else {
                elm.addClass('closed');
            }
        };

        h.click(open);
        link.click(open);
    });
});