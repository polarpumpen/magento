<li class="nc-pp-classic classic nav-item level0 nav-2 level-top nav-item--parent nav-item--only-subcategories parent">
    <a href="#" class="level-top"><span>Luft-luft</span><span class="caret">&nbsp;</span></a>
    <span class="opener"></span>

    <ul class="level0 nav-submenu nav-panel--dropdown nav-panel">
        <li class="nav-item level1 nav-2-1 simple nav-item--only-subcategories parent">
            <a class="nav-item-title" title="Varumärken" href="#"><span>Varumärken</span><span class="caret">&nbsp;</span></a><span class="opener"></span>

            <!-- sub-menu start -->
            <ul class="level1 nav-submenu nav-panel">
                <li class="nav-item level2 nav-2-1-1 classic first"><a href="#"><span>Daikin</span></a></li>
                <li class="nav-item level2 nav-2-1-2 classic"><a href="#"><span>Fujitsu</span></a></li>
                <li class="nav-item level2 nav-2-1-3 classic"><a href="#"><span>LG</span></a></li>
                <li class="nav-item level2 nav-2-1-4 classic"><a href="#"><span>Mitsubishi</span></a></li>
                <li class="nav-item level2 nav-2-1-5 classic"><a href="#"><span>Panasonic</span></a></li>
                <li class="nav-item level2 nav-2-1-6 classic"><a href="#"><span>Sharp</span></a></li>
                <li class="nav-item level2 nav-2-1-7 classic last"><a href="#"><span>Toshiba</span></a></li>
            </ul>
            <!-- sub-menu end -->
        </li>
        <li class="nav-item level1 nav-2-2 simple nav-item--only-subcategories parent">
            <a class="nav-item-title" title="Tjänster" href="#"><span>Tjänster</span><span class="caret">&nbsp;</span></a><span class="opener"></span>
            <!-- sub-menu start -->
            <ul class="level1 nav-submenu nav-panel">
                <li class="nav-item level2 nav-2-2-1 classic first"><a href="#"><span>Installation</span></a></li>
                <li class="nav-item level2 nav-2-2-2 classic"><a href="#"><span>Service</span></a></li>
                <li class="nav-item level2 nav-2-2-3 classic last"><a href="#"><span>Felsökning</span></a></li>
            </ul>
            <!-- sub-menu end -->
        </li>
        <li class="nav-item level1 nav-2-3 simple nav-item--only-subcategories parent">
            <a class="nav-item-title" title="Typ" href="#"><span>Typ</span><span class="caret">&nbsp;</span></a><span class="opener"></span>
            <!-- sub-menu start -->
            <ul class="level1 nav-submenu nav-panel">
                <li class="nav-item level2 nav-2-3-1 classic"><a href="#"><span>Flera innedelar</span></a></li>
                <li class="nav-item level2 nav-2-3-2 classic"><a href="#"><span>Golvmodeller</span></a></li>
            </ul>
            <!-- sub-menu end -->
        </li>
        <li class="nav-item level1 nav-2-4 simple nav-item--only-subcategories parent">
            <a class="nav-item-title" title="Tillbehör" href="#"><span>Tillbehör</span><span class="caret">&nbsp;</span></a><span class="opener"></span>
            <!-- sub-menu start -->
            <ul class="level1 nav-submenu nav-panel">
                <li class="nav-item level2 nav-2-4-1 classic first"><a href="#"><span>SMS-styrning</span></a></li>
                <li class="nav-item level2 nav-2-4-2 classic"><a href="#"><span>Verktyg</span></a></li>
                <li class="nav-item level2 nav-2-4-3 classic"><a href="#"><span>Monteringstillbehör</span></a></li>
                <li class="nav-item level2 nav-2-4-4 classic last"><a href="#"><span>Reservdelar</span></a></li>
            </ul>
            <!-- sub-menu end -->
        </li>
        <li class="nav-item level1 nav-2-5 simple nav-item--only-subcategories parent">
            <a class="nav-item-title" title="Husstorlek" href="#"><span>Husstorlek</span><span class="caret">&nbsp;</span></a><span class="opener"></span>
            <!-- sub-menu start -->
            <ul class="level1 nav-submenu nav-panel">
                <li class="nav-item level2 nav-2-5-1 classic first"><a href="#"><span>Hus mellan 10-100m<sup>2</sup></span></a></li>
                <li class="nav-item level2 nav-2-5-2 classic"><a href="#"><span>Hus mellan 10-100m<sup>2</sup></span></a></li>
                <li class="nav-item level2 nav-2-5-3 classic"><a href="#"><span>Hus mellan 10-100m<sup>2</sup></span></a></li>
                <li class="nav-item level2 nav-2-5-4 classic"><a href="#"><span>Hus mellan 10-100m<sup>2</sup></span></a></li>
                <li class="nav-item level2 nav-2-5-5 classic"><a href="#"><span> Hus mellan 10-100m<sup>2</sup></span></a></li>
                <li class="nav-item level2 nav-2-5-6 classic"><a href="#"><span> Hus mellan 10-100m<sup>2</sup></span></a></li>
                <li class="nav-item level2 nav-2-5-7 classic last"><a href="#"><span> Hus mellan 10-100m<sup>2</sup></span></a></li>
            </ul>
            <!-- sub-menu end -->
        </li>
    </ul>
</li>
<li class="nc-pp-classic classic nav-item level0 nav-2 level-top nav-item--parent nav-item--only-subcategories parent">
    <a href="#" class="level-top"><span>Luft-vatten</span><span class="caret">&nbsp;</span></a>
    <span class="opener"></span>

    <ul class="level0 nav-submenu nav-panel--dropdown nav-panel">
        <li class="nav-item level1 nav-2-1 simple nav-item--only-subcategories parent">
            <a class="nav-item-title" title="Varumärken" href="#"><span>Varumärken</span><span class="caret">&nbsp;</span></a><span class="opener"></span>

            <!-- sub-menu start -->
            <ul class="level1 nav-submenu nav-panel">
                <li class="nav-item level2 nav-2-1-1 classic first"><a href="#"><span>Daikin</span></a></li>
                <li class="nav-item level2 nav-2-1-2 classic"><a href="#"><span>Fujitsu</span></a></li>
                <li class="nav-item level2 nav-2-1-3 classic"><a href="#"><span>LG</span></a></li>
                <li class="nav-item level2 nav-2-1-4 classic"><a href="#"><span>Mitsubishi</span></a></li>
                <li class="nav-item level2 nav-2-1-5 classic"><a href="#"><span>Panasonic</span></a></li>
                <li class="nav-item level2 nav-2-1-6 classic"><a href="#"><span>Sharp</span></a></li>
                <li class="nav-item level2 nav-2-1-7 classic last"><a href="#"><span>Toshiba</span></a></li>
            </ul>
            <!-- sub-menu end -->
        </li>
        <li class="nav-item level1 nav-2-2 simple nav-item--only-subcategories parent">
            <a class="nav-item-title" title="Tjänster" href="#"><span>Tjänster</span><span class="caret">&nbsp;</span></a><span class="opener"></span>
            <!-- sub-menu start -->
            <ul class="level1 nav-submenu nav-panel">
                <li class="nav-item level2 nav-2-2-1 classic first"><a href="#"><span>Installation</span></a></li>
                <li class="nav-item level2 nav-2-2-2 classic"><a href="#"><span>Service</span></a></li>
                <li class="nav-item level2 nav-2-2-3 classic last"><a href="#"><span>Felsökning</span></a></li>
            </ul>
            <!-- sub-menu end -->
        </li>
        <li class="nav-item level1 nav-2-3 simple nav-item--only-subcategories parent">
            <a class="nav-item-title" title="Typ" href="#"><span>Typ</span><span class="caret">&nbsp;</span></a><span class="opener"></span>
            <!-- sub-menu start -->
            <ul class="level1 nav-submenu nav-panel">
                <li class="nav-item level2 nav-2-3-1 classic"><a href="#"><span>Flera innedelar</span></a></li>
                <li class="nav-item level2 nav-2-3-2 classic"><a href="#"><span>Golvmodeller</span></a></li>
            </ul>
            <!-- sub-menu end -->
        </li>
        <li class="nav-item level1 nav-2-4 simple nav-item--only-subcategories parent">
            <a class="nav-item-title" title="Tillbehör" href="#"><span>Tillbehör</span><span class="caret">&nbsp;</span></a><span class="opener"></span>
            <!-- sub-menu start -->
            <ul class="level1 nav-submenu nav-panel">
                <li class="nav-item level2 nav-2-4-1 classic first"><a href="#"><span>SMS-styrning</span></a></li>
                <li class="nav-item level2 nav-2-4-2 classic"><a href="#"><span>Verktyg</span></a></li>
                <li class="nav-item level2 nav-2-4-3 classic"><a href="#"><span>Monteringstillbehör</span></a></li>
                <li class="nav-item level2 nav-2-4-4 classic last"><a href="#"><span>Reservdelar</span></a></li>
            </ul>
            <!-- sub-menu end -->
        </li>
        <li class="nav-item level1 nav-2-5 simple nav-item--only-subcategories parent">
            <a class="nav-item-title" title="Husstorlek" href="#"><span>Husstorlek</span><span class="caret">&nbsp;</span></a><span class="opener"></span>
            <!-- sub-menu start -->
            <ul class="level1 nav-submenu nav-panel">
                <li class="nav-item level2 nav-2-5-1 classic first"><a href="#"><span>Hus mellan 10-100m<sup>2</sup></span></a></li>
                <li class="nav-item level2 nav-2-5-2 classic"><a href="#"><span>Hus mellan 10-100m<sup>2</sup></span></a></li>
                <li class="nav-item level2 nav-2-5-3 classic"><a href="#"><span>Hus mellan 10-100m<sup>2</sup></span></a></li>
                <li class="nav-item level2 nav-2-5-4 classic"><a href="#"><span>Hus mellan 10-100m<sup>2</sup></span></a></li>
                <li class="nav-item level2 nav-2-5-5 classic"><a href="#"><span> Hus mellan 10-100m<sup>2</sup></span></a></li>
                <li class="nav-item level2 nav-2-5-6 classic"><a href="#"><span> Hus mellan 10-100m<sup>2</sup></span></a></li>
                <li class="nav-item level2 nav-2-5-7 classic last"><a href="#"><span> Hus mellan 10-100m<sup>2</sup></span></a></li>
            </ul>
            <!-- sub-menu end -->
        </li>
    </ul>
</li>
<li class="nc-pp-classic classic nav-item level0 nav-6 level-top nav-item--parent nav-item--only-subcategories parent">
    <a href="#" class="level-top"><span>Frånluft</span><span class="caret">&nbsp;</span></a>
    <span class="opener"></span>
    <ul class="level0 nav-submenu nav-panel--dropdown nav-panel">
        <li class="nav-item simple level1 nav-6-1 nav-item--only-subcategories parent">
            <a class="nav-item-title" title="Varumärken" href="#"><span>Varumärken</span><span class="caret">&nbsp;</span></a><span class="opener"></span>
            <ul class="level1 nav-submenu nav-panel">
                <li class="nav-item level2 nav-6-1-0 classic"><a href="#">Bosch</a></li>
                <li class="nav-item level2 nav-6-1-1 classic"><a href="#">ComfortZone</a></li>
            </ul>
        </li>
    </ul>
</li>

<li class="nc-pp-classic classic nav-item level0 nav-7 level-top nav-item--parent nav-item--only-subcategories parent">
    <a href="#" class="level-top"><span>Installation</span><span class="caret">&nbsp;</span></a>
    <span class="opener"></span>
    <ul class="level0 nav-submenu nav-panel--dropdown nav-panel">
        <li class="nav-7-0 classic nav-item"><a href="#">Installation Luft-luft</a></li>
        <li class="nav-7-1 classic nav-item"><a href="#">Installation A/C</a></li>
        <li class="nav-7-2 classic nav-item"><a href="#">Installation Luft-vatten</a></li>
        <li class="nav-7-3 classic nav-item"><a href="#">Installation Bergvärme</a></li>
        <li class="nav-7-4 classic nav-item"><a href="#">Installation Poolvärme</a></li>
        <li class="nav-7-5 classic nav-item"><a href="#">ROT-avdrag</a></li>
    </ul>
</li>

<li class="nav-item level0 level-top">
    <a class="level-top" href="#" title="Service">
        <span>Service</span>
    </a>
</li>

<li class="nc-pp-classic classic nav-item level0 nav-8 level-top nav-item--parent nav-item--only-subcategories parent">
    <a href="#" class="level-top"><span>A/C</span><span class="caret">&nbsp;</span></a>
    <span class="opener"></span>
    <ul class="level0 nav-submenu nav-panel--dropdown nav-panel">
        <li class="nav-item level1 simple nav-6-1 nav-item--only-subcategories parent">
            <a class="nav-item-title" title="Luftkonditionering" href="#"><span>Luftkonditionering</span><span class="caret">&nbsp;</span></a><span class="opener"></span>
            <ul class="level1 nav-submenu nav-panel">
                <li class="nav-item nav-8-1-0 classic"><a href="#">Hem & Kontor</a></li>
                <li class="nav-item nav-8-1-1 classic"><a href="#">Tillbehör</a></li>
            </ul>
        </li>
    </ul>
</li>

<li class="nav-item level0 level-top">
    <a class="level-top" href="#" title="Poolvärme">
        <span>Poolvärme</span>
    </a>
</li>

<li class="nc-pp-classic classic nav-item level0 nav-9 level-top nav-item--parent nav-item--only-subcategories parent">
    <a href="#" class="level-top"><span>Övrigt</span><span class="caret">&nbsp;</span></a>
    <span class="opener"></span>
    <ul class="level0 nav-submenu nav-panel--dropdown nav-panel">
        <li class="first nav-9-0 classic nav-item"><a href="#">Airmove</a></li>
        <li class="nav-9-1 classic nav-item"><a href="#">Värmeväxlare</a></li>
        <li class="nav-9-2 classic nav-item"><a href="#">Värmeflyttare</a></li>
        <li class="nav-9-3 classic nav-item"><a href="#">Ventiler</a></li>
        <li class="nav-9-4 classic nav-item"><a href="#">Varmvattensberedare</a></li>
        <li class="nav-9-5 nav-item classic"><a href="#">Fukt i hus</a></li>
        <li class="nav-9-6 nav-item classic"><a href="#">Bergvärme</a></li>
        <li class="nav-9-7 nav-item classic"><a href="#">Jordvarme</a></li>
        <li class="nav-9-8 nav-item classic"><a href="#">Sjövarme</a></li>
        <li class="nav-9-9 nav-item classic"><a href="#">Markiser</a></li>
        <li class="nav-9-10 nav-item classic"><a href="#">Solfångare</a></li>
        <li class="nav-9-11 last nav-item classic"><a href="#">Solceller</a></li>
    </ul>
</li>

<li class="nav-item level0 level-top">
    <a href="#" class="level-top" title="Fynd">
        <span>Fynd</span>
    </a>
</li>

<li class="nav-item level0 level-top right campaign">
    <a href="#" class="level-top" title="Kampanjer">
        <span>Kampanjer</span>
        <span class="badge">5</span>
    </a>
</li>
