// jQuery-ui TABS
;(function ($, undefined) {
    //{
    //    "mode": 1,
    //    "threshold": 1024,
    //    "collapsed": "0"
    //}
    $(document).ready(function () {
        var tabOperator = {
            root: ''
            , $rootContainer: null
            , $tabsContainer: null
            , $panelsContainer: null

            //1 - tabs/accordion, 2 - accordion, 3 - tabs
            , mode: 1
            , threshold: 1024
            , initialAccIndex: 0
            , tabEffect: 'default'
            , accEffect: 'default'

            , init: function (root) {

                // If no param, set default selector
                tabOperator.$rootContainer = $(root);
                tabOperator.$tabsContainer = tabOperator.$rootContainer.children('.tabs');

                tabOperator.$panelsContainer = tabOperator.$rootContainer.children('.tab-parent').children('.tabs-panels');
                tabOperator.initialAccIndex = 0;
                //tabOperator.mode = mode || 1;

                if (tabOperator.mode === 1) {
                    //Initial value of the flag which indicates whether viewport was above the threshold
                    var previousAboveThreshold = $(window).width() >= tabOperator.threshold;

                    //Activate tabs or accordion
                    if (previousAboveThreshold) {
                        //If above threshold - activate tabs
                        tabOperator.initTabs();
                    }
                    else {
                        //If below threshold - activate accordion
                        tabOperator.initAccordion(tabOperator.initialAccIndex);
                    }

                    //On tab click
                    tabOperator.hookToAccordionOnClick();

                    //On window resize
                    $(window).on('themeResize', function (e, resizeEvent) {

                        if ($(window).width() < tabOperator.threshold) {
                            if (previousAboveThreshold) {
                                //Now below threshold, previously above, so switch to accordion
                                if (tabOperator.$tabsContainer.data("tabs")) {
                                    var api = tabOperator.$tabsContainer.data("tabs");
                                    var index = api.getIndex();
                                    api.destroy();
                                }
                                tabOperator.initAccordion(index);
                            }
                            previousAboveThreshold = false;
                        } else {
                            if (!previousAboveThreshold) {
                                //Now above threshold, previously below, so switch to tabs
                                var api = tabOperator.$panelsContainer.data("tabs");
                                var index = api.getIndex();
                                api.destroy();
                                tabOperator.$rootContainer.removeClass("accor");

                                tabOperator.initTabs(index);
                            }
                            previousAboveThreshold = true;
                        }

                    });
                }
                else if (tabOperator.mode === 2) {
                    tabOperator.initAccordion(tabOperator.initialAccIndex);

                    //On tab click
                    tabOperator.hookToAccordionOnClick();
                }
                else {
                    tabOperator.initTabs();
                }

            } //end: init

            , initTabs: function (index) {
                //If no param, set it to 0
                if (typeof index === "undefined") {
                    index = 0;
                }

                tabOperator.$tabsContainer.tabs(".tabs-panels .panel", {
                    effect: tabOperator.tabEffect,
                    initialIndex: index
                });
            }

            , initAccordion: function (index) {
                //If no param, set it to 0
                if (typeof index === "undefined") {
                    index = 0;
                }
                tabOperator.$rootContainer.addClass("accor");
                tabOperator.$panelsContainer.tabs(".tabs-panels .panel", {
                    tabs: '.acctab',
                    effect: tabOperator.accEffect,
                    initialIndex: index
                });
            }

            , hookToAccordionOnClick: function () {
                // @FIX: This allows toggle on the tabs!
                var previousTarget = null, isOpen = true;
                tabOperator.$panelsContainer.on('click', function (e) {
                    var targetElement = $(e.target),
                        _id = targetElement.attr('id');

                    if (targetElement.hasClass('acctab')) {
                        if (targetElement.hasClass('current')) {
                            if (_id == previousTarget) {
                                if (isOpen) {
                                    targetElement.next().hide();
                                    targetElement.removeClass('current');
                                    isOpen = false;
                                } else {
                                    console.log('should open here?');
                                }
                            } else if (previousTarget === null) {
                                targetElement.next().hide();
                                targetElement.removeClass('current');
                            } else {
                                isOpen = true;
                            }
                        } else {
                            targetElement.addClass('current');
                            targetElement.next().show();
                            isOpen = true;
                        }
                        previousTarget = _id;
                    }
                });

                //Attach a handler to an event after a tab is clicked
                tabOperator.$panelsContainer.bind("onClick", function (event, index) {

                    //Note: "this" is a reference to the DOM element of tabs
                    //var theTabs = this;
                    var target = event.target || event.srcElement || event.originalTarget;

                    //If viewport is lower than the item, scroll to that item
                    var itemOffsetTop = $(target).offset().top;
                    var viewportOffsetTop = jQuery(window).scrollTop();
                    if (itemOffsetTop < viewportOffsetTop) {
                        $("html, body").delay(150).animate({scrollTop: (itemOffsetTop - 50)}, 600, 'easeOutCubic');
                    }

                }); //end: bind onClick
            }

            , openTab: function () {
                if (tabOperator.$rootContainer.hasClass("accor")) {
                    var $panels = tabOperator.$panelsContainer;
                    var indexOfTab = $panels.children(".acctab").index($("#acctab-tabreviews"));
                    $panels.data("tabs").click(indexOfTab);
                }
                else {
                    var $tabs = tabOperator.$tabsContainer;
                    var indexOfTab = $tabs.children("#tab-tabreviews").index();
                    $tabs.data("tabs").click(indexOfTab);
                }
            }

            , slideTo: function (target, offset) {
                //Slide to tab (minus height of sticky menu)
                var itemOffsetTop = $(target).offset().top - offset;
                $("html, body").animate({scrollTop: itemOffsetTop}, 600, 'easeOutCubic');
            }

        };

        //Initialize tabs
        var selector = ".polarpumpen-tabs";
        var selectorElements = $(selector);
        if (selectorElements.length) {
            $.each(selectorElements, function() {
                tabOperator.init($(this));
            });
        }
    });
})(jQuery);

// Custom TABS, like fortum
;(function($, undefined){
    $(document).ready(function() {
        var threshold = 1024;
        var $idTabs = $('.id-tabs');
        var $tabsParent, $tabPanels, $tabTriggers;

        if ($idTabs.length) {
            $tabsParent = $idTabs.parent();
            $tabPanels = $('.tabs-panels .panel', $tabsParent);
            $tabTriggers = $('.click-tab-item', $tabsParent);

            $.each($tabTriggers, function(i, v) {
                $(this).on('click', function(e, target) {
                    e.preventDefault();
                    $tabPanels.css('display', 'none');

                    var tabToOpen = $('#panel-' + $(this).attr('id').split('-')[1], $tabsParent);
                    tabToOpen.css('display', 'block');

                    var anchor = $('a', $(this));
                    var anchors = $('a', $tabTriggers).not(anchor);
                    anchor.addClass('current');
                    anchors.removeClass('current');
                });
            });

            var windowObject = $(window);
            var setAccordionClasses = function() {
                if (windowObject.width() < threshold) {
                    var h2AccTabs = $('h2.acctab', $tabsParent);
                    $idTabs.addClass('accor');
                    $tabPanels.css('display', 'none');
                    h2AccTabs.removeClass('current');
                    if (!h2AccTabs.first().hasClass('current')) {
                        h2AccTabs.first().addClass('current');
                    }

                    h2AccTabs.first().next().css('display', 'block');
                    h2AccTabs.each(function() {
                        var _current = $(this);
                        _current.on('click', function() {
                            $tabPanels.css('display', 'none');
                            if (_current.hasClass('current')) {
                                _current.removeClass('current');
                                _current.next().css('display', 'none');
                            } else {
                                h2AccTabs.not(_current).removeClass('current');
                                _current.addClass('current');
                                _current.next().css('display', 'block');
                            }
                        });
                    });
                } else {
                    if ($idTabs.hasClass('accor')) {
                        $idTabs.removeClass('accor');
                    }

                    $tabPanels.each(function() {
                        $(this).css('display', 'none');
                    });

                    $tabPanels.first().css('display', 'block');
                    var $tabTriggerFirst = $tabTriggers.first();
                    var anchor = $('a', $tabTriggerFirst);
                    var anchors = $('a', $tabTriggers).not(anchor);

                    anchor.addClass('current');
                    anchors.removeClass('current');
                }
            };

            setAccordionClasses();
            var previous = null;
            windowObject.on('themeResize', function() {
                /*if (previous && windowObject.width() < previous && (previous - windowObject.width() === 100)) {

                } else {
                    previous = windowObject.width();
                }*/
                setAccordionClasses();
            });
        }
    });
})(jQuery);