;(function($, window, undefined) {
    $(document).ready(function() {
        var scrollLinks = $('a.page-scroll');
        var headerNav = $('#header-nav');
        var defaultHeight = 150;
        if (headerNav.length) {
            defaultHeight = headerNav.height();
        }

        if (scrollLinks.length) {
            scrollLinks.each(function() {
                var currentLink = $(this);
                currentLink.on('click', function(e) {
                    e.preventDefault();

                    var href = currentLink.attr('href').split('#');
                    if (href.length) {
                        var target = $('[name=' + href[1] +']');
                        var _target = $('#' +  href[1]);
                        if (target.length) {

                            $('html, body').animate({
                                scrollTop: target.offset().top - defaultHeight - 20
                            }, 1000);
                        } else if (_target.length) {

                            $('html, body').animate({
                                scrollTop: _target.offset().top - defaultHeight - 20
                            }, 1000);
                        }
                    }
                });
            });
        }
    });
})(jQuery, window);