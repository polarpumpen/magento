;(function($, window, undefined) {
  $(document).ready(function() {
    var _windowDom = $(window);
    var owlWrapper = $('#brands-bar');
    var owlWrapperParent = owlWrapper.parent();

    var setupOwl = function() {
      if (owlWrapper && owlWrapper.length) {
        owlWrapper.owlCarousel({
          autoPlay          : 3000,
          items             : 5,
          itemsDesktop      : [1199, 4],
          itemsDesktopSmall : [979, 4],
          itemsMobile: [479, 2],
          itemsTablet: [768, 3]
        });

        $('.previous', owlWrapperParent).click(function() {
          owlWrapper.trigger('owl.prev');
        });

        $('.next', owlWrapperParent).click(function() {
          owlWrapper.trigger('owl.next');
        });
      }
    };

    var removeOwl = function() {
      if (owlWrapper && owlWrapper.length) {
        var data = owlWrapper.data('owlCarousel');
        if (data) {
          data.destroy();
          owlWrapper.removeClass('owl-carousel');
        }
      }
    };

    var init = function(_current) {
      if (_current.width() < 1025) {
        setupOwl();
      } else {
        removeOwl();
      }
    };

    _windowDom.on('themeResize', function() {
      var _current = $(this);
      init(_current);
    });
    init(_windowDom);
  });
})(jQuery, window);
