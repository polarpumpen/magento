;(function($jq, _window) {
    $jq(document).ready(function () {

        var guideBlock = $jq('.block.guide');
        if (guideBlock.length) {
            var calculateArea = function (size, area, location, isolation) {
                return Math.round(sizeValue
                + areaValue * sizeValue
                + locationValue * sizeValue
                + isolationValue * sizeValue);
            };

            var sizeInput = $jq('select[name=td_v_kv]');
            var areaInput = $jq('input[name=td_v_kv-area]');

            $jq('input#half-open').attr('checked', 'checked');

            //var calculatedMinInput = $jq('input[name=td_v_kvmin]');
            var calculatedMaxInput = $jq('input[name=td_v_kvmax]');

            var sizeValue = parseFloat(sizeInput.val());
            var areaValue = 0.1;
            var isolationValue = 0.1;
            var locationValue = 0.0001;

            var calculatedAreaValue = calculateArea(sizeValue, areaValue, locationValue, isolationValue);

            //calculatedMinInput.val('0-' + calculatedAreaValue);
            calculatedMaxInput.val(calculatedAreaValue + '-' + (calculatedAreaValue+30));

            $jq.merge(sizeInput, areaInput).change(function () {
                sizeValue = parseFloat(sizeInput.val());

                if ($jq(this).attr('name') == 'td_v_kv-area') {
                    areaValue = parseFloat($jq(this).val());
                }

                calculatedAreaValue = calculateArea(sizeValue, areaValue, locationValue, isolationValue);

                //calculatedMinInput.val('0-' + calculatedAreaValue);
                calculatedMaxInput.val(calculatedAreaValue + '-' + (calculatedAreaValue+30));
            });

            // Frontpage: Set equal height on guide block
            if ($jq(_window).width() >= 960) {
                var slideshowHeight = $jq('#nextend-smart-slider-2').height();
                guideBlock.css('height', slideshowHeight - 20);
            }
            $jq(_window).on('themeResize', function () {
                if ($jq(_window).width() < 960) {
                    guideBlock.height('');
                } else {
                    slideshowHeight = $jq('#nextend-smart-slider-2').height();
                    guideBlock.css('height', slideshowHeight - 20);
                }
            });
        }
    });
})(jQuery, window);
