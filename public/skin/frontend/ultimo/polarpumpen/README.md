
## Developing with sass

`sass --watch sass/custom.scss:css/custom.css`

## Building sass

`sass sass/custom.scss:css/custom.css`

## Enabled sourcemaps in your chosen browser

http://thesassway.com/intermediate/using-source-maps-with-sass
