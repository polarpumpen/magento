if (!Itoris) {
    var Itoris = {};
}

Itoris.PriceFormula = Class.create({
    initialize : function(conditions, optionsData, specialPrice, dataBySku, productId) {
        this.conditions = conditions;
        this.optionsData = optionsData;
		this.specialPrice = specialPrice;
		this.dataBySku = dataBySku;
		this.productId = productId;
		
		for(var i=0; i<this.optionsData.length; i++) {
			if (this.optionsData[i].values) {
				for(var key in this.optionsData[i].values) {
					var option = this.optionsData[i].values[key];
					if (option.sku) this.dataBySku['{'+option.sku+'}'] = {type: this.optionsData[i].type, id: option.id};
				}
			} else if (this.optionsData[i].sku) {
				this.dataBySku['{'+this.optionsData[i].sku+'}'] = {type: this.optionsData[i].type, id: this.optionsData[i].id};
			}
		}
		
        var curObj = this;
        if ($('qty')) {
            Event.observe($('qty'), 'change', function(){optionsPrice.reload();});
        }
        if ($('super-product-table')) {
            for (var i = 0; i < $('super-product-table').select('.input-text').length; i++) {
                Event.observe($('super-product-table').select('.input-text')[i], 'change', function(){eval('optionsPrice' + productId).reload();});
            }
        }
		
		if (!window['optionsPrice' + productId] || !window['optionsPrice' + productId].reload) return;
		
		var origFunc = window['optionsPrice' + productId].reload.toString(), pos = origFunc.lastIndexOf('displayZeroPrice'), pos = origFunc.lastIndexOf('if', pos);
		if (pos) {
			origFunc = origFunc.substr(0, pos) + ';price=this.formulaUpdate(price,pair);' + origFunc.substr(pos);
			eval("window['optionsPrice' + productId].reload = "+origFunc);
			window['optionsPrice' + productId].formulaUpdate = function(price, pair){
				var productCurrentPrice = price;
				curObj.configuredPrice = price;
				var priceForCompare = 0, multiplyByQty = false;
				for (var i = 0; i < conditions.length; i++) {
					var conditionData = conditions[i];
					this.isRightCondition = false;
					for (var j = 0; j < conditionData.length; j++) {
						if (!this.isRightCondition) {
							var condition = curObj.parseCondition(conditionData[j].condition, 0, price / curObj.priceFormulaCurrencyConversionRate, this.productOldPrice / curObj.priceFormulaCurrencyConversionRate, productCurrentPrice / curObj.priceFormulaCurrencyConversionRate);
							var priceCondition = curObj.parseCondition(conditionData[j].price, 0, price / curObj.priceFormulaCurrencyConversionRate, this.productOldPrice / curObj.priceFormulaCurrencyConversionRate, productCurrentPrice / curObj.priceFormulaCurrencyConversionRate);								
							//if (condition.indexOf && condition.indexOf('no_value') > -1) continue;

							eval("if ("+condition+") {this.isRightCondition = true; priceForCompare ="+ priceCondition +";}");
							if (priceForCompare > 0) productCurrentPrice = priceForCompare * curObj.priceFormulaCurrencyConversionRate;
							multiplyByQty = !!parseInt(conditionData[j].apply_to_total) && !parseInt(conditionData[j].frontend_total);
						} else {
							continue;
						}
					}
				}
				$$('.benefit').each(function(el) {
						var c = el.up('.tier-prices');
						if (c) {
							if (!c._prevDisplay) c._prevDisplay = c.getStyle('display');
							c.setStyle({display: priceForCompare > 0 ? 'none' : c._prevDisplay});
						}
				});
				curObj.finalPrice = priceForCompare > 0 ? priceForCompare * curObj.priceFormulaCurrencyConversionRate / (multiplyByQty && parseFloat($('qty').value) > 0 ? $('qty').value : 1) : price;
				curObj.finalPrice = curObj.finalPrice.toFixed(2);
				return curObj.finalPrice;
			}
		}
        window['optionsPrice' + productId].reload();
		
		//check for error conditions before adding product to cart
		$$('#product_addtocart_form .btn-cart').each(function(btn){
			btn._clickEvent = btn.onclick;
			btn.onclick = function(){
				var isError = false;
				if (window.itorisPriceFormulaErrors) {
					window.itorisPriceFormulaErrors.each(function(criteria){
						if (isError) return;
						var formula = curObj.parseCondition(criteria.formula, 0, curObj.configuredPrice / curObj.priceFormulaCurrencyConversionRate, window['optionsPrice' + productId].productOldPrice / curObj.priceFormulaCurrencyConversionRate, curObj.finalPrice / curObj.priceFormulaCurrencyConversionRate);
						eval("if ("+formula+") {isError = true}");
						if (isError) alert(criteria.message);
					});
				}
				if (!isError) {
					if (btn._clickEvent) {
						btn._clickEvent();
					} else if (btn.callback) {
						btn.callback();
					} else {
						btn.click();
					}
				}
			}
		});
    },
	parseCondition: function(string, def_value, confPrice, initialPrice, productPrice) {
		if (!string.replace) return string;
		for (key in this.dataBySku) {
			var value = def_value;
			if (this.dataBySku[key].value) value = this.dataBySku[key].value;
            for (var i = 0; i < this.optionsData.length; i++) {
                if (this.optionsData[i].type == 'field' || this.optionsData[i].type == 'area') {
                    if (this.dataBySku[key] && this.optionsData[i].id == this.dataBySku[key].id) {
                        if (this.dataBySku[key].type && this.optionsData[i].type == this.dataBySku[key].type) {
                            var textOptionId = 'options_' + this.optionsData[i].id + '_text';
                            if ($(textOptionId) && $(textOptionId).value) value = $(textOptionId).value;
                        }
                    }
                }
				if (this.optionsData[i].type == 'checkbox' || this.optionsData[i].type == 'radio') {
					if (this.dataBySku[key] && this.dataBySku[key].type && this.optionsData[i].type == this.dataBySku[key].type) {
						var optionId = 'options-' + this.optionsData[i].id + '-list';
						var input = $(optionId).select('input');
						for (var j = 0; j < input.length; j++) {
							if (input[j].checked) {
								var subOptionId = input[j].value;
								var values = this.optionsData[i].values;
								var subOptionById = values[subOptionId];
								var skuSubOption = subOptionById ? subOptionById.sku : '';
								var skuSubOptionStr = '{' + skuSubOption + '}';
								if (key == skuSubOptionStr) {
									value = $(optionId).select('label[for='+input[j].id+']')[0].innerHTML;
								}
							}
						}
					}
				}
				if (this.optionsData[i].type == 'drop_down') {
					if (this.dataBySku[key] && this.dataBySku[key].type && this.optionsData[i].type == this.dataBySku[key].type) {
						var selectId = 'select_' + this.optionsData[i].id;
						var selectValue = $(selectId).value;
						if (selectValue) {
							var values = this.optionsData[i].values;
							var subOptionById = values[selectValue];
							var skuSubOption = subOptionById ? subOptionById.sku : '';
							var skuSubOptionStr = '{' + skuSubOption + '}';
							if (key == skuSubOptionStr) {								
								value = $(selectId).options[$(selectId).selectedIndex].text;
							}
						}
					}
				}
				if (this.optionsData[i].type == 'multiple') {
					if (this.dataBySku[key] && this.dataBySku[key].type && this.optionsData[i].type == this.dataBySku[key].type) {
						var selectId = 'select_' + this.optionsData[i].id;
						var options = $(selectId).select('option');
						for (var j = 0; j < options.length; j++) {
							if (options[j].selected) {
								var subOptionId = options[j].value;
								var values = this.optionsData[i].values;
								var subOptionById = values[subOptionId];
								var skuSubOption = subOptionById ? subOptionById.sku : '';
								var skuSubOptionStr = '{' + skuSubOption + '}';
								if (key == skuSubOptionStr) {
									value = options[j].innerHTML;
								}
							}
						}
					}
				}
            }
			if (!isNaN(parseFloat(value)) && isFinite(value)) {} else value = "'"+value.replace(/^\s+|\s+$/g,"")+"'";
			string = string.replace(new RegExp(key, "gi"), value);			
		}
		string = string.replace(new RegExp('{configured_price}', "gi"), confPrice);
		string = string.replace(new RegExp('{initial_price}', "gi"), initialPrice);
		string = string.replace(new RegExp('{special_price}', "gi"), this.specialPrice / this.priceFormulaCurrencyConversionRate);
		string = string.replace(new RegExp('{price}', "gi"), productPrice);
		if ($('qty')) {
			var qty = $('qty').value == 0 ? 1 : $('qty').value;
			string = string.replace(new RegExp('{qty}', "gi"), qty);
		}
		Object.getOwnPropertyNames(Math).each(function(key){
			string = string.replace(new RegExp(key, "g"), 'Math.'+key);
		});
		return string;
	}
});