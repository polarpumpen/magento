jQuery(document).ready(function(){
    
    jQuery(".faq-question").append(" <span class='arrow-down'>&#9660;</span><span class='arrow-up'>&#9650;</span>");
    
    jQuery(".faq-question").click(function(event){ 
        if(jQuery(event.target).find(".arrow-down").is(":visible")) {
            var questionText = jQuery(event.target).text(); 
            var question = questionText.substring(0, questionText.length-3);
            enhancedEcommerceFAQ(question);
        }
        
        jQuery(event.target).next(".faq-answer").toggle();
        jQuery(event.target).find(".arrow-down, .arrow-up").toggle();
    });
    
    jQuery(".arrow-down, .arrow-up").click(function(event){ 
        jQuery(event.target).closest(".faq-question").next(".faq-answer").toggle();
        jQuery(event.target).toggle();
        if(jQuery(event.target).hasClass("arrow-down")) {
            var questionText = jQuery(event.target).closest(".faq-question").text(); 
            var question = questionText.substring(0, questionText.length-3);
            enhancedEcommerceFAQ(question);
            jQuery(event.target).next(".arrow-up").toggle();
        }
        if(jQuery(event.target).hasClass("arrow-up")) {
            jQuery(event.target).prev(".arrow-down").toggle();
        }
        
    });
});