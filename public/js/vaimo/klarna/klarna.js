/**
 * Copyright (c) 2009-2014 Vaimo AB
 *
 * Vaimo reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of Vaimo, except as provided by licence. A licence
 * under Vaimo's rights in the Program may be available directly from
 * Vaimo.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    Vaimo
 * @package     Vaimo_Klarna
 * @copyright   Copyright (c) 2009-2014 Vaimo AB
 */

/** Storing the state of the form **/
var ppApp = {
    cache: {}
};

var ppSetupKlarnaCredentialsFormMsgs = function() {
    var klarnaTelField = $('billing:telephone');
    var klarnaTelError = $('klarna_tel_error');
	

    if (klarnaTelError == null && klarnaTelField) {
        klarnaTelField.up().insert('<p id="klarna_tel_error">Vänligen fyll i telefonnummer tillsammans med personnummer.</p>');
    }

    var klarnaPnoField = $('persnr_orgnr');
    var klarnaPnoError = $('klarna_pno_error');

    if (klarnaPnoError == null && klarnaPnoField) {
        klarnaPnoField.up().insert('<p id="klarna_pno_error">Vänligen fyll i personnummer under betalsätt.</p>');
    }
};

var ppSetupObserver = function(method) {
    var klarnaTelField = $(method + '_phonenumber');
    var formTelField   = $("billing:telephone");
    klarnaTelField.observe('keyup', function(e) {
        if (formTelField) {
            formTelField.value = klarnaTelField.value;
        }
    });
};

var ppUpdateAddressForm = function(jsonItem, method) {
    var fieldItemPrefix = "billing:";
    var addressKeys = Object.keys(jsonItem);
    var itemName;
    // This method is only for required fields
    // that should not be modified after that we get the credentials
    var setValues = function(itemName, value) {
        var element = $(itemName);
        value = (value !== null) ? ('' + value) : '';
        if (element) {
            element.removeAttribute('readonly');
            element.value = value;

            if (value.length > 0) {
                element.setAttribute('readonly', 'readonly');
            }
        }
    };

    addressKeys.forEach(function(key) {
        switch(key) {
            case 'first_name':
                itemName = fieldItemPrefix + 'firstname';
                setValues(itemName, jsonItem[key]);
                break;
            case 'last_name':
                itemName = fieldItemPrefix + 'lastname';
                setValues(itemName, jsonItem[key]);
                break;
            case 'zip':
                itemName = fieldItemPrefix + 'postcode';
                setValues(itemName, jsonItem[key]);
                break;
            case 'city':
            case 'street':
                if (key === 'street') {
                    setValues((fieldItemPrefix + key + '1'), jsonItem[key]);
                } else {
                    setValues((fieldItemPrefix + key), jsonItem[key]);
                }
                break;
            case 'company_name':
                var _companyElement = $(fieldItemPrefix +  'company');
                if (_companyElement) {
                    _companyElement.value = jsonItem[key];
                }
            // Select box following..
            case 'country_code':
                itemName = fieldItemPrefix + 'country_id';
                $(itemName).setValue(jsonItem[key]);
                /*Element.writeAttribute(itemName, 'readonly', 'readonly');*/
                break;
        }
    });

    // Setup SS-number from klarna fields
    setValues('persnr_orgnr',  $(method + '_pno').value);

    // Setup telephone from klarna fields
    var phoneNumberField = $(method + '_phonenumber');
    if (phoneNumberField && phoneNumberField.value) {
        setValues('billing:telephone', phoneNumberField.value);
    }
    ppSetupKlarnaCredentialsFormMsgs();
    ppApp.cache[method] = jsonItem;
};

var ppSetupListeners = function(items, method) {
    var inputIdPrefix = 'address_';
    var context = $('payment_form_' + method);
    if (items && items.length && context) {
        // Setup with the first item
        ppUpdateAddressForm(items.first().object, method);
        // Attach listeners to each object
        items.forEach(function(item) {
            var inputElement = context.select('input#' + (inputIdPrefix + item.id));
            if (inputElement && inputElement.length) {
                Event.observe(inputElement.first(), 'click', function() {
                    ppUpdateAddressForm.call(null, item.object, method);
                });
            }
        });
        ppSetupObserver(method);
    }
};

/*
 * Address search in Checkout
 */
function disableUpdateAddressButton(method)
{
    Form.Element.disable(method + '_update_address_button');
    $(method + '_update_address_button').addClassName('disabled');
}

function enableUpdateAddressButton(method)
{
    Form.Element.enable(method + '_update_address_button');
    $(method + '_update_address_button').removeClassName('disabled');
}

function disableUpdateAddressIndicator(method)
{
    Element.hide(method + '_update_address_span');
}

function enableUpdateAddressIndicator(method)
{
    Element.show(method + '_update_address_span');
}

function clearUpdateAddressMessage(method)
{
    $(method + '_update_address_message').update('');
}

function doAddressSearch(url, method)
{
    var removeKlarFieldsError = function() {
        var fields = document.querySelectorAll('.klarna-fields-error');
        [].forEach.call(fields, function(element) {
            element.parentNode.removeChild(element);
        });
    };

    var addressReceived = function(transport)
    {
        if (transport.responseText.isJSON()) {
            response = transport.responseText.evalJSON()
            if (response.error) {
                alert(response.message);
            } else {
                $(method + '_update_address_message').update(response.html);

                // Empty the cache first.
                ppApp.cache[method] = null;
                ppSetupListeners(response.json, method);
            }
        }
        enableUpdateAddressButton(method);
        disableUpdateAddressIndicator(method);
        removeKlarFieldsError();
    };

    var addressError = function(transport)
    {
        if (transport.responseText.isJSON()) {
            response = transport.responseText.evalJSON()
            if (response.error) {
                alert(response.message);
            } else {
                $(method + '_update_address_message').update(response.message);
            }
        }
        enableUpdateAddressButton(method);
        disableUpdateAddressIndicator(method);
        removeKlarFieldsError();
    };


    var pnoField = $(method + '_pno');
    var telField = $(method + '_phonenumber');
    if (pnoField.value == '') {
        pnoField.up().insert('<span class="klarna-fields-error">Vänligen fyll i personnummer.</span>');
    } else if (telField.value == '') {
        telField.up().insert('<span class="klarna-fields-error">Vänligen fyll i telefonnummer.</span>');
    } else {
        new Ajax.Request(url, {
            parameters: {
                pno: pnoField.value,
                method: method
            },
            method:    'POST',
            onSuccess: addressReceived,
            onFailure : addressError
        });
        enableUpdateAddressIndicator(method);
        disableUpdateAddressButton(method);
        clearUpdateAddressMessage(method);
    }
}

/*
 * Payment plan information in checkout, mainly for Norway
 */
function disableUpdateInformationIndicator(method)
{
    Element.hide(method + '_update_paymentplan_information_span');
}

function enableUpdateInformationIndicator(method)
{
    Element.show(method + '_update_paymentplan_information_span');
}

function clearUpdateInformationMessage(method)
{
    $(method + '_update_paymentplan_information_message').update('');
}

function doPaymentPlanInformation(url, method, plan, storeId)
{
    var informationReceived = function(transport)
    {
        if (transport.responseText.isJSON()) {
            response = transport.responseText.evalJSON()
            if (response.error) {
                alert(response.message);
            } else {
                $(method + '_update_paymentplan_information_message').update(response.html);
            }
        }
        disableUpdateInformationIndicator(method);
    };

    var informationError = function(transport)
    {
        if (transport.responseText.isJSON()) {
            response = transport.responseText.evalJSON()
            if (response.error) {
                alert(response.message);
            } else {
                $(method + '_update_paymentplan_information_message').update(response.message);
            }
        }
        disableUpdateInformationIndicator(method);
    };


    var params = {payment_plan:plan, method:method, store_id:storeId};
    new Ajax.Request(url, {
        parameters: params,
        method:    'POST',
        onSuccess: informationReceived,
        onFailure : informationError
    });

    enableUpdateInformationIndicator(method);
    clearUpdateInformationMessage(method);
}

/*
 * Update PClasses in Admin
 */
function disableUpdatePclassButton()
{
    Form.Element.disable('update_pclass_button');
    $('update_pclass_button').addClassName('disabled');
}

function enableUpdatePclassButton()
{
    Form.Element.enable('update_pclass_button');
    $('update_pclass_button').removeClassName('disabled');
}

function clearUpdatePclassMessage()
{
    $("update_pclass_message").update('');
}

function doUpdatePClass(url)
{
    var pclassReceived = function(transport)
    {
        if (transport.responseText.isJSON()) {
            response = transport.responseText.evalJSON();
            if (response.error) {
                alert(response.message);
            } else {
                var message = $("update_pclass_message");
                var messageContainer = message.up('td');

                var next;
                var counter = 0;

                while (next = messageContainer.next()) {
                    next.remove();
                    console.log('removed');
                    counter++;
                }

                if (counter) {
                    messageContainer.setAttribute('colspan', counter);
                }

                message.update(response.html);

            }
        }
        enableUpdatePclassButton();
    };

    var pclassError = function(transport)
    {
        if (transport.responseText.isJSON()) {
            response = transport.responseText.evalJSON();
            if (response.error) {
                alert(response.message);
            } else {
                $("update_pclass_message").update(response.message);
            }
        }
        enableUpdatePclassButton();
    };

    new Ajax.Request(url, {
        method:    'POST',
        onSuccess: pclassReceived,
        onFailure : pclassError
    });

    disableUpdatePclassButton();
    clearUpdatePclassMessage();
}

function insertKlarnaInvoiceElements(merchant, locale, invoiceFee) {

    var klarnaElementId,
        klarnaElement = '';

    klarnaElementId = 'klarna_invoice_readme';
    if(klarnaElement = document.getElementById(klarnaElementId)){
        while(klarnaElement.firstChild) klarnaElement.removeChild(klarnaElement.firstChild);
        new Klarna.Terms.Invoice({
            el: klarnaElementId,
            eid: merchant,
            locale: locale,
            charge: invoiceFee,
            type: 'desktop'
        });
    }

    klarnaElementId = 'vaimo_klarna_invoice_consent_span';
    if(klarnaElement = document.getElementById(klarnaElementId)){
        while(klarnaElement.firstChild) klarnaElement.removeChild(klarnaElement.firstChild);
        new Klarna.Terms.Consent({
            el: klarnaElementId,
            eid: merchant,
            locale: locale,
            type: 'desktop'
        });
    }

}

function insertKlarnaAccountElements(merchant, locale) {

    var klarnaElementId,
        klarnaElement = '';

    klarnaElementId = 'klarna_account_readme';
    if(klarnaElement = document.getElementById(klarnaElementId)){
        while(klarnaElement.firstChild) klarnaElement.removeChild(klarnaElement.firstChild);
        new Klarna.Terms.Account({
            el: klarnaElementId,
            eid: merchant,
            locale: locale,
            type: 'desktop'
        });
    }

    klarnaElementId = 'vaimo_klarna_account_consent_span';
    if(klarnaElement = document.getElementById(klarnaElementId)){
        while(klarnaElement.firstChild) klarnaElement.removeChild(klarnaElement.firstChild);
        new Klarna.Terms.Consent({
            el: klarnaElementId,
            eid: merchant,
            locale: locale,
            type: 'desktop'
        });
    }

}

function insertKlarnaSpecialElements(merchant, locale) {

    var klarnaElementId,
        klarnaElement = '';

    klarnaElementId = 'klarna_special_readme';
    if(klarnaElement = document.getElementById(klarnaElementId)){
        while(klarnaElement.firstChild) klarnaElement.removeChild(klarnaElement.firstChild);
        new Klarna.Terms.Special({
            el: klarnaElementId,
            eid: merchant,
            locale: locale,
            type: 'desktop'
        });
    }

    klarnaElementId = 'vaimo_klarna_special_consent_span';
    if(klarnaElement = document.getElementById(klarnaElementId)){
        while(klarnaElement.firstChild) klarnaElement.removeChild(klarnaElement.firstChild);
        new Klarna.Terms.Consent({
            el: klarnaElementId,
            eid: merchant,
            locale: locale,
            type: 'desktop'
        });
    }
}

function klarnaCheckoutGo(url) {
    window.location.assign(url);
}

function toggleInformationBoxes(id) {
    var innerDivs = document.querySelectorAll('.payment_plan_info_wrapper');
    for(var i=0; i<innerDivs.length; i++)
    {
        Element.hide(innerDivs[i].id);
    }
    Element.show('infobox_pclass_' + id);
}

$(document).observe('dom:loaded', function() {

    /** Patch methods that are used to change payment method
     *
     * The payment selector is fetched with an AJAX request and inserted in the
     * document. In order to react on payment method changes we patch
     * whatever function the method selector might call.
     */
    (function() {
        "use strict";
        /*global Payment Streamcheckout */

        var _oldPaymentSwitchMethod;

        // Patch 'Payment'
        if (typeof Payment !== 'undefined') {
            _oldPaymentSwitchMethod = Payment.prototype.switchMethod;
            // 'billing:country_id',
            var fields = ['billing:firstname', 'billing:lastname', 'billing:postcode',
                'billing:city', 'billing:street1', 'billing:telephone', 'persnr_orgnr',
                'billing:company'];
            Payment.prototype.switchMethod = function(method) {
                _oldPaymentSwitchMethod.call(this, method);
                if (method === 'vaimo_klarna_invoice' || method === 'vaimo_klarna_account') {
                    fields.forEach(function(fieldName) {
                        if ($(fieldName) !== null) {
                            if (fieldName !== 'billing:company') {
                                $(fieldName).readonly = 'readonly';
                            }
                            $(fieldName).value = '';
                        }
                    });
                    ppSetupKlarnaCredentialsFormMsgs();

                    // Update the address-form from cache,
                    // so that states are kept in tact when switching methods.
                    if (ppApp.cache.hasOwnProperty(method) && ppApp.cache[method] !== null) {
                        var cache = ppApp.cache[method];
                        ppUpdateAddressForm.call(null, cache, method);
                    }
                    ppSetupObserver(method);
                } else {
                    fields.forEach(function(fieldName) {
                        var element = $(fieldName);
                        if (element) {
                            if (fieldName === 'persnr_orgnr') {
                                element.value = '';
                            }
                            element.removeAttribute('readonly');
                        }
                    });

                    if ($('klarna_tel_error') != null) {
                        $('klarna_tel_error').remove();
                    }

                    if ($('klarna_pno_error') != null) {
                        $('klarna_pno_error').remove();
                    }
                }
            };
        }
    })();

});
