function amshopby_extension_start(){
    $$('.amshopby-slider-param-single').each(function (inp) {
        var inps = inp.value.split("||");
        var item = inps[0];
        var param = item.split(',');
        amshopby_slider_single(param[0], param[1], param[2], param[3], param[4], param[5], inp, inps[1]);
    });
}

function amshopby_slider_single(from, to, max_value, prefix, min_value, step, item, config) {
    if(item)
        var slider =  item.siblings().first();
    if(!slider || typeof slider == 'undefined'){
        var slider = $(prefix);
    }

    max_value = parseFloat(max_value);
    min_value = parseFloat(min_value);
    from = (from === "") ? min_value : parseFloat(from);
    to = (to === "") ? max_value : parseFloat(to);

    var width = parseFloat(slider.offsetWidth);
    if (!width) {
        // filter collapsed, we will wait
        setTimeout(function() {
            amshopby_slider_single(from, to, max_value, prefix, min_value, step, item, config);
        }, 200);
        return;
    }

    var conf = config.evalJSON(true);

    var area = $(conf.area);
    var location = $(conf.location);
    var isolation = $(conf.isolation);

    var slider;

    var urlUpdate = function (parent) {
        var minPrefix = prefix + "min";
        var maxPrefix = prefix + "max";

        var fromValue = amshopby_round(min_value + ratePP * slider.value, step);

        var areaValue = parseFloat(area.getValue());
        var locationValue = parseFloat(location.getValue());
        var isolationValue = parseFloat(isolation.getValue());

        var calculatedArea = Math.round(fromValue
            + areaValue * fromValue
            + locationValue * fromValue
            + isolationValue * fromValue);

        var urlElement = (parent)? parent.select('#' + prefix +'-url').first(): $(prefix +'-url');
        var url =  urlElement.value
            .gsub(minPrefix + "1", 0)
            .gsub(minPrefix + "2", calculatedArea)
            .gsub(maxPrefix + "1", calculatedArea)
            .gsub(maxPrefix + "2", calculatedArea+30)
            .gsub(prefix + "-value", fromValue)
            .gsub(prefix + "-location-value", locationValue)
            .gsub(prefix + "-area-value", areaValue)
            .gsub(prefix + "-isolation-value", isolationValue);

        amshopby_set_location(url);
    };

    var triggerFunction = function() {
        var element = $(this);
        var parent      = element.up('ol');
        urlUpdate(parent);
    };

    area.observe('change', triggerFunction);
    location.observe('change', triggerFunction);
    isolation.observe('change', triggerFunction);

    var ratePP = (max_value - min_value) / 100;

    var fromPixel = ((from - min_value) || (from - to != 0 || from != 0)) ? ((from - min_value) / ratePP) : 0;
    var toPixel = ((to - min_value) || (from - to != 0 || to != 0)) ? ((to - min_value) / ratePP) : 100;

    Control.Slider.prototype.translateToPx = function (value) {
        return value + '%';
    };

    Control.Slider.prototype.translateToValue = function (offset) {
        return ((offset/((this.maximumOffset() - this.minimumOffset())-this.handleLength) *
        (this.range.end-this.range.start)) + this.range.start);
    };

    Control.Slider.prototype.updateFinished = function () {
        if (this.handles[0].hasClassName('active')) {
            this.handles[0].removeClassName('active');
        }

        if (this.initialized && this.options.onChange)
            this.options.onChange(this.value, this);
        this.event = null;
    };

    slider = new Control.Slider(slider.select('.handle'), slider, {
        range: $R(0, 100),
        sliderValue: [fromPixel, toPixel],
        restricted: true,

        onChange: function (value, element){
            this.onSlide(value, element);
            var fromValue = amshopby_round(min_value + ratePP * value, step);
            if (fromValue != from) {
                amshopby_price_click_callback(prefix, element.track);
            }

            var parent = element.track.up('ol');
            urlUpdate(parent);
        },
        onSlide: function amastyOnSlide(value, element) {
            if (isNaN(value)) {
                return;
            }

            var fromValue = amshopby_round(min_value + ratePP * value, step);

            var parent      = element.track.up('ol');
            var sliderFrom  = parent.select('#' + prefix + '-from').first();

            if(sliderFrom){
                sliderFrom.value = fromValue;
            }

            sliderFrom = parent.select('#' + prefix + '-from-slider').first();
            if(sliderFrom){
                sliderFrom.update(fromValue)
            }
        }
    });
}

document.observe("dom:loaded", function() {
    amshopby_extension_start();
});
