
function enhancedEcommerceProductClick(productId) {
    var product = jQuery('.product-item-sku-' + productId);
    
    dataLayer.push({
        'event': 'ee_product_click',
        'ecommerce': {
            'click': {
                'actionField': { 'list': product.data('productlist') },
                'products': [{     
                    'name': product.data('name'),
                    'id': productId,
                    'price': product.data('price') + '.00',
                    'brand': product.data('brand'),
                    'category': product.data('category'), 
                    'variant': '',
                    'position': '1'
                }],
             },
         },
    });
    return;
}

function enhancedEcommerceAddToCart(productId) {
    var product = jQuery('.add-product-sku-' + productId);
    
    dataLayer.push({
        'event': 'ee_add_to_cart',
        'ecommerce': {
            'currencyCode': 'SEK',
            'add': {
                'actionField': { 'list': product.data('productlist') },
                'products': [{ 
                        'name': product.data('name'),
                        'id': productId,
                        'price': product.data('price') + '.00' ,
                        'brand': product.data('brand'),
                        'category': product.data('category'),  
                        'variant': '', 
                        'quantity': '1'
                }],
             },
         },
    });
    return;
}

function enhancedEcommerceRemoveFromCart(productId, confirmText) {
    if(confirmText != '' && !confirm(confirmText)) {
        return false;
    }
    var product;
    if(jQuery('#product-item-cart-sku-' + productId).length) { 
        product = jQuery('#product-item-cart-sku-' + productId);
    } else if(jQuery('#sku_' + productId).length) {
        product = jQuery('#sku_' + productId);
    } else {
        product = jQuery('.product-item-sku-' + productId);
    }
    
    dataLayer.push({
        'event': 'ee_remove_from_cart',
        'ecommerce': {
            'currencyCode': 'SEK',
            'remove': {
                'actionField': { 'list': product.data('productlist') },
                'products': [{ 
                        'name': product.data('name'),
                        'id': productId,
                        'price': product.data('price') + '.00',
                        'brand': product.data('brand'),
                        'category': product.data('category'),
                        'variant': '',
                        'quantity': '1',
                }],
             },
         },
    });
    
    return;
}

function getProductArrayFromInput(inputId) {
    var productsString = jQuery(inputId).val();
    var productsFromInput = productsString.split(";");
    var productArray = [];

    jQuery(productsFromInput).each(function() {
        var product = this.split(",");
        var productData = {}
        jQuery(product).each(function() {
            var productDetails = this.split(":");
            productData[productDetails[0]] = productDetails[1];
        });
        
        productArray.push(productData);
    }); 
    
    return productArray;
}


function enhancedEcommerceCheckoutShipping(title) {
    var products = getProductArrayFromInput('#products-in-cart');
    
    if(window.isStep2Sent) {
        dataLayer.push({
            'event': 'checkoutOption',
            'ecommerce': {
                'checkout_option': {
                    'actionField': { 'step': '2', 'option': title }
                }
            }
        });
    } else {
        dataLayer.push({
            'event': 'checkout',
            'ecommerce': {
                'checkout': {
                    'actionField': { 'step': '2', 'option': title }, 
                    'products': products ,
                }
            }
        });
        window.isStep2Sent = true; 
    }
    if(!window.isStep4Sent) {
        enhancedEcommerceCheckoutInvoiceAddress();
    }
    return;
}

function enhancedEcommerceCheckoutPayment(title) {
    var products = getProductArrayFromInput('#products-in-cart');
    
    if(window.isStep3Sent) {
        dataLayer.push({
            'event': 'checkoutOption',
            'ecommerce': {
                'checkout_option': {
                    'actionField': { 'step': '3', 'option': title }
                }
            }
        });
    } else {
        dataLayer.push({
            'event': 'checkout',
            'ecommerce': {
                'checkout': {
                    'actionField': { 'step': '3', 'option': title }, 
                    'products': products ,
                }
            }
        });
        window.isStep3Sent = true; 
    }
    if(!window.isStep4Sent) {
        enhancedEcommerceCheckoutInvoiceAddress();
    }
    return;
}

function enhancedEcommerceCheckoutInvoiceAddress() {
    var type;
    if(jQuery('#pers_comp_154').is(':checked')) {
        type = 'Företag';
    } else {
        type = 'Privatperson';
    }
        
    var isComplete = true;
    jQuery('#billing-address').find("input[type=text]").each(function() {
        if(jQuery(this).attr('id') == 'billing:company') {
            return;
        }
        if(!jQuery(this).val() && jQuery(this).hasClass('required-entry')) {
            isComplete = false;
            return false;
        }
    });
    
    if(!jQuery('#persnr_orgnr').val()) {
        isComplete = false;
        return false;
    }
    
    if(!jQuery('input[name="shipping_method"]').is(':checked')) {
        isComplete = false;
        return false;
    }
    
    if(!jQuery('input[name="payment[method]"]').is(':checked')) {
        isComplete = false;
        return false;
    }
    
    if(isComplete === true) {
        var products = getProductArrayFromInput('#products-in-cart');
        enhancedEcommercePushCheckoutInvoiceAddress(type, products);
    }
    return;
}

function enhancedEcommercePushCheckoutInvoiceAddress(type, products) {
    if(window.isStep4Sent) {
        dataLayer.push({
            'event': 'checkoutOption',
            'ecommerce': {
                'checkout_option': {
                    'actionField': { 'step': '4', 'option': type }
                }
            }
        });
    } else {
        dataLayer.push({
            'event': 'checkout',
            'ecommerce': {
                'checkout': {
                    'actionField': { 'step': '4', 'option': type }, 
                    'products': products ,
                }
            }
        });
        window.isStep4Sent = true; 
    }
    return;
}

function enhancedEcommerceCheckoutAddToCart() {
    var products = getProductArrayFromInput('#products-in-cart');
    
    dataLayer.push({
        'event': 'checkout',
        'ecommerce': {
            'checkout': {
                'actionField': { 'step': '5' }, 
                'products': products ,
            }
        }
    });
    return;
}

function enhancedEcommerceFAQ(question) {
    dataLayer.push({
            'event': 'ee_faq',
            'click_text': question,
    });
}
