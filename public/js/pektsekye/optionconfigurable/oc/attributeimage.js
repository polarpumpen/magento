
var OptionConfigurableImage = {};
var optionConfigurableImage = {};
OptionConfigurableImage.Main = Class.create({

	uploaders : [],         	  		
	
	initialize : function(){
		Object.extend(this, OptionConfigurableImage.Config);
	},
    
  
  loadImage : function(selectId, src){ 
    var img = $('oc_preview_'+selectId); 
    img.src = src;
    img.show(); 
    if (this.uploaders[selectId] != undefined)
      $('oc_link_'+selectId+'_file-flash').hide();
    $('oc_delete_button_'+selectId).show();		    	
  },


  loadUploader : function(selectId){ 
    this.addUploader(selectId);
    $(selectId+'_uploader_place_holder').hide();
    $(selectId+'_uploader_row').show();
  },


  addUploader : function(selectId){


     var uploader = new Flex.Uploader('oc_link_'+selectId+'_file', OptionConfigurableImage.Config.uploaderUrl, this.uploaderConfig);
     uploader.selectId = selectId;

      uploader.handleSelect = function(event) { 
        this.files = event.getData().files;
        this.checkFileSize(); 
        this.updateFiles();
        this.upload();
      };


      uploader.onFilesComplete = function (files) { 
        var item = files[0];
        if (!item.response.isJSON()) {
          alert(OptionConfigurableImage.Config.expiredMessage); 
          return;
        }

        var response = item.response.evalJSON();
        if (response.error) {
          return;
        }

        this.removeFile(item.id);

        $('oc_value_'+this.selectId+'_image').value = response.file;
        $('oc_link_'+this.selectId+'_file-new').hide();

        optionConfigurableImage.loadImage(this.selectId, response.url);

        $('oc_link_'+this.selectId+'_file-old').show();
      };

      this.uploaders[selectId] = 1;

  },


  deleteImage : function(selectId){
    $('oc_value_'+selectId+'_delete_image').value = 1;
    $('oc_value_'+selectId+'_image').value = '';
    $('oc_preview_'+selectId).hide();
    $('oc_delete_button_'+selectId).hide();
    if (this.uploaders[selectId] != undefined)
      $('oc_link_'+selectId+'_file-flash').show(); 
    else
      this.addUploader(selectId); 
  },
  
  
	changePopup : function(){
		var popupCheckbox = $('popup');
		var layout = $('layout').value;
		if (layout == 'swap'){
			popupCheckbox.checked = false;
			popupCheckbox.disabled = true;
		} else {
			popupCheckbox.disabled = false;			
		}	
	}
		
}); 



