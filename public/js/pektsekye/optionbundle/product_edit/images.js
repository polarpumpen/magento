
var OptionBundleImage = {};
var optionBundleImage = {};
OptionBundleImage.Main = Class.create({

	uploaders : [],         	  		
	
	initialize : function(){
		Object.extend(this, OptionBundleImage.Config);
	},
    
  
  loadImage : function(selectId, src){ 
    var img = $('optionbundle_link_'+selectId+'_file_img'); 
    img.src = src;
    img.show(); 
    if (this.uploaders[selectId] != undefined)
      $('optionbundle_link_'+selectId+'_file-flash').hide();
    $('optionbundle_link_'+selectId+'_file_delete').show();		    	
  },


  loadUploader : function(selectId){ 
    var value = $('optionbundle_link_'+selectId+'_file_save').value; 
    if (value.isJSON()) 
      this.loadImage(selectId, value.evalJSON().url);
    else 
      this.addUploader(selectId);
    $(selectId+'_uploader_place-holder').hide();
    $(selectId+'_uploader_row').show();
  },


  addUploader : function(selectId){

     uploaderOITemplate = new
     Template(OptionBundleImage.Config.uploaderTemplate,
     /(^|.|\r|\n)(\[\[(\w+)\]\])/);

     Element.insert('optionbundle_image_cell_'+selectId, {'top' :uploaderOITemplate.evaluate({'idName' :'optionbundle_link_'+selectId+'_file'})});


     var uploader = new Flex.Uploader('optionbundle_link_'+selectId+'_file',OptionBundleImage.Config.uploaderUrl, this.uploaderConfig);
     uploader.selectId = selectId;

      uploader.handleSelect = function(event) { 
        this.files = event.getData().files;
        this.checkFileSize(); 
        this.updateFiles();
        this.upload();
      };


      uploader.onFilesComplete = function (files) { 
        var item = files[0];
        if (!item.response.isJSON()) {
          alert(OptionBundleImage.Config.expiredMessage); 
          return;
        }

        var response = item.response.evalJSON();
        if (response.error) {
          return;
        }

        this.removeFile(item.id);

        $('optionbundle_link_'+this.selectId+'_file_save').value = Object.toJSON(response);
        $('optionbundle_link_'+this.selectId+'_file-new').hide();

        optionBundleImage.loadImage(this.selectId, response.url);

        $('optionbundle_link_'+this.selectId+'_file-old').show();
      };

      this.uploaders[selectId] = 1;

  },


  deleteImage : function(selectId){

    $('optionbundle_link_'+selectId+'_file_save').value = '{}';
    $('optionbundle_link_'+selectId+'_file_img').hide();
    $('optionbundle_link_'+selectId+'_file_delete').hide();
    if (this.uploaders[selectId] != undefined)
      $('optionbundle_link_'+selectId+'_file-flash').show(); 
    else
      this.addUploader(selectId); 
  },
  
	changePopup : function(t, optionId){
		var popupCheckbox = $('optionbundle_'+t+'_'+optionId+'_popup');
		var layout = $('optionbundle_'+t+'_'+optionId+'_layout').value;
		if (layout == 'swap'){
			popupCheckbox.checked = false;
			popupCheckbox.disabled = true;
		} else if(popupCheckbox.disabled){
			popupCheckbox.disabled = false;			
		}	
	},  
  
  setScope : function(element, inputId){
    var input = $(inputId);
    if (element.checked)
      input.disable();
    else
      input.enable();

  //  if (type == 'note' || type == 'description'){
      var clickToEditLink = $(inputId+'_show');    
      if (element.checked)
        clickToEditLink.hide();
      else 
        clickToEditLink.show();        
 //   }
     
  }, 	
  
	showTextArea : function(element){
		var inputId = element.id.sub('_show', '');
		var input = $(inputId);
		var textArea = new Element('textarea', {'id' : inputId, 'name' : input.name}).addClassName('optionbundle-textarea');
		textArea.value = input.value;
		element.hide();		
		Element.replace(input, textArea);
		$(inputId + '_hide').show();		
	},
	
	
	hideTextArea : function(element){
		var inputId = element.id.sub('_hide', '');
		var textArea = $(inputId);
		var input = new Element('input', {'id' : inputId, 'name' : textArea.name, 'type' : 'text', 'value' : textArea.value}).addClassName('input-text');
		element.hide();
		Element.replace(textArea, input);
		$(inputId + '_show').show();
	},
	
	
	useProductImage : function(id, checked){
    $('ob_optionimages_b_'+id).select('.optionbundle-value-container-image .files').each(function(el){
      if (checked)
        el.hide();
      else  
        el.show();   	  
    });
	},
	
	
	useProductDescription : function(id, checked){
    $('ob_optionimages_b_'+id).select('.optionbundle-value-container-description').each(function(el){
      if (checked)
        el.hide();
      else  
        el.show();   
    });
	}			
}); 



