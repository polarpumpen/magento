angular.module('productConfigurator', [])
    .controller('productConfiguratorController', function($scope, $http) {
        var pc = this;
        var data = window.pcData;

        pc.loaded = false;
        pc.templates = [];
        pc.saveMessage = '';

        $http.get(data.loadUrl).success(function(data) {
            pc.templates = data;
            pc.loaded = true;
        });

        pc.activeTemplate = pc.templates[1];

        pc.addTemplate = function() {
            if (pc.newTemplateName) {
                pc.templates.push(pc.activeTemplate = {
                    name: pc.newTemplateName,
                    columns: [{value: 189}, {value: 239}, {value: 339}, {value: 350}, {value: 400}, {value: 450}, {value: 500}, {value: 550}],
                    rows: [{value: 150}, {value: 200}, {value: 250}, {value: 300}],
                    matrix: [[], [], [], []],
                    supplementGroups: []
                });
                pc.newTemplateName = '';
            }
        };

        pc.addRow = function(template) {
            template.rows.push({value: 0});
            template.matrix.push([]);
        };

        pc.removeRow = function(template) {
            template.rows.pop();
            template.matrix = template.matrix.slice(0, template.rows.length);
        };

        pc.addColumn = function(template) {
            template.columns.push({value: 0});
        };

        pc.removeColumn = function(template) {
            template.columns.pop();
            angular.forEach(template.matrix, function(row, rowKey) {
                template.matrix[rowKey] = row.slice(0, template.columns.length);
            });
        };

        pc.addPriceRule = function(supplement) {
            supplement.priceRules.push({})
        };

        pc.addSupplementGroup = function() {
            if (pc.newSupplementGroupName) {
                pc.activeTemplate.supplementGroups.push({
                    name: pc.newSupplementGroupName,
                    supplements: []
                });
                pc.newSupplementGroupName = '';

            }
        };

        pc.addSupplement = function(supplementGroup) {
            supplementGroup.supplements.push({
                name: '',
                priceRules: []
            });
        };

        pc.save = function() {
            pc.saveMessage = 'Saving...';
            $http({
                method: 'POST',
                url: data.saveUrl + '&form_key=' + data.formKey,
                data: {
                    templates: pc.templates
                }
            }).success(function(data) {
                pc.saveMessage = data.message;
                setTimeout(function(){
                    pc.saveMessage = '';
                    $scope.$digest();
                }, 1500)
            });
        };

        pc.deleteTemplate = function(template) {
            if (confirm('Are you sure that you want to delete template “'+template.name+'”')) {
                pc.templates.splice(pc.templates.indexOf(template), 1);
                if (pc.activeTemplate == template) {
                    pc.activeTemplate = false;
                }
            }
        }

    });
