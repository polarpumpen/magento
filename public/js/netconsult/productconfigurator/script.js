/**
 * Copyright NetConsult Sweden AB
 * Author: Mikael Mattsson
 *
 * slider source: https://github.com/angular-slider/angularjs-slider
 */

angular.module('productConfiguratorFrontend', ['rzModule'])
    .filter('toTrusted', ['$sce', function($sce) {
        return function(text) {
            return $sce.trustAsHtml(text);
        };
    }])
    .controller('productConfiguratorFrontendController', function($scope, $http) {
        var pc = this;
        pc.data = window.pcData;

        var minWidth = 0;
        if (!getCellValue(pc.data.matrix[0][0])) {
            minWidth = pc.data.columns[0].value + 1;
        }
        var maxWidth = parseInt(pc.data.columns[pc.data.columns.length - 1].value);

        pc.getRowValue = getRowValue;
        pc.selectSupplement = selectSupplement;
        pc.getPrice = getPrice;
        pc.getSupplementPrice = getSupplementPrice;
        pc.getAppliedRule = getAppliedRule;
        pc.getValidationErrors = getValidationErrors;

        pc.widthSlider = { //requires angular-bootstrap to display tooltips
            value: minWidth,
            options: {
                floor: minWidth,
                ceil: maxWidth,
                showSelectionBar: true,
                translate: getColumnValue,
                onChange: updateDepthSlider
            }
        };

        pc.depthSlider = { //requires angular-bootstrap to display tooltips
            value: 1,
            options: {
                floor: 1,
                ceil: pc.data.rows.length,
                showTicksValues: true,
                showSelectionBar: true,
                translate: getRowValue
            }
        };

        setTimeout(function() {
            var $ = jQuery;
            var $productConfiguratorInputWrapper = $('<div class="no-display">').appendTo($('#product_addtocart_form'));
            var $inputFields = $('.pcf .product-input');
            $inputFields.appendTo($productConfiguratorInputWrapper);
        });

        setTimeout(function() {
            updateDepthSlider();
            $scope.$digest();
        }, 1);

        function updateDepthSlider() {
            var matrix = pc.data.matrix;
            var selectedColumn = getColumnNumber();
            var max = 0;
            var min = pc.data.columns.length;

            // Loop current column
            for (var i = 0; i < pc.data.rows.length; i++) {
                if (matrix[i] != null && getCellValue(matrix[i][selectedColumn])) {
                    if (i <= min) {
                        min = i;
                    }
                    if (i >= max) {
                        max = i;
                    }
                }
            }

            pc.depthSlider.options.floor = min + 1;
            pc.depthSlider.options.ceil = max + 1;

            if (pc.depthSlider.value > max + 1) {
                pc.depthSlider.value = max + 1
            }
            if (pc.depthSlider.value < min + 1) {
                pc.depthSlider.value = min + 1
            }
        }

        function getColumnValue(v) {
            return v + ' cm';
        }

        function getRowValue(v) {
            if (typeof pc.data.rows[v - 1] !== 'undefined') {
                return pc.data.rows[v - 1].value + ' cm';
            }
            return '';
        }

        function getCellValue(cell) {
            if (!cell) {
                return 0;
            }

            return parseInt(cell.value)
        }

        function getColumnNumber() {
            for (var i = 0; i < pc.data.columns.length; i++) {
                if (pc.widthSlider.value <= pc.data.columns[i].value) {
                    return i;
                }
            }

            return 0;
        }

        function getRowNumber() {
            return pc.depthSlider.value - 1;
        }

        function selectSupplement(group, selection) {
            group.selected = selection;
        }

        function getPrice() {
            if (pc.data.useMatrix) {
                var matrixPrice = getSelectedCell();
                var price;

                if (!matrixPrice) {
                    return 0;
                }

                price = parseInt(matrixPrice.value, 10);
            } else {
                price = parseInt(pc.data.basePrice, 10);
            }

            for (var i = 0; i < pc.data.supplementGroups.length; i++) {
                var group = pc.data.supplementGroups[i];
                if (group.selected) {
                    var supplementPrice = getSupplementPrice(group.selected)
                    if (supplementPrice) {
                        price += parseInt(supplementPrice, 10);
                    }
                }
            }

            return price;
        }

        function getSelectedCell() {
            var selectedMatrixRow = pc.data.matrix[getRowNumber()];
            if (!selectedMatrixRow) {
                return false;
            }
            return selectedMatrixRow[getColumnNumber()];
        }

        function getSupplementPrice(supplement) {
            var rule;

            if (rule = getAppliedRule(supplement)) {
                return parseInt(rule.price, 10)
            }

            return parseInt(supplement.defaultPrice, 10);
        }

        function getAppliedRule(supplement) {

            if (!supplement) {
                return false;
            }

            for (var i = 0; i < supplement.priceRules.length; i++) {
                var rule = supplement.priceRules[i];
                var value = 0;
                var applyRule = false;

                if (rule.propertyKey == 'width') {
                    if (pc.data.useMatrix) {
                        value = pc.widthSlider.value;
                    } else {
                        value = parseInt(pc.data.width, 10);
                    }
                } else if (rule.propertyKey == 'depth') {
                    if (pc.data.useMatrix) {
                        value = pc.depthSlider.value;
                    } else {
                        value = parseInt(pc.data.depth, 10);
                    }
                }

                switch (rule.operator) {
                    case '>':
                        applyRule = value > rule.propertyValue;
                        break;
                    case '=':
                        applyRule = value == rule.propertyValue;
                        break;
                    case '<':
                        applyRule = value < rule.propertyValue;
                        break;
                }

                if (applyRule) {
                    return rule;
                }
            }
            return false;
        }

        function getValidationErrors() {
            var errors = [];
            for (var i = 0; i < pc.data.supplementGroups.length; i++) {
                var group = pc.data.supplementGroups[i];
                if (!group.selected) {
                    errors.push(group.name + ' är ej angivet.');
                }
            }
            return errors;
        }


    });

jQuery(document).ready(function($) {
    var $sticky = $('.summary-sticky');
    var $parent = $sticky.parent();
    var $grandParent = $parent.parent();
    var $window = $(window);
    var offset = 70;
    $(window).scroll(updateSticky);

    function updateSticky() {
        var top = $window.scrollTop() + offset - $parent.offset().top;
        top = Math.max(0, top);
        var marginBottom = $grandParent.height() - top - $sticky.outerHeight();

        if (marginBottom < 0) {
            top = top + marginBottom;
        }
        $sticky.css({top: top});
    }
});

