jQuery.noConflict();

jQuery(document).ready(function() {
    jQuery(".tags").each(function(index, item) {
        var e = jQuery(item);
        e.tagit({
            fieldName: e.attr('data-target') + "[]",
            allowSpaces: true
        });
    });
});
