<?php
#########################################################################################################################################################
# NOTICE - READ ME!!!
#########################################################################################################################################################
/**

This file is only a copy of the original file. A stack trace / exception containing this file does NOT indicate an error with Extendware.
No source code content has been modified from the original file. The only change has been in the hierachy of the classes.
Here is information about this file: 

Original Class: Extendware_EWEmail_Model_Networking_Client_Email
Original File: /var/www/html/public/app/code/local/Extendware/EWEmail/Model/Networking/Client/Email.php

*/
#########################################################################################################################################################




?><?php define('Extendware:Extendware_EWEmail_Model_Networking_Client_Email:Rewrite', true);
require_once('Extendware/EWEmail/Model/Networking/Client/Email.php');
class Extendware_EWEmail_Model_Networking_Client_EmailOverriddenClass extends ExtendwareInternal_EWEmail_Model_Networking_Client_Email {

}
?><?php
if (class_exists('Extendware_EWEmail_Model_Override_Extendware_EWEmail_Networking_Client_Email_Bridge', false) === false) {
	abstract class Extendware_EWEmail_Model_Override_Extendware_EWEmail_Networking_Client_Email_Bridge extends Extendware_EWEmail_Model_Networking_Client_EmailOverriddenClass  {

	}
} else {
	if (class_exists('Mage', false) === true) {
		Mage::log('Bridge class (Extendware_EWEmail_Model_Override_Extendware_EWEmail_Networking_Client_Email_Bridge) encountered twice');
	}
}
?><?php
class Extendware_EWEmail_Model_Networking_Client_Email extends Extendware_EWEmail_Model_Override_Extendware_EWEmail_Networking_Client_Email {

}
?>