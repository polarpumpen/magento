Polarpumpen Magento
============

Below is the documentation to setup a development environment for Polarpumpen Magento.

## Setup Database

When running docker-compose, it will automatically import the database located in *db/db.sql*. This
 can be created by downloading a backup from live and running *db.sh*.

## Docker Machine for Windows

To install Docker Toolbox for Windows, go to the following URL:

    https://docs.docker.com/engine/installation/windows/
    
Docker Toolbox include both Docker Engine and Docker Compose which is needed for this setup.

## Docker for Mac

When using Mac there are several way to get this up and running. The easiest way is to us Docker for Mac but this has
really bad performance when it comes to I/O. Therefore we thing it is better to use Docker with VirtualBox or
even better is Docker with VMWare Fusion.

### Docker Machine with VirtualBox
    
    docker-machine create --virtualbox-memory 4096 --virtualbox-cpu-count 1 --driver virtualbox vb
    eval $(docker-machine env vb)

### Docker Machine with VMware Fusion

#### Install Homebrew and Homebrew Cask (Skip if already installed)

    ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)" < /dev/null 2> /dev/null
    brew install caskroom/cask/brew-cask 2> /dev/null

#### Install VMware fusion  (Skip if already installed)
    
    brew cask install vmware-fusion

Open the VMware fusion and enter activation code or select trial

#### Setup docker machine using VMware Fusion

    docker-machine create --vmwarefusion-memory-size 4096 --vmwarefusion-cpu-count 1 --driver vmwarefusion default
    eval $(docker-machine env)

## XDebug

Set up bridge for XDebug

    sudo ifconfig lo0 alias 172.16.123.1
