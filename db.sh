#!/usr/bin/env bash
rm db/db.sql
cp db/db-source.sql db/db.sql
sed -i '' 's#http://www.140kvadrat.se/#http://docker.local/#g' db/db.sql
sed -i '' 's#https://www.140kvadrat.se/#http://docker.local/#g' db/db.sql
sed -i '' 's#http://140kvadrat.se/#http://docker.local/#g' db/db.sql
sed -i '' 's#https://140kvadrat.se/#http://docker.local/#g' db/db.sql
sed -i '' 's#http://140kvadratny.nordicwebteam.se/#http://docker.local/#g' db/db.sql

sed -i '' 's#http://www.fuktkollen.se/#http://docker.local/#g' db/db.sql
sed -i '' 's#https://www.fuktkollen.se/#http://docker.local/#g' db/db.sql
sed -i '' 's#http://fuktkollen.se/#http://docker.local/#g' db/db.sql
sed -i '' 's#https://fuktkollen.se/#http://docker.local/#g' db/db.sql

sed -i '' 's#http://www.pumplagret.se/#http://docker.local/#g' db/db.sql
sed -i '' 's#https://www.pumplagret.se/#http://docker.local/#g' db/db.sql
sed -i '' 's#http://pumplagret.se/#http://docker.local/#g' db/db.sql
sed -i '' 's#https://pumplagret.se/#http://docker.local/#g' db/db.sql

sed -i '' 's#http://www.polarpumpen.se/#http://docker.local/#g' db/db.sql
sed -i '' 's#https://www.polarpumpen.se/#http://docker.local/#g' db/db.sql
sed -i '' 's#http://polarpumpen.se/#http://docker.local/#g' db/db.sql
sed -i '' 's#https://polarpumpen.se/#http://docker.local/#g' db/db.sql
sed -i '' 's#http://polarpumpen.local/#http://docker.local/#g' db/db.sql
