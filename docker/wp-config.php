<?php
/**
 * Baskonfiguration för WordPress.
 *
 * Denna fil används av wp-config.php-genereringsskript under installationen.
 * Du behöver inte använda webbplatsen, du kan kopiera denna fil direkt till
 * "wp-config.php" och fylla i värdena.
 *
 * Denna fil innehåller följande konfigurationer:
 *
 * * Inställningar för MySQL
 * * Säkerhetsnycklar
 * * Tabellprefix för databas
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL-inställningar - MySQL-uppgifter får du från ditt webbhotell ** //
/** Namnet på databasen du vill använda för WordPress */
define('DB_NAME', 'blogg');

/** MySQL-databasens användarnamn */
define('DB_USER', 'blogg');

/** MySQL-databasens lösenord */
define('DB_PASSWORD', 'ONAisfa(as112');

/** MySQL-server */
define('DB_HOST', 'mysql-blogg');

/** Teckenkodning för tabellerna i databasen. */
define('DB_CHARSET', 'utf8mb4');

/** Kollationeringstyp för databasen. Ändra inte om du är osäker. */
define('DB_COLLATE', '');

/**#@+
 * Unika autentiseringsnycklar och salter.
 *
 * Ändra dessa till unika fraser!
 * Du kan generera nycklar med {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * Du kan när som helst ändra dessa nycklar för att göra aktiva cookies obrukbara, vilket tvingar alla användare att logga in på nytt.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '39}J+Yoa/p:d5tsjOGNvZoXQ&*ayDq xTz9aC$ Q3^$?_+4]~Bxh|bqM,v?OK[oB');
define('SECURE_AUTH_KEY',  'x:Dt~qim/Axotr@b1(Zp![W{aiO2&.1gZJXy7_*KTmZS?24c/.vqX8?SzW|D)YL?');
define('LOGGED_IN_KEY',    'rT37q7=D.PAA3YH|/JE96g(d(GC~WdQRa9F]n-k|p-0lT}Fs4F3NI]1=w}XKx9HO');
define('NONCE_KEY',        'jxNfd*L,VJVjl#*/|R9CV{Hd/-0$KO*8rA^A3m?a`OH8~Ay+qkqZGRZvueefxvt9');
define('AUTH_SALT',        'D%!#szNSgB1v|rj|H|n}`kz5LlrlnB g:-xW@JKcT@S(cXl6xRxHC&~@U,vs%[j?');
define('SECURE_AUTH_SALT', '<g;Zy4=gpWW]Xjm2.=x-hW!|k&-Iq_)LCEK.CH6E1$1k;.Z{PD>!VC)1*#-fw(zH');
define('LOGGED_IN_SALT',   '>KX>TlTOXa~t7g>-@<0P|GaC|#-D_NkO%ZS.S*mDU)~d!Z6R.&q]Lb>Q|V +aLhV');
define('NONCE_SALT',       '2Wt_-#-A!ZxKD+gx=F?,R&A(IuO<J7@jbs3=|mF0/S1h4WIzn#y{uyJuggA>mo+E');

/**#@-*/

/**
 * Tabellprefix för WordPress Databasen.
 *
 * Du kan ha flera installationer i samma databas om du ger varje installation ett unikt
 * prefix. Endast siffror, bokstäver och understreck!
 */
$table_prefix  = 'wp_';

/**
 * För utvecklare: WordPress felsökningsläge.
 *
 * Ändra detta till true för att aktivera meddelanden under utveckling.
 * Det är rekommderat att man som tilläggsskapare och temaskapare använder WP_DEBUG
 * i sin utvecklingsmiljö.
 *
 * För information om andra konstanter som kan användas för felsökning,
 * se dokumentationen.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Det var allt, sluta redigera här! Blogga på. */

/** Absoluta sökväg till WordPress-katalogen. */
if ( !defined('ABSPATH') )
    define('ABSPATH', dirname(__FILE__) . '/');

/** Anger WordPress-värden och inkluderade filer. */
require_once(ABSPATH . 'wp-settings.php');